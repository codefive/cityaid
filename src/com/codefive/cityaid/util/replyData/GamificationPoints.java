package com.codefive.cityaid.util.replyData;

public class GamificationPoints {

	public long modLevel;
	public long createIssue;
	public long comment;
	public long rateIssue;
	public long rateEntity;
	public long deleteIssue;
	public long deleteComment;
	public long issueWIP;
	public long issueClosed;
	public long issueSolved;
	
	public GamificationPoints(){}
	
	public GamificationPoints(long modLevel, long createIssue, long comment, long rateIssue, long rateEntity, long deleteIssue, long deleteComment, long issueWIP, long issueClosed, long issueSolved) {
		this.modLevel = modLevel;
		this.createIssue = createIssue;
		this.comment = comment;
		this.rateIssue = rateIssue;
		this.rateEntity = rateEntity;
		this.deleteIssue = deleteIssue;
		this.deleteComment = deleteComment;
		this.issueWIP = issueWIP;
		this.issueClosed = issueClosed;
		this.issueSolved = issueSolved;
	}
	
}
