package com.codefive.cityaid.util.replyData;

import java.util.Date;

public class User {

	public String username;
	public String role;
	public String name;
	public String email;
	public String address;
	public String phone;
	public String gender;
	public Date birthdate;
	public Date registered;
	public boolean isManager;
	public String employer;
	public String job;
	public String avatar;
	public boolean frozen;
	public long points;
	public long level;
	public long thresholdCurrentLevel;
	public long thresholdNextLevel;

	public User() {}

	public User(String username, String role, String name, String email, String address, String phone, String gender, Date birthdate, Date registered, boolean isManager, String employer, String job, String avatar, boolean frozen, long points, long level) {
		this.username = username;
		this.role = role;
		this.name = name;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.gender = gender;
		this.birthdate = birthdate;
		this.registered = registered;
		this.isManager= isManager;
		this.employer = employer;
		this.job = job;
		this.avatar = avatar;
		this.frozen = frozen;
		this.points = points;
		this.level = level;
		this.thresholdCurrentLevel = (long) Math.ceil(10 * Math.pow(level-1, 1.5));
		this.thresholdNextLevel = (long) Math.ceil(10 * Math.pow(level, 1.5));
	}
}
