package com.codefive.cityaid.util.replyData;

import com.codefive.cityaid.resources.GamificationResource;

public class UserStats {

	public long points;
	public long level;
	public long thresholdCurrentLevel;
	public long thresholdNextLevel;
	public long numAwardedMedals;
	public long issuesCreated;
	public long issuesCreatedTypeVandalism;
	public long issuesCreatedTypeGarbage;
	public long issuesCreatedTypeAccident;
	public long issuesCreatedTypeStreetDamage;
	public long issuesCreatedTypeFurnitureDamage;
	public long issuesCreatedTypeNature;
	public long issuesCreatedTypeOther;
	public long issuesProgress;
	public long issuesSolved;
	public long issuesClosed;
	public long issuesCommented;
	public long issuesCommentedTypeVandalism;
	public long issuesCommentedTypeGarbage;
	public long issuesCommentedTypeAccident;
	public long issuesCommentedTypeStreetDamage;
	public long issuesCommentedTypeFurnitureDamage;
	public long issuesCommentedTypeNature;
	public long issuesCommentedTypeOther;
	public long issuesFollowed;
	public long issuesFollowedTypeVandalism;
	public long issuesFollowedTypeGarbage;
	public long issuesFollowedTypeAccident;
	public long issuesFollowedTypeStreetDamage;
	public long issuesFollowedTypeFurnitureDamage;
	public long issuesFollowedTypeNature;
	public long issuesFollowedTypeOther;
	public long issuesRated;
	public long issuesRatedTypeVandalism;
	public long issuesRatedTypeGarbage;
	public long issuesRatedTypeAccident;
	public long issuesRatedTypeStreetDamage;
	public long issuesRatedTypeFurnitureDamage;
	public long issuesRatedTypeNature;
	public long issuesRatedTypeOther;
	
	public UserStats() { }
	
	public UserStats(long points, long level, long numAwardedMedals, long issuesCreated, long issuesCreatedTypeVandalism, long issuesCreatedTypeGarbage, long issuesCreatedTypeAccident, long issuesCreatedTypeStreetDamage, long issuesCreatedTypeFurnitureDamage, long issuesCreatedTypeNature, long issuesCreatedTypeOther, long issuesProgress, long issuesSolved, long issuesClosed, long issuesCommented, long issuesCommentedTypeVandalism, long issuesCommentedTypeGarbage, long issuesCommentedTypeAccident, long issuesCommentedTypeStreetDamage, long issuesCommentedTypeFurnitureDamage, long issuesCommentedTypeNature, long issuesCommentedTypeOther, long issuesFollowed, long issuesFollowedTypeVandalism, long issuesFollowedTypeGarbage, long issuesFollowedTypeAccident, long issuesFollowedTypeStreetDamage, long issuesFollowedTypeFurnitureDamage, long issuesFollowedTypeNature, long issuesFollowedTypeOther, long issuesRated, long issuesRatedTypeVandalism, long issuesRatedTypeGarbage, long issuesRatedTypeAccident, long issuesRatedTypeStreetDamage, long issuesRatedTypeFurnitureDamage, long issuesRatedTypeNature, long issuesRatedTypeOther) {
		this.points = points;
		this.level = level;
		this.thresholdCurrentLevel = GamificationResource.calculateNextLevelThreshold(level-1);
		this.thresholdNextLevel = GamificationResource.calculateNextLevelThreshold(level);
		this.numAwardedMedals = numAwardedMedals;
		this.issuesCreated = issuesCreated;
		this.issuesCreatedTypeVandalism = issuesCreatedTypeVandalism;
		this.issuesCreatedTypeGarbage = issuesCreatedTypeGarbage;
		this.issuesCreatedTypeAccident = issuesCreatedTypeAccident;
		this.issuesCreatedTypeStreetDamage = issuesCreatedTypeStreetDamage;
		this.issuesCreatedTypeFurnitureDamage = issuesCreatedTypeFurnitureDamage;
		this.issuesCreatedTypeNature = issuesCreatedTypeNature;
		this.issuesCreatedTypeOther = issuesCreatedTypeOther;
		this.issuesProgress = issuesProgress;
		this.issuesSolved = issuesSolved;
		this.issuesClosed = issuesClosed;
		this.issuesCommented = issuesCommented;
		this.issuesCommentedTypeVandalism = issuesCommentedTypeVandalism;
		this.issuesCommentedTypeGarbage = issuesCommentedTypeGarbage;
		this.issuesCommentedTypeAccident = issuesCommentedTypeAccident;
		this.issuesCommentedTypeStreetDamage = issuesCommentedTypeStreetDamage;
		this.issuesCommentedTypeFurnitureDamage = issuesCommentedTypeFurnitureDamage;
		this.issuesCommentedTypeNature = issuesCommentedTypeNature;
		this.issuesCommentedTypeOther = issuesCommentedTypeOther;
		this.issuesFollowed = issuesFollowed;
		this.issuesFollowedTypeVandalism = issuesFollowedTypeVandalism;
		this.issuesFollowedTypeGarbage = issuesFollowedTypeGarbage;
		this.issuesFollowedTypeAccident = issuesFollowedTypeAccident;
		this.issuesFollowedTypeStreetDamage = issuesFollowedTypeStreetDamage;
		this.issuesFollowedTypeFurnitureDamage = issuesFollowedTypeFurnitureDamage;
		this.issuesFollowedTypeNature = issuesFollowedTypeNature;
		this.issuesFollowedTypeOther = issuesFollowedTypeOther;
		this.issuesRated = issuesRated;
		this.issuesRatedTypeVandalism = issuesRatedTypeVandalism;
		this.issuesRatedTypeGarbage = issuesRatedTypeGarbage;
		this.issuesRatedTypeAccident = issuesRatedTypeAccident;
		this.issuesRatedTypeStreetDamage = issuesRatedTypeStreetDamage;
		this.issuesRatedTypeFurnitureDamage = issuesRatedTypeFurnitureDamage;
		this.issuesRatedTypeNature = issuesRatedTypeNature;
		this.issuesRatedTypeOther = issuesRatedTypeOther;
	}
}
