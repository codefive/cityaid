package com.codefive.cityaid.util.replyData;

public class UserReports {

	String id;
	String username;
	String name;
	String reporter;
	String comment;

	public UserReports() {}
	
	public UserReports(String id, String username, String name, String reporter, String comment) {
		this.id = id;
		this.username = username;
		this.name = name;
		this.reporter = reporter;
		this.comment = comment;
	}
}
