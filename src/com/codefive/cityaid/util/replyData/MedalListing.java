package com.codefive.cityaid.util.replyData;

import java.util.List;

public class MedalListing {

	List<Medal> medals;
	String cursor;
	
	public MedalListing() {}
	
	public MedalListing(List<Medal> medals, String cursor) {
		this.medals = medals;
		this.cursor = cursor;
	}
}