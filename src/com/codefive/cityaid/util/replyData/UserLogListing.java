package com.codefive.cityaid.util.replyData;

import java.util.List;

public class UserLogListing {

	List<UserLogEntry> logs;
	String cursor;
	
	public UserLogListing() {}
	
	public UserLogListing(List<UserLogEntry> logs, String cursor) {
		this.logs = logs;
		this.cursor = cursor;
	}
}