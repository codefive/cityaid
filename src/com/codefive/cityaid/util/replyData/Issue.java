package com.codefive.cityaid.util.replyData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.codefive.cityaid.util.enums.IssueType;

public class Issue {

	public String issue;
	public String state;
	public Date created;
	public String submitter;
	public String title;
	public String description;
	public double latitude;
	public double longitude;
	public String type;
	public long priority;
	public boolean following;
	public List<String> pictures;
	public List<String> thumbnails;
	public List<MinEntity> entities;

	public Issue() {}
	
	public Issue(String issue, String state, Date created, String submitter, String title, String description, double latitude, double longitude, String type, long priority, boolean following, List<String> pictures, List<String> thumbnails, List<MinEntity> entities){
		this.issue = issue;
		this.state = state;
		this.created = created;
		this.submitter = submitter;
		this.title = title;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
		switch(IssueType.valueOf(type.toUpperCase())){
			case ACCIDENT:
				this.type = "Acidente";
				break;
			case FURNITURE_DAMAGE:
				this.type = "Danos de material p�blico";
				break;
			case GARBAGE:
				this.type = "Lixo";
				break;
			case NATURE:
				this.type = "Natureza";
				break;
			case OTHER:
				this.type = "Outros";
				break;
			case STREET_DAMAGE:
				this.type = "Danos de vias p�blicas";
				break;
			case VANDALISM:
				this.type = "Vandalismo";
				break;
			default:
				this.type = "Desconhecido";
				break;					
		}
		this.priority = priority;
		this.following = following;
		if (pictures != null){
			this.pictures = pictures;
		} else {
			this.pictures = new ArrayList<String>(1);
			this.pictures.add("default.jpg");
		}
		if (thumbnails != null){
			this.thumbnails = thumbnails;
		} else {
			this.thumbnails = new ArrayList<String>(1);
			this.thumbnails.add("default_thumb.jpg");
		}
		this.entities = entities;
	}	
}
