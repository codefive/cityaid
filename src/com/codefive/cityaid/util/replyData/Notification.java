package com.codefive.cityaid.util.replyData;

public class Notification {
	
	public String receiver;
	public String title;
	public String icon;
	public String body;
	public String url;
	public String emailTitle;
	public String emailBody;
	public String type;
	
	public Notification(){}
	
	public Notification(String receiver, String title, String icon, String body, String url, String emailTitle, String emailBody, String type){
		this.receiver = receiver;
		this.title = title;
		this.icon = icon;
		this.body = body;
		this.url = url;
		this.emailTitle = emailTitle;
		this.emailBody = emailBody;
		this.type = type;
	}
}
