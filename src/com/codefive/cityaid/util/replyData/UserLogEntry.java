package com.codefive.cityaid.util.replyData;

import java.util.Date;

public class UserLogEntry {
	
	public String id;
	public Date date;
	public String type;
	public String description;
	public String remote_addr;
	public String remote_host;
	public String x_appengine_city;
	public String x_appengine_citylatlong;
	public String x_appengine_country;
	
	public UserLogEntry(){}
	
	public UserLogEntry(String id, Date date, String type, String description, String remote_addr, String remote_host, String x_appengine_city, String x_appengine_citylatlong, String x_appengine_country){
		this.id = id;
		this.date = date;
		this.type = type;
		this.description = description;
		this.remote_addr = remote_addr;
		this.remote_host = remote_host;
		this.x_appengine_city = x_appengine_city;
		this.x_appengine_citylatlong = x_appengine_citylatlong;
		this.x_appengine_country = x_appengine_country;
	}
}
