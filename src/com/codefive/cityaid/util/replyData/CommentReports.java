package com.codefive.cityaid.util.replyData;

import java.util.Date;

public class CommentReports {

	//id do report
	String id;
	//id do comment
	String idComment;
	//id do issue
	String issue;
	//title da ocorrencia
	String title;
	//comentario reportado
	String text;
	String reporter;
	String submiter;
	//comentario feito pelo reporter sobre o pq de estar a reportar
	String comment;
	//data do comentario
	Date date;

	public CommentReports() {}
	
	public CommentReports(String id, String idComment, String issue, String title, String text, String reporter,
			String submiter, String comment, Date date) {
		this.id = id;
		this.idComment = idComment;
		this.issue = issue;
		this.title = title;
		this.text = text;
		this.reporter = reporter;
		this.submiter = submiter;
		this.comment = comment;
		this.date = date;
	}
}
