package com.codefive.cityaid.util.replyData;

public class Medal {
	
	public long id;
	public String title;
	public String description;
	public long points;
	public String picture;
	public boolean active;
	
	public Medal(){}
	
	public Medal(long id, String title, String description, long points, String picture, boolean active) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.points = points;
		this.picture = picture;
		this.active = active;
	}
}
