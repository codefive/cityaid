package com.codefive.cityaid.util.replyData;

import java.util.List;

public class IssueReportListing {

	List<IssueReports> reports;
	String cursor;
	
	public IssueReportListing() {}
	
	public IssueReportListing(List<IssueReports> reports, String cursor) {
		this.reports = reports;
		this.cursor = cursor;
	}
}