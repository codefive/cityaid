package com.codefive.cityaid.util.replyData;

import java.util.List;

public class CommentReportListing {

	List<CommentReports> reports;
	String cursor;
	
	public CommentReportListing() {}
	
	public CommentReportListing(List<CommentReports> reports, String cursor) {
		this.reports = reports;
		this.cursor = cursor;
	}
}