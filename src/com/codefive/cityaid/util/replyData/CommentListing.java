package com.codefive.cityaid.util.replyData;

import java.util.List;

public class CommentListing {

	List<Comment> comments;
	String cursor;
	
	public CommentListing() {}
	
	public CommentListing(List<Comment> comments, String cursor) {
		this.comments = comments;
		this.cursor = cursor;
	}
}