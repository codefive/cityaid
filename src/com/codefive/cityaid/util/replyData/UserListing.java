package com.codefive.cityaid.util.replyData;

import java.util.List;

public class UserListing {

	List<User> users;
	String cursor;
	
	public UserListing() {}
	
	public UserListing(List<User> users, String cursor) {
		this.users = users;
		this.cursor = cursor;
	}
}