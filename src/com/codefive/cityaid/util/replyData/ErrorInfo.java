package com.codefive.cityaid.util.replyData;

import com.codefive.cityaid.util.enums.ErrorType;

public class ErrorInfo {

	public String type;
	public String message;
	
	
	public ErrorInfo() {}
	
	public ErrorInfo(ErrorType error){
		this.type = error.name().toLowerCase();
		this.message = error.toString();
	}
}
