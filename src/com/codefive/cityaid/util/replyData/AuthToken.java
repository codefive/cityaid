package com.codefive.cityaid.util.replyData;

import java.util.UUID;

public class AuthToken {

	public static final long EXPIRATION_TIME = 60*60*20; //20h

	public String username;
	public String token;
	public long creation;
	public long expiration;
	public String role;
	public boolean isManager;
	public boolean isModerator;
	public String employer;
	
	public AuthToken(){}

	/**
	 * 
	 * @param device	the device this session is from
	 */
	public AuthToken(String username, String role, boolean isManager, boolean isModerator, String employer) {
		this.username = username;
		token = UUID.randomUUID().toString();
		creation = (System.currentTimeMillis()/1000);
		expiration = creation + EXPIRATION_TIME;
		this.role = role;
		this.isManager = isManager;
		this.isModerator = isModerator;
		this.employer = employer;
	}
	
}
