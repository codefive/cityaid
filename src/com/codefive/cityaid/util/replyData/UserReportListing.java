package com.codefive.cityaid.util.replyData;

import java.util.List;

public class UserReportListing {

	List<UserReports> reports;
	String cursor;
	
	public UserReportListing() {}
	
	public UserReportListing(List<UserReports> reports, String cursor) {
		this.reports = reports;
		this.cursor = cursor;
	}
}