package com.codefive.cityaid.util.replyData;

import java.util.Date;

public class Comment {
	
	String id;
	String issue;
	Date submitted;
	String author;
	String comment;
	
	public Comment(){}
	
	public Comment(String id, String issue, Date submitted, String author, String comment) {
		this.id = id;
		this.issue = issue;
		this.submitted = submitted;
		this.author = author;
		this.comment = comment;
	}
}
