package com.codefive.cityaid.util.replyData;

public class UserOptions {

	public boolean auto_follow;
	public boolean push_medal;
	public boolean push_level_up;
	public boolean push_issue_wip;
	public boolean push_issue_closed;
	public boolean push_issue_solved;
	public boolean push_log_comment;
	public boolean push_edit;
	public boolean push_new_photos;
	public boolean push_comment;
	public boolean email_medal;
	public boolean email_level_up;
	public boolean email_issue_wip;
	public boolean email_issue_closed;
	public boolean email_issue_solved;
	public boolean email_log_comment;
	public boolean email_edit;
	public boolean email_new_photos;
	public boolean email_comment;
	public long mobile_cache;

	public UserOptions(){}
	
	public UserOptions(boolean auto_follow, boolean push_medal, boolean push_level_up, boolean push_issue_wip, boolean push_issue_closed, boolean push_issue_solved, boolean push_log_comment, boolean push_edit, boolean push_new_photos, boolean push_comment, boolean email_medal, boolean email_level_up, boolean email_issue_wip, boolean email_issue_closed, boolean email_issue_solved, boolean email_log_comment, boolean email_edit, boolean email_new_photos, boolean email_comment, long mobile_cache){
		this.auto_follow = auto_follow;
		this.push_medal = push_medal;
		this.push_level_up = push_level_up;
		this.push_issue_wip = push_issue_wip;
		this.push_issue_closed = push_issue_closed;
		this.push_issue_solved = push_issue_solved;
		this.push_log_comment = push_log_comment;
		this.push_edit = push_edit;
		this.push_new_photos = push_new_photos;
		this.push_comment = push_comment;
		this.email_medal = email_medal;
		this.email_level_up = email_level_up;
		this.email_issue_wip = email_issue_wip;
		this.email_issue_closed = email_issue_closed;
		this.email_issue_solved = email_issue_solved;
		this.email_log_comment = email_log_comment;
		this.email_edit = email_edit;
		this.email_new_photos = email_new_photos;
		this.email_comment = email_comment;
		this.mobile_cache = mobile_cache;
	}
}
