package com.codefive.cityaid.util.replyData;

import java.util.List;

public class IssueListing {

	List<Issue> issues;
	String cursor;
	
	public IssueListing() {}
	
	public IssueListing(List<Issue> issues, String cursor) {
		this.issues = issues;
		this.cursor = cursor;
	}
}
