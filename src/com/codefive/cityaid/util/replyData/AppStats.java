package com.codefive.cityaid.util.replyData;

public class AppStats {
	
	public long users;
	public long reporters;
	public long backoffice;
	public long workers;
	public long activeUsers;
	public long issues;
	public long issuesCreated;
	public long issuesProgress;
	public long issuesSolved;
	public long issuesClosed;
	public long entities;
	public long moderators;
	
	public AppStats(){}
	
	public AppStats(long users, long reporters, long backoffice, long workers, long activeUsers, long issues,
			long issuesCreated, long issuesProgress, long issuesSolved, long issuesClosed, long entities, long moderators) {
		this.users = users;
		this.reporters = reporters;
		this.backoffice = backoffice;
		this.workers = workers;
		this.activeUsers = activeUsers;
		this.issues = issues;
		this.issuesCreated = issuesCreated;
		this.issuesProgress = issuesProgress;
		this.issuesSolved = issuesSolved;
		this.issuesClosed = issuesClosed;
		this.entities = entities;
		this.moderators = moderators;
	}
}
