package com.codefive.cityaid.util.replyData;

import java.util.Date;

public class IssueLogEntry {
	
	public String id;
	public Date date;
	public String type;
	public String description;
	public String user;
	
	public IssueLogEntry(){}
	
	public IssueLogEntry(String id, Date date, String type, String description, String user) {
		this.id = id;
		this.date = date;
		this.type = type;
		this.description = description;
		this.user = user;
	}
}
