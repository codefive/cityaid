package com.codefive.cityaid.util.replyData;

import java.util.List;

public class IssueLogListing {

	List<IssueLogEntry> logs;
	String cursor;
	
	public IssueLogListing() {}
	
	public IssueLogListing(List<IssueLogEntry> logs, String cursor) {
		this.logs = logs;
		this.cursor = cursor;
	}
}