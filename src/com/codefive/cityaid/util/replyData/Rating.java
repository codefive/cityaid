package com.codefive.cityaid.util.replyData;

public class Rating {
	
	public String username;
	public long priority;
	
	public Rating(){}
	
	public Rating(String username, long priority){
		this.username = username;
		this.priority = priority;
	}
}
