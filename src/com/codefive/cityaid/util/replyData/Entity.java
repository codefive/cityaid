package com.codefive.cityaid.util.replyData;

import java.util.List;

public class Entity {

	String nif;
	String name;
	String type;
	String phone;
	List<User> managers;
	long rating;
	String logo;

	public Entity() {}

	public Entity(String nif, String name, String type, String phone, List<User> managers, long rating, String logo) {
		this.nif = nif;
		this.name = name;
		this.type = type;
		this.phone = phone;
		this.managers = managers;
		this.rating = rating;
		this.logo = logo;
	}
}
