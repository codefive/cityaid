package com.codefive.cityaid.util.replyData;

public class IssueReports {

	String id;
	String issue;
	String title;
	String reporter;
	String submiter;
	String comment;

	public IssueReports() {}
	
	public IssueReports(String id, String issue, String title, String reporter, String submiter, String comment) {
		this.id = id;
		this.issue = issue;
		this.title = title;
		this.reporter = reporter;
		this.submiter = submiter;
		this.comment = comment;
	}
}
