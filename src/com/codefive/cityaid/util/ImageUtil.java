package com.codefive.cityaid.util;

public class ImageUtil {

	//Obrigado a https://stackoverflow.com/a/10245583 por n�o me fazer ir rever a mat�ria de CGI
	public static int[] getScaledDimension(int imageWidth, int imageHeight, int containerWidth, int containerHeight) {
	    int original_width = imageWidth;
	    int original_height = imageHeight;
	    int bound_width = containerWidth;
	    int bound_height = containerHeight;
	    int new_width = original_width;
	    int new_height = original_height;

	    // first check if we need to scale width
	    if (original_width > bound_width) {
	        //scale width to fit
	        new_width = bound_width;
	        //scale height to maintain aspect ratio
	        new_height = (new_width * original_height) / original_width;
	    }

	    // then check if we need to scale even with the new height
	    if (new_height > bound_height) {
	        //scale height to fit instead
	        new_height = bound_height;
	        //scale width to maintain aspect ratio
	        new_width = (new_height * original_width) / original_height;
	    }

	    return new int[]{new_width, new_height};
	}
	
}
