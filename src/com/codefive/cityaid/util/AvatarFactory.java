package com.codefive.cityaid.util;

import org.apache.commons.codec.digest.DigestUtils;

public class AvatarFactory {

	/**
	 * Get the url for the gravatar associated with given email
	 * @param email		the user's email
	 * @return	url for the gravatar picture
	 */
	public static String getAvatarUrl(String email) {
		String hash = DigestUtils.md5Hex(email.trim().toLowerCase());
		return "https://www.gravatar.com/avatar/"+hash+".jpg?d=https%3A%2F%2Fcity-aid.appspot.com%2Fresource%2FBoneco.png&r=pg&s=300";
	}
	
}
