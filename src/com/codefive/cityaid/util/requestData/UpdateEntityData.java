package com.codefive.cityaid.util.requestData;

public class UpdateEntityData {

	public String name;
	public String phone;
	
	public UpdateEntityData(){}
	
	public UpdateEntityData(String name, String phone){
		this.name = name;
		this.phone = phone;
	}
	
	//Check if registration data is valid
	public boolean valid(){
	return validField(name) &&
			validField(phone);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
