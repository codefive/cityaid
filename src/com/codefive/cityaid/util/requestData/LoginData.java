package com.codefive.cityaid.util.requestData;

public class LoginData {
	
	public String username;
	public String password;
	public String device;
	
	public LoginData(){}
	
	public LoginData(String username, String password, String device){
		this.username = username;
		this.password = password;
		this.device = device;
	}
	
	//Check if login data is valid
	public boolean valid(){
	return validField(username) &&
		   validField(password) &&
		   validField(device);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}

}
