package com.codefive.cityaid.util.requestData;

public class IssueEntityData {

	public String entity;
	
	public IssueEntityData(){}
	
	public IssueEntityData(String entity) {
		this.entity = entity;
	}
	
	public boolean valid(){
		return validField(entity);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
}
