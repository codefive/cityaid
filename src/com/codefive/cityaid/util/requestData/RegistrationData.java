package com.codefive.cityaid.util.requestData;

import java.util.Date;

public class RegistrationData {

	public String username;
	public String password;
	public String name;
	public String email;
	public String phone;
	public String gender;
	public Date birthdate;
	
	public RegistrationData(){}
	
	public RegistrationData(String username, String password, String name, String email, String phone, String gender, Date birthdate){
		this.username = username;
		this.password = password;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.birthdate = birthdate;
	}
	
	//Check if registration data is valid
	public boolean valid(){
	return validField(username) &&
		   !username.matches("^.*[^a-zA-Z0-9 ].*$") &&
		   validField(password) &&
		   validField(name) &&
		   validField(email) &&
		   validField(phone) &&
		   validField(gender) &&
		   birthdate != null &&
		   email.contains("@");
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
