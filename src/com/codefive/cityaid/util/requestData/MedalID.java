package com.codefive.cityaid.util.requestData;

public class MedalID {

	public long medal;

	public MedalID() {}

	public MedalID(long medal) {
		this.medal = medal;
	}

	// Check if issue data is valid
	public boolean valid() {
		return medal > 0;
	}
}