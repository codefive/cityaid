package com.codefive.cityaid.util.requestData;

import java.util.Date;

public class BackofficeEmployeeRegistrationData {

	public String username;
	public String name;
	public String email;
	public String phone;
	public String gender;
	public Date birthdate;
	
	public BackofficeEmployeeRegistrationData(){}
	
	public BackofficeEmployeeRegistrationData(String username, String name, String email, String phone, String gender, Date birthdate) {
		this.username = username;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.birthdate = birthdate;
	}
	
	//Check if registration data is valid
	public boolean valid(){
	return validField(username) &&
		   !username.matches("^.*[^a-zA-Z0-9 ].*$") &&
		   validField(name) &&
		   validField(email) &&
		   validField(phone) &&
		   validField(gender) &&
		   birthdate != null &&
		   email.contains("@");
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
