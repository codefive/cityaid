package com.codefive.cityaid.util.requestData;

public class CommentData {

	public String comment;
	
	public CommentData(){}
	
	public CommentData(String comment){
		this.comment = comment;
	}
	
	public boolean valid(){
		return validField(comment);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
}
