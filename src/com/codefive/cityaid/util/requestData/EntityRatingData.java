package com.codefive.cityaid.util.requestData;

public class EntityRatingData {

	public int rating;

	public EntityRatingData() {
	}

	public EntityRatingData(int rating) {
		this.rating = rating;
	}

	// Check if issue data is valid
	public boolean valid() {
		return rating > 0;
	}
}
