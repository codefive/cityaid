package com.codefive.cityaid.util.requestData;

public class IssueStatusData {

	public String comment;
	
	public IssueStatusData(){}
	
	public IssueStatusData(String comment){
		this.comment = comment;
	}
	
	public boolean valid(){
		return validField(comment);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
}
