package com.codefive.cityaid.util.requestData;

public class PictureData {
	
	public String extension;
	public String base64;
	
	public PictureData(){}
	
	public PictureData(String extension, String base64){
		this.extension = extension;
		this.base64 = base64;
	}
}
