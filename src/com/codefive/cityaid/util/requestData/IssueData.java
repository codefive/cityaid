package com.codefive.cityaid.util.requestData;

import java.util.List;

import com.codefive.cityaid.util.enums.IssuePriority;
import com.codefive.cityaid.util.enums.IssueType;
import com.google.appengine.api.datastore.GeoPt;

public class IssueData {
	
	public String title;
	public String description;
	public double latitude;
	public double longitude;
	public String type;
	public int priority;
	public List<PictureData> pictures;
	
	public IssueData(){}
	
	public IssueData(String title, String description, double latitude, double longitude, String type, int priority, List<PictureData> pictures){
		this.title = title;
		this.description = description;
		this.latitude = latitude;
		this.longitude = longitude;
		this.type = type;
		this.priority = priority;
		this.pictures = pictures;
	}
	
	//Check if issue data is valid
	public boolean valid(){
	return validField(title) &&
		   validField(description) &&
		   validField(type) &&
		   (IssueType.VANDALISM.value().equals(type) || IssueType.GARBAGE.value().equals(type) || IssueType.ACCIDENT.value().equals(type) || IssueType.STREET_DAMAGE.value().equals(type) || IssueType.FURNITURE_DAMAGE.value().equals(type) || IssueType.NATURE.value().equals(type) || IssueType.OTHER.value().equals(type)) &&
		   priority != IssuePriority.UNDEFINED.value() &&
		   pictures != null;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
}
