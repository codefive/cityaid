package com.codefive.cityaid.util.requestData;

public class CommentReport {
	
	public String report;
	public String comment;

	public CommentReport() {}

	public CommentReport(String report, String comment) {
		this.report = report;
		this.comment = comment;
	}

	// Check if comment data is valid
	public boolean valid() {
		return validField(report) && validField(comment);
	}
	
	private boolean validField(String field) {
		return field!=null && !field.isEmpty();
	}
}
