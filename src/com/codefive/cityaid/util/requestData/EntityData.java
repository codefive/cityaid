package com.codefive.cityaid.util.requestData;

public class EntityData {

	public String nif;
	public String name;
	public String type;
	public String phone;
	public PictureData logo;
	
	public EntityData(){}
	
	public EntityData(String nif, String name, String type, String phone, PictureData logo) {
		this.nif = nif;
		this.name = name;
		this.type = type;
		this.phone = phone;
		this.logo = logo;
	}
	
	//Check if registration data is valid
	public boolean valid(){
		return validField(nif) &&
			   validField(name) &&
			   validField(type) &&
			   validField(phone);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
