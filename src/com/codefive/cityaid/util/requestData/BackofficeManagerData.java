package com.codefive.cityaid.util.requestData;

public class BackofficeManagerData {

	public String username;
	
	public BackofficeManagerData(){}
	
	public BackofficeManagerData(String username){
		this.username = username;
	}
	
	//Check if registration data is valid
	public boolean valid(){
		return validField(username);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
