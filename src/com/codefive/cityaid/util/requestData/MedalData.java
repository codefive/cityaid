package com.codefive.cityaid.util.requestData;

import com.codefive.cityaid.util.replyData.UserStats;

public class MedalData {

	public String title;
	public String description;
	public int points;
	public PictureData picture;
	public UserStats preconditions;
	
	public MedalData(){}
	
	public MedalData(String title, String description, int points, PictureData picture, UserStats preconditions) {
		this.title = title;
		this.description = description;
		this.points = points;
		this.picture = picture;
		this.preconditions = preconditions;
	}
	
	//Check if registration data is valid
	public boolean valid(){
		return validField(title) &&
			   validField(description) &&
			   points > 0 &&
			   preconditions != null;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
