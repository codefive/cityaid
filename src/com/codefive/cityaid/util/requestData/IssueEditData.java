package com.codefive.cityaid.util.requestData;

import com.codefive.cityaid.util.enums.IssueType;

public class IssueEditData {
	
	public String title;
	public String description;
	public String type;
	
	public IssueEditData(){}
	
	public IssueEditData(String title, String description, String type){
		this.title = title;
		this.description = description;
		this.type = type;
	}
	
	//Check if issue data is valid
	public boolean valid(){
	return validField(title) &&
		   validField(description) &&
		   validField(type) &&
		   (IssueType.VANDALISM.value().equals(type) || IssueType.GARBAGE.value().equals(type) || IssueType.ACCIDENT.value().equals(type) || IssueType.STREET_DAMAGE.value().equals(type) || IssueType.FURNITURE_DAMAGE.value().equals(type) || IssueType.NATURE.value().equals(type) || IssueType.OTHER.value().equals(type));
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
}
