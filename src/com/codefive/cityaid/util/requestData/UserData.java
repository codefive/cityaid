package com.codefive.cityaid.util.requestData;

import java.util.Date;

public class UserData {

	public String name;
	public String email;
	public String address;
	public String phone;
	public String gender;
	public Date birthdate;
	
	public UserData(){}
	
	public UserData(String username, String password, String name, String email, String address, String phone, String gender, Date birthdate) {
		this.name = name;
		this.email = email;
		this.address = address;
		this.phone = phone;
		this.gender = gender;
		this.birthdate = birthdate;
	}
	
	//Check if registration data is valid
	public boolean valid(){
	return validField(name) &&
		   validField(email) &&
		   validField(phone) &&
		   validField(gender) &&
		   birthdate != null &&
		   email.contains("@");
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
