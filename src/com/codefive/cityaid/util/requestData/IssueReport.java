package com.codefive.cityaid.util.requestData;

public class IssueReport {

	public String issue;
	public String comment;

	public IssueReport() {}

	public IssueReport(String issue, String comment) {
		this.issue = issue;
		this.comment = comment;
	}

	// Check if issue data is valid
	public boolean valid() {
		return issue!=null && !issue.isEmpty() && comment!=null && !comment.isEmpty();
	}
}
