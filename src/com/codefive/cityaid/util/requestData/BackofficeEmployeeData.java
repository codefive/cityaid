package com.codefive.cityaid.util.requestData;

public class BackofficeEmployeeData {

	public String username;

	public BackofficeEmployeeData() {}

	public BackofficeEmployeeData(String username) {
		this.username = username;
	}

	// Check if registration data is valid
	public boolean valid() {
		return validField(username);
	}

	private boolean validField(String value) {
		return value != null && !value.equals("");
	}

}
