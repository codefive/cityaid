package com.codefive.cityaid.util.requestData;

public class IssueID {

	public String issue;

	public IssueID() {}

	public IssueID(String issue) {
		this.issue = issue;
	}

	// Check if issue data is valid
	public boolean valid() {
		return issue!=null && !issue.isEmpty();
	}
}