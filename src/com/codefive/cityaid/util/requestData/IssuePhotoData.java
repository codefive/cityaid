package com.codefive.cityaid.util.requestData;

import java.util.List;

public class IssuePhotoData {
	
	public List<PictureData> pictures;
	
	public IssuePhotoData(){}
	
	public IssuePhotoData(List<PictureData> pictures){
		this.pictures = pictures;
	}
	
	//Check if issue data is valid
	public boolean valid(){
		return pictures != null;
	}
}
