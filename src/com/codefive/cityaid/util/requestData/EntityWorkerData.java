package com.codefive.cityaid.util.requestData;

public class EntityWorkerData {

	public String username;
	public String job;
	
	public EntityWorkerData(){}
	
	public EntityWorkerData(String username, String job) {
		this.username = username;
		this.job = job;
	}
	
	//Check if registration data is valid
	public boolean valid(){
	return validField(username) &&
			validField(job);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
