package com.codefive.cityaid.util.requestData;

public class UserReport {

	public String user;
	public String comment;

	public UserReport() {}

	public UserReport(String user, String comment) {
		this.user = user;
		this.comment = comment;
	}

	// Check if issue data is valid
	public boolean valid() {
		return user!=null && !user.isEmpty() && comment!=null && !comment.isEmpty();
	}
}
