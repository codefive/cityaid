package com.codefive.cityaid.util.requestData;

import com.codefive.cityaid.util.enums.IssuePriority;

public class RatingData {

	public int priority;

	public RatingData() {
	}

	public RatingData(int priority) {
		this.priority = priority;
	}

	// Check if issue data is valid
	public boolean valid() {
		return priority != IssuePriority.UNDEFINED.value();
	}
}
