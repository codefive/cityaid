package com.codefive.cityaid.util.requestData;

public class PasswordData {

	public String resetToken;
	public String oldPassword;
	public String newPassword;
	
	public PasswordData(){}
	
	public PasswordData(String resetToken, String oldPassword, String newPassword){
		this.resetToken = resetToken;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
	}
	
	//Check if registration data is valid
	public boolean valid(){
	return ((validField(resetToken) && !validField(oldPassword)) ||
		   (!validField(resetToken) && validField(oldPassword))) &&
		   validField(newPassword);
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	
}
