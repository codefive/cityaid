package com.codefive.cityaid.util.enums;

public enum IssueStatus {
	CREATED ("created"),
	WORK_IN_PROGRESS ("work_in_progress"),
	SOLVED ("solved"),
	CLOSED ("closed");

    private final String status;       

    private IssueStatus(String s) {
    	status = s;
    }

    public boolean equals(IssueStatus other) {
        return status.equals(other.toString());
    }

    public String toString() {
       return this.status;
    }

    public String value() {
       return this.status;
    }
}
