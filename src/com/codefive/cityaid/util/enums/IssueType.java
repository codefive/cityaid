package com.codefive.cityaid.util.enums;

public enum IssueType {
	VANDALISM ("vandalism"),
	GARBAGE ("garbage"),
	ACCIDENT ("accident"),
	STREET_DAMAGE ("street_damage"),
	FURNITURE_DAMAGE ("furniture_damage"),
	NATURE ("nature"),
	OTHER ("other");

    private final String type;       

    private IssueType(String t) {
    	type = t;
    }

    public boolean equals(IssueType other) {
        return type.equals(other.toString());
    }

    public String toString() {
       return this.type;
    }

    public String value() {
       return this.type;
    }
}
