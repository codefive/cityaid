package com.codefive.cityaid.util.enums;

public enum NotificationType {
	MEDAL ("medal"),
	LEVEL_UP ("level_up"),
	ISSUE_WIP ("issue_wip"),
	ISSUE_CLOSED ("issue_closed"),
	ISSUE_SOLVED ("issue_solved"),
	LOG_COMMENT ("log_comment"),
	EDIT ("edit"),
	NEW_PHOTOS ("new_photos"),
	COMMENT ("comment");

    private final String type;       

    private NotificationType(String t) {
    	type = t;
    }

    public boolean equals(NotificationType other) {
        return type.equals(other.toString());
    }

    public String toString() {
       return this.type;
    }
}
