package com.codefive.cityaid.util.enums;

public enum ErrorType {
	INTERNAL_SERVER_ERROR ("Ocorreu um erro"),
	MISSING_WRONG_PARAM ("Par�metro errado ou em falta"),
	WRONG_PWD ("Password incorreta"),
	USER_NOT_EXISTS ("Utilizador n�o existe"),
	UNAUTHORIZED ("Sem autoriza��o"),
	INVALID_TOKEN ("Sess�o expirada"),
	ISSUE_NOT_EXISTS ("Ocorr�ncia n�o existe"),
	USER_ALREADY_EXISTS ("Utilizador j� existe"),
	RATING_NOT_EXISTS ("Rating n�o existe"),
	RATING_ALREADY_EXISTS ("Rating j� existe"),
	UNVERIFIED ("Conta n�o verificada"),
	ENTITY_ALREADY_EXISTS ("Entidade j� existe"),
	ENTITY_NOT_EXISTS ("Entitidade n�o existe"),
	USER_ALREADY_BACKOFFICE ("Utilizador j� � de backoffice"),
	USER_ALREADY_MANAGER ("Utilizador j� � gestor"),
	USER_NOT_MANAGER ("Utilizador n�o � gestor"),
	INVALID_RESET_TOKEN ("Token de reset inv�lido"),
	ALREADY_FOLLOWED ("Ocorr�ncia j� seguida"),
	NOT_FOLLOWING ("Ocorr�ncia n�o est� a ser seguida"),
	COMMENT_NOT_EXISTS ("Coment�rio n�o existe"),
	USER_NOT_BACKOFFICE ("Utilizador n�o � de backoffice"),
	USER_ALREADY_WORKER ("Utilizador j� � trabalhador"),
	USER_NOT_WORKER ("Utilizador n�o � trabalhador"),
	ENTITY_ALREADY_ASSIGNED ("Entidade j� atribuida"),
	FROZEN_ACCOUNT ("Conta congelada"),
	REPORT_EXISTS ("J� foi reportado"),
	REPORT_NOT_EXISTS ("Ainda n�o foi reportado"),
	FORCED_RATING ("Rating est� a ser for�ada"),
	ISSUE_ALREADY_DELETED ("Ocorr�ncia j� apagada"),
	COMMENT_ALREADY_DELETED ("Coment�rio j� apagado"),
	ISSUE_ALREADY_CLOSED ("Ocorr�ncia j� fechada"),
	ISSUE_ALREADY_SOLVED ("Ocorr�ncia j� resolvida"),
	PHOTO_NOT_EXISTS ("Fotografia n�o existe"),
	MEDAL_NOT_EXISTS ("Medalha n�o existe");
	
	
	private final String type;
	
	private ErrorType(String s) {
		type = s;
	}
	
	public boolean equals(ErrorType other) {
		return type.equals(other.toString());
	}
	
	public String toString(){
		return this.type;
	}

}
