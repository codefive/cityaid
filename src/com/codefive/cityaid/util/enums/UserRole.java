package com.codefive.cityaid.util.enums;

public enum UserRole {
	REPORTER ("reporter"),
	BACKOFFICE ("backoffice"),
	WORKER ("worker"),
	FROZEN ("frozen");

    private final String role;       

    private UserRole(String s) {
        role = s;
    }

    public boolean equals(UserRole other) {
        return role.equals(other.toString());
    }

    public String toString() {
       return this.role;
    }
}
