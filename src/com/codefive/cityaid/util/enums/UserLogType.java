package com.codefive.cityaid.util.enums;

public enum UserLogType {
	REGISTERED ("registered"),
	LOGIN_ATTEMPT ("login_attempt"),
	LOGGED_IN ("logged_in"),
	LOGGED_OUT ("logged_out"),
	O_CREATED ("o_created"),
	O_WATCHED ("o_watched"),
	O_RATED ("o_rated"),
	ROLE_CHANGE ("role_change"),
	VERIFIED ("verified"),
	PASSWORD_UPDATE_ATTEMPT ("password_update_attempt"),
	PASSWORD_UPDATE ("password_update"),
	E_RATED ("e_rated"),
	COMMENT ("comment"),
	FOLLOWED ("followed"),
	UNFOLLOWED ("unfollowed"),
	EDITED ("edited"),
	MEDAL ("medal"),
	LEVEL_UP ("level_up"),
	BANNED ("banned"),
	SOLVED_ISSUE ("solved_issue"),
	ISSUE_LOG_COMMENT ("issue_log_comment");

    private final String type;       

    private UserLogType(String s) {
    	type = s;
    }

    public boolean equals(IssueStatus other) {
        return type.equals(other.toString());
    }

    public String toString() {
       return this.type;
    }
}
