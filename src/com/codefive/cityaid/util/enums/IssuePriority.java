package com.codefive.cityaid.util.enums;

public enum IssuePriority {
	UNDEFINED (0),
	LOW (1),
	MEDIUM (2),
	HIGH (3);

    private final int type;       

    private IssuePriority(int s) {
    	type = s;
    }

    public boolean equals(IssuePriority other) {
        return type == other.value();
    }

    public int value() {
       return this.type;
    }
}
