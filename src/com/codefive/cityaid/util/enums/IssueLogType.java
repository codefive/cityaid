package com.codefive.cityaid.util.enums;

public enum IssueLogType {
	CREATED ("created"),
	STATUS_CHANGED ("status_changed"),
	LOG_COMMENT ("log_comment"),
	DELETED ("deleted"),
	EDITED ("edited");

    private final String type;       

    private IssueLogType(String s) {
    	type = s;
    }

    public boolean equals(IssueLogType other) {
        return type.equals(other.toString());
    }

    public String toString() {
       return this.type;
    }
}
