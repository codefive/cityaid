package com.codefive.cityaid.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.IssueType;
import com.codefive.cityaid.util.enums.NotificationType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.Comment;
import com.codefive.cityaid.util.replyData.CommentListing;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.requestData.CommentData;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;

@Path("/issues/{issue}/comments")
public class CommentResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(CommentResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	private static final int PAGE_SIZE = 10;
	
	public CommentResource() { } //Nothing to be done here...
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response submitComment(@PathParam("issue") String issueKeyStr, CommentData data, @Context HttpServletRequest request, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				//User has permission to perform operation
				txn.commit();
				
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				try {
					//Get issue
					Entity issueEnt = datastore.get(issueKey);
					
					//Add issue comment
					Entity issueComment = new Entity("IssueComment", issueKey);
					issueComment.setProperty("submitted", new Date());
					issueComment.setUnindexedProperty("user", authUsername);
					issueComment.setUnindexedProperty("comment", data.comment);
					issueComment.setProperty("hidden", false);
					datastore.put(issueComment);
					
					//Create user log entry
					Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
					userLogEntry.setProperty("date", new Date());
					userLogEntry.setUnindexedProperty("type", UserLogType.COMMENT.toString());
					userLogEntry.setUnindexedProperty("description", "Comentou a ocorr�ncia \""+(String)issueEnt.getProperty("title")+"\"");
					userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
					userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
					userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
					userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
					userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
					datastore.put(userLogEntry);

					//Update points
					Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
					Entity appPoints = datastore.get(appPointsKey);
					txn = datastore.beginTransaction();
					Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
					Entity userGamificationStats = datastore.get(txn, userGamificationStatsKey);
					long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("comment"));
					userGamificationStats.setUnindexedProperty("points", newPoints);
					datastore.put(txn, userGamificationStats);
					txn.commit();

					txn = datastore.beginTransaction();
					//Get user stats
					Key userCommentStatsKey = KeyFactory.createKey("UserCommentStats", authUsername);
					Entity userCommentStatsEnt = datastore.get(txn, userCommentStatsKey);
					
					//Update user stats
					userCommentStatsEnt.setUnindexedProperty("issues_commented", ((long)userCommentStatsEnt.getProperty("issues_commented"))+1);
					switch(IssueType.valueOf(((String)issueEnt.getProperty("type")).toUpperCase())){
						case ACCIDENT:
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_accident", ((long)userCommentStatsEnt.getProperty("issues_commented_type_accident"))+1);
							break;
						case FURNITURE_DAMAGE:
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_furnituredmg", ((long)userCommentStatsEnt.getProperty("issues_commented_type_furnituredmg"))+1);
							break;
						case GARBAGE:
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_garbage", ((long)userCommentStatsEnt.getProperty("issues_commented_type_garbage"))+1);
							break;
						case NATURE:
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_nature", ((long)userCommentStatsEnt.getProperty("issues_commented_type_nature"))+1);
							break;
						case OTHER:
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_other", ((long)userCommentStatsEnt.getProperty("issues_commented_type_other"))+1);
							break;
						case STREET_DAMAGE:
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_streetdmg", ((long)userCommentStatsEnt.getProperty("issues_commented_type_streetdmg"))+1);
							break;
						case VANDALISM:
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_vandalism", ((long)userCommentStatsEnt.getProperty("issues_commented_type_vandalism"))+1);
							break;
						default:
							break;					
					}
					datastore.put(txn, userCommentStatsEnt);
					txn.commit();
										
					//Update gamification system and notify followers
					Queue queue = QueueFactory.getDefaultQueue();
					queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+authUsername));
					queue.add(TaskOptions.Builder.withUrl("/rest/notifications/followers/"+issueKeyStr)
									.param("title", "CityAid")
									.param("icon", "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg")
									.param("body", authUsername+" comentou numa ocorr�ncia que est�s a seguir")
									.param("url", "https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr)
									.param("emailTitle", "Coment�rio em ocorr�ncia")
									.param("emailBody", "O utilizador <a href=\"https://city-aid.appspot.com/frontoffice/viewUser.html?username="+authUsername+"\">"+(authUsername)+"</a> fez o seguinte coment�rio na ocorr�ncia <a href=\"https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr+"\">\""+((String)issueEnt.getProperty("title"))+"\"</a>:<br><br><div style=\"margin-left:5em; display:block;\">\""+data.comment+"\"</div>")
									.param("type", NotificationType.COMMENT.toString())
									.param("except", authUsername));
					
					return Response.status(Status.OK).entity("{\"comment\":\""+Long.toString(issueComment.getKey().getId())+"\"}").build();
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					txn.rollback();
					
					LOG.warning("Issue does not exist: " + issueKeyStr);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getComments(@PathParam("issue") String issueKeyStr, @QueryParam("cursor") String cursor,
			@Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				//User has permission to perform operation
				txn.commit();
				
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				try {
					//Get issue
					datastore.get(issueKey);
					
					//Retrieve comments
					Query query = new Query("IssueComment").setAncestor(issueKey).addSort("submitted", SortDirection.DESCENDING);
					query.setFilter(new FilterPredicate("hidden", FilterOperator.EQUAL, false));
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> results = datastore.prepare(query).asQueryResultList(fetchOptions);
					
					List<Comment> comments = new ArrayList<Comment>(results.size());
					for(Entity comment: results) {
						comments.add(new Comment(KeyFactory.keyToString(comment.getKey()),
								issueKeyStr, (Date)comment.getProperty("submitted"),
								(String)comment.getProperty("user"),
								(String)comment.getProperty("comment")));
					}
					
					// Cursor
					Cursor c = results.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					CommentListing ret = new CommentListing(comments, cursor);
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					
					LOG.warning("Issue does not exist: " + issueKeyStr);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/{comment}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewComment(@PathParam("issue") String issueKeyStr, @PathParam("comment") String com, 
			@Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				
				//User has permission to perform operation
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				Key comKey = KeyFactory.createKey(issueKey, "IssueComment", com);
				try {
					
					//Get comment
					Entity comment = datastore.get(txn, comKey);
					txn.commit();
					
					Comment ret = new Comment(Long.toString(comment.getKey().getId()),
								issueKeyStr, (Date)comment.getProperty("submitted"),
								(String)comment.getProperty("user"),
								(String)comment.getProperty("comment"));
					
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} catch (EntityNotFoundException e) {
					//Comment does not exist
					txn.rollback();
					
					LOG.warning("Comment does not exist: " + com);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.COMMENT_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/{comment}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response deleteComment(@PathParam("issue") String issueKeyStr, @PathParam("comment") String commentKeyStr, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				
				Entity user = datastore.get(session.getParent());
				Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
				Entity userGamificationStats = datastore.get(userGamificationStatsKey);
				Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
				Entity appPoints = datastore.get(appPointsKey);
				try {
					//Get comment
					Key comKey = KeyFactory.stringToKey(commentKeyStr);
					Entity comment = datastore.get(comKey);					
					
					if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) || ((String)comment.getProperty("user")).equals(authUsername) ||
							((long)userGamificationStats.getProperty("level") >= (long)appPoints.getProperty("modLevel"))) {
						
						//User has permission to perform operation
						if(!(boolean) comment.getProperty("hidden")) {
							
							//User didnt make the comment
							if(!((String)comment.getProperty("user")).equals(authUsername)) {
								Transaction txn = datastore.beginTransaction();
								userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", (String)comment.getProperty("user"));
								userGamificationStats = datastore.get(txn, userGamificationStatsKey);
								
								//Update points
								long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("deleteComment"));
								if(newPoints < 0)
									newPoints = 0;
								userGamificationStats.setUnindexedProperty("points", newPoints);
								datastore.put(txn, userGamificationStats);
								
								txn.commit();
							}
							
							Transaction txn = datastore.beginTransaction();
							//Get user stats
							Key userCommentStatsKey = KeyFactory.createKey("UserCommentStats", (String)comment.getProperty("user"));
							Entity userCommentStatsEnt = datastore.get(txn, userCommentStatsKey);
							
							//Update user stats
							userCommentStatsEnt.setUnindexedProperty("issues_commented", ((long)userCommentStatsEnt.getProperty("issues_commented"))-1);
							datastore.put(txn, userCommentStatsEnt);
							txn.commit();
												
							//Hide comment
							comment.setProperty("hidden", true);
							datastore.put(comment);
							
							return Response.status(Status.NO_CONTENT).build();
						} else {
							//Comment already deleted
							return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.COMMENT_ALREADY_DELETED))).build();
						}
					} else {
						//User does not belong to required role
						
						LOG.warning("User attempted to perform operation without required access level: " + authUsername);
						return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
					}
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					LOG.warning("Issue does not exist: " + issueKeyStr);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				}
			} else {
				//Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
}
