package com.codefive.cityaid.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.CommentReportListing;
import com.codefive.cityaid.util.replyData.CommentReports;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.IssueReportListing;
import com.codefive.cityaid.util.replyData.IssueReports;
import com.codefive.cityaid.util.replyData.UserReportListing;
import com.codefive.cityaid.util.replyData.UserReports;
import com.codefive.cityaid.util.requestData.CommentReport;
import com.codefive.cityaid.util.requestData.UserReport;
import com.codefive.cityaid.util.requestData.IssueReport;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

@Path("/reports")
public class ReportsResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(ReportsResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	private static final int PAGE_SIZE = 5;
	
	public ReportsResource() { }
	
	@POST
	@Path("/issues")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response reportIssue(IssueReport data, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){				
				//User has permission to perform operation
				txn.commit();
				
				txn = datastore.beginTransaction();
				Key issueKey = KeyFactory.stringToKey(data.issue);
				try {
					//Get issue
					datastore.get(txn, issueKey);
					
					try {
						//If the report does not exist an Exception is thrown. Otherwise,
						Key reportKey = KeyFactory.createKey(issueKey, "IssueReport", authUsername);
						datastore.get(txn, reportKey);
						txn.rollback();
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.REPORT_EXISTS))).build();
					} catch (EntityNotFoundException e){
						//Create issue report
						Entity report = new Entity("IssueReport", authUsername, issueKey);
						report.setIndexedProperty("read", false);
						report.setUnindexedProperty("comment", data.comment);
						datastore.put(txn, report);
						txn.commit();
						
						return Response.status(Status.NO_CONTENT).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					txn.rollback();
					
					LOG.warning("Issue does not exist: " + data.issue + "|" + authUsername);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@POST
	@Path("/comments")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response reportComment(CommentReport data, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())){				
				//User has permission to perform operation
				Key commentKey = KeyFactory.stringToKey(data.report);
				try {
					//Get comment
					datastore.get(commentKey);
					
					try {
						//If the report does not exist an Exception is thrown. Otherwise,
						Key reportKey = KeyFactory.createKey(commentKey, "CommentReport", authUsername);
						datastore.get(reportKey);
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.REPORT_EXISTS))).build();
					} catch (EntityNotFoundException e) {
						//Create comment report
						Entity report = new Entity("CommentReport", authUsername, commentKey);
						report.setIndexedProperty("read", false);
						report.setUnindexedProperty("comment", data.comment);
						datastore.put(report);
						
						return Response.status(Status.NO_CONTENT).build();
					}
				} catch (EntityNotFoundException e) {
					// Comment does not exist
					LOG.warning("Comment does not exist: " + data.report + "|" + authUsername);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.COMMENT_NOT_EXISTS))).build();
				}
			} else {
				//Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@POST
	@Path("/users")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response reportUser(UserReport data, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {				
				//User has permission to perform operation
				txn.commit();
				
				txn = datastore.beginTransaction();
				Key userKey = KeyFactory.createKey("User", data.user);
				try {
					//Get user
					datastore.get(txn, userKey);
					
					try {
						//If the report does not exist an Exception is thrown. Otherwise,
						Key reportKey = KeyFactory.createKey(userKey, "UserReport", authUsername);
						datastore.get(txn, reportKey);
						txn.rollback();
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.REPORT_EXISTS))).build();
					} catch (EntityNotFoundException e){
						//Create user report
						Entity report = new Entity("UserReport", authUsername, userKey);
						report.setIndexedProperty("read", false);
						report.setUnindexedProperty("comment", data.comment);
						datastore.put(txn, report);
						txn.commit();
						
						return Response.status(Status.NO_CONTENT).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} catch (EntityNotFoundException e) {
					// User does not exist
					txn.rollback();
					
					LOG.warning("User does not exist: " + data.user + "|" + authUsername);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/issues")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response unreadIssueReports(@QueryParam("cursor") String cursor, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(session.getParent());
				Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
				Entity userGamificationStats = datastore.get(userGamificationStatsKey);
				Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
				Entity appPoints = datastore.get(appPointsKey);
				if (((long)userGamificationStats.getProperty("level") >= (long)appPoints.getProperty("modLevel")) || 
						((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					
					// Get issues
					Query reports = new Query("IssueReport");
					reports.setFilter(new FilterPredicate("read", FilterOperator.EQUAL, false));
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> results = datastore.prepare(reports).asQueryResultList(fetchOptions);

					// Create response
					List<IssueReports> issues = new ArrayList<IssueReports>(results.size());
					
					for(Entity report: results) {
						Entity i = datastore.get(report.getKey().getParent());
						IssueReports r = new IssueReports(KeyFactory.keyToString(report.getKey()), 
								KeyFactory.keyToString(i.getKey()), (String) i.getProperty("title"),
								report.getKey().getName(), i.getKey().getParent().getName(),
								(String) report.getProperty("comment"));
						issues.add(r);
					}
					
					// Cursor
					Cursor c = results.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					IssueReportListing ret = new IssueReportListing(issues, cursor);
					
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
					
				} else {
					//Wrong user role

					LOG.warning("User does not have required role: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_BACKOFFICE))).build();
				}
			} else {
				//Token does not belong to user
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@GET
	@Path("/comments")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response unreadCommentReports(@QueryParam("cursor") String cursor, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(session.getParent());
				Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
				Entity userGamificationStats = datastore.get(userGamificationStatsKey);
				Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
				Entity appPoints = datastore.get(appPointsKey);
				if (((long)userGamificationStats.getProperty("level") >= (long)appPoints.getProperty("modLevel")) || 
						((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					
					// Get comments
					Query reports = new Query("CommentReport");
					reports.setFilter(new FilterPredicate("read", FilterOperator.EQUAL, false));
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> results = datastore.prepare(reports).asQueryResultList(fetchOptions);

					// Create response
					List<CommentReports> comments = new ArrayList<CommentReports>(results.size());
					
					for(Entity report: results) {
						Entity comment = datastore.get(report.getKey().getParent());
						Entity issue = datastore.get(comment.getKey().getParent());
						CommentReports r = new CommentReports(KeyFactory.keyToString(report.getKey()), 
								KeyFactory.keyToString(comment.getKey()), KeyFactory.keyToString(issue.getKey()),
								(String) issue.getProperty("title"), (String) comment.getProperty("comment"),
								report.getKey().getName(), (String) comment.getProperty("user"),
								(String) report.getProperty("comment"), (Date) comment.getProperty("submitted"));
						comments.add(r);
					}
					
					// Cursor
					Cursor c = results.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					CommentReportListing ret = new CommentReportListing(comments, cursor);
					
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
					
				} else {
					//Wrong user role

					LOG.warning("User does not have required role: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_BACKOFFICE))).build();
				}
			} else {
				//Token does not belong to user
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@GET
	@Path("/users")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response unreadUserReports(@QueryParam("cursor") String cursor, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					
					// Get users
					Query reports = new Query("UserReport");
					reports.setFilter(new FilterPredicate("read", FilterOperator.EQUAL, false));
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> results = datastore.prepare(reports).asQueryResultList(fetchOptions);

					// Create response
					List<UserReports> users = new ArrayList<UserReports>(results.size());
					
					for(Entity report: results) {
						Entity u = datastore.get(report.getKey().getParent());
						UserReports r = new UserReports(KeyFactory.keyToString(report.getKey()),
								u.getKey().getName(), (String) u.getProperty("name"),
								report.getKey().getName(), (String) report.getProperty("comment"));
						users.add(r);
					}
					
					// Cursor
					Cursor c = results.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					UserReportListing ret = new UserReportListing(users, cursor);
					
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
					
				} else {
					//Wrong user role

					LOG.warning("User does not have required role: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_BACKOFFICE))).build();
				}
			} else {
				//Token does not belong to user
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@PUT
	@Path("/issues/{report}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response readIssueReport(@PathParam("report") String report, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(session.getParent());
				Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
				Entity userGamificationStats = datastore.get(userGamificationStatsKey);
				Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
				Entity appPoints = datastore.get(appPointsKey);
				if (((long)userGamificationStats.getProperty("level") >= (long)appPoints.getProperty("modLevel")) || 
						((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())) {
					//User has permission to perform operation
					
					Transaction txn = datastore.beginTransaction();
					Key reportKey = KeyFactory.stringToKey(report);
					try {
						//Get report
						Entity rep = datastore.get(txn, reportKey);
						rep.setProperty("read", true);
						datastore.put(txn, rep);
						txn.commit();
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						//Report does not exist
						txn.rollback();
						
						LOG.warning("Report does not exist: " + report + "|" + authUsername);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.REPORT_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					LOG.warning("User does not have required role: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_BACKOFFICE))).build();
				}
			} else {
				//Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@PUT
	@Path("/comments/{report}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response readCommentReport(@PathParam("report") String report, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(session.getParent());
				Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
				Entity userGamificationStats = datastore.get(userGamificationStatsKey);
				Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
				Entity appPoints = datastore.get(appPointsKey);
				if (((long)userGamificationStats.getProperty("level") >= (long)appPoints.getProperty("modLevel")) || 
						((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())) {
					//User has permission to perform operation
					Transaction txn = datastore.beginTransaction();
					Key reportKey = KeyFactory.stringToKey(report);
					try {
						//Get report
						Entity rep = datastore.get(txn, reportKey);
						rep.setProperty("read", true);
						datastore.put(txn, rep);
						txn.commit();
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						//Report does not exist
						txn.rollback();
						
						LOG.warning("Report does not exist: " + report + "|" + authUsername);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.REPORT_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					LOG.warning("User does not have required role: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_BACKOFFICE))).build();
				}
			} else {
				//Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@PUT
	@Path("/users/{report}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response readUserReport(@PathParam("report") String report, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())) {
					//User has permission to perform operation
					
					Transaction txn = datastore.beginTransaction();
					Key reportKey = KeyFactory.stringToKey(report);
					try {
						//Get report
						Entity rep = datastore.get(txn, reportKey);
						rep.setProperty("read", true);
						datastore.put(txn, rep);
						txn.commit();
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						//Report does not exist
						txn.rollback();
						
						LOG.warning("Report does not exist: " + report + "|" + authUsername);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.REPORT_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					LOG.warning("User does not have required role: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_BACKOFFICE))).build();
				}
			} else {
				//Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
}
