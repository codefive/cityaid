package com.codefive.cityaid.resources;

import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.IssueStatus;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;

@Path("/cron")
public class CronResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(CronResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public CronResource() { } //Nothing to be done here...
	
	@GET
	@Path("/clean/auth")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response cleanupTokens() throws EntityNotFoundException{
		LOG.fine("Token cleanup task started");

		try {
			//Fetch expired tokens
			Filter timeFilter = new FilterPredicate("expiration", FilterOperator.LESS_THAN, (System.currentTimeMillis()/1000));
			Query query = new Query("Session").setFilter(timeFilter);
			List<Entity> results = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
			
			//Remove tokens
			for(Entity e : results){
				datastore.delete(e.getKey());
			}
			
			LOG.fine("Finished token cleanup cron job");
			return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e){
			return Response.status(Status.NO_CONTENT).build();
			//Nothing to see here. Please disperse. Please.
		}
	}
	
	@GET
	@Path("/clean/password")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response cleanupResetPwdTokens() throws EntityNotFoundException{
		LOG.fine("Password Reset Token cleanup task started");

		try {
			//Fetch expired tokens
			Filter timeFilter = new FilterPredicate("expiration", FilterOperator.LESS_THAN, (System.currentTimeMillis()/1000));
			Query query = new Query("ResetToken").setFilter(timeFilter);
			List<Entity> results = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
			
			//Remove tokens
			for(Entity e : results){
				datastore.delete(e.getKey());
			}
			
			LOG.fine("Finished password reset token cleanup cron job");
			return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e){
			return Response.status(Status.NO_CONTENT).build();
			//Nothing to see here. Please disperse. Please.
		}
	}
	
	@GET
	@Path("/clean/accounts")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response cleanupUnverifiedAccounts(){
		LOG.fine("Unverified accounts cleanup task started");

		try {
			//Fetch unverified accounts
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -7);
			cal.getTime();
			Filter timeFilter = new FilterPredicate("registered_since", FilterOperator.LESS_THAN, cal.getTime());
			Filter verifiedFilter = new FilterPredicate("verified", FilterOperator.EQUAL, false);
			Query query = new Query("User").setFilter(CompositeFilterOperator.and(timeFilter, verifiedFilter));
			List<Entity> results = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
			
			//Remove tokens
			for(Entity e : results){
				datastore.delete(e.getKey());
			}
			
			LOG.fine("Finished unverified accounts cleanup cron job");
			return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e){
			return Response.status(Status.NO_CONTENT).build();
			//Nothing to see here. Please disperse. Please.
		}
	}
	
	@GET
	@Path("/rating/issues")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateIssueRatings(){
		LOG.fine("Update issue ratings cron task started");

		try {
			//Fetch issue ratings update flags
			Query query = new Query("IssueRatingFlag");
			List<Entity> results = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
			
			//Update ratings
			for(Entity e : results){
				Key issueKey = KeyFactory.stringToKey(e.getKey().getName());
				Transaction txn = datastore.beginTransaction();
				try {
					//Get issue
					Entity issueEnt = datastore.get(txn, issueKey);
					if ((boolean) issueEnt.getProperty("forceRating")){
						//Rating is being forced, can't update rating
					} else {
						//Issue still exists and rating is not being forced
						
						//Get ratings
						Query ratingQuery = new Query("Rating").setAncestor(issueKey);
						List<Entity> ratings = datastore.prepare(ratingQuery).asList(FetchOptions.Builder.withDefaults());					
						
						//Compute average priority
						double avgPriority = 0;
						for (Entity rating : ratings){
							avgPriority += (long)rating.getProperty("priority");
						}
						avgPriority /= ratings.size();
						
						//Update issue data
						issueEnt.setProperty("rating", Math.round(avgPriority));
						datastore.put(txn, issueEnt);
						txn.commit();
					}
				} catch (EntityNotFoundException e1) {
					//Issue no longer exists, who cares...
					txn.rollback();
				} catch (Exception e1){
					//Nothing to see here. Please disperse. Please.
					txn.rollback();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.OK).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
				
				//Delete rating flag
				datastore.delete(e.getKey());
			}
			
			LOG.fine("Finished update issue ratings cron job");
			return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e){
			return Response.status(Status.NO_CONTENT).build();
			//Nothing to see here. Please disperse. Please.
		}
	}
	
	@GET
	@Path("/rating/entities")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateEntityRatings(){
		LOG.fine("Update issue ratings cron task started");

		try {
			//Fetch issue ratings update flags
			Query query = new Query("EntityRatingFlag");
			List<Entity> results = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
			
			//Update ratings
			for(Entity e : results){
				Key entityKey = KeyFactory.createKey("Entity", e.getKey().getName());
				Transaction txn = datastore.beginTransaction();
				try {
					//Get entity
					Entity entityEnt = datastore.get(txn, entityKey);
						
					//Get ratings
					Query ratingQuery = new Query("EntityRating").setAncestor(entityKey);
					List<Entity> ratings = datastore.prepare(ratingQuery).asList(FetchOptions.Builder.withDefaults());					
					
					//Compute average rating
					double avgRating = 0;
					for (Entity rating : ratings){
						avgRating += (long)rating.getProperty("rating");
					}
					avgRating /= ratings.size();
					
					//Update entity data
					entityEnt.setProperty("rating", Math.round(avgRating));
					datastore.put(txn, entityEnt);
					txn.commit();
				} catch (EntityNotFoundException e1) {
					//Entity no longer exists, who cares...
					txn.rollback();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.OK).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
				
				//Delete rating flag
				datastore.delete(e.getKey());
			}
			
			LOG.fine("Finished update issue ratings cron job");
			return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e){
			return Response.status(Status.NO_CONTENT).build();
			//Nothing to see here. Please disperse. Please.
		}
	}
	
	@GET
	@Path("/stats/app")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateAppStats(){
		LOG.fine("Update app stats cron task started");

		try {
			//Fetch issue ratings update flags
			Query reportersQuery = new Query("User")
					.setFilter(new FilterPredicate("role", FilterOperator.EQUAL, UserRole.REPORTER.toString()))
					.setKeysOnly();
			long numReporters = datastore.prepare(reportersQuery).asList(FetchOptions.Builder.withDefaults()).size();
			Query backofficeQuery = new Query("User")
					.setFilter(new FilterPredicate("role", FilterOperator.EQUAL, UserRole.BACKOFFICE.toString()))
					.setKeysOnly();
			long numBackoffice = datastore.prepare(backofficeQuery).asList(FetchOptions.Builder.withDefaults()).size();
			Query workersQuery = new Query("User")
					.setFilter(new FilterPredicate("role", FilterOperator.EQUAL, UserRole.WORKER.toString()))
					.setKeysOnly();
			long numWorkers = datastore.prepare(workersQuery).asList(FetchOptions.Builder.withDefaults()).size();
			long numUsers = numReporters+numBackoffice+numWorkers;
			Query activeUsersQuery = new Query("Session")
					.setKeysOnly();
			long numActiveUsers = datastore.prepare(activeUsersQuery).asList(FetchOptions.Builder.withDefaults()).size();
			Query createdIssuesQuery = new Query("Occurrence")
					.setFilter(new FilterPredicate("status", FilterOperator.EQUAL, IssueStatus.CREATED.toString()))
					.setKeysOnly();
			long numIssuesCreated = datastore.prepare(createdIssuesQuery).asList(FetchOptions.Builder.withDefaults()).size();
			Query progressIssuesQuery = new Query("Occurrence")
					.setFilter(new FilterPredicate("status", FilterOperator.EQUAL, IssueStatus.WORK_IN_PROGRESS.toString()))
					.setKeysOnly();
			long numIssuesProgress = datastore.prepare(progressIssuesQuery).asList(FetchOptions.Builder.withDefaults()).size();
			Query closedIssuesQuery = new Query("Occurrence")
					.setFilter(new FilterPredicate("status", FilterOperator.EQUAL, IssueStatus.CLOSED.toString()))
					.setKeysOnly();
			long numIssuesClosed = datastore.prepare(closedIssuesQuery).asList(FetchOptions.Builder.withDefaults()).size();
			Query solvedIssuesQuery = new Query("Occurrence")
					.setFilter(new FilterPredicate("status", FilterOperator.EQUAL, IssueStatus.SOLVED.toString()))
					.setKeysOnly();
			long numIssuesSolved = datastore.prepare(solvedIssuesQuery).asList(FetchOptions.Builder.withDefaults()).size();
			long numIssues = numIssuesCreated+numIssuesProgress+numIssuesClosed+numIssuesSolved;
			Query entitiesQuery = new Query("Entity")
					.setKeysOnly();
			long numEntities = datastore.prepare(entitiesQuery).asList(FetchOptions.Builder.withDefaults()).size();
			Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
			Entity appPoints = datastore.get(appPointsKey);
			Query moderatorsQuery = new Query("UserGamificationStats")
					.setFilter(new FilterPredicate("level", FilterOperator.GREATER_THAN_OR_EQUAL, (long)appPoints.getProperty("modLevel")))
					.setKeysOnly();
			long numModerators = datastore.prepare(moderatorsQuery).asList(FetchOptions.Builder.withDefaults()).size();
			
			
			//Generate App Stats entity
			Key appStatsKey = KeyFactory.createKey("Stats", "_APP_");
			Entity appStats = datastore.get(appStatsKey);
			appStats.setUnindexedProperty("users", numUsers);
			appStats.setUnindexedProperty("reporters", numReporters);
			appStats.setUnindexedProperty("backoffice", numBackoffice);
			appStats.setUnindexedProperty("workers", numWorkers);
			appStats.setUnindexedProperty("activeUsers", numActiveUsers);
			appStats.setUnindexedProperty("issues", numIssues);
			appStats.setUnindexedProperty("issuesCreated", numIssuesCreated);
			appStats.setUnindexedProperty("issuesProgress", numIssuesProgress);
			appStats.setUnindexedProperty("issuesSolved", numIssuesSolved);
			appStats.setUnindexedProperty("issuesClosed", numIssuesClosed);
			appStats.setUnindexedProperty("entities", numEntities);
			appStats.setUnindexedProperty("moderators", numModerators);
			datastore.put(appStats);
			
			LOG.fine("Finished update app stats cron job");
			return Response.status(Status.NO_CONTENT).build();
		} catch (Exception e){
			return Response.status(Status.NO_CONTENT).build();
			//Nothing to see here. Please disperse. Please.
		}
	}
	
}
