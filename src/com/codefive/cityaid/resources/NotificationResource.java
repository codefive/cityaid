package com.codefive.cityaid.resources;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.Notification;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;
import com.pusher.rest.Pusher;

@Path("/notifications")
public class NotificationResource {
	
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(NotificationResource.class.getName());
	private final static Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
	public static final String PUSHER_APP_ID = "363029";
	public static final String PUSHER_KEY = "bc5606e25bb8d1738c33";
	public static final String PUSHER_SECRET = "091fd5f2008af8f86171";
		
	public NotificationResource() { } //Nothing to be done here...
	
	@POST
	@Path("/auth")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response authenticateNotificationUser(@QueryParam("token") String authToken, @QueryParam("user") String authUsername, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			LOG.warning("No Authorization");
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		String socketId = request.getParameter("socket_id");
		String channel = request.getParameter("channel_name");
		
		if (socketId == null || socketId.isEmpty() || channel == null || channel.isEmpty() || !channel.equals("private-"+authUsername)){
			LOG.warning("Post data wrong. Expected (private-"+authUsername+"). Got ("+channel+")");
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				//User has permission
				Pusher pusher = new Pusher(PUSHER_APP_ID, PUSHER_KEY, PUSHER_SECRET);
				pusher.setCluster("eu");
				pusher.setEncrypted(true);
				
				String authString = pusher.authenticate(socketId, channel);
				
				return Response.status(Status.OK).entity(authString).build();
			} else {
				//Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response notifyTest() {
		Pusher pusher = new Pusher(PUSHER_APP_ID, PUSHER_KEY, PUSHER_SECRET);
		pusher.setCluster("eu");
		pusher.setEncrypted(true);
		Notification notification = new Notification("test1", "CityAid", "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg",
													"Hello World!", "https://city-aid.appspot.com/", "Email Title", "Email Body", "test");
		pusher.trigger("private-test1", "notification", Collections.singletonMap("message", g.toJson(notification)));
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
	@POST
	@Path("/followers/{issue}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response notifyFollowers(@PathParam("issue") String issueKeyStr, @Context HttpServletRequest request){
		String notificationTitle = request.getParameter("title");
		String notificationIcon = request.getParameter("icon");
		String notificationBody = request.getParameter("body");
		String notificationUrl = request.getParameter("url");
		String notificationEmailTitle = request.getParameter("emailTitle");
		String notificationEmailBody = request.getParameter("emailBody");
		String notificationType = request.getParameter("type");
		String userToIgnore = request.getParameter("except");
		
		//Get followers
		Filter issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, issueKeyStr);
		Query watchlistQuery = new Query("IssueFollow").setFilter(issueFilter);
		List<Entity> results = datastore.prepare(watchlistQuery).asList(FetchOptions.Builder.withDefaults());

		//Create notifications
		List<Notification> notifications = new ArrayList<Notification>(results.size());
		for (Entity result : results) {
			String receiver = (String) result.getProperty("follower");
			if (userToIgnore != null && userToIgnore.equals(receiver)){
				continue;
			}
			notifications.add(new Notification(receiver, notificationTitle, notificationIcon, notificationBody, notificationUrl, notificationEmailTitle, notificationEmailBody, notificationType));
		}
		
		//Send notifications
		sendNotifications(notifications);
		
		return Response.status(Status.NO_CONTENT).build();
	}
	
	//Send notifications
	public static void sendNotifications(List<Notification> notifications) {
		for (Notification notification : notifications){
			//Get user and user options
			Key userKey = KeyFactory.createKey("User", notification.receiver);
			Key userOptionsKey = KeyFactory.createKey("UserOptions", notification.receiver);
			Entity user;
			Entity userOptions;
			try {
				user = datastore.get(userKey);
				userOptions = datastore.get(userOptionsKey);
			} catch (EntityNotFoundException e) {
				//Could not find user, ignore and move to next one
				continue;
			}
			
			//Email
			if ((boolean) userOptions.getProperty("email_"+notification.type)){
				Properties props = new Properties();
				Session session = Session.getDefaultInstance(props, null);
				try {
					Message msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress("noreply@city-aid.appspotmail.com", "CityAid"));
					msg.addRecipient(Message.RecipientType.TO, new InternetAddress((String) user.getProperty("email"), (String) user.getProperty("name")));
					msg.setSubject(MimeUtility.encodeText(notification.emailTitle, "UTF-8", "Q"));
		
					String htmlBody = notification.emailBody+getEmailSignature();
					Multipart mp = new MimeMultipart();
		
					MimeBodyPart htmlPart = new MimeBodyPart();
					htmlPart.setContent(htmlBody, "text/html");
					mp.addBodyPart(htmlPart);
		
					msg.setContent(mp);
		
					Transport.send(msg);
					
					LOG.info("Notification sent via Mail to "+notification.receiver+". ["+notification.emailTitle+"]\n\n\n\n\n\n");
				} catch (AddressException e1) {
					LOG.warning("Email not sent to "+(String) user.getProperty("email")+"\nException: "+e1.getMessage());
				} catch (MessagingException e1) {
					LOG.warning("Email not sent to "+(String) user.getProperty("email")+"\nException: "+e1.getMessage());
				} catch (UnsupportedEncodingException e1) {
					LOG.warning("Email not sent to "+(String) user.getProperty("email")+"\nException: "+e1.getMessage());
				} catch (Exception e1) {
					LOG.warning("Email not sent to "+(String) user.getProperty("email")+"\nException: "+e1.getMessage());
				}
			}
			
			//Push
			if ((boolean) userOptions.getProperty("push_"+notification.type)){
				//Pusher
				Pusher pusher = new Pusher(PUSHER_APP_ID, PUSHER_KEY, PUSHER_SECRET);
				pusher.setCluster("eu");
				pusher.setEncrypted(true);
				pusher.trigger("private-"+notification.receiver, "notification", Collections.singletonMap("message", g.toJson(notification)));
				LOG.info("Notification sent via Pusher to "+notification.receiver+". ["+notification.body+"]\n\n\n\n\n\n");
			}
		}
	}
	
	public static String getEmailSignature(){
		return "<br><br><hr><br>CityAid © 2017";
	}
}
