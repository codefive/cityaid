package com.codefive.cityaid.resources;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.Issue;
import com.codefive.cityaid.util.replyData.IssueListing;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.gson.Gson;

@Path("/issues")
public class IssuesResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(IssuesResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public IssuesResource() {
	} // Nothing to be done here...

	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response listIssues(@QueryParam("state") String status, @QueryParam("type") String type,
			@QueryParam("northeastlat") double northeastlat, @QueryParam("northeastlng") double northeastlng,
			@QueryParam("southwestlat") double southwestlat, @QueryParam("southwestlng") double southwestlng,
			@QueryParam("submitter") String submitter, @QueryParam("recent") boolean recent,
			@QueryParam("entity") String entity, @QueryParam("priority") long priority,
			@QueryParam("cursor") String cursor, @QueryParam("page_size") long page_size,
			@Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				// User has permission to perform operation
				
				// Get issues
				Query issuesQuery = new Query("Occurrence").addSort("creation_date", SortDirection.ASCENDING);

				List<Filter> filters = new ArrayList<Filter>(20);
				
				filters.add(new FilterPredicate("hidden", FilterOperator.EQUAL, false));
				
				if (submitter != null) {
					issuesQuery.setAncestor(KeyFactory.createKey("User", submitter));
				}

				if (status != null) {
//					String[] states = status.split("[ \\+]");
//					
//					if(states.length > 1) {
//						List<Filter> statusFilters = new ArrayList<Filter>(states.length);
//						for (int i = 0; i<states.length; i++) {
//							statusFilters.add(new FilterPredicate("status", FilterOperator.EQUAL, states[i]));
//						}
//						filters.add(CompositeFilterOperator.or(statusFilters));
//					} else {
						filters.add(new FilterPredicate("status", FilterOperator.EQUAL, status));
//					}
				}
				
				if (type != null) {
//					String[] types = type.split("[ \\+]");
//					
//					if(types.length > 1){
//						List<Filter> typeFilters = new ArrayList<Filter>(types.length);
//						for (int i = 0; i<types.length; i++) {
//							types[i].trim();
//							typeFilters.add(new FilterPredicate("type", FilterOperator.EQUAL, types[i]));
//						}
//						filters.add(CompositeFilterOperator.or(typeFilters));
//					} else {
						filters.add(new FilterPredicate("type", FilterOperator.EQUAL, type));
//					}
				}
				
				if (entity != null) {
					filters.add(new FilterPredicate("entities", FilterOperator.EQUAL, entity));
				}
				
				if (priority > 0) {
					filters.add(new FilterPredicate("priority", FilterOperator.EQUAL, priority));
				}
				
				if (recent) {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DATE, -5);
					Date fiveDaysAgo = cal.getTime();
					filters.add(new FilterPredicate("creation_date", FilterOperator.GREATER_THAN_OR_EQUAL, fiveDaysAgo));
				}
				
				if (filters.size() > 1) {
					issuesQuery.setFilter(CompositeFilterOperator.and(filters));
				}
				
				// Cursor
				FetchOptions fetchOptions;
				if(page_size > 0)
					fetchOptions = FetchOptions.Builder.withLimit((int) page_size);
				else
					fetchOptions = FetchOptions.Builder.withDefaults();
				
				if (cursor != null && !cursor.equals("null")) {
					fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
				}
				
				QueryResultList<Entity> results = datastore.prepare(issuesQuery).asQueryResultList(fetchOptions);
				
				// Create response
				List<Issue> issues = new ArrayList<Issue>(results.size());
				
				for (Entity result : results) {
					
					if(northeastlat != 0 && northeastlng != 0 && southwestlat != 0 && southwestlng != 0){
						if ((double) result.getProperty("longitude") < northeastlng && (double) result.getProperty("longitude") > southwestlng && (double) result.getProperty("latitude") < northeastlat && (double) result.getProperty("latitude") > southwestlat) {
							LOG.warning("\n\n\nAdding one filtered");
							Filter followerFilter = new FilterPredicate("follower", FilterOperator.EQUAL, authUsername);
							Filter issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, KeyFactory.keyToString(result.getKey()));
							Query watchlistQuery = new Query("IssueFollow").setFilter(CompositeFilterOperator.and(followerFilter, issueFilter));
							List<Entity> watchlistResults = datastore.prepare(watchlistQuery).asList(FetchOptions.Builder.withDefaults());
							boolean following = watchlistResults.size() > 0;
							
							Issue issue = new Issue(KeyFactory.keyToString(result.getKey()),
									(String) result.getProperty("status"), (Date) result.getProperty("creation_date"),
									result.getParent().getName(), (String) result.getProperty("title"),
									(String) result.getProperty("description"), (double) result.getProperty("latitude"),
									(double) result.getProperty("longitude"), (String) result.getProperty("type"),
									(long)result.getProperty("rating"), following, (List<String>) result.getProperty("picture_ids"),
									(List<String>)result.getProperty("thumbnail_ids"), Collections.EMPTY_LIST);
							
							issues.add(issue);
						}
					} else {
						LOG.warning("\n\n\nAdding one");
						Filter followerFilter = new FilterPredicate("follower", FilterOperator.EQUAL, authUsername);
						Filter issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, KeyFactory.keyToString(result.getKey()));
						Query watchlistQuery = new Query("IssueFollow").setFilter(CompositeFilterOperator.and(followerFilter, issueFilter));
						List<Entity> watchlistResults = datastore.prepare(watchlistQuery).asList(FetchOptions.Builder.withDefaults());
						boolean following = watchlistResults.size() > 0;
						
						Issue issue = new Issue(KeyFactory.keyToString(result.getKey()),
								(String) result.getProperty("status"), (Date) result.getProperty("creation_date"),
								result.getParent().getName(), (String) result.getProperty("title"),
								(String) result.getProperty("description"), (double) result.getProperty("latitude"),
								(double) result.getProperty("longitude"), (String) result.getProperty("type"),
								(long)result.getProperty("rating"), following, (List<String>) result.getProperty("picture_ids"),
								(List<String>)result.getProperty("thumbnail_ids"), Collections.EMPTY_LIST);
						
						issues.add(issue);
					}
				}
				
				// Cursor
				Cursor c = results.getCursor();
				if (c != null){
					cursor = c.toWebSafeString();
				} else {
					cursor = null;
				}
				
				IssueListing ret = new IssueListing(issues, cursor);
				
				return Response.status(Status.OK).entity(g.toJson(ret)).build();
				
			} else {
				// Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} catch (Exception e){
			LOG.warning("\n\n\n\n\n\nError: " + e.getMessage());
			return Response.status(Status.NOT_IMPLEMENTED).build();
		}
	}
}