package com.codefive.cityaid.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import com.codefive.cityaid.util.AvatarFactory;
import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.User;
import com.codefive.cityaid.util.replyData.UserListing;
import com.codefive.cityaid.util.requestData.EntityData;
import com.codefive.cityaid.util.requestData.EntityWorkerData;
import com.codefive.cityaid.util.requestData.EntityWorkerRegistrationData;
import com.codefive.cityaid.util.requestData.UpdateEntityData;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gson.Gson;

@Path("/entities")
public class EntityResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(EntityResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	/**
	* This is where backoff parameters are configured. Here it is aggressively retrying with
	* backoff, up to 10 times but taking no more that 15 seconds total to do so.
	*/
	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
															.initialRetryDelayMillis(10)
															.retryMaxAttempts(10)
															.totalRetryPeriodMillis(15000)
															.build());
	
	public EntityResource() { } //Nothing to be done here...
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response registerEntity(EntityData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					Key entityKey = KeyFactory.createKey("Entity", data.nif);
					try {
						//If the entity does not exist an Exception is thrown. Otherwise,
						datastore.get(txn, entityKey);
						txn.rollback();
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_ALREADY_EXISTS))).build();
					} catch (EntityNotFoundException e) {
						//Save photo on Cloud Storage
						String photoId = "default.jpg";
						if (data.logo != null){
							try {
								photoId = uploadEntityLogo(data.nif, data.logo.base64, data.logo.extension);
							} catch (IOException e1) {
								LOG.warning("Failed to upload issue photo. Exception: "+e1.getMessage());
								
								if (txn.isActive()) {
									txn.rollback();
									return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
								}
							} catch (Exception e1) {
								LOG.warning("Something happenned. Exception: "+e1.getMessage());

								if (txn.isActive()) {
									txn.rollback();
									return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
								}
							}
						}
						
						//Create entity
						Entity entity = new Entity("Entity", data.nif);
						entity.setUnindexedProperty("name", data.name);
						entity.setUnindexedProperty("type", data.type);
						entity.setUnindexedProperty("phone", data.phone);
						entity.setUnindexedProperty("logo", photoId);
						entity.setUnindexedProperty("rating", 0);
						datastore.put(txn, entity);
						
						LOG.info("Entity registered " + data.nif);
						txn.commit();
						
						return Response.status(Status.NO_CONTENT).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required role: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@PUT
	@Path("/{entity}")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateEntity(@PathParam("entity") String entityId, UpdateEntityData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (entityId.equals((String)user.getProperty("employer")) && ((boolean)user.getProperty("isManager"))){
					//User has permission to perform operation
					txn.commit();
				
					txn = datastore.beginTransaction();
					Key entityKey = KeyFactory.createKey("Entity", entityId);
					try {
						//Update entity data
						Entity entity = datastore.get(txn, entityKey);
						entity.setUnindexedProperty("name", data.name);
						entity.setUnindexedProperty("phone", data.phone);
						datastore.put(txn, entity);
						
						LOG.info("Updated entity data" +entityId);
						txn.commit();
						
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e){
						LOG.info("Tried to update non-existing entity " + entityId);
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@POST
	@Path("/{entity}/managers")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addEntityManager(@PathParam("entity") String entityId, EntityWorkerData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) || (entityId.equals((String)user.getProperty("employer")) && ((boolean)user.getProperty("isManager")))){
					//User has permission to perform operation
					Key entityKey = KeyFactory.createKey("Entity", entityId);
					try {
						//Check entity exists
						datastore.get(txn, entityKey);
						
						Key managerKey = KeyFactory.createKey("User", data.username);
						try {
							Entity manager = datastore.get(txn, managerKey);
							if (entityId.equals((String)manager.getProperty("employer")) && ((boolean)manager.getProperty("isManager"))){
								//Worker already is an entity manager
								LOG.info("Worker ("+data.username+") already is an entity manager " + entityId);
								txn.rollback();
								
								return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.USER_ALREADY_MANAGER))).build();
							} else {
								//Update user data
								manager.setProperty("role", UserRole.WORKER.toString());
								manager.setProperty("isManager", true);
								manager.setProperty("employer", entityId);
								manager.setUnindexedProperty("job", data.job);
								datastore.put(txn, manager);
								
								txn.commit();
								LOG.info("Added entity ("+entityId+") manager " +data.username);
								
								return Response.status(Status.NO_CONTENT).build();
							}
						} catch (EntityNotFoundException e){
							LOG.info("Tried to add non-existing manager ("+data.username+") to entity " + entityId);
							txn.rollback();
							
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
						} finally {
							if (txn.isActive()) {
								txn.rollback();
								return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
							}
						}
					} catch (EntityNotFoundException e){
						LOG.info("Tried to add manager to non-existing entity " + entityId);
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/{entity}/managers/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response removeEntityManager(@PathParam("entity") String entityId, @PathParam("username") String username, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) || (entityId.equals((String)user.getProperty("employer")) && ((boolean)user.getProperty("isManager")))){
					//User has permission to perform operation
					Key entityKey = KeyFactory.createKey("Entity", entityId);
					try {
						//Check entity exists
						datastore.get(txn, entityKey);
						
						Key managerKey = KeyFactory.createKey("User", username);
						try {
							Entity manager = datastore.get(txn, managerKey);
							if (entityId.equals((String)manager.getProperty("employer")) && ((boolean)manager.getProperty("isManager"))){
								//Update user data
								manager.setProperty("isManager", false);
								datastore.put(txn, manager);
								
								txn.commit();
								LOG.info("Removed entity ("+entityId+") manager " + username);
								
								return Response.status(Status.NO_CONTENT).build();
							} else {
								//Worker isn't an entity manager
								LOG.info("Worker ("+username+") isn't an entity manager " + entityId);
								txn.rollback();
								
								return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_MANAGER))).build();
							}
						} catch (EntityNotFoundException e){
							LOG.info("Tried to remove non-existing manager ("+username+") from entity " + entityId);
							txn.rollback();
							
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
						} finally {
							if (txn.isActive()) {
								txn.rollback();
								return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
							}
						}
					} catch (EntityNotFoundException e){
						LOG.info("Tried to remove manager from non-existing entity " + entityId);
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/{entity}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewEntity(@PathParam("entity") String entityId, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				//User has permission to perform operation
				txn.commit();

				txn = datastore.beginTransaction();
				Key entityKey = KeyFactory.createKey("Entity", entityId);
				try {
					// Get entity
					Entity entityEnt = datastore.get(txn, entityKey);
					
					//Get managers
					List<Filter> filters = new ArrayList<Filter>(2);
					filters.add(new FilterPredicate("isManager", FilterOperator.EQUAL, true));
					filters.add(new FilterPredicate("employer", FilterOperator.EQUAL, entityId));
					Query query = new Query("User");
					query.setFilter(CompositeFilterOperator.and(filters));					
					List<Entity> queryResult = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());

					txn.commit();
					
					// Create response
					List<User> managers = new ArrayList<User>(queryResult.size());
					for (Entity result : queryResult) {
						User user = new User((String) result.getKey().getName(), (String) result.getProperty("role"),
							(String) result.getProperty("name"), (String) result.getProperty("email"),
							(String) result.getProperty("address"), (String) result.getProperty("phone"),
							(String) result.getProperty("gender"), (Date) result.getProperty("birth_date"),
							(Date) result.getProperty("registered_since"), (boolean) result.getProperty("isManager"),
							(String) result.getProperty("employer"), (String) result.getProperty("job"),
							AvatarFactory.getAvatarUrl((String) result.getProperty("email")),
							(boolean) result.getProperty("frozen"), 0, 1);
						
						managers.add(user);
					}
					com.codefive.cityaid.util.replyData.Entity entity = new com.codefive.cityaid.util.replyData.Entity(entityId, (String) entityEnt.getProperty("name"),
							(String) entityEnt.getProperty("type"), (String) entityEnt.getProperty("phone"),
							managers, (long)entityEnt.getProperty("rating"), (String) entityEnt.getProperty("logo"));

					return Response.status(Status.OK).entity(g.toJson(entity)).build();
				} catch (EntityNotFoundException e) {
					//Entity does not exist
					txn.rollback();

					LOG.warning("Entity does not exist: " + entityId);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewEntities(@Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				//User has permission to perform operation
				txn.commit();

				txn = datastore.beginTransaction();
				// Get entities
				Query query = new Query("Entity");
				List<Entity> queryResult = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());

				txn.commit();
				
				// Create response
				List<com.codefive.cityaid.util.replyData.Entity> entities = new ArrayList<com.codefive.cityaid.util.replyData.Entity>(queryResult.size());
				for (Entity result : queryResult) {
					com.codefive.cityaid.util.replyData.Entity entity = new com.codefive.cityaid.util.replyData.Entity(result.getKey().getName(), (String) result.getProperty("name"),
							(String) result.getProperty("type"), (String) result.getProperty("phone"),
							Collections.<User>emptyList(), (long)result.getProperty("rating"), (String) result.getProperty("logo"));
					
					entities.add(entity);
				}
				
				return Response.status(Status.OK).entity(g.toJson(entities)).build();
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@POST
	@Path("/{entity}/workers")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addEntityWorker(@PathParam("entity") String entityId, EntityWorkerData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if ((entityId.equals((String)user.getProperty("employer")) && ((boolean)user.getProperty("isManager")))){
					//User has permission to perform operation
					Key entityKey = KeyFactory.createKey("Entity", entityId);
					try {
						//Check entity exists
						datastore.get(txn, entityKey);

						Key workerKey = KeyFactory.createKey("User", data.username);
						try {
							Entity worker = datastore.get(txn, workerKey);
							if (entityId.equals((String)worker.getProperty("employer"))){
								//Worker already is an entity worker
								LOG.info("Worker ("+data.username+") already is an entity worker " + entityId);
								txn.rollback();
								
								return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.USER_ALREADY_WORKER))).build();
							} else {
								//Update user data
								worker.setProperty("role", UserRole.WORKER.toString());
								worker.setProperty("employer", entityId);
								worker.setUnindexedProperty("job", data.job);
								datastore.put(txn, worker);
								
								txn.commit();
								LOG.info("Added entity ("+entityId+") worker " +data.username);
								
								return Response.status(Status.NO_CONTENT).build();
							}
						} catch (EntityNotFoundException e){
							LOG.info("Tried to add non-existing worker ("+data.username+") to entity " + entityId);
							txn.rollback();
							
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
						} finally {
							if (txn.isActive()) {
								txn.rollback();
								return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
							}
						}
					} catch (EntityNotFoundException e){
						LOG.info("Tried to add worker to non-existing entity " + entityId);
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/{entity}/workers/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response removeEntityWorker(@PathParam("entity") String entityId, @PathParam("username") String username, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if ((entityId.equals((String)user.getProperty("employer")) && ((boolean)user.getProperty("isManager")))){
					//User has permission to perform operation
					Key entityKey = KeyFactory.createKey("Entity", entityId);
					try {
						//Check entity exists
						datastore.get(txn, entityKey);

						Key workerKey = KeyFactory.createKey("User", username);
						try {
							Entity worker = datastore.get(txn, workerKey);
							if (entityId.equals((String)worker.getProperty("employer"))){
								//Update user data
								worker.setProperty("role", UserRole.FROZEN.toString());
								worker.setProperty("isManager", false);
								worker.setProperty("employer", null);
								worker.setUnindexedProperty("job", null);
								worker.setProperty("frozen", true);
								datastore.put(txn, worker);
								
								//Delete user auth tokens
								Query query = new Query("Session").setAncestor(KeyFactory.createKey("User", username));
								List<Entity> tokens = datastore.prepare(query.setKeysOnly()).asList(FetchOptions.Builder.withDefaults());
								List<Key> tokenKeys = new ArrayList<Key>(tokens.size());
								for (Entity token : tokens) {
									tokenKeys.add(token.getKey());
								}
								datastore.delete(tokenKeys);
								
								txn.commit();
								LOG.info("Removed entity ("+entityId+") worker " + username);
								
								return Response.status(Status.NO_CONTENT).build();
							} else {
								//Worker isn't an entity worker
								LOG.info("Worker ("+username+") isn't an entity worker " + entityId);
								txn.rollback();
								
								return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_WORKER))).build();
							}
						} catch (EntityNotFoundException e){
							LOG.info("Tried to remove non-existing worker ("+username+") from entity " + entityId);
							txn.rollback();
							
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
						} finally {
							if (txn.isActive()) {
								txn.rollback();
								return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
							}
						}
					} catch (EntityNotFoundException e){
						LOG.info("Tried to remove worker from non-existing entity " + entityId);
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/{entity}/workers")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getEntityWorkers(@PathParam("entity") String entityId, @QueryParam("cursor") String cursor,
			@QueryParam("page_size") long page_size, @Context HttpHeaders headers) {
		
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Key entityKey = KeyFactory.createKey("Entity", entityId);
				
				try {
					datastore.get(entityKey);
					
					Query query = new Query("User").addSort(Entity.KEY_RESERVED_PROPERTY, SortDirection.ASCENDING);
					query.setFilter(new FilterPredicate("employer", FilterOperator.EQUAL, entityId));
					
					// Cursor
					FetchOptions fetchOptions;
					if(page_size > 0)
						fetchOptions = FetchOptions.Builder.withLimit((int) page_size);
					else
						fetchOptions = FetchOptions.Builder.withDefaults();
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> users = datastore.prepare(query).asQueryResultList(fetchOptions);
					
					// Create response
					List<User> res = new ArrayList<User>(users.size());
					
					for (Entity result : users) {
						User user = new User(result.getKey().getName(), (String) result.getProperty("role"),
							(String) result.getProperty("name"), (String) result.getProperty("email"),
							(String) result.getProperty("address"), (String) result.getProperty("phone"),
							(String) result.getProperty("gender"), (Date) result.getProperty("birth_date"),
							(Date) result.getProperty("registered_since"), (boolean) result.getProperty("isManager"),
							(String) result.getProperty("employer"), (String) result.getProperty("job"),
							AvatarFactory.getAvatarUrl((String) result.getProperty("email")),
							(boolean) result.getProperty("frozen"), 0, 1);
						
						res.add(user);
					}
					
					// Cursor
					Cursor c = users.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					UserListing ret = new UserListing(res, cursor);
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} catch (EntityNotFoundException e) {
					LOG.info("Tried fetch workers from non-existing entity " + entityId);
					
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
				}
			} else {
				// Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@POST
	@Path("/{entity}/managers/create")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response createEntityManager(@PathParam("entity") String entityId, EntityWorkerRegistrationData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) || (entityId.equals((String)user.getProperty("employer")) && ((boolean)user.getProperty("isManager")))){
					//User has permission to perform operation
					Key entityKey = KeyFactory.createKey("Entity", entityId);
					try {
						//Check entity exists
						datastore.get(txn, entityKey);
						
						//Create user
						try {
							//If the user does not exist an Exception is thrown. Otherwise,
							Key userKey = KeyFactory.createKey("User", data.username);
							datastore.get(txn, userKey);
							txn.rollback();
							return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.USER_ALREADY_EXISTS))).build();
						} catch (EntityNotFoundException e){
							String generatedPassword = UUID.randomUUID().toString();
							
							//Create manager
							Entity manager = new Entity("User", data.username);
							manager.setUnindexedProperty("password", DigestUtils.sha512Hex(generatedPassword));
							manager.setUnindexedProperty("email", data.email);
							manager.setUnindexedProperty("name", data.name);
							manager.setUnindexedProperty("phone", data.phone);
							manager.setUnindexedProperty("gender", data.gender);
							manager.setUnindexedProperty("birth_date", data.birthdate);
							manager.setProperty("role", UserRole.WORKER.toString());
							manager.setUnindexedProperty("watch_list", new ArrayList<String>());
							manager.setProperty("registered_since", new Date());
							manager.setProperty("verified", true);
							manager.setProperty("frozen", false);
							manager.setProperty("isManager", true);
							manager.setProperty("employer", entityId);
							manager.setUnindexedProperty("job", data.job);
							datastore.put(txn, manager);
							txn.commit();
							
							//Initialize options
							Entity userOptionsEnt = new Entity("UserOptions", data.username);
							userOptionsEnt.setUnindexedProperty("auto_follow", true);
							userOptionsEnt.setUnindexedProperty("push_medal", true);
							userOptionsEnt.setUnindexedProperty("push_level_up", true);
							userOptionsEnt.setUnindexedProperty("push_issue_wip", true);
							userOptionsEnt.setUnindexedProperty("push_issue_closed", true);
							userOptionsEnt.setUnindexedProperty("push_issue_solved", true);
							userOptionsEnt.setUnindexedProperty("push_log_comment", true);
							userOptionsEnt.setUnindexedProperty("push_edit", true);
							userOptionsEnt.setUnindexedProperty("push_new_photos", true);
							userOptionsEnt.setUnindexedProperty("push_comment", true);
							userOptionsEnt.setUnindexedProperty("email_medal", true);
							userOptionsEnt.setUnindexedProperty("email_level_up", true);
							userOptionsEnt.setUnindexedProperty("email_issue_wip", true);
							userOptionsEnt.setUnindexedProperty("email_issue_closed", true);
							userOptionsEnt.setUnindexedProperty("email_issue_solved", true);
							userOptionsEnt.setUnindexedProperty("email_log_comment", true);
							userOptionsEnt.setUnindexedProperty("email_edit", true);
							userOptionsEnt.setUnindexedProperty("email_new_photos", true);
							userOptionsEnt.setUnindexedProperty("email_comment", true);
							userOptionsEnt.setUnindexedProperty("mobile_cache", 5);
							datastore.put(userOptionsEnt);
							
							//Initialize stats
							Entity userGamificationStatsEnt = new Entity("UserGamificationStats", data.username);
							userGamificationStatsEnt.setUnindexedProperty("points", 0);
							userGamificationStatsEnt.setProperty("level", 1);
							userGamificationStatsEnt.setUnindexedProperty("num_awarded_medals", 0);
							datastore.put(userGamificationStatsEnt);
							Entity userIssueStatsEnt = new Entity("UserIssueStats", data.username);
							userIssueStatsEnt.setUnindexedProperty("issues_created", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_vandalism", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_garbage", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_accident", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_streetdmg", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_furnituredmg", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_nature", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_other", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_in_progress", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_solved", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_closed", 0);
							datastore.put(userIssueStatsEnt);
							Entity userCommentStatsEnt = new Entity("UserCommentStats", data.username);
							userCommentStatsEnt.setUnindexedProperty("issues_commented", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_vandalism", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_garbage", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_accident", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_streetdmg", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_furnituredmg", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_nature", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_other", 0);
							datastore.put(userCommentStatsEnt);
							Entity userFollowStatsEnt = new Entity("UserFollowStats", data.username);
							userFollowStatsEnt.setUnindexedProperty("issues_followed", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_vandalism", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_garbage", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_accident", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_streetdmg", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_furnituredmg", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_nature", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_other", 0);
							datastore.put(userFollowStatsEnt);
							Entity userRateStatsEnt = new Entity("UserRateStats", data.username);
							userRateStatsEnt.setUnindexedProperty("issues_rated", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_vandalism", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_garbage", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_accident", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_streetdmg", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_furnituredmg", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_nature", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_other", 0);
							datastore.put(userRateStatsEnt);
							
							//Create user log entry
							Entity userLogEntry = new Entity("UserLog", manager.getKey());
							userLogEntry.setProperty("date", new Date());
							userLogEntry.setUnindexedProperty("type", UserLogType.REGISTERED.toString());
							userLogEntry.setUnindexedProperty("description", "Conta registada (Gestor de Entidade)");
							userLogEntry.setUnindexedProperty("remote_addr", "");
							userLogEntry.setUnindexedProperty("remote_host", "");
							userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
							userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
							userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
							datastore.put(userLogEntry);
							
							LOG.info("User registered " + data.username);
							
							//Send verification email
							Properties props = new Properties();
							Session emailSession = Session.getDefaultInstance(props, null);

							try {
								Message msg = new MimeMessage(emailSession);
								msg.setFrom(new InternetAddress("noreply@city-aid.appspotmail.com", "CityAid"));
								msg.addRecipient(Message.RecipientType.TO, new InternetAddress(data.email, data.name));
								msg.setSubject(MimeUtility.encodeText("Credenciais da sua conta CityAid", "UTF-8", "Q"));

								String htmlBody = "Foi criada uma conta de gestor da entidade \""+entityId+"\".<br><b>Nome de utilizador:</b> "+data.username+"<br><b>Password:</b> "+generatedPassword+"<br><br>Se não criou uma conta CityAid, ignore esta mensagem."+NotificationResource.getEmailSignature();
								Multipart mp = new MimeMultipart();

								MimeBodyPart htmlPart = new MimeBodyPart();
								htmlPart.setContent(htmlBody, "text/html");
								mp.addBodyPart(htmlPart);

								msg.setContent(mp);

								Transport.send(msg);
								
								LOG.fine("Account credentials email sent to "+data.email);
							} catch (AddressException e1) {
								LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
							} catch (MessagingException e1) {
								LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
							} catch (UnsupportedEncodingException e1) {
								LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
							} catch (Exception e1) {
								LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
							}
							
							return Response.status(Status.NO_CONTENT).build();
						} finally {
							if (txn.isActive()) {
								txn.rollback();
								return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
							}
						}
					} catch (EntityNotFoundException e){
						LOG.info("Tried to create manager of non-existing entity " + entityId);
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@POST
	@Path("/{entity}/workers/create")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response createEntityWorker(@PathParam("entity") String entityId, EntityWorkerRegistrationData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if ((entityId.equals((String)user.getProperty("employer")) && ((boolean)user.getProperty("isManager")))){
					//User has permission to perform operation
					Key entityKey = KeyFactory.createKey("Entity", entityId);
					try {
						//Check entity exists
						datastore.get(txn, entityKey);
						
						//Create user
						try {
							//If the user does not exist an Exception is thrown. Otherwise,
							Key userKey = KeyFactory.createKey("User", data.username);
							datastore.get(txn, userKey);
							txn.rollback();
							return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.USER_ALREADY_EXISTS))).build();
						} catch (EntityNotFoundException e){
							String generatedPassword = UUID.randomUUID().toString();
							
							//Create worker
							Entity worker = new Entity("User", data.username);
							worker.setUnindexedProperty("password", DigestUtils.sha512Hex(generatedPassword));
							worker.setUnindexedProperty("email", data.email);
							worker.setUnindexedProperty("name", data.name);
							worker.setUnindexedProperty("phone", data.phone);
							worker.setUnindexedProperty("gender", data.gender);
							worker.setUnindexedProperty("birth_date", data.birthdate);
							worker.setProperty("role", UserRole.WORKER.toString());
							worker.setUnindexedProperty("watch_list", new ArrayList<String>());
							worker.setProperty("registered_since", new Date());
							worker.setProperty("verified", true);
							worker.setProperty("frozen", false);
							worker.setProperty("isManager", false);
							worker.setProperty("employer", entityId);
							worker.setUnindexedProperty("job", data.job);
							datastore.put(txn, worker);
							txn.commit();
							
							//Initialize options
							Entity userOptionsEnt = new Entity("UserOptions", data.username);
							userOptionsEnt.setUnindexedProperty("auto_follow", true);
							userOptionsEnt.setUnindexedProperty("push_medal", true);
							userOptionsEnt.setUnindexedProperty("push_level_up", true);
							userOptionsEnt.setUnindexedProperty("push_issue_wip", true);
							userOptionsEnt.setUnindexedProperty("push_issue_closed", true);
							userOptionsEnt.setUnindexedProperty("push_issue_solved", true);
							userOptionsEnt.setUnindexedProperty("push_log_comment", true);
							userOptionsEnt.setUnindexedProperty("push_edit", true);
							userOptionsEnt.setUnindexedProperty("push_new_photos", true);
							userOptionsEnt.setUnindexedProperty("push_comment", true);
							userOptionsEnt.setUnindexedProperty("email_medal", true);
							userOptionsEnt.setUnindexedProperty("email_level_up", true);
							userOptionsEnt.setUnindexedProperty("email_issue_wip", true);
							userOptionsEnt.setUnindexedProperty("email_issue_closed", true);
							userOptionsEnt.setUnindexedProperty("email_issue_solved", true);
							userOptionsEnt.setUnindexedProperty("email_log_comment", true);
							userOptionsEnt.setUnindexedProperty("email_edit", true);
							userOptionsEnt.setUnindexedProperty("email_new_photos", true);
							userOptionsEnt.setUnindexedProperty("email_comment", true);
							userOptionsEnt.setUnindexedProperty("mobile_cache", 5);
							datastore.put(userOptionsEnt);
							
							//Initialize stats
							Entity userGamificationStatsEnt = new Entity("UserGamificationStats", data.username);
							userGamificationStatsEnt.setUnindexedProperty("points", 0);
							userGamificationStatsEnt.setProperty("level", 1);
							userGamificationStatsEnt.setUnindexedProperty("num_awarded_medals", 0);
							datastore.put(userGamificationStatsEnt);
							Entity userIssueStatsEnt = new Entity("UserIssueStats", data.username);
							userIssueStatsEnt.setUnindexedProperty("issues_created", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_vandalism", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_garbage", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_accident", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_streetdmg", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_furnituredmg", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_nature", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_other", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_in_progress", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_solved", 0);
							userIssueStatsEnt.setUnindexedProperty("issues_closed", 0);
							datastore.put(userIssueStatsEnt);
							Entity userCommentStatsEnt = new Entity("UserCommentStats", data.username);
							userCommentStatsEnt.setUnindexedProperty("issues_commented", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_vandalism", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_garbage", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_accident", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_streetdmg", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_furnituredmg", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_nature", 0);
							userCommentStatsEnt.setUnindexedProperty("issues_commented_type_other", 0);
							datastore.put(userCommentStatsEnt);
							Entity userFollowStatsEnt = new Entity("UserFollowStats", data.username);
							userFollowStatsEnt.setUnindexedProperty("issues_followed", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_vandalism", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_garbage", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_accident", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_streetdmg", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_furnituredmg", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_nature", 0);
							userFollowStatsEnt.setUnindexedProperty("issues_followed_type_other", 0);
							datastore.put(userFollowStatsEnt);
							Entity userRateStatsEnt = new Entity("UserRateStats", data.username);
							userRateStatsEnt.setUnindexedProperty("issues_rated", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_vandalism", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_garbage", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_accident", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_streetdmg", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_furnituredmg", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_nature", 0);
							userRateStatsEnt.setUnindexedProperty("issues_rated_type_other", 0);
							datastore.put(userRateStatsEnt);
							
							//Create user log entry
							Entity userLogEntry = new Entity("UserLog", worker.getKey());
							userLogEntry.setProperty("date", new Date());
							userLogEntry.setUnindexedProperty("type", UserLogType.REGISTERED.toString());
							userLogEntry.setUnindexedProperty("description", "Conta registada (Trabalhador de Entidade)");
							userLogEntry.setUnindexedProperty("remote_addr", "");
							userLogEntry.setUnindexedProperty("remote_host", "");
							userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
							userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
							userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
							datastore.put(userLogEntry);
							
							LOG.info("User registered " + data.username);
							
							//Send verification email
							Properties props = new Properties();
							Session emailSession = Session.getDefaultInstance(props, null);

							try {
								Message msg = new MimeMessage(emailSession);
								msg.setFrom(new InternetAddress("noreply@city-aid.appspotmail.com", "CityAid"));
								msg.addRecipient(Message.RecipientType.TO, new InternetAddress(data.email, data.name));
								msg.setSubject(MimeUtility.encodeText("Credenciais da sua conta CityAid", "UTF-8", "Q"));

								String htmlBody = "Foi criada uma conta de trabalhador da entidade \""+entityId+"\".<br><b>Nome de utilizador:</b> "+data.username+"<br><b>Password:</b> "+generatedPassword+"<br><br>Se não criou uma conta CityAid, ignore esta mensagem."+NotificationResource.getEmailSignature();
								Multipart mp = new MimeMultipart();

								MimeBodyPart htmlPart = new MimeBodyPart();
								htmlPart.setContent(htmlBody, "text/html");
								mp.addBodyPart(htmlPart);

								msg.setContent(mp);

								Transport.send(msg);
								
								LOG.fine("Account credentials email sent to "+data.email);
							} catch (AddressException e1) {
								LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
							} catch (MessagingException e1) {
								LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
							} catch (UnsupportedEncodingException e1) {
								LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
							} catch (Exception e1) {
								LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
							}
							
							return Response.status(Status.NO_CONTENT).build();
						} finally {
							if (txn.isActive()) {
								txn.rollback();
								return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
							}
						}
					} catch (EntityNotFoundException e){
						LOG.info("Tried to create worker of non-existing entity " + entityId);
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	/**
	 * /**
	 * Upload entity logo to Google Cloud Storage
	 * @param entity		nif of entity
	 * @param base64Photo	photo data enconded in base 64
	 * @param extension		photo file extension
	 * @return photoName	filename in Cloud Storage
	 * @throws IOException 
	 */
	private String uploadEntityLogo(String entity, String base64Photo, String extension) throws IOException{
		//Generate photo name
		String photoName = "entity_"+entity+"_logo."+extension;
		
		//Decode base64 photo
		byte[] binPhoto = Base64.decodeBase64(base64Photo);
	    
		//Upload to Cloud Storage
		GcsFileOptions instance = new GcsFileOptions.Builder().mimeType("image/"+extension).build();
	    GcsFilename fileName = new GcsFilename("city-aid.appspot.com", photoName);
	    GcsOutputChannel outputChannel = gcsService.createOrReplace(fileName, instance);
	    OutputStream output = Channels.newOutputStream(outputChannel);
		try {
			output.write(binPhoto, 0, binPhoto.length);
		} finally {
			output.close();
		}
		
		return photoName;
	}
}
