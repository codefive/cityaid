package com.codefive.cityaid.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.replyData.AppStats;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.UserStats;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;

@Path("/stats")
public class StatsResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(StatsResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public StatsResource() { } //Nothing to be done here...
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAppStats() {
		Transaction txn = datastore.beginTransaction();
		Key appStatsKey = KeyFactory.createKey("Stats", "_APP_");
		try {
			//Get stats
			Entity statsEnt = datastore.get(txn, appStatsKey);
			txn.commit();
			
			//Create response
			AppStats stats = new AppStats((long)statsEnt.getProperty("users"),
					(long)statsEnt.getProperty("reporters"),
					(long)statsEnt.getProperty("backoffice"),
					(long)statsEnt.getProperty("workers"),
					(long)statsEnt.getProperty("activeUsers"),
					(long)statsEnt.getProperty("issues"),
					(long)statsEnt.getProperty("issuesCreated"),
					(long)statsEnt.getProperty("issuesProgress"),
					(long)statsEnt.getProperty("issuesSolved"),
					(long)statsEnt.getProperty("issuesClosed"),
					(long)statsEnt.getProperty("entities"),
					(long)statsEnt.getProperty("moderators"));
			
			return Response.status(Status.OK).entity(g.toJson(stats)).build();
		} catch (EntityNotFoundException e) {
			// App stats does not exist. Generate stats entry
			Entity appStats = new Entity("Stats", "_APP_");
			appStats.setUnindexedProperty("users", 0);
			appStats.setUnindexedProperty("reporters", 0);
			appStats.setUnindexedProperty("backoffice", 0);
			appStats.setUnindexedProperty("workers", 0);
			appStats.setUnindexedProperty("activeUsers", 0);
			appStats.setUnindexedProperty("issues", 0);
			appStats.setUnindexedProperty("issuesCreated", 0);
			appStats.setUnindexedProperty("issuesProgress", 0);
			appStats.setUnindexedProperty("issuesSolved", 0);
			appStats.setUnindexedProperty("issuesClosed", 0);
			appStats.setUnindexedProperty("entities", 0);
			appStats.setUnindexedProperty("moderators", 0);
			datastore.put(txn, appStats);
			txn.commit();
			
			LOG.warning("App stats did not exist. Generated default as 0");
			
			//Create response
			AppStats stats = new AppStats(0,0,0,0,0,0,0,0,0,0,0,0);
			
			return Response.status(Status.OK).entity(g.toJson(stats)).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	
	@GET
	@Path("/users/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getUserStats(@PathParam("username") String username, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				// User has permission to perform operation
				txn.commit();

				Key userKey = KeyFactory.createKey("User", username);
				try {
					// Get user
					datastore.get(userKey);

					Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", username);
					Entity userGamificationStatsEnt = datastore.get(userGamificationStatsKey);
					Key userIssueStatsKey = KeyFactory.createKey("UserIssueStats", username);
					Entity userIssueStatsEnt = datastore.get(userIssueStatsKey);
					Key userCommentStatsKey = KeyFactory.createKey("UserCommentStats", username);
					Entity userCommentStatsEnt = datastore.get(userCommentStatsKey);
					Key userFollowStatsKey = KeyFactory.createKey("UserFollowStats", username);
					Entity userFollowStatsEnt = datastore.get(userFollowStatsKey);
					Key userRateStatsKey = KeyFactory.createKey("UserRateStats", username);
					Entity userRateStatsEnt = datastore.get(userRateStatsKey);
					
					UserStats stat = new UserStats((long) userGamificationStatsEnt.getProperty("points"), (long) userGamificationStatsEnt.getProperty("level"), (long) userGamificationStatsEnt.getProperty("num_awarded_medals"), (long) userIssueStatsEnt.getProperty("issues_created"), (long) userIssueStatsEnt.getProperty("issues_created_type_vandalism"), (long) userIssueStatsEnt.getProperty("issues_created_type_garbage"), (long) userIssueStatsEnt.getProperty("issues_created_type_accident"), (long) userIssueStatsEnt.getProperty("issues_created_type_streetdmg"), (long) userIssueStatsEnt.getProperty("issues_created_type_furnituredmg"), (long) userIssueStatsEnt.getProperty("issues_created_type_nature"), (long) userIssueStatsEnt.getProperty("issues_created_type_other"), (long) userIssueStatsEnt.getProperty("issues_in_progress"), (long) userIssueStatsEnt.getProperty("issues_solved"), (long) userIssueStatsEnt.getProperty("issues_closed"),
							(long) userCommentStatsEnt.getProperty("issues_commented"), (long) userCommentStatsEnt.getProperty("issues_commented_type_vandalism"), (long) userCommentStatsEnt.getProperty("issues_commented_type_garbage"), (long) userCommentStatsEnt.getProperty("issues_commented_type_accident"), (long) userCommentStatsEnt.getProperty("issues_commented_type_streetdmg"), (long) userCommentStatsEnt.getProperty("issues_commented_type_furnituredmg"), (long) userCommentStatsEnt.getProperty("issues_commented_type_nature"), (long) userCommentStatsEnt.getProperty("issues_commented_type_other"),
							(long) userFollowStatsEnt.getProperty("issues_followed"), (long) userFollowStatsEnt.getProperty("issues_followed_type_vandalism"), (long) userFollowStatsEnt.getProperty("issues_followed_type_garbage"), (long) userFollowStatsEnt.getProperty("issues_followed_type_accident"), (long) userFollowStatsEnt.getProperty("issues_followed_type_streetdmg"), (long) userFollowStatsEnt.getProperty("issues_followed_type_furnituredmg"), (long) userFollowStatsEnt.getProperty("issues_followed_type_nature"), (long) userFollowStatsEnt.getProperty("issues_followed_type_other"),
							(long) userRateStatsEnt.getProperty("issues_rated"), (long) userRateStatsEnt.getProperty("issues_rated_type_vandalism"), (long) userRateStatsEnt.getProperty("issues_rated_type_garbage"), (long) userRateStatsEnt.getProperty("issues_rated_type_accident"), (long) userRateStatsEnt.getProperty("issues_rated_type_streetdmg"), (long) userRateStatsEnt.getProperty("issues_rated_type_furnituredmg"), (long) userRateStatsEnt.getProperty("issues_rated_type_nature"), (long) userRateStatsEnt.getProperty("issues_rated_type_other"));
					return Response.status(Status.OK).entity(g.toJson(stat)).build();
					
				} catch (EntityNotFoundException e) {
					// User does not exist
					LOG.warning("User does not exist: " + username);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	} 
}
