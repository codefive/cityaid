package com.codefive.cityaid.resources;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.codefive.cityaid.util.AvatarFactory;
import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.IssueType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.Issue;
import com.codefive.cityaid.util.replyData.IssueListing;
import com.codefive.cityaid.util.replyData.Medal;
import com.codefive.cityaid.util.replyData.MedalListing;
import com.codefive.cityaid.util.replyData.User;
import com.codefive.cityaid.util.replyData.UserLogEntry;
import com.codefive.cityaid.util.replyData.UserLogListing;
import com.codefive.cityaid.util.requestData.IssueID;
import com.codefive.cityaid.util.requestData.PasswordData;
import com.codefive.cityaid.util.requestData.UserData;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.gson.Gson;

@Path("/users")
public class UserResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(UserResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	private static final long PWD_RESET_EXPIRATION_TIME = 60*60*20; //20h
	private static final int PAGE_SIZE = 5;

	public UserResource() {
	} // Nothing to be done here...
	
	@GET
	@Path("/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response fetchUserData(@PathParam("username") String username, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				//User has permission to perform operation
				
				datastore.get(txn, session.getParent());
				//User has permission to perform operation
				txn.commit();

				Key userKey = KeyFactory.createKey("User", username);
				try {
					// Get user
					Entity userEnt = datastore.get(userKey);
					Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", username);
					Entity userGamificationStatsEnt = datastore.get(userGamificationStatsKey);

					// Create response
					User user = new User(username, (String) userEnt.getProperty("role"),
							(String) userEnt.getProperty("name"), (String) userEnt.getProperty("email"),
							(String) userEnt.getProperty("address"), (String) userEnt.getProperty("phone"),
							(String) userEnt.getProperty("gender"), (Date) userEnt.getProperty("birth_date"),
							(Date) userEnt.getProperty("registered_since"), (boolean) userEnt.getProperty("isManager"),
							(String) userEnt.getProperty("employer"), (String) userEnt.getProperty("job"),
							AvatarFactory.getAvatarUrl((String) userEnt.getProperty("email")), (boolean) userEnt.getProperty("frozen"),
							(long) userGamificationStatsEnt.getProperty("points"), (long) userGamificationStatsEnt.getProperty("level"));

					return Response.status(Status.OK).entity(g.toJson(user)).build();
				} catch (EntityNotFoundException e) {
					// User does not exist

					LOG.warning("User does not exist: " + username);
					return Response.status(Status.NOT_FOUND)
							.entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}

	@PUT
	@Path("/{username}")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateUser(@PathParam("username") String username, UserData data, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		if (data == null || !data.valid()) {
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM)))
					.build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				if (authUsername.equals(username)) {
					// User has permission to perform operation
					Key userKey = KeyFactory.createKey("User", username);
					try {
						// Get user
						Entity userEnt = datastore.get(txn, userKey);

						// Update user
						userEnt.setUnindexedProperty("name", data.name);
						userEnt.setUnindexedProperty("email", data.email);
						userEnt.setUnindexedProperty("address", data.address);
						userEnt.setUnindexedProperty("phone", data.phone);
						userEnt.setUnindexedProperty("gender", data.gender);
						userEnt.setUnindexedProperty("birth_date", data.birthdate);
						datastore.put(txn, userEnt);

						txn.commit();

						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// User does not exist
						txn.rollback();

						LOG.warning("User does not exist: " + username);
						return Response.status(Status.NOT_FOUND)
								.entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR)
									.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					// User does not have permission
					txn.rollback();

					LOG.warning("User attempted to perform operation without permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}

	@GET
	@Path("/{username}/log")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewUserLog(@PathParam("username") String username, @QueryParam("cursor") String cursor,
			@Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if(authUsername.equals(session.getParent().getName())) {
				datastore.get(txn, session.getParent());
				//User has permission to perform operation
				txn.commit();
				
				Key userKey = KeyFactory.createKey("User", username);
				try {
					// Get user
					datastore.get(userKey);

					// Retrieve user log entries
					Query query = new Query("UserLog").setAncestor(userKey).addSort("date", SortDirection.DESCENDING);
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> results = datastore.prepare(query).asQueryResultList(fetchOptions);
					
					List<UserLogEntry> entries = new ArrayList<UserLogEntry>(results.size());
					for (Entity entry : results) {
						entries.add(new UserLogEntry(KeyFactory.keyToString(entry.getKey()),
								(Date) entry.getProperty("date"), (String) entry.getProperty("type"),
								(String) entry.getProperty("description"), (String) entry.getProperty("remote_addr"),
								(String) entry.getProperty("remote_host"), (String) entry.getProperty("X-AppEngine-City"),
								(String) entry.getProperty("X-AppEngine-CityLatLong"),
								(String) entry.getProperty("X-AppEngine-Country")));
					}
					
					// Cursor
					Cursor c = results.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					UserLogListing ret = new UserLogListing(entries, cursor);
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} catch (EntityNotFoundException e) {
					// User does not exist

					LOG.warning("User does not exist: " + username);
					return Response.status(Status.NOT_FOUND)
							.entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}

	@GET
	@Path("/{username}/verify")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response verifyAccount(@PathParam("username") String username, @QueryParam("token") String verificationToken,
			@Context HttpServletRequest request, @Context HttpHeaders headers) {
		if (verificationToken == null || verificationToken.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM)))
					.build();
		}

		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key verificationKey = KeyFactory.createKey("Verification", verificationToken);
		try {
			// Get verification entry
			Entity verification = datastore.get(txn, verificationKey);

			// Get user
			Key userKey = KeyFactory.createKey("User", (String) verification.getProperty("username"));
			Entity user = datastore.get(txn, userKey);

			boolean verifiedAccount = (boolean) user.getProperty("verified");
			if (!verifiedAccount) {
				// Account hasn't been verified yet

				// Update verified
				user.setProperty("verified", true);
				datastore.put(txn, user);

				// Create user log entry
				Entity userLogEntry = new Entity("UserLog", userKey);
				userLogEntry.setProperty("date", new Date());
				userLogEntry.setUnindexedProperty("type", UserLogType.VERIFIED.toString());
				userLogEntry.setUnindexedProperty("description", "Conta verificada");
				userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
				userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
				userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong",
						headers.getHeaderString("X-AppEngine-CityLatLong"));
				userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
				userLogEntry.setUnindexedProperty("X-AppEngine-Country",
						headers.getHeaderString("X-AppEngine-Country"));
				datastore.put(txn, userLogEntry);
				
				//Remove verification token
				datastore.delete(txn, verificationKey);

				LOG.fine("Verified account " + username);
			}

			txn.commit();

			return Response.status(Status.NO_CONTENT).build();
		} catch (EntityNotFoundException e) {
			// User does not exist
			txn.rollback();

			LOG.warning("Tried to verify non-existing account" + username);
			return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}

	@GET
	@Path("/{username}/password/reset")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response askForReset(@PathParam("username") String username, @Context HttpHeaders headers) {
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		try {
			// Get user
			Key userKey = KeyFactory.createKey("User", username);
			Entity user = datastore.get(txn, userKey);

			// Create password reset token
			String resetToken = UUID.randomUUID().toString();
			Entity resetTokenEnt = new Entity("ResetToken", resetToken);
			resetTokenEnt.setUnindexedProperty("username", username);
			resetTokenEnt.setProperty("expiration", (System.currentTimeMillis()/1000) + PWD_RESET_EXPIRATION_TIME);
			datastore.put(txn, resetTokenEnt);

			LOG.info("Password reset requested for user: " + username);
			txn.commit();

			// Send email
			// Send verification email
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);

			String userEmail = (String) user.getProperty("email");
			try {
				Message msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress("noreply@city-aid.appspotmail.com", "CityAid"));
				msg.addRecipient(Message.RecipientType.TO,
						new InternetAddress(userEmail, (String) user.getProperty("name")));
				msg.setSubject("Reset de password de conta CityAid");

				String htmlBody = "Fa�a reset da password da sua conta CityAid <a href=\"https://city-aid.appspot.com/reset.html?user="
						+ username + "&token=" + resetToken
						+ "\">visitando esta p�gina</a>.<br>Se n�o pediu um reset de password, ignore esta mensagem.<br><br>Code Five � 2017";
				Multipart mp = new MimeMultipart();

				MimeBodyPart htmlPart = new MimeBodyPart();
				htmlPart.setContent(htmlBody, "text/html");
				mp.addBodyPart(htmlPart);

				msg.setContent(mp);

				Transport.send(msg);

				LOG.fine("Account confirmation email sent to " + userEmail);
			} catch (AddressException e1) {
				LOG.warning("Account confirmation email not sent to " + userEmail + "\nException: " + e1.getMessage());
			} catch (MessagingException e1) {
				LOG.warning("Account confirmation email not sent to " + userEmail + "\nException: " + e1.getMessage());
			} catch (UnsupportedEncodingException e1) {
				LOG.warning("Account confirmation email not sent to " + userEmail + "\nException: " + e1.getMessage());
			} catch (Exception e1) {
				LOG.warning("Account confirmation email not sent to " + userEmail + "\nException: " + e1.getMessage());
			}

			return Response.status(Status.NO_CONTENT).build();
		} catch (EntityNotFoundException e) {
			// User does not exist
			txn.rollback();

			LOG.warning("Tried to request password reset of non-existing account" + username);
			return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}

	@PUT
	@Path("/{username}/password")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updatePassword(@PathParam("username") String username, PasswordData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		if (data == null || !data.valid()) {
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}

		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		try {
			// Get user
			Key userKey = KeyFactory.createKey("User", username);
			Entity user = datastore.get(txn, userKey);

			if (data.resetToken != null && !data.resetToken.isEmpty()) {
				// Reset password
				try {
					// Get reset token
					Key resetTokenKey = KeyFactory.createKey("ResetToken", data.resetToken);
					Entity resetTokenEnt = datastore.get(txn, resetTokenKey);

					if (username.equals((String) resetTokenEnt.getProperty("username"))) {
						// Reset token exists and matches given user

						// Update password
						user.setUnindexedProperty("password", DigestUtils.sha512Hex(data.newPassword.trim()));
						datastore.put(txn, user);

						// Create user log entry
						Entity userLogEntry = new Entity("UserLog", user.getKey());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.PASSWORD_UPDATE.toString());
						userLogEntry.setUnindexedProperty("description", "Password alterada");
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong",
								headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City",
								headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country",
								headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(txn, userLogEntry);

						// Delete reset token
						datastore.delete(txn, resetTokenKey);

						txn.commit();

						LOG.fine("Updated password for user: " + username);
						return Response.status(Status.NO_CONTENT).build();
					} else {
						// Reset token does not match given user
						txn.rollback();

						LOG.warning("Tried to reset password with another user reset token: " + username);
						return Response.status(Status.FORBIDDEN)
								.entity(g.toJson(new ErrorInfo(ErrorType.INVALID_RESET_TOKEN))).build();
					}
				} catch (EntityNotFoundException e) {
					// Reset token does not exist
					txn.rollback();

					LOG.warning("Tried to reset password with expired/non-existing reset token: " + username);
					return Response.status(Status.FORBIDDEN)
							.entity(g.toJson(new ErrorInfo(ErrorType.INVALID_RESET_TOKEN))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR)
								.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else if (data.oldPassword != null && !data.oldPassword.isEmpty()){
				// Change password

				String hashedPassword = (String) user.getProperty("password");
				if (hashedPassword.equals(DigestUtils.sha512Hex(data.oldPassword.trim()))) {
					// Old password matches record

					// Update password
					user.setUnindexedProperty("password", DigestUtils.sha512Hex(data.newPassword.trim()));
					datastore.put(txn, user);

					// Create user log entry
					Entity userLogEntry = new Entity("UserLog", user.getKey());
					userLogEntry.setProperty("date", new Date());
					userLogEntry.setUnindexedProperty("type", UserLogType.PASSWORD_UPDATE.toString());
					userLogEntry.setUnindexedProperty("description", "Password alterada");
					userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
					userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
					userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong",
							headers.getHeaderString("X-AppEngine-CityLatLong"));
					userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
					userLogEntry.setUnindexedProperty("X-AppEngine-Country",
							headers.getHeaderString("X-AppEngine-Country"));
					datastore.put(txn, userLogEntry);

					txn.commit();

					LOG.fine("Updated password for user: " + username);
					return Response.status(Status.NO_CONTENT).build();
				} else {
					// Incorrect password
					txn.rollback();

					LOG.warning("Wrong old password for user: " + username);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.WRONG_PWD))).build();
				}
			} else {
				return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
			}
		} catch (EntityNotFoundException e) {
			// User does not exist
			txn.rollback();

			LOG.warning("Tried to change/reset password of non-existing account" + username);
			return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}

	@GET
	@Path("/{username}/watchlist")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewWatchlist(@PathParam("username") String username, @QueryParam("cursor") String cursor,
			@QueryParam("page_size") long page_size, @Context HttpHeaders headers) {

		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				// Get user
				Key userKey = KeyFactory.createKey("User", username);
				try {
					datastore.get(userKey);
					
					//Get followed issues
					Filter followerFilter = new FilterPredicate("follower", FilterOperator.EQUAL, username);
					Query watchlistQuery = new Query("IssueFollow").setFilter(followerFilter);
					watchlistQuery.setFilter(new FilterPredicate("hidden", FilterOperator.EQUAL, false));
					
					// Cursor
					FetchOptions fetchOptions;
					if(page_size > 0)
						fetchOptions = FetchOptions.Builder.withLimit((int) page_size);
					else
						fetchOptions = FetchOptions.Builder.withDefaults();
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> results = datastore.prepare(watchlistQuery).asQueryResultList(fetchOptions);
					
					List<Issue> watchlist = new ArrayList<Issue>(results.size());
					for (Entity result : results) {
						
						Key issueKey = KeyFactory.stringToKey((String) result.getProperty("issue"));
						Entity issueEnt = datastore.get(issueKey);
						@SuppressWarnings("unchecked")
						Issue issue = new Issue(KeyFactory.keyToString(issueEnt.getKey()),
								(String) issueEnt.getProperty("status"), (Date) issueEnt.getProperty("creation_date"),
								issueEnt.getParent().getName(), (String) issueEnt.getProperty("title"),
								(String) issueEnt.getProperty("description"), (double) issueEnt.getProperty("longitude"),
								(double) issueEnt.getProperty("longitude"), (String) issueEnt.getProperty("type"),
								(long) issueEnt.getProperty("rating"), true, (List<String>) issueEnt.getProperty("picture_ids"),
								(List<String>)result.getProperty("thumbnail_ids"), Collections.EMPTY_LIST);
						watchlist.add(issue);
					}
					
					//Watchlist retrieved
					
					Cursor c = results.getCursor();
					if (c != null)
						cursor = c.toWebSafeString();
					else cursor = null;
					
					IssueListing ret = new IssueListing(watchlist, cursor);

					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} catch (EntityNotFoundException e) {
					// User does not exist
					LOG.warning("User does not exist: " + username);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS)))
							.build();
				}
			} else {
				// Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@POST
	@Path("/{username}/watchlist")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response follow(@PathParam("username") String username, IssueID data, @Context HttpServletRequest request, @Context HttpHeaders headers) {

		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (!data.valid()) {
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				// Get user
				Key userKey = KeyFactory.createKey("User", username);
				try {
					datastore.get(txn, userKey);
					//Get issue
					Key issueKey = KeyFactory.stringToKey(data.issue);
					try {
						Entity issueEnt = datastore.get(txn, issueKey);
						
						//Check if user isn't already following issue
						Filter followerFilter = new FilterPredicate("follower", FilterOperator.EQUAL, username);
						Filter issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, data.issue);
						Query watchlistQuery = new Query("IssueFollow").setFilter(CompositeFilterOperator.and(followerFilter, issueFilter));
						List<Entity> results = datastore.prepare(watchlistQuery).asList(FetchOptions.Builder.withDefaults());

						if (results.size() > 0){
							//User already following issue
							txn.rollback();
							
							LOG.warning("User already followed issue: " + username);
							return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.ALREADY_FOLLOWED))).build();
						}
						
						//Follow issue
						Entity follow = new Entity("IssueFollow");
						follow.setProperty("follower", username);
						follow.setProperty("issue", data.issue);
						follow.setProperty("hidden", false);
						datastore.put(txn, follow);
						
						//Get user stats
						Key userFollowStatsKey = KeyFactory.createKey("UserFollowStats", authUsername);
						Entity userFollowStatsEnt = datastore.get(userFollowStatsKey);
						
						//Update user stats
						userFollowStatsEnt.setUnindexedProperty("issues_followed", ((long)userFollowStatsEnt.getProperty("issues_followed"))+1);
						switch(IssueType.valueOf(((String)issueEnt.getProperty("type")).toUpperCase())){
							case ACCIDENT:
								userFollowStatsEnt.setUnindexedProperty("issues_followed_type_accident", ((long)userFollowStatsEnt.getProperty("issues_followed_type_accident"))+1);
								break;
							case FURNITURE_DAMAGE:
								userFollowStatsEnt.setUnindexedProperty("issues_followed_type_furnituredmg", ((long)userFollowStatsEnt.getProperty("issues_followed_type_furnituredmg"))+1);
								break;
							case GARBAGE:
								userFollowStatsEnt.setUnindexedProperty("issues_followed_type_garbage", ((long)userFollowStatsEnt.getProperty("issues_followed_type_garbage"))+1);
								break;
							case NATURE:
								userFollowStatsEnt.setUnindexedProperty("issues_followed_type_nature", ((long)userFollowStatsEnt.getProperty("issues_followed_type_nature"))+1);
								break;
							case OTHER:
								userFollowStatsEnt.setUnindexedProperty("issues_followed_type_other", ((long)userFollowStatsEnt.getProperty("issues_followed_type_other"))+1);
								break;
							case STREET_DAMAGE:
								userFollowStatsEnt.setUnindexedProperty("issues_followed_type_streetdmg", ((long)userFollowStatsEnt.getProperty("issues_followed_type_streetdmg"))+1);
								break;
							case VANDALISM:
								userFollowStatsEnt.setUnindexedProperty("issues_followed_type_vandalism", ((long)userFollowStatsEnt.getProperty("issues_followed_type_vandalism"))+1);
								break;
							default:
								break;					
						}
						datastore.put(txn, userFollowStatsEnt);
						
						//Issue followed
						txn.commit();
						
						//Create user log entry
						Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.FOLLOWED.toString());
						userLogEntry.setUnindexedProperty("description", "Seguiu a ocorr�ncia \""+(String)issueEnt.getProperty("title")+"\"");
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);
						
						//Update gamification system
						Queue queue = QueueFactory.getDefaultQueue();
						queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+authUsername));
						
						LOG.fine(username+" followed issue "+data.issue);
						
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						//Issue does not exist
						txn.rollback();

						LOG.warning("Issue does not exist: " + data.issue);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
					}
				} catch (EntityNotFoundException e) {
					// User does not exist
					txn.rollback();

					LOG.warning("User does not exist: " + username);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS)))
							.build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/{username}/watchlist/{issue}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response unfollow(@PathParam("username") String username, @PathParam("issue") String issueKeyStr, @Context HttpServletRequest request, @Context HttpHeaders headers) {

		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				// Get user
				Key userKey = KeyFactory.createKey("User", username);
				try {
					datastore.get(txn, userKey);
					//Get issue
					Key issueKey = KeyFactory.stringToKey(issueKeyStr);
					try {
						Entity issueEnt = datastore.get(txn, issueKey);
						
						//Check if user isn't following issue
						Filter followerFilter = new FilterPredicate("follower", FilterOperator.EQUAL, username);
						Filter issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, issueKeyStr);
						Query watchlistQuery = new Query("IssueFollow").setFilter(CompositeFilterOperator.and(followerFilter, issueFilter));
						List<Entity> results = datastore.prepare(watchlistQuery).asList(FetchOptions.Builder.withDefaults());

						if (results.size() == 0){
							//User wasn't following issue
							txn.rollback();
							
							LOG.warning("User wasn't following issue: " + username);
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.NOT_FOLLOWING))).build();
						}
						
						//Unfollow issue
						datastore.delete(txn, results.get(0).getKey());
						
						//Issue unfollowed
						txn.commit();
						LOG.fine(username+" unfollowed issue "+issueKeyStr);
						
						//Create user log entry
						Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.UNFOLLOWED.toString());
						userLogEntry.setUnindexedProperty("description", "Deixou de seguir a ocorr�ncia \""+(String)issueEnt.getProperty("title")+"\"");
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);
						
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						//Issue does not exist
						txn.rollback();

						LOG.warning("Issue does not exist: " + issueKeyStr);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
					}
				} catch (EntityNotFoundException e) {
					// User does not exist
					txn.rollback();

					LOG.warning("User does not exist: " + username);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS)))
							.build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/{username}/points")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response revokePoints(@PathParam("username") String username, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					txn.commit();

					txn = datastore.beginTransaction();
					Key userKey = KeyFactory.createKey("User", username);
					try {
						// Get user
						datastore.get(txn, userKey);

						//Get user stats
						Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", username);
						Entity userGamificationStatsEnt = datastore.get(userGamificationStatsKey);
						
						//Update user stats
						userGamificationStatsEnt.setUnindexedProperty("points", 0);
						userGamificationStatsEnt.setUnindexedProperty("level", 0);
						datastore.put(txn, userGamificationStatsEnt);

						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// User does not exist
						txn.rollback();

						LOG.warning("User does not exist: " + username);
						return Response.status(Status.NOT_FOUND)
								.entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR)
									.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					// User does not belong to required role
					txn.rollback();

					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response banUser(@PathParam("username") String username, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					txn.commit();

					txn = datastore.beginTransaction();
					Key userKey = KeyFactory.createKey("User", username);
					try {
						// Get user
						Entity bannedUser = datastore.get(txn, userKey);

						//Update user data
						bannedUser.setProperty("role", UserRole.FROZEN.toString());
						bannedUser.setProperty("frozen", true);
						datastore.put(txn, bannedUser);
						
						//Delete user auth tokens
						Query query = new Query("Session").setAncestor(KeyFactory.createKey("User", username));
						List<Entity> tokens = datastore.prepare(query.setKeysOnly()).asList(FetchOptions.Builder.withDefaults());
						List<Key> tokenKeys = new ArrayList<Key>(tokens.size());
						for (Entity token : tokens) {
							tokenKeys.add(token.getKey());
						}
						datastore.delete(tokenKeys);
						
						txn.commit();
						
						// Create user log entry
						Entity userLogEntry = new Entity("UserLog", userKey);
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.BANNED.toString());
						userLogEntry.setUnindexedProperty("description", "Conta banida");
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong",
								headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country",
								headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);

						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// User does not exist
						txn.rollback();

						LOG.warning("User does not exist: " + username);
						return Response.status(Status.NOT_FOUND)
								.entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR)
									.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					// User does not belong to required role
					txn.rollback();

					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/{username}/medals")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewUserMedals(@PathParam("username") String username, @QueryParam("cursor") String cursor,
			@Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				//User has permission to perform operation

				Key userKey = KeyFactory.createKey("User", username);
				try {
					// Get user
					datastore.get(userKey);
					
					// Retrieve medals
					Query query = new Query("UserMedal").setAncestor(userKey);
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> results = datastore.prepare(query.setKeysOnly()).asQueryResultList(fetchOptions);
					
					List<Medal> medals = new ArrayList<Medal>(results.size());
					for (Entity result : results) {
						//Get medal
						Entity medal = datastore.get(KeyFactory.createKey("Medal", result.getKey().getId()));
						medals.add(new Medal(medal.getKey().getId(), (String) medal.getProperty("title"),
								(String) medal.getProperty("description"), (long) medal.getProperty("points"),
								(String) medal.getProperty("picture"), (boolean) medal.getProperty("active")));
					}
					
					// Cursor
					Cursor c = results.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					MedalListing ret = new MedalListing(medals, cursor);
					
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} catch (EntityNotFoundException e) {
					// User does not exist
					LOG.warning("User does not exist: " + username);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
				}
			} else {
				// Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
}
