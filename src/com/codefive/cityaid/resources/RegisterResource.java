package com.codefive.cityaid.resources;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;
import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.requestData.RegistrationData;

@Path("/register")
public class RegisterResource {
	
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public RegisterResource() { } //Nothing to be done here...
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doRegistration(RegistrationData data, @Context HttpServletRequest request, @Context HttpHeaders headers) throws EntityNotFoundException{
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		try {
			//If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.username);
			datastore.get(txn, userKey);
			txn.rollback();
			return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.USER_ALREADY_EXISTS))).build();
		} catch (EntityNotFoundException e){
			//Create user
			Entity user = new Entity("User", data.username);
			user.setUnindexedProperty("password", DigestUtils.sha512Hex(data.password.trim()));
			user.setUnindexedProperty("email", data.email.trim());
			user.setUnindexedProperty("name", data.name);
			user.setUnindexedProperty("phone", data.phone);
			user.setUnindexedProperty("gender", data.gender);
			user.setUnindexedProperty("birth_date", data.birthdate);
			user.setProperty("role", UserRole.REPORTER.toString());
			user.setProperty("registered_since", new Date());
			user.setProperty("verified", false);
			user.setProperty("frozen", false);
			user.setProperty("isManager", false);
			user.setProperty("employer", null);
			user.setUnindexedProperty("job", null);
			datastore.put(txn, user);
			txn.commit();
			
			//Initialize options
			Entity userOptionsEnt = new Entity("UserOptions", data.username);
			userOptionsEnt.setUnindexedProperty("auto_follow", true);
			userOptionsEnt.setUnindexedProperty("push_medal", true);
			userOptionsEnt.setUnindexedProperty("push_level_up", true);
			userOptionsEnt.setUnindexedProperty("push_issue_wip", true);
			userOptionsEnt.setUnindexedProperty("push_issue_closed", true);
			userOptionsEnt.setUnindexedProperty("push_issue_solved", true);
			userOptionsEnt.setUnindexedProperty("push_log_comment", true);
			userOptionsEnt.setUnindexedProperty("push_edit", true);
			userOptionsEnt.setUnindexedProperty("push_new_photos", true);
			userOptionsEnt.setUnindexedProperty("push_comment", true);
			userOptionsEnt.setUnindexedProperty("email_medal", false);
			userOptionsEnt.setUnindexedProperty("email_level_up", false);
			userOptionsEnt.setUnindexedProperty("email_issue_wip", true);
			userOptionsEnt.setUnindexedProperty("email_issue_closed", true);
			userOptionsEnt.setUnindexedProperty("email_issue_solved", true);
			userOptionsEnt.setUnindexedProperty("email_log_comment", false);
			userOptionsEnt.setUnindexedProperty("email_edit", false);
			userOptionsEnt.setUnindexedProperty("email_new_photos", false);
			userOptionsEnt.setUnindexedProperty("email_comment", false);
			userOptionsEnt.setUnindexedProperty("mobile_cache", 5);
			datastore.put(userOptionsEnt);
			
			//Initialize stats
			Entity userGamificationStatsEnt = new Entity("UserGamificationStats", data.username);
			userGamificationStatsEnt.setUnindexedProperty("points", 0);
			userGamificationStatsEnt.setProperty("level", 1);
			userGamificationStatsEnt.setUnindexedProperty("num_awarded_medals", 0);
			datastore.put(userGamificationStatsEnt);
			Entity userIssueStatsEnt = new Entity("UserIssueStats", data.username);
			userIssueStatsEnt.setUnindexedProperty("issues_created", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_created_type_vandalism", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_created_type_garbage", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_created_type_accident", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_created_type_streetdmg", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_created_type_furnituredmg", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_created_type_nature", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_created_type_other", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_in_progress", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_solved", 0);
			userIssueStatsEnt.setUnindexedProperty("issues_closed", 0);
			datastore.put(userIssueStatsEnt);
			Entity userCommentStatsEnt = new Entity("UserCommentStats", data.username);
			userCommentStatsEnt.setUnindexedProperty("issues_commented", 0);
			userCommentStatsEnt.setUnindexedProperty("issues_commented_type_vandalism", 0);
			userCommentStatsEnt.setUnindexedProperty("issues_commented_type_garbage", 0);
			userCommentStatsEnt.setUnindexedProperty("issues_commented_type_accident", 0);
			userCommentStatsEnt.setUnindexedProperty("issues_commented_type_streetdmg", 0);
			userCommentStatsEnt.setUnindexedProperty("issues_commented_type_furnituredmg", 0);
			userCommentStatsEnt.setUnindexedProperty("issues_commented_type_nature", 0);
			userCommentStatsEnt.setUnindexedProperty("issues_commented_type_other", 0);
			datastore.put(userCommentStatsEnt);
			Entity userFollowStatsEnt = new Entity("UserFollowStats", data.username);
			userFollowStatsEnt.setUnindexedProperty("issues_followed", 0);
			userFollowStatsEnt.setUnindexedProperty("issues_followed_type_vandalism", 0);
			userFollowStatsEnt.setUnindexedProperty("issues_followed_type_garbage", 0);
			userFollowStatsEnt.setUnindexedProperty("issues_followed_type_accident", 0);
			userFollowStatsEnt.setUnindexedProperty("issues_followed_type_streetdmg", 0);
			userFollowStatsEnt.setUnindexedProperty("issues_followed_type_furnituredmg", 0);
			userFollowStatsEnt.setUnindexedProperty("issues_followed_type_nature", 0);
			userFollowStatsEnt.setUnindexedProperty("issues_followed_type_other", 0);
			datastore.put(userFollowStatsEnt);
			Entity userRateStatsEnt = new Entity("UserRateStats", data.username);
			userRateStatsEnt.setUnindexedProperty("issues_rated", 0);
			userRateStatsEnt.setUnindexedProperty("issues_rated_type_vandalism", 0);
			userRateStatsEnt.setUnindexedProperty("issues_rated_type_garbage", 0);
			userRateStatsEnt.setUnindexedProperty("issues_rated_type_accident", 0);
			userRateStatsEnt.setUnindexedProperty("issues_rated_type_streetdmg", 0);
			userRateStatsEnt.setUnindexedProperty("issues_rated_type_furnituredmg", 0);
			userRateStatsEnt.setUnindexedProperty("issues_rated_type_nature", 0);
			userRateStatsEnt.setUnindexedProperty("issues_rated_type_other", 0);
			datastore.put(userRateStatsEnt);
			
			//Create user log entry
			Entity userLogEntry = new Entity("UserLog", user.getKey());
			userLogEntry.setProperty("date", new Date());
			userLogEntry.setUnindexedProperty("type", UserLogType.REGISTERED.toString());
			userLogEntry.setUnindexedProperty("description", "Conta registada");
			userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
			userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
			userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
			userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
			userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
			datastore.put(userLogEntry);
			
			//Create account verification entry
			String verificationToken = UUID.randomUUID().toString();
			Entity verificationTokenEntry = new Entity("Verification", verificationToken);
			verificationTokenEntry.setUnindexedProperty("username", data.username);
			datastore.put(verificationTokenEntry);
			
			LOG.info("User registered " + data.username);
			
			//Send verification email
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);

			try {
				Message msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress("noreply@city-aid.appspotmail.com", "CityAid"));
				msg.addRecipient(Message.RecipientType.TO, new InternetAddress(data.email, data.name));
				msg.setSubject(MimeUtility.encodeText("Ative a sua conta CityAid", "UTF-8", "Q"));
	
				String htmlBody = "Ative a sua conta CityAid <a href=\"https://city-aid.appspot.com/verify.html?user="+data.username+"&token="+verificationToken+"\">visitando esta p�gina</a>.<br>Se n�o criou uma conta CityAid, ignore esta mensagem."+NotificationResource.getEmailSignature();
				Multipart mp = new MimeMultipart();
	
				MimeBodyPart htmlPart = new MimeBodyPart();
				htmlPart.setContent(htmlBody, "text/html");
				mp.addBodyPart(htmlPart);
	
				msg.setContent(mp);
	
				Transport.send(msg);
				
				LOG.fine("Account confirmation email sent to "+data.email);
			} catch (AddressException e1) {
				LOG.warning("Account confirmation email not sent to "+data.email+"\nException: "+e1.getMessage());
			} catch (MessagingException e1) {
				LOG.warning("Account confirmation email not sent to "+data.email+"\nException: "+e1.getMessage());
			} catch (UnsupportedEncodingException e1) {
				LOG.warning("Account confirmation email not sent to "+data.email+"\nException: "+e1.getMessage());
			} catch (Exception e1) {
				LOG.warning("Account confirmation email not sent to "+data.email+"\nException: "+e1.getMessage());
			}
			
			return Response.status(Status.NO_CONTENT).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}

}
