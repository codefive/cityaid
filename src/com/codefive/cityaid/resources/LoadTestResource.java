package com.codefive.cityaid.resources;

import java.util.Date;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.codefive.cityaid.util.enums.IssueLogType;
import com.codefive.cityaid.util.enums.IssueStatus;
import com.codefive.cityaid.util.enums.IssueType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

@Path("/load")
public class LoadTestResource {

	/**
	 * A logger object.
	 */
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public LoadTestResource() { } //Nothing to be done here...
	
	@GET
	@Path("/issues/{username}/{num}")
	public Response triggerGenerateIssuesTask(@PathParam("username") String authUsername, @PathParam("num") String num){
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/rest/load/issues/"+authUsername+"/"+num));
		return Response.ok().build();
	}
	
	@POST
	@Path("/issues/{username}/{num}")
	public Response generateIssues(@PathParam("username") String authUsername, @PathParam("num") String num){
		int numIssues;
		try {
			numIssues = Integer.parseInt(num);
			
			Transaction txn = null;
			for (int i = 1; i<=numIssues; i++){
				try {
					//Create issue
					Entity issue = new Entity("Occurrence", KeyFactory.createKey("User", authUsername));
					issue.setProperty("status", IssueStatus.CREATED.toString());
					issue.setUnindexedProperty("title", "Ocorr�ncia N�mero "+i);
					issue.setUnindexedProperty("description", "Descri��o da Ocorr�ncia N�mero "+i);
					issue.setProperty("latitude", (double)(38.66+i*0.0001));
					issue.setProperty("longitude", (double)(-9.20+i*0.0001));
					String issueType;
					switch ((i % 10)){
					case 1:
						issueType = "vandalism";
						break;
					case 2:
						issueType = "garbage";
						break;
					case 3:
						issueType = "accident";
						break;
					case 4:
						issueType = "street_damage";
						break;
					case 5:
						issueType = "furniture_damage";
						break;
					case 6:
						issueType = "nature";
						break;
					case 7:
						issueType = "other";
						break;
					case 8:
						issueType = "garbage";
						break;
					case 9:
						issueType = "accident";
						break;
					case 0:
					default:
						issueType = "vandalism";
						break;
					}
					issue.setProperty("type", issueType);
					issue.setProperty("creation_date", new Date());
					issue.setUnindexedProperty("picture_ids", null);
					issue.setUnindexedProperty("thumbnail_ids", null);
					issue.setProperty("rating", (i % 3)+1);
					issue.setUnindexedProperty("forceRating", false);
					issue.setProperty("hidden", false);
					datastore.put(issue);
					
					//Create issue rating
					Entity rating = new Entity("Rating", authUsername, issue.getKey());
					rating.setIndexedProperty("priority", (i % 3)+1);
					datastore.put(rating);
					
					String issueKeyStr = KeyFactory.keyToString(issue.getKey());
					
					//Follow issue
					Key userOptionsKey = KeyFactory.createKey("UserOptions", authUsername);
					Entity userOptionsEnt = datastore.get(userOptionsKey);
					if ((boolean) userOptionsEnt.getProperty("auto_follow")) {
						//User wants to automatically follow issue
						Entity follow = new Entity("IssueFollow");
						follow.setProperty("follower", authUsername);
						follow.setProperty("issue", issueKeyStr);
						follow.setProperty("hidden", false);
						datastore.put(follow);
					}
					
					//Create user log entry
					Entity userLogEntry = new Entity("UserLog", new KeyFactory.Builder("User", authUsername).getKey());
					userLogEntry.setProperty("date", new Date());
					userLogEntry.setUnindexedProperty("type", UserLogType.O_CREATED.toString());
					userLogEntry.setUnindexedProperty("description", "Ocorr�ncia \"T�tulo da Ocorr�ncia N�mero "+i+"\" gerada automaticamente");
					userLogEntry.setUnindexedProperty("remote_addr", "Desconhecido");
					userLogEntry.setUnindexedProperty("remote_host", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-City", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-Country", "Desconhecido");
					datastore.put(userLogEntry);
					
					//Add issue log entry
					Entity issueLogEntry = new Entity("IssueLog", issue.getKey());
					issueLogEntry.setProperty("date", new Date());
					issueLogEntry.setUnindexedProperty("type", IssueLogType.CREATED.toString());
					issueLogEntry.setUnindexedProperty("user", authUsername);
					issueLogEntry.setUnindexedProperty("description", "Ocorr�ncia criada");
					datastore.put(issueLogEntry);					
					
					//Update points
					Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
					Entity appPoints = datastore.get(appPointsKey);
					txn = datastore.beginTransaction();
					Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
					Entity userGamificationStats = datastore.get(txn, userGamificationStatsKey);
					long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("createIssue"));
					userGamificationStats.setUnindexedProperty("points", newPoints);
					datastore.put(txn, userGamificationStats);
					txn.commit();
					
					txn = datastore.beginTransaction();
					//Get user stats
					Key userIssueStatsKey = KeyFactory.createKey("UserIssueStats", authUsername);
					Entity userIssueStatsEnt = datastore.get(txn, userIssueStatsKey);
					
					//Update user stats
					userIssueStatsEnt.setUnindexedProperty("issues_created", ((long)userIssueStatsEnt.getProperty("issues_created"))+1);
					switch(IssueType.valueOf(issueType.toUpperCase())){
						case ACCIDENT:
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_accident", ((long)userIssueStatsEnt.getProperty("issues_created_type_accident"))+1);
							break;
						case FURNITURE_DAMAGE:
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_furnituredmg", ((long)userIssueStatsEnt.getProperty("issues_created_type_furnituredmg"))+1);
							break;
						case GARBAGE:
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_garbage", ((long)userIssueStatsEnt.getProperty("issues_created_type_garbage"))+1);
							break;
						case NATURE:
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_nature", ((long)userIssueStatsEnt.getProperty("issues_created_type_nature"))+1);
							break;
						case OTHER:
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_other", ((long)userIssueStatsEnt.getProperty("issues_created_type_other"))+1);
							break;
						case STREET_DAMAGE:
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_streetdmg", ((long)userIssueStatsEnt.getProperty("issues_created_type_streetdmg"))+1);
							break;
						case VANDALISM:
							userIssueStatsEnt.setUnindexedProperty("issues_created_type_vandalism", ((long)userIssueStatsEnt.getProperty("issues_created_type_vandalism"))+1);
							break;
						default:
							break;					
					}
					datastore.put(txn, userIssueStatsEnt);
					
					txn.commit();
				} catch (EntityNotFoundException e) {
					if (txn != null){
						txn.rollback();
					}
					//Please disperse. Nothing to see here. Please.
					continue;
				} finally {
					if (txn != null){
						if (txn.isActive()) {
							txn.rollback();
							//Please disperse. Nothing to see here. Please.
							continue;
						}
					}
				}
			}
		} catch (NumberFormatException numE){
			//Please disperse. Nothing to see here. Please.
		}
			
		//Update gamification system
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+authUsername));
		
		return Response.status(Status.OK).build();
	}
	
	@GET
	@Path("/users/{num}")
	public Response triggerGenerateUsersTask(@PathParam("num") String num){
		Queue queue = QueueFactory.getDefaultQueue();
		queue.add(TaskOptions.Builder.withUrl("/rest/load/users/"+num));
		return Response.ok().build();
	}
	
	@POST
	@Path("/users/{num}")
	public Response generateUsers(@PathParam("num") String num){
		int numUsers;
		try {
			numUsers = Integer.parseInt(num);
			
			Transaction txn = null;
			for (int i = 1; i<=numUsers; i++){
				try {
					//Create user
					Entity user = new Entity("User", "loadTestU"+i);
					user.setUnindexedProperty("password", DigestUtils.sha512Hex("loadTestU"+i));
					user.setUnindexedProperty("email", "user"+i+"@example.com");
					user.setUnindexedProperty("name", "Load Test User "+i);
					user.setUnindexedProperty("phone", "960000000");
					user.setUnindexedProperty("gender", "Masculino");
					user.setUnindexedProperty("birth_date", new Date());
					user.setProperty("role", UserRole.REPORTER.toString());
					user.setProperty("registered_since", new Date());
					user.setProperty("verified", true);
					user.setProperty("frozen", false);
					user.setProperty("isManager", false);
					user.setProperty("employer", null);
					user.setUnindexedProperty("job", null);
					datastore.put(user);
					
					//Initialize options
					Entity userOptionsEnt = new Entity("UserOptions", "loadTestU"+i);
					userOptionsEnt.setUnindexedProperty("auto_follow", true);
					userOptionsEnt.setUnindexedProperty("push_medal", true);
					userOptionsEnt.setUnindexedProperty("push_level_up", true);
					userOptionsEnt.setUnindexedProperty("push_issue_wip", true);
					userOptionsEnt.setUnindexedProperty("push_issue_closed", true);
					userOptionsEnt.setUnindexedProperty("push_issue_solved", true);
					userOptionsEnt.setUnindexedProperty("push_log_comment", true);
					userOptionsEnt.setUnindexedProperty("push_edit", true);
					userOptionsEnt.setUnindexedProperty("push_new_photos", true);
					userOptionsEnt.setUnindexedProperty("push_comment", true);
					userOptionsEnt.setUnindexedProperty("email_medal", true);
					userOptionsEnt.setUnindexedProperty("email_level_up", true);
					userOptionsEnt.setUnindexedProperty("email_issue_wip", true);
					userOptionsEnt.setUnindexedProperty("email_issue_closed", true);
					userOptionsEnt.setUnindexedProperty("email_issue_solved", true);
					userOptionsEnt.setUnindexedProperty("email_log_comment", true);
					userOptionsEnt.setUnindexedProperty("email_edit", true);
					userOptionsEnt.setUnindexedProperty("email_new_photos", true);
					userOptionsEnt.setUnindexedProperty("email_comment", true);
					userOptionsEnt.setUnindexedProperty("mobile_cache", 5);
					datastore.put(userOptionsEnt);
					
					//Initialize stats
					Entity userGamificationStatsEnt = new Entity("UserGamificationStats", "loadTestU"+i);
					userGamificationStatsEnt.setUnindexedProperty("points", 0);
					userGamificationStatsEnt.setProperty("level", 1);
					userGamificationStatsEnt.setUnindexedProperty("num_awarded_medals", 0);
					datastore.put(userGamificationStatsEnt);
					Entity userIssueStatsEnt = new Entity("UserIssueStats", "loadTestU"+i);
					userIssueStatsEnt.setUnindexedProperty("issues_created", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_created_type_vandalism", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_created_type_garbage", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_created_type_accident", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_created_type_streetdmg", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_created_type_furnituredmg", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_created_type_nature", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_created_type_other", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_in_progress", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_solved", 0);
					userIssueStatsEnt.setUnindexedProperty("issues_closed", 0);
					datastore.put(userIssueStatsEnt);
					Entity userCommentStatsEnt = new Entity("UserCommentStats", "loadTestU"+i);
					userCommentStatsEnt.setUnindexedProperty("issues_commented", 0);
					userCommentStatsEnt.setUnindexedProperty("issues_commented_type_vandalism", 0);
					userCommentStatsEnt.setUnindexedProperty("issues_commented_type_garbage", 0);
					userCommentStatsEnt.setUnindexedProperty("issues_commented_type_accident", 0);
					userCommentStatsEnt.setUnindexedProperty("issues_commented_type_streetdmg", 0);
					userCommentStatsEnt.setUnindexedProperty("issues_commented_type_furnituredmg", 0);
					userCommentStatsEnt.setUnindexedProperty("issues_commented_type_nature", 0);
					userCommentStatsEnt.setUnindexedProperty("issues_commented_type_other", 0);
					datastore.put(userCommentStatsEnt);
					Entity userFollowStatsEnt = new Entity("UserFollowStats", "loadTestU"+i);
					userFollowStatsEnt.setUnindexedProperty("issues_followed", 0);
					userFollowStatsEnt.setUnindexedProperty("issues_followed_type_vandalism", 0);
					userFollowStatsEnt.setUnindexedProperty("issues_followed_type_garbage", 0);
					userFollowStatsEnt.setUnindexedProperty("issues_followed_type_accident", 0);
					userFollowStatsEnt.setUnindexedProperty("issues_followed_type_streetdmg", 0);
					userFollowStatsEnt.setUnindexedProperty("issues_followed_type_furnituredmg", 0);
					userFollowStatsEnt.setUnindexedProperty("issues_followed_type_nature", 0);
					userFollowStatsEnt.setUnindexedProperty("issues_followed_type_other", 0);
					datastore.put(userFollowStatsEnt);
					Entity userRateStatsEnt = new Entity("UserRateStats", "loadTestU"+i);
					userRateStatsEnt.setUnindexedProperty("issues_rated", 0);
					userRateStatsEnt.setUnindexedProperty("issues_rated_type_vandalism", 0);
					userRateStatsEnt.setUnindexedProperty("issues_rated_type_garbage", 0);
					userRateStatsEnt.setUnindexedProperty("issues_rated_type_accident", 0);
					userRateStatsEnt.setUnindexedProperty("issues_rated_type_streetdmg", 0);
					userRateStatsEnt.setUnindexedProperty("issues_rated_type_furnituredmg", 0);
					userRateStatsEnt.setUnindexedProperty("issues_rated_type_nature", 0);
					userRateStatsEnt.setUnindexedProperty("issues_rated_type_other", 0);
					datastore.put(userRateStatsEnt);
					
					//Create user log entry
					Entity userLogEntry = new Entity("UserLog", user.getKey());
					userLogEntry.setProperty("date", new Date());
					userLogEntry.setUnindexedProperty("type", UserLogType.REGISTERED.toString());
					userLogEntry.setUnindexedProperty("description", "Conta registada");
					userLogEntry.setUnindexedProperty("remote_addr", "Desconhecido");
					userLogEntry.setUnindexedProperty("remote_host", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-City", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-Country", "Desconhecidoo");
					datastore.put(userLogEntry);
				} catch (Exception e) {
					if (txn != null){
						txn.rollback();
					}
					//Please disperse. Nothing to see here. Please.
					continue;
				} finally {
					if (txn != null){
						if (txn.isActive()) {
							txn.rollback();
							//Please disperse. Nothing to see here. Please.
							continue;
						}
					}
				}
			}
		} catch (NumberFormatException numE){
			//Please disperse. Nothing to see here. Please.
		}
		
		return Response.status(Status.OK).build();
	}
}
