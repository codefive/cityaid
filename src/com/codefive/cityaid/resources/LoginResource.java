package com.codefive.cityaid.resources;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;
import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.replyData.AuthToken;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.requestData.LoginData;

@Path("/login")
public class LoginResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public LoginResource() { } //Nothing to be done here...
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogin(LoginData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}

		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			//Get user
			Entity user = datastore.get(userKey);
			String hashedPassword = (String) user.getProperty("password");
			boolean verifiedAccount = (boolean) user.getProperty("verified");
			if (verifiedAccount) {
				//Verified account
				
				boolean frozenAccount = (boolean) user.getProperty("frozen");
				if (!frozenAccount){
					//Account is not frozen
					
					if (hashedPassword.equals(DigestUtils.sha512Hex(data.password.trim()))) {
						// Password correct
						
						Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", data.username);
						Entity userGamificationStats = datastore.get(userGamificationStatsKey);
						Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
						Entity appPoints = datastore.get(appPointsKey);
						
						// Construct the token
						AuthToken token = new AuthToken(data.username, (String)user.getProperty("role"), (boolean)user.getProperty("isManager"), (long)userGamificationStats.getProperty("level") >= (long)appPoints.getProperty("modLevel"), (String)user.getProperty("employer"));
						Entity t = new Entity("Session", token.token, user.getKey());
						t.setUnindexedProperty("creation", token.creation);
						t.setProperty("expiration", token.expiration);
						t.setProperty("device", data.device);		
						datastore.put(t);
						
						// Return token
						LOG.info("Logged in: " + data.username);
						return Response.status(Status.OK).entity(g.toJson(token)).build();
					} else {
						// Incorrect password						
						LOG.warning("Wrong password for user: " + data.username);
						return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.WRONG_PWD))).build();
					}
				} else {
					//Frozen account
					LOG.warning("Frozen account attempted to login: " + data.username);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.FROZEN_ACCOUNT))).build();
				}
			} else {
				//Unverified account
				LOG.warning("Unverified account attempted to login: " + data.username);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.UNVERIFIED))).build();
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Someone tried to login as unexisting user: " + data.username);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.WRONG_PWD))).build();
		}
		
	}
	
}
