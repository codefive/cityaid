package com.codefive.cityaid.resources;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.requestData.EntityRatingData;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;

@Path("/issues/{issue}/entities/{entity}/ratings")
public class EntityRatingResource {
	
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(EntityRatingResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public EntityRatingResource() { } //Nothing to be done here...

	@POST
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response submitEntityRating(@PathParam("issue") String issueKeyStr, @PathParam("entity") String entityId, EntityRatingData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				//Check if user isn't following issue
				Filter followerFilter = new FilterPredicate("follower", FilterOperator.EQUAL, authUsername);
				Filter issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, issueKeyStr);
				Query watchlistQuery = new Query("IssueFollow").setFilter(CompositeFilterOperator.and(followerFilter, issueFilter));
				List<Entity> results = datastore.prepare(watchlistQuery).asList(FetchOptions.Builder.withDefaults());

				if (results.size() == 0){
					//User wasn't following issue
					txn.rollback();
					
					LOG.warning("User wasn't following issue: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.NOT_FOLLOWING))).build();
				}
				
				//User has permission to perform operation
				Key entityKey = KeyFactory.createKey("Entity", entityId);
				try {
					//Get entity
					datastore.get(txn, entityKey);
					
					Filter userFilter = new FilterPredicate("user", FilterOperator.EQUAL, authUsername);
					issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, issueKeyStr);
					Query ratingQuery = new Query("EntityRating").setAncestor(entityKey).setFilter(CompositeFilterOperator.and(userFilter, issueFilter));
					List<Entity> ratings = datastore.prepare(ratingQuery).asList(FetchOptions.Builder.withDefaults());

					if (ratings.size() == 0){
						//User hasn't rated entity performance for this issue yet

						//Create issue rating
						Entity rating = new Entity("EntityRating", entityKey);
						rating.setIndexedProperty("user", authUsername);
						rating.setIndexedProperty("issue", issueKeyStr);
						rating.setIndexedProperty("rating", data.rating);
						datastore.put(txn, rating);
						
						//Create rating flag, if it doesn't exist
						try {
							Key ratingFlagKey = KeyFactory.createKey("EntityRatingFlag", entityId);
							datastore.get(txn, ratingFlagKey);
						} catch (EntityNotFoundException e1){
							Entity ratingFlag = new Entity("EntityRatingFlag", entityId);
							datastore.put(txn, ratingFlag);
						}
												
						//Update points
						Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
						Entity appPoints = datastore.get(appPointsKey);
						Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
						Entity userGamificationStats = datastore.get(txn, userGamificationStatsKey);
						long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("rateEntity"));
						userGamificationStats.setUnindexedProperty("points", newPoints);
						datastore.put(txn, userGamificationStats);
						
						txn.commit();
						
						//Create user log entry
						Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.E_RATED.toString());
						userLogEntry.setUnindexedProperty("description", "Avaliou a performance da entidade "+entityId+" na ocorrência "+issueKeyStr+" como "+data.rating);
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);
						
						//Update gamification system
						Queue queue = QueueFactory.getDefaultQueue();
						queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+authUsername));
						
						return Response.status(Status.NO_CONTENT).build();
					} else {
						//User already rated entity performance for this issue
						txn.rollback();
						
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.RATING_ALREADY_EXISTS))).build();
					}
				} catch (EntityNotFoundException e) {
					// Entity does not exist
					txn.rollback();
					
					LOG.warning("Entity does not exist: " + entityId);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@PUT
	@Path("/{username}")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateEntityRating(@PathParam("issue") String issueKeyStr, @PathParam("entity") String entityId, @PathParam("username") String username, EntityRatingData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName()) && authUsername.equals(username)){
				//User has permission to perform operation
				Key entityKey = KeyFactory.createKey("Entity", entityId);
				Filter userFilter = new FilterPredicate("user", FilterOperator.EQUAL, authUsername);
				Filter issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, issueKeyStr);
				Query ratingQuery = new Query("EntityRating").setAncestor(entityKey).setFilter(CompositeFilterOperator.and(userFilter, issueFilter));
				List<Entity> ratings = datastore.prepare(ratingQuery).asList(FetchOptions.Builder.withDefaults());

				if (ratings.size() > 0){
					//User already rated entity performance for this issue

					//Update issue rating
					Entity rating = ratings.get(0);
					rating.setIndexedProperty("rating", data.rating);
					datastore.put(txn, rating);
					
					//Create rating flag, if it doesn't exist
					try {
						Key ratingFlagKey = KeyFactory.createKey("EntityRatingFlag", entityId);
						datastore.get(txn, ratingFlagKey);
					} catch (EntityNotFoundException e1){
						Entity ratingFlag = new Entity("EntityRatingFlag", entityId);
						datastore.put(txn, ratingFlag);
					}
					
					//Create user log entry
					Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
					userLogEntry.setProperty("date", new Date());
					userLogEntry.setUnindexedProperty("type", UserLogType.E_RATED.toString());
					userLogEntry.setUnindexedProperty("description", "Avaliou performance da entidade "+entityId+" na ocorrência "+issueKeyStr+" como "+data.rating);
					userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
					userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
					userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
					userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
					userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
					datastore.put(txn, userLogEntry);
					
					txn.commit();
					
					return Response.status(Status.NO_CONTENT).build();
				} else {
					//User hasn't rated entity performance for this issue yet
					txn.rollback();
					
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.RATING_NOT_EXISTS))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
}
