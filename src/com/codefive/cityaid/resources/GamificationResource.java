package com.codefive.cityaid.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.NotificationType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.GamificationPoints;
import com.codefive.cityaid.util.replyData.Medal;
import com.codefive.cityaid.util.replyData.MedalListing;
import com.codefive.cityaid.util.replyData.Notification;
import com.codefive.cityaid.util.requestData.MedalData;
import com.codefive.cityaid.util.requestData.MedalID;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gson.Gson;

@Path("/gamification")
public class GamificationResource {
	
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(GamificationResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private static final int PAGE_SIZE = 20;
	
	/**
	* This is where backoff parameters are configured. Here it is aggressively retrying with
	* backoff, up to 10 times but taking no more that 15 seconds total to do so.
	*/
	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
															.initialRetryDelayMillis(10)
															.retryMaxAttempts(10)
															.totalRetryPeriodMillis(15000)
															.build());
	
	public GamificationResource() { } //Nothing to be done here...

	@POST
	@Path("/medals")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addMedal(MedalData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) && ((boolean)user.getProperty("isManager"))){
					//User has permission to perform operation
					txn.commit();
				
					//Save photo on Cloud Storage
					String photoId = "default.jpg";
					if (data.picture != null){
						try {
							photoId = uploadMedalPicture(data.picture.base64, data.picture.extension);
						} catch (IOException e1) {
							LOG.warning("Failed to upload issue photo. Exception: "+e1.getMessage());
							
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						} catch (Exception e1) {
							LOG.warning("Something happenned. Exception: "+e1.getMessage());
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
					
					//Create medal
					Entity medal = new Entity("Medal");
					medal.setProperty("title", data.title);
					medal.setUnindexedProperty("description", data.description);
					medal.setUnindexedProperty("points", data.points);
					medal.setUnindexedProperty("picture", photoId);
					medal.setUnindexedProperty("precond_points", data.preconditions.points);
					medal.setUnindexedProperty("precond_level", data.preconditions.level);
					medal.setUnindexedProperty("precond_num_awarded_medals", data.preconditions.numAwardedMedals);
					medal.setUnindexedProperty("precond_issues_created", data.preconditions.issuesCreated);
					medal.setUnindexedProperty("precond_issues_created_type_vandalism", data.preconditions.issuesCreatedTypeVandalism);
					medal.setUnindexedProperty("precond_issues_created_type_garbage", data.preconditions.issuesCreatedTypeGarbage);
					medal.setUnindexedProperty("precond_issues_created_type_accident", data.preconditions.issuesCreatedTypeAccident);
					medal.setUnindexedProperty("precond_issues_created_type_streetdmg", data.preconditions.issuesCreatedTypeStreetDamage);
					medal.setUnindexedProperty("precond_issues_created_type_furnituredmg", data.preconditions.issuesCreatedTypeFurnitureDamage);
					medal.setUnindexedProperty("precond_issues_created_type_nature", data.preconditions.issuesCreatedTypeNature);
					medal.setUnindexedProperty("precond_issues_created_type_other", data.preconditions.issuesCreatedTypeOther);
					medal.setUnindexedProperty("precond_issues_in_progress", data.preconditions.issuesProgress);
					medal.setUnindexedProperty("precond_issues_solved", data.preconditions.issuesSolved);
					medal.setUnindexedProperty("precond_issues_closed", data.preconditions.issuesClosed);
					medal.setUnindexedProperty("precond_issues_commented", data.preconditions.issuesCommented);
					medal.setUnindexedProperty("precond_issues_commented_type_vandalism", data.preconditions.issuesCommentedTypeVandalism);
					medal.setUnindexedProperty("precond_issues_commented_type_garbage", data.preconditions.issuesCommentedTypeGarbage);
					medal.setUnindexedProperty("precond_issues_commented_type_accident", data.preconditions.issuesCommentedTypeAccident);
					medal.setUnindexedProperty("precond_issues_commented_type_streetdmg", data.preconditions.issuesCommentedTypeStreetDamage);
					medal.setUnindexedProperty("precond_issues_commented_type_furnituredmg", data.preconditions.issuesCommentedTypeFurnitureDamage);
					medal.setUnindexedProperty("precond_issues_commented_type_nature", data.preconditions.issuesCommentedTypeNature);
					medal.setUnindexedProperty("precond_issues_commented_type_other", data.preconditions.issuesCommentedTypeOther);
					medal.setUnindexedProperty("precond_issues_followed", data.preconditions.issuesFollowed);
					medal.setUnindexedProperty("precond_issues_followed_type_vandalism", data.preconditions.issuesFollowedTypeVandalism);
					medal.setUnindexedProperty("precond_issues_followed_type_garbage", data.preconditions.issuesFollowedTypeGarbage);
					medal.setUnindexedProperty("precond_issues_followed_type_accident", data.preconditions.issuesFollowedTypeAccident);
					medal.setUnindexedProperty("precond_issues_followed_type_streetdmg", data.preconditions.issuesFollowedTypeStreetDamage);
					medal.setUnindexedProperty("precond_issues_followed_type_furnituredmg", data.preconditions.issuesFollowedTypeFurnitureDamage);
					medal.setUnindexedProperty("precond_issues_followed_type_nature", data.preconditions.issuesFollowedTypeNature);
					medal.setUnindexedProperty("precond_issues_followed_type_other", data.preconditions.issuesFollowedTypeOther);
					medal.setUnindexedProperty("precond_issues_rated", data.preconditions.issuesRated);
					medal.setUnindexedProperty("precond_issues_rated_type_vandalism", data.preconditions.issuesRatedTypeVandalism);
					medal.setUnindexedProperty("precond_issues_rated_type_garbage", data.preconditions.issuesRatedTypeGarbage);
					medal.setUnindexedProperty("precond_issues_rated_type_accident", data.preconditions.issuesRatedTypeAccident);
					medal.setUnindexedProperty("precond_issues_rated_type_streetdmg", data.preconditions.issuesRatedTypeStreetDamage);
					medal.setUnindexedProperty("precond_issues_rated_type_furnituredmg", data.preconditions.issuesRatedTypeFurnitureDamage);
					medal.setUnindexedProperty("precond_issues_rated_type_nature", data.preconditions.issuesRatedTypeNature);
					medal.setUnindexedProperty("precond_issues_rated_type_other", data.preconditions.issuesRatedTypeOther);
					medal.setProperty("active", true);
					datastore.put(medal);
					
					LOG.info("Medal added");
					return Response.status(Status.NO_CONTENT).build();
				} else {
					// User does not belong to required role
					txn.rollback();

					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/medals")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewSystemMedals(@QueryParam("cursor") String cursor, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				//User has permission to perform operation
				txn.commit();

				// Retrieve medals
				Query query = new Query("Medal").addSort("title", SortDirection.ASCENDING);;
				
				FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
				
				if (cursor != null && !cursor.equals("null")) {
					fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
				}
				
				QueryResultList<Entity> results = datastore.prepare(query).asQueryResultList(fetchOptions);

				List<Medal> medals = new ArrayList<Medal>(results.size());
				for (Entity medal : results) {
					medals.add(new Medal(medal.getKey().getId(), (String) medal.getProperty("title"),
							(String) medal.getProperty("description"), (long) medal.getProperty("points"),
							(String) medal.getProperty("picture"), (boolean) medal.getProperty("active")));
				}
				
				// Cursor
				Cursor c = results.getCursor();
				if (c != null){
					cursor = c.toWebSafeString();
				} else {
					cursor = null;
				}
				
				MedalListing ret = new MedalListing(medals, cursor);

				return Response.status(Status.OK).entity(g.toJson(ret)).build();
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@POST
	@Path("/users/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateUserGamification(@PathParam("username") String username){
		
		Key userKey = KeyFactory.createKey("User", username);
		try {
			// Get user
			Entity user = datastore.get(userKey);
			
			//Check if user is regular or not
			if (!((String)user.getProperty("role")).equalsIgnoreCase(UserRole.REPORTER.name())){
				//Backoffice and entity users do not use the gamification system
				return Response.status(Status.NO_CONTENT).build();
			}

			//Get user stats
			Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", username);
			Entity userGamificationStatsEnt = datastore.get(userGamificationStatsKey);
			Key userIssueStatsKey = KeyFactory.createKey("UserIssueStats", username);
			Entity userIssueStatsEnt = datastore.get(userIssueStatsKey);
			Key userCommentStatsKey = KeyFactory.createKey("UserCommentStats", username);
			Entity userCommentStatsEnt = datastore.get(userCommentStatsKey);
			Key userFollowStatsKey = KeyFactory.createKey("UserFollowStats", username);
			Entity userFollowStatsEnt = datastore.get(userFollowStatsKey);
			Key userRateStatsKey = KeyFactory.createKey("UserRateStats", username);
			Entity userRateStatsEnt = datastore.get(userRateStatsKey);

			// Retrieve system medals
			Filter activeFilter = new FilterPredicate("active", FilterOperator.EQUAL, true);
			Query query = new Query("Medal").setFilter(activeFilter);
			List<Entity> medals = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());

			int pointsInc = 0;
			int numAwardedMedalsInc = 0;
			List<Notification> notifications = new ArrayList<Notification>(medals.size());
			
			//Check if unawarded medals preconditions are met
			for (Entity medal : medals) {
				Key medalKey = KeyFactory.createKey(userKey, "UserMedal", medal.getKey().getId());
				try {
					datastore.get(medalKey);
					
					//User already has medal
					continue;
				} catch (EntityNotFoundException e) {
					// User does not have this medal

					//Forgive me, Father, for I have sinned
					if (((long)medal.getProperty("precond_points")) > 0){
						if (((long)medal.getProperty("precond_points")) > ((long)userGamificationStatsEnt.getProperty("points"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_level")) > 0){
						if (((long)medal.getProperty("precond_level")) > ((long)userGamificationStatsEnt.getProperty("level"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_num_awarded_medals")) > 0){
						if (((long)medal.getProperty("precond_num_awarded_medals")) > ((long)userGamificationStatsEnt.getProperty("num_awarded_medals"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_created")) > 0){
						if (((long)medal.getProperty("precond_issues_created")) > ((long)userIssueStatsEnt.getProperty("issues_created"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_created_type_vandalism")) > 0){
						if (((long)medal.getProperty("precond_issues_created_type_vandalism")) > ((long)userIssueStatsEnt.getProperty("issues_created_type_vandalism"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_created_type_garbage")) > 0){
						if (((long)medal.getProperty("precond_issues_created_type_garbage")) > ((long)userIssueStatsEnt.getProperty("issues_created_type_garbage"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_created_type_accident")) > 0){
						if (((long)medal.getProperty("precond_issues_created_type_accident")) > ((long)userIssueStatsEnt.getProperty("issues_created_type_accident"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_created_type_streetdmg")) > 0){
						if (((long)medal.getProperty("precond_issues_created_type_streetdmg")) > ((long)userIssueStatsEnt.getProperty("issues_created_type_streetdmg"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_created_type_furnituredmg")) > 0){
						if (((long)medal.getProperty("precond_issues_created_type_furnituredmg")) > ((long)userIssueStatsEnt.getProperty("issues_created_type_furnituredmg"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_created_type_nature")) > 0){
						if (((long)medal.getProperty("precond_issues_created_type_nature")) > ((long)userIssueStatsEnt.getProperty("issues_created_type_nature"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_created_type_other")) > 0){
						if (((long)medal.getProperty("precond_issues_created_type_other")) > ((long)userIssueStatsEnt.getProperty("issues_created_type_other"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_in_progress")) > 0){
						if (((long)medal.getProperty("precond_issues_in_progress")) > ((long)userIssueStatsEnt.getProperty("issues_in_progress"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_solved")) > 0){
						if (((long)medal.getProperty("precond_issues_solved")) > ((long)userIssueStatsEnt.getProperty("issues_solved"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_closed")) > 0){
						if (((long)medal.getProperty("precond_issues_closed")) > ((long)userIssueStatsEnt.getProperty("issues_closed"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_commented")) > 0){
						if (((long)medal.getProperty("precond_issues_commented")) > ((long)userCommentStatsEnt.getProperty("issues_commented"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_commented_type_vandalism")) > 0){
						if (((long)medal.getProperty("precond_issues_commented_type_vandalism")) > ((long)userCommentStatsEnt.getProperty("issues_commented_type_vandalism"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_commented_type_garbage")) > 0){
						if (((long)medal.getProperty("precond_issues_commented_type_garbage")) > ((long)userCommentStatsEnt.getProperty("issues_commented_type_garbage"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_commented_type_accident")) > 0){
						if (((long)medal.getProperty("precond_issues_commented_type_accident")) > ((long)userCommentStatsEnt.getProperty("issues_commented_type_accident"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_commented_type_streetdmg")) > 0){
						if (((long)medal.getProperty("precond_issues_commented_type_streetdmg")) > ((long)userCommentStatsEnt.getProperty("issues_commented_type_streetdmg"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_commented_type_furnituredmg")) > 0){
						if (((long)medal.getProperty("precond_issues_commented_type_furnituredmg")) > ((long)userCommentStatsEnt.getProperty("issues_commented_type_furnituredmg"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_commented_type_nature")) > 0){
						if (((long)medal.getProperty("precond_issues_commented_type_nature")) > ((long)userCommentStatsEnt.getProperty("issues_commented_type_nature"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_commented_type_other")) > 0){
						if (((long)medal.getProperty("precond_issues_commented_type_other")) > ((long)userCommentStatsEnt.getProperty("issues_commented_type_other"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_followed")) > 0){
						if (((long)medal.getProperty("precond_issues_followed")) > ((long)userFollowStatsEnt.getProperty("issues_followed"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_followed_type_vandalism")) > 0){
						if (((long)medal.getProperty("precond_issues_followed_type_vandalism")) > ((long)userFollowStatsEnt.getProperty("issues_followed_type_vandalism"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_followed_type_garbage")) > 0){
						if (((long)medal.getProperty("precond_issues_followed_type_garbage")) > ((long)userFollowStatsEnt.getProperty("issues_followed_type_garbage"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_followed_type_accident")) > 0){
						if (((long)medal.getProperty("precond_issues_followed_type_accident")) > ((long)userFollowStatsEnt.getProperty("issues_followed_type_accident"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_followed_type_streetdmg")) > 0){
						if (((long)medal.getProperty("precond_issues_followed_type_streetdmg")) > ((long)userFollowStatsEnt.getProperty("issues_followed_type_streetdmg"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_followed_type_furnituredmg")) > 0){
						if (((long)medal.getProperty("precond_issues_followed_type_furnituredmg")) > ((long)userFollowStatsEnt.getProperty("issues_followed_type_furnituredmg"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_followed_type_nature")) > 0){
						if (((long)medal.getProperty("precond_issues_followed_type_nature")) > ((long)userFollowStatsEnt.getProperty("issues_followed_type_nature"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_followed_type_other")) > 0){
						if (((long)medal.getProperty("precond_issues_followed_type_other")) > ((long)userFollowStatsEnt.getProperty("issues_followed_type_other"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_rated")) > 0){
						if (((long)medal.getProperty("precond_issues_rated")) > ((long)userRateStatsEnt.getProperty("issues_rated"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_rated_type_vandalism")) > 0){
						if (((long)medal.getProperty("precond_issues_rated_type_vandalism")) > ((long)userRateStatsEnt.getProperty("issues_rated_type_vandalism"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_rated_type_garbage")) > 0){
						if (((long)medal.getProperty("precond_issues_rated_type_garbage")) > ((long)userRateStatsEnt.getProperty("issues_rated_type_garbage"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_rated_type_accident")) > 0){
						if (((long)medal.getProperty("precond_issues_rated_type_accident")) > ((long)userRateStatsEnt.getProperty("issues_rated_type_accident"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_rated_type_streetdmg")) > 0){
						if (((long)medal.getProperty("precond_issues_rated_type_streetdmg")) > ((long)userRateStatsEnt.getProperty("issues_rated_type_streetdmg"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_rated_type_furnituredmg")) > 0){
						if (((long)medal.getProperty("precond_issues_rated_type_furnituredmg")) > ((long)userRateStatsEnt.getProperty("issues_rated_type_furnituredmg"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_rated_type_nature")) > 0){
						if (((long)medal.getProperty("precond_issues_rated_type_nature")) > ((long)userRateStatsEnt.getProperty("issues_rated_type_nature"))){
							continue;
						}
					}
					if (((long)medal.getProperty("precond_issues_rated_type_other")) > 0){
						if (((long)medal.getProperty("precond_issues_rated_type_other")) > ((long)userRateStatsEnt.getProperty("issues_rated_type_other"))){
							continue;
						}
					}

					//All preconditions met, award medal
					Entity userMedal = new Entity("UserMedal", medal.getKey().getId(), userKey);
					userMedal.setProperty("awarded", new Date());
					datastore.put(userMedal);
					pointsInc += (long) medal.getProperty("points");
					numAwardedMedalsInc++;

					//Criar notifica��o
					notifications.add(new Notification(username, "CityAid",
							"https://city-aid.appspot.com/resource/"+((String) medal.getProperty("picture")),
							"Medalha \""+(String) medal.getProperty("title")+"\" ganha! (+"+(long) medal.getProperty("points")+"XP)",
							"https://city-aid.appspot.com/",
							"Medalha ganha",
							"<img src=\"https://city-aid.appspot.com/resource/"+((String) medal.getProperty("picture"))+"\" /><br>Ganhaste a medalha \""+(String) medal.getProperty("title")+"\"! (+"+(long) medal.getProperty("points")+"XP)",
							NotificationType.MEDAL.toString()));
					
					//Create user log entry
					Entity userLogEntry = new Entity("UserLog", userKey);
					userLogEntry.setProperty("date", new Date());
					userLogEntry.setUnindexedProperty("type", UserLogType.MEDAL.toString());
					userLogEntry.setUnindexedProperty("description", "Ganhou a medalha \""+(String)medal.getProperty("title")+"\"");
					userLogEntry.setUnindexedProperty("remote_addr", "Desconhecido");
					userLogEntry.setUnindexedProperty("remote_host", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-City", "Desconhecido");
					userLogEntry.setUnindexedProperty("X-AppEngine-Country", "Desconhecido");
					datastore.put(userLogEntry);
				}
			}

			Transaction txn = datastore.beginTransaction();
			Entity userGamificationStats = datastore.get(txn, userGamificationStatsKey);
			//Update points
			long newPoints = ((long)userGamificationStats.getProperty("points"))+pointsInc;
			if(newPoints < 0)
				newPoints = 0;
			userGamificationStats.setUnindexedProperty("points", newPoints);
			//Update level
			long oldLevel = (long)userGamificationStats.getProperty("level");
			long thresholdCurrentLevel = calculateNextLevelThreshold(oldLevel-1);
			Entity userLogEntry = null;
			if (newPoints < thresholdCurrentLevel && oldLevel > 1) {
				notifications.add(new Notification(username, "CityAid",
						"https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg",
						"Desceste para o nivel "+(oldLevel-1)+".",
						"https://city-aid.appspot.com/",
						"Desceste de nivel",
						"Desceste para o nivel "+(oldLevel-1)+". Tens "+newPoints+" pontos. Tenta n�o continuar a fazer asneira... sim? Sim, obrigado!",
						NotificationType.LEVEL_UP.toString()));
				userGamificationStats.setProperty("level", oldLevel-1);
				
				//Create user log entry
				userLogEntry = new Entity("UserLog", userKey);
				userLogEntry.setProperty("date", new Date());
				userLogEntry.setUnindexedProperty("type", UserLogType.LEVEL_UP.toString());
				userLogEntry.setUnindexedProperty("description", "Desceu para o nivel "+(oldLevel+1));
				userLogEntry.setUnindexedProperty("remote_addr", "Desconhecido");
				userLogEntry.setUnindexedProperty("remote_host", "Desconhecido");
				userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", "Desconhecido");
				userLogEntry.setUnindexedProperty("X-AppEngine-City", "Desconhecido");
				userLogEntry.setUnindexedProperty("X-AppEngine-Country", "Desconhecido");
			}
			long thresholdNextLevel = calculateNextLevelThreshold(oldLevel);
			if (newPoints >= thresholdNextLevel){
				notifications.add(new Notification(username, "CityAid",
						"https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg",
						"Subiste para o nivel "+(oldLevel+1)+"!",
						"https://city-aid.appspot.com/",
						"Subiste de nivel",
						"Subiste para o nivel "+(oldLevel+1)+"! Tens "+newPoints+" pontos.",
						NotificationType.LEVEL_UP.toString()));
				userGamificationStats.setProperty("level", oldLevel+1);
				
				//Create user log entry
				userLogEntry = new Entity("UserLog", userKey);
				userLogEntry.setProperty("date", new Date());
				userLogEntry.setUnindexedProperty("type", UserLogType.LEVEL_UP.toString());
				userLogEntry.setUnindexedProperty("description", "Subiu para o nivel "+(oldLevel+1));
				userLogEntry.setUnindexedProperty("remote_addr", "Desconhecido");
				userLogEntry.setUnindexedProperty("remote_host", "Desconhecido");
				userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", "Desconhecido");
				userLogEntry.setUnindexedProperty("X-AppEngine-City", "Desconhecido");
				userLogEntry.setUnindexedProperty("X-AppEngine-Country", "Desconhecido");
			}
			//Update number awarded medals
			userGamificationStats.setUnindexedProperty("num_awarded_medals", ((long)userGamificationStats.getProperty("num_awarded_medals"))+numAwardedMedalsInc);
			datastore.put(txn, userGamificationStats);
			txn.commit();
			//Add log entry for level change if it occurred (it's out here because transactions)
			if (userLogEntry != null){
				datastore.put(userLogEntry);
			}
			
			//Send notifications
			NotificationResource.sendNotifications(notifications);
			
			return Response.status(Status.NO_CONTENT).build();
		} catch (EntityNotFoundException e) {
			// User does not exist
			LOG.warning("\n\n\n\n\n\n\n[UPDATE USER GAMIFICATION] User does not exist: " + username);
			return Response.status(Status.NO_CONTENT).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
		}
	}
	
	@PUT
	@Path("/points")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updatePoints(GamificationPoints data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) && ((boolean)user.getProperty("isManager"))){
					//User has permission to perform operation
					txn.commit();
					
					//Update gamification points
					Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
					Entity appPoints = datastore.get(appPointsKey);
					appPoints.setUnindexedProperty("modLevel", data.modLevel);
					appPoints.setUnindexedProperty("createIssue", data.createIssue);
					appPoints.setUnindexedProperty("comment", data.comment);
					appPoints.setUnindexedProperty("rateIssue", data.rateIssue);
					appPoints.setUnindexedProperty("rateEntity", data.rateEntity);
					appPoints.setUnindexedProperty("deleteIssue", data.deleteIssue);
					appPoints.setUnindexedProperty("deleteComment", data.deleteComment);
					appPoints.setUnindexedProperty("issueWIP", data.issueWIP);
					appPoints.setUnindexedProperty("issueClosed", data.issueClosed);
					appPoints.setUnindexedProperty("issueSolved", data.issueSolved);
					datastore.put(appPoints);
					
					LOG.info("Points updated");
					return Response.status(Status.NO_CONTENT).build();
				} else {
					// User does not belong to required role
					txn.rollback();

					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@PUT
	@Path("/medals")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response activateMedal(MedalID data, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()) {
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())
						&& ((boolean)user.getProperty("isManager"))) {
					
					//User has permission to perform operation
					txn.commit();
					Key medalKey = KeyFactory.createKey("Medal", data.medal);
					try {
						Entity medal = datastore.get(medalKey);
						boolean active = (boolean) medal.getProperty("active");
						medal.setProperty("active", !active);
						datastore.put(medal);
						
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						//Medal does not exist
						
						LOG.warning("Medal does not exist: " + data);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.MEDAL_NOT_EXISTS))).build();
					}
				} else {
					//User does not belong to required role
					txn.rollback();

					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/points")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAppPoints(@Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				// User has permission to perform operation
				txn.commit();

				Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
				try {
					Entity appPoints = datastore.get(appPointsKey);
					
					GamificationPoints points = new GamificationPoints((long)appPoints.getProperty("modLevel"),
							(long)appPoints.getProperty("createIssue"),
							(long)appPoints.getProperty("comment"),
							(long)appPoints.getProperty("rateIssue"),
							(long)appPoints.getProperty("rateEntity"),
							(long)appPoints.getProperty("deleteIssue"),
							(long)appPoints.getProperty("deleteComment"),
							(long)appPoints.getProperty("issueWIP"),
							(long)appPoints.getProperty("issueClosed"),
							(long)appPoints.getProperty("issueSolved"));
					return Response.status(Status.OK).entity(g.toJson(points)).build();
					
				} catch (EntityNotFoundException e) {
					// App points does not exist
					LOG.warning("App points does not exist");
					return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	/**
	 * Calculates the number of total points necessary to reach the level after the given one
	 * @param level		level previous to next one
	 * @return threshold to next level
	 */
	public static long calculateNextLevelThreshold(long level){
		return (long) Math.ceil(10 * Math.pow(level, 1.5));
	}
	
	/**
	 * Upload medal picture to Google Cloud Storage
	 * @param base64Photo	picture data enconded in base 64
	 * @param extension		picture file extension
	 * @return photoName	filename in Cloud Storage
	 * @throws IOException 
	 */
	private String uploadMedalPicture(String base64Photo, String extension) throws IOException{
		//Generate photo name
		String photoName = "medal_"+System.currentTimeMillis()+"."+extension;
		
		//Decode base64 photo
		byte[] binPhoto = Base64.decodeBase64(base64Photo);
	    
		//Upload to Cloud Storage
		GcsFileOptions instance = new GcsFileOptions.Builder().mimeType("image/"+extension).build();
	    GcsFilename fileName = new GcsFilename("city-aid.appspot.com", photoName);
	    GcsOutputChannel outputChannel = gcsService.createOrReplace(fileName, instance);
	    OutputStream output = Channels.newOutputStream(outputChannel);
		try {
			output.write(binPhoto, 0, binPhoto.length);
		} finally {
			output.close();
		}
		
		return photoName;
	}
}
