package com.codefive.cityaid.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.AvatarFactory;
import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.User;
import com.codefive.cityaid.util.replyData.UserListing;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.gson.Gson;

@Path("/users")
public class UsersResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(UsersResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public UsersResource() { } //Nothing to be done here...

	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getUsers(@QueryParam("cursor") String cursor,
			@QueryParam("page_size") long page_size, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity userEnt = datastore.get(txn, session.getParent());
				if (((String)userEnt.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					txn.commit();
					
					Query query = new Query("User").addSort(Entity.KEY_RESERVED_PROPERTY, SortDirection.ASCENDING);
					query.setFilter(new FilterPredicate("hidden", FilterOperator.EQUAL, false));
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit((int) page_size);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> users = datastore.prepare(query).asQueryResultList(fetchOptions);
					
					// Create response
					List<User> res = new ArrayList<User>(users.size());
					
					for (Entity result : users) {
						User user = new User(result.getKey().getName(), (String) result.getProperty("role"),
							(String) result.getProperty("name"), (String) result.getProperty("email"),
							(String) result.getProperty("address"), (String) result.getProperty("phone"),
							(String) result.getProperty("gender"), (Date) result.getProperty("birth_date"),
							(Date) result.getProperty("registered_since"), (boolean) result.getProperty("isManager"),
							(String) result.getProperty("employer"),(String) result.getProperty("job"), 
							AvatarFactory.getAvatarUrl((String) result.getProperty("email")),
							(boolean) result.getProperty("frozen"), 0, 1);
						
						res.add(user);
					}
					
					// Cursor
					Cursor c = users.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					UserListing ret = new UserListing(res, cursor);
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} else {
					// User does not belong to required role
					txn.rollback();

					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();
	
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
	
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@Path("/reporters")
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getReporters(@QueryParam("cursor") String cursor,
			@QueryParam("page_size") long page_size, @Context HttpHeaders headers) {
		
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity userEnt = datastore.get(txn, session.getParent());
				if (((String)userEnt.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					txn.commit();
				
					Query query = new Query("User").addSort(Entity.KEY_RESERVED_PROPERTY, SortDirection.ASCENDING);
					query.setFilter(new FilterPredicate("hidden", FilterOperator.EQUAL, false));
					query.setFilter(new FilterPredicate("role", FilterOperator.EQUAL, UserRole.REPORTER.toString()));
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit((int) page_size);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> users = datastore.prepare(query).asQueryResultList(fetchOptions);
					
					// Create response
					List<User> res = new ArrayList<User>(users.size());
					
					for (Entity result : users) {
						User user = new User(result.getKey().getName(), (String) result.getProperty("role"),
							(String) result.getProperty("name"), (String) result.getProperty("email"),
							(String) result.getProperty("address"), (String) result.getProperty("phone"),
							(String) result.getProperty("gender"), (Date) result.getProperty("birth_date"),
							(Date) result.getProperty("registered_since"), (boolean) result.getProperty("isManager"),
							(String) result.getProperty("employer"),(String) result.getProperty("job"), 
							AvatarFactory.getAvatarUrl((String) result.getProperty("email")),
							(boolean) result.getProperty("frozen"), 0, 1);
						
						res.add(user);
					}
					
					// Cursor
					Cursor c = users.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					UserListing ret = new UserListing(res, cursor);
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} else {
					// User does not belong to required role
					txn.rollback();

					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/workers")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getWorkers(@QueryParam("cursor") String cursor,
			@QueryParam("page_size") long page_size, @Context HttpHeaders headers) {
		
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity userEnt = datastore.get(txn, session.getParent());
				if (((String)userEnt.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					txn.commit();
					
					Query query = new Query("User").addSort(Entity.KEY_RESERVED_PROPERTY, SortDirection.ASCENDING);
					query.setFilter(new FilterPredicate("role", FilterOperator.EQUAL, UserRole.WORKER.toString()));
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit((int) page_size);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> users = datastore.prepare(query).asQueryResultList(fetchOptions);
					
					// Create response
					List<User> res = new ArrayList<User>(users.size());
					
					for (Entity result : users) {
						User user = new User(result.getKey().getName(), (String) result.getProperty("role"),
							(String) result.getProperty("name"), (String) result.getProperty("email"),
							(String) result.getProperty("address"), (String) result.getProperty("phone"),
							(String) result.getProperty("gender"), (Date) result.getProperty("birth_date"),
							(Date) result.getProperty("registered_since"), (boolean) result.getProperty("isManager"),
							(String) result.getProperty("employer"),(String) result.getProperty("job"), 
							AvatarFactory.getAvatarUrl((String) result.getProperty("email")),
							(boolean) result.getProperty("frozen"), 0, 1);
						
						res.add(user);
					}
					
					// Cursor
					Cursor c = users.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					UserListing ret = new UserListing(res, cursor);
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} else {
					// User does not belong to required role
					txn.rollback();
	
					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
}
