package com.codefive.cityaid.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.IssueLogType;
import com.codefive.cityaid.util.enums.IssueStatus;
import com.codefive.cityaid.util.enums.IssueType;
import com.codefive.cityaid.util.enums.NotificationType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.Issue;
import com.codefive.cityaid.util.replyData.IssueLogEntry;
import com.codefive.cityaid.util.replyData.IssueLogListing;
import com.codefive.cityaid.util.replyData.MinEntity;
import com.codefive.cityaid.util.requestData.CommentData;
import com.codefive.cityaid.util.requestData.IssueData;
import com.codefive.cityaid.util.requestData.IssueEditData;
import com.codefive.cityaid.util.requestData.IssueEntityData;
import com.codefive.cityaid.util.requestData.IssuePhotoData;
import com.codefive.cityaid.util.requestData.IssueStatusData;
import com.codefive.cityaid.util.requestData.PictureData;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.appengine.api.images.Image;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.Transform;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.gson.Gson;

@Path("/issues")
public class IssueResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(IssueResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	/**
	* This is where backoff parameters are configured. Here it is aggressively retrying with
	* backoff, up to 10 times but taking no more that 15 seconds total to do so.
	*/
	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
															.initialRetryDelayMillis(10)
															.retryMaxAttempts(10)
															.totalRetryPeriodMillis(15000)
															.build());
	private static final int MAX_NUM_PHOTOS_UPLOAD = 10;
	private static final int PAGE_SIZE = 10; 
	
	public IssueResource() { } //Nothing to be done here...
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response submitIssue(IssueData data, @Context HttpServletRequest request, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid() || (data.pictures != null && data.pictures.size() > MAX_NUM_PHOTOS_UPLOAD)){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				//User has permission to perform operation
				
				//Save photos on Cloud Storage
				List<String> pictureIds = null;
				List<String> thumbnailIds = null;
				if (data.pictures != null) {
					try {
						pictureIds = new ArrayList<String>(data.pictures.size());
						thumbnailIds = new ArrayList<String>(data.pictures.size());
						for (PictureData picture : data.pictures){
							String photoId = uploadIssuePhoto(authUsername, picture.base64, picture.extension);
							pictureIds.add(photoId+"."+picture.extension);
							thumbnailIds.add(photoId+"_thumb."+picture.extension);
						}
					} catch (IOException e) {
						LOG.warning("Failed to upload issue photo. Exception: "+e.getMessage());
					} catch (Exception e) {
						LOG.warning("Something happenned. Exception: "+e.getMessage());
					}
				}
				
				//Create issue
				Entity issue = new Entity("Occurrence", KeyFactory.createKey("User", authUsername));
				issue.setProperty("status", IssueStatus.CREATED.toString());
				issue.setUnindexedProperty("title", data.title);
				issue.setUnindexedProperty("description", data.description);
				issue.setProperty("latitude", data.latitude);
				issue.setProperty("longitude", data.longitude);
				issue.setProperty("type", data.type);
				issue.setProperty("creation_date", new Date());
				issue.setUnindexedProperty("picture_ids", pictureIds);
				issue.setUnindexedProperty("thumbnail_ids", thumbnailIds);
				issue.setProperty("rating", data.priority);
				issue.setUnindexedProperty("forceRating", false);
				issue.setProperty("hidden", false);
				datastore.put(issue);
				
				//Create issue rating
				Entity rating = new Entity("Rating", authUsername, issue.getKey());
				rating.setIndexedProperty("priority", data.priority);
				datastore.put(rating);
				
				String issueKeyStr = KeyFactory.keyToString(issue.getKey());
				
				//Follow issue
				Key userOptionsKey = KeyFactory.createKey("UserOptions", authUsername);
				Entity userOptionsEnt = datastore.get(userOptionsKey);
				if ((boolean) userOptionsEnt.getProperty("auto_follow")){
					//User wants to automatically follow issue
					Entity follow = new Entity("IssueFollow");
					follow.setProperty("follower", authUsername);
					follow.setProperty("issue", issueKeyStr);
					follow.setProperty("hidden", false);
					datastore.put(follow);
				}
				
				//Create user log entry
				Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
				userLogEntry.setProperty("date", new Date());
				userLogEntry.setUnindexedProperty("type", UserLogType.O_CREATED.toString());
				userLogEntry.setUnindexedProperty("description", "Ocorr�ncia \""+data.title+"\" submetida");
				userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
				userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
				userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
				userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
				userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
				datastore.put(userLogEntry);
				
				//Add issue log entry
				Entity issueLogEntry = new Entity("IssueLog", issue.getKey());
				issueLogEntry.setProperty("date", new Date());
				issueLogEntry.setUnindexedProperty("type", IssueLogType.CREATED.toString());
				issueLogEntry.setUnindexedProperty("user", authUsername);
				issueLogEntry.setUnindexedProperty("description", "Ocorr�ncia criada");
				datastore.put(issueLogEntry);
				
				
				//Update points
				Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
				Entity appPoints = datastore.get(appPointsKey);
				Transaction txn = datastore.beginTransaction();
				Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
				Entity userGamificationStats = datastore.get(txn, userGamificationStatsKey);
				long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("createIssue"));
				userGamificationStats.setUnindexedProperty("points", newPoints);
				datastore.put(txn, userGamificationStats);
				txn.commit();
				
				txn = datastore.beginTransaction();
				//Get user stats
				Key userIssueStatsKey = KeyFactory.createKey("UserIssueStats", authUsername);
				Entity userIssueStatsEnt = datastore.get(txn, userIssueStatsKey);
				
				//Update user stats
				userIssueStatsEnt.setUnindexedProperty("issues_created", ((long)userIssueStatsEnt.getProperty("issues_created"))+1);
				switch(IssueType.valueOf(data.type.toUpperCase())){
					case ACCIDENT:
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_accident", ((long)userIssueStatsEnt.getProperty("issues_created_type_accident"))+1);
						break;
					case FURNITURE_DAMAGE:
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_furnituredmg", ((long)userIssueStatsEnt.getProperty("issues_created_type_furnituredmg"))+1);
						break;
					case GARBAGE:
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_garbage", ((long)userIssueStatsEnt.getProperty("issues_created_type_garbage"))+1);
						break;
					case NATURE:
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_nature", ((long)userIssueStatsEnt.getProperty("issues_created_type_nature"))+1);
						break;
					case OTHER:
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_other", ((long)userIssueStatsEnt.getProperty("issues_created_type_other"))+1);
						break;
					case STREET_DAMAGE:
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_streetdmg", ((long)userIssueStatsEnt.getProperty("issues_created_type_streetdmg"))+1);
						break;
					case VANDALISM:
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_vandalism", ((long)userIssueStatsEnt.getProperty("issues_created_type_vandalism"))+1);
						break;
					default:
						break;					
				}
				datastore.put(txn, userIssueStatsEnt);
				
				txn.commit();
				
				//Update gamification system
				Queue queue = QueueFactory.getDefaultQueue();
				queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+authUsername));
				
				LOG.info("Issue registered: " + issueKeyStr);
				return Response.status(Status.OK).entity("{\"issue\":\""+issueKeyStr+"\"}").build();
			} else {
				//Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}

	@SuppressWarnings("unchecked")
	@GET
	@Path("/{issue}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response fetchIssueData(@PathParam("issue") String issueKeyStr, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				//User has permission to perform operation
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				try {
					//Get issue
					Entity issueEnt = datastore.get(txn, issueKey);
					
					//Get assigned entities
					List<String> entityIds = (List<String>) issueEnt.getProperty("entities");
					if (entityIds == null){
						entityIds = new ArrayList<String>(1);
					}
					List<MinEntity> entities = new ArrayList<MinEntity>(entityIds.size());
					for (String entityId : entityIds) {
						Key entityKey = KeyFactory.createKey("Entity", entityId);
						// Get entity
						Entity entityEnt = datastore.get(txn, entityKey);

						// Create response
						MinEntity entity = new MinEntity(entityId, (String) entityEnt.getProperty("name"));
						entities.add(entity);
					}
					
					Filter followerFilter = new FilterPredicate("follower", FilterOperator.EQUAL, authUsername);
					Filter issueFilter = new FilterPredicate("issue", FilterOperator.EQUAL, issueKeyStr);
					Query watchlistQuery = new Query("IssueFollow").setFilter(CompositeFilterOperator.and(followerFilter, issueFilter));
					List<Entity> watchlistResults = datastore.prepare(watchlistQuery).asList(FetchOptions.Builder.withDefaults());
					boolean following = watchlistResults.size() > 0;
					
					//Create response
					Issue issue = new Issue(issueKeyStr, (String) issueEnt.getProperty("status"),
								(Date) issueEnt.getProperty("creation_date"), issueKey.getParent().getName(),
								(String) issueEnt.getProperty("title"), (String) issueEnt.getProperty("description"),
								(double) issueEnt.getProperty("latitude"), (double) issueEnt.getProperty("longitude"),
								(String) issueEnt.getProperty("type"), (long) issueEnt.getProperty("rating"),
								following, (List<String>) issueEnt.getProperty("picture_ids"),
								(List<String>) issueEnt.getProperty("thumbnail_ids"), entities);
					
					txn.commit();
					
					return Response.status(Status.OK).entity(g.toJson(issue)).build();
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					txn.rollback();
					
					LOG.warning("Issue does not exist: " + issueKeyStr);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/{issue}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response deleteIssue(@PathParam("issue") String issueKeyStr, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity user = datastore.get(txn, session.getParent());
				Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
				Entity userGamificationStats = datastore.get(txn, userGamificationStatsKey);
				Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
				Entity appPoints = datastore.get(appPointsKey);
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				try {
					//Get issue
					Entity issueEnt = datastore.get(txn, issueKey);
					if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) || issueEnt.getParent().equals(user.getKey()) ||
							((long)userGamificationStats.getProperty("level") >= (long)appPoints.getProperty("modLevel"))) {
						
						//User has permission to perform operation
						if(!(boolean) issueEnt.getProperty("hidden")) {
							
							//Add issue log entry
							Entity issueLogEntry = new Entity("IssueLog", issueKey);
							issueLogEntry.setProperty("date", new Date());
							issueLogEntry.setUnindexedProperty("type", IssueLogType.DELETED.toString());
							issueLogEntry.setUnindexedProperty("user", user.getKey().getName());
							datastore.put(txn, issueLogEntry);
							
							//Get user stats
							Key userIssueStatsKey = KeyFactory.createKey("UserIssueStats", issueEnt.getParent().getName());
							Entity userIssueStatsEnt = datastore.get(userIssueStatsKey);
							
							//Update user stats
							if(((String)issueEnt.getProperty("status")).equals(IssueStatus.CREATED))
								userIssueStatsEnt.setUnindexedProperty("issues_created", ((long)userIssueStatsEnt.getProperty("issues_created"))-1);
							else if(((String)issueEnt.getProperty("status")).equals(IssueStatus.WORK_IN_PROGRESS))
								userIssueStatsEnt.setUnindexedProperty("issues_in_progress", ((long)userIssueStatsEnt.getProperty("issues_in_progress"))-1);
							else if(((String)issueEnt.getProperty("status")).equals(IssueStatus.SOLVED))
								userIssueStatsEnt.setUnindexedProperty("issues_solved", ((long)userIssueStatsEnt.getProperty("issues_solved"))-1);
							datastore.put(txn, userIssueStatsEnt);
							
							//User didnt submit the issue
							if(!issueEnt.getParent().getName().equals(authUsername)) {
								userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", issueEnt.getParent().getName());
								userGamificationStats = datastore.get(txn, userGamificationStatsKey);

								//Update points
								long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("deleteIssue"));
								if(newPoints < 0)
									newPoints = 0;
								userGamificationStats.setUnindexedProperty("points", newPoints);
								datastore.put(txn, userGamificationStats);
							}
							//Delete all issue follows
							Filter followerFilter = new FilterPredicate("follower", FilterOperator.EQUAL, issueEnt.getParent().getName());
							Query watchlistQuery = new Query("IssueFollow").setFilter(followerFilter);
							
							List<Entity> results = datastore.prepare(txn, watchlistQuery).asList(FetchOptions.Builder.withDefaults());
														
							for(Entity i: results) {
								i.setProperty("hidden", true);
								datastore.put(txn, i);
							}
							
							issueEnt.setProperty("hidden", true);
							datastore.put(txn, issueEnt);
							txn.commit();
							return Response.status(Status.NO_CONTENT).build();
						} else {
							//Issue already deleted
							txn.rollback();
							return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_ALREADY_DELETED))).build();
						}
					} else {
						//User does not belong to required role
						txn.rollback();
						
						LOG.warning("User attempted to perform operation without required access level: " + authUsername);
						return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
					}
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					txn.rollback();
					
					LOG.warning("Issue does not exist: " + issueKeyStr);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@POST
	@Path("/{issue}/entities")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response assignEntity(@PathParam("issue") String issueKeyStr, @Context HttpHeaders headers, IssueEntityData data) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					Key issueKey = KeyFactory.stringToKey(issueKeyStr);
					try {
						//Get issue
						Entity issueEnt = datastore.get(txn, issueKey);
						
						try {
							//Get entity
							Key entityKey = KeyFactory.createKey("Entity", data.entity);
							Entity entityEnt = datastore.get(txn, entityKey);

							//Update issue data
							@SuppressWarnings("unchecked")
							List<String> entities = (List<String>) issueEnt.getProperty("entities");
							if (entities == null){
								entities = new ArrayList<String>(1);
							}
							if (entities.contains(data.entity)) {
								//Entity is already assigned
								LOG.info("Entity ("+data.entity+") already is assigned " + issueKeyStr);
								txn.rollback();
								
								return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_ALREADY_ASSIGNED))).build();
							}
							entities.add(data.entity);
							String oldStatus = (String) issueEnt.getProperty("status");
							issueEnt.setProperty("status", IssueStatus.WORK_IN_PROGRESS.toString());
							issueEnt.setProperty("entities", entities);
							datastore.put(txn, issueEnt);
							
							//Add issue log entry
							Entity issueLogEntry = new Entity("IssueLog", issueKey);
							issueLogEntry.setProperty("date", new Date());
							issueLogEntry.setUnindexedProperty("type", IssueLogType.STATUS_CHANGED.toString());
							issueLogEntry.setUnindexedProperty("user", user.getKey().getName());
							issueLogEntry.setUnindexedProperty("description", "A entidade "+((String)entityEnt.getProperty("name"))+" irá resolver esta ocorrência");
							datastore.put(txn, issueLogEntry);
							
							if (oldStatus != IssueStatus.WORK_IN_PROGRESS.value()){
								//Get user stats
								Key userIssueStatsKey = KeyFactory.createKey("UserIssueStats", issueEnt.getParent().getName());
								Entity userIssueStatsEnt = datastore.get(userIssueStatsKey);
								
								//Update user stats
								userIssueStatsEnt.setUnindexedProperty("issues_in_progress", ((long)userIssueStatsEnt.getProperty("issues_in_progress"))+1);
								datastore.put(txn, userIssueStatsEnt);
																
								//Update points
								Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
								Entity appPoints = datastore.get(txn, appPointsKey);
								Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", issueEnt.getParent().getName());
								Entity userGamificationStats = datastore.get(userGamificationStatsKey);
								long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("issueWIP"));
								userGamificationStats.setUnindexedProperty("points", newPoints);
								datastore.put(txn, userGamificationStats);
							}

							txn.commit();

							//Update gamification system and notify followers
							Queue queue = QueueFactory.getDefaultQueue();
							queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+issueEnt.getParent().getName()));
							queue.add(TaskOptions.Builder.withUrl("/rest/notifications/followers/"+issueKeyStr).param("title", "CityAid")
									.param("icon", "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg")
									.param("body", "A entidade "+((String)entityEnt.getProperty("name"))+" irá resolver a ocorrência que estás a seguir.")
									.param("url", "https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr)
									.param("emailTitle", "Ocorrência a ser resolvida")
									.param("emailBody", "A ocorrência <a href=\"https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr+"\">\""+((String)issueEnt.getProperty("title"))+"\"</a> irá ser resolvida pela entidade "+((String)entityEnt.getProperty("name"))+".")
									.param("type", NotificationType.ISSUE_WIP.toString()));
							
							return Response.status(Status.NO_CONTENT).build();
						} catch (EntityNotFoundException e1){
							// Entity does not exist
							txn.rollback();
							
							LOG.warning("Entity does not exist: " + data.entity);
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ENTITY_NOT_EXISTS))).build();
						} finally {
							if (txn.isActive()) {
								txn.rollback();
								return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
							}
						}
					} catch (EntityNotFoundException e) {
						// Issue does not exist
						txn.rollback();
						
						LOG.warning("Issue does not exist: " + issueKeyStr);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//User does not belong to required role
					txn.rollback();
					
					LOG.warning("User attempted to perform operation without required access level: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@PUT
	@Path("/{issue}/status")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response closeSolveIssue(@PathParam("issue") String issueKeyStr, @Context HttpHeaders headers, IssueStatusData data) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				try {
					//Get issue
					Entity issueEnt = datastore.get(txn, issueKey);
					
					//Check if we're not repeating ops
					if (issueEnt.getProperty("status").equals(IssueStatus.CLOSED.toString())){
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_ALREADY_CLOSED))).build();
					}
					if (issueEnt.getProperty("status").equals(IssueStatus.SOLVED.toString())){
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_ALREADY_SOLVED))).build();
					}
					
					if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
						//User is backoffice; User has permission
						
						//Update status
						issueEnt.setProperty("status", IssueStatus.CLOSED.toString());
						issueEnt.setUnindexedProperty("forceRating", true);
						datastore.put(txn, issueEnt);
						
						//Add issue log entry
						Entity issueLogEntry = new Entity("IssueLog", issueKey);
						issueLogEntry.setProperty("date", new Date());
						issueLogEntry.setUnindexedProperty("type", IssueLogType.STATUS_CHANGED.toString());
						issueLogEntry.setUnindexedProperty("user", authUsername);
						issueLogEntry.setUnindexedProperty("description", "Ocorrência fechada: "+data.comment);
						datastore.put(txn, issueLogEntry);
						
						//Get user stats
						Key userIssueStatsKey = KeyFactory.createKey("UserIssueStats", issueEnt.getParent().getName());
						Entity userIssueStatsEnt = datastore.get(userIssueStatsKey);
						
						//Update user stats
						userIssueStatsEnt.setUnindexedProperty("issues_in_progress", ((long)userIssueStatsEnt.getProperty("issues_in_progress"))-1);
						userIssueStatsEnt.setUnindexedProperty("issues_closed", ((long)userIssueStatsEnt.getProperty("issues_closed"))+1);
						datastore.put(txn, userIssueStatsEnt);
						
						//Update points
						Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
						Entity appPoints = datastore.get(txn, appPointsKey);
						Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", issueEnt.getParent().getName());
						Entity userGamificationStats = datastore.get(userGamificationStatsKey);
						long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("issueClosed"));
						userGamificationStats.setUnindexedProperty("points", newPoints);
						datastore.put(txn, userGamificationStats);
						
						txn.commit();
						
						//Update gamification system
						Queue queue = QueueFactory.getDefaultQueue();
						queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+issueEnt.getParent().getName()));
						queue.add(TaskOptions.Builder.withUrl("/rest/notifications/followers/"+issueKeyStr).param("title", "CityAid")
																.param("icon", "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg")
																.param("body", "A ocorr�ncia que est�s a seguir foi fechada")
																.param("url", "https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr)
																.param("emailTitle", "Ocorr�ncia fechada")
																.param("emailBody", "A ocorr�ncia <a href=\"https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr+"\">\""+((String)issueEnt.getProperty("title"))+"\"</a> foi fechada.")
																.param("type", NotificationType.ISSUE_CLOSED.toString()));
						
						return Response.status(Status.NO_CONTENT).build();
					} else if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.WORKER.name())){
						//User is worker
						
						@SuppressWarnings("unchecked")
						List<String> entities = (List<String>) issueEnt.getProperty("entities");
						if (entities == null){
							entities = new ArrayList<String>(1);
						}
						if (entities.contains((String)user.getProperty("employer"))) {
							//User employer is assigned to issue; User has permission

							//Update status
							issueEnt.setProperty("status", IssueStatus.SOLVED.toString());
							datastore.put(txn, issueEnt);
							
							//Add issue log entry
							Entity issueLogEntry = new Entity("IssueLog", issueKey);
							issueLogEntry.setProperty("date", new Date());
							issueLogEntry.setUnindexedProperty("type", IssueLogType.STATUS_CHANGED.toString());
							issueLogEntry.setUnindexedProperty("user", authUsername);
							issueLogEntry.setUnindexedProperty("description", "Ocorr�ncia resolvida: "+data.comment);
							datastore.put(txn, issueLogEntry);
							
							//Create user log entry
							Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
							userLogEntry.setProperty("date", new Date());
							userLogEntry.setUnindexedProperty("type", UserLogType.SOLVED_ISSUE.toString());
							userLogEntry.setUnindexedProperty("description", "Resolveu a ocorr�ncia \""+(String)issueEnt.getProperty("title")+"\"");
							userLogEntry.setUnindexedProperty("remote_addr", "Desconhecido");
							userLogEntry.setUnindexedProperty("remote_host", "Desconhecido");
							userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
							userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
							userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
							datastore.put(userLogEntry);
							
							//Get user stats
							Key userIssueStatsKey = KeyFactory.createKey("UserIssueStats", issueEnt.getParent().getName());
							Entity userIssueStatsEnt = datastore.get(userIssueStatsKey);
							
							//Update user stats
							userIssueStatsEnt.setUnindexedProperty("issues_in_progress", ((long)userIssueStatsEnt.getProperty("issues_in_progress"))-1);
							userIssueStatsEnt.setUnindexedProperty("issues_solved", ((long)userIssueStatsEnt.getProperty("issues_solved"))+1);
							datastore.put(txn, userIssueStatsEnt);
							
							//Update points
							Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
							Entity appPoints = datastore.get(txn, appPointsKey);
							Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", issueEnt.getParent().getName());
							Entity userGamificationStats = datastore.get(userGamificationStatsKey);
							long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("issueSolved"));
							userGamificationStats.setUnindexedProperty("points", newPoints);
							datastore.put(txn, userGamificationStats);
							
							txn.commit();
							
							//Update gamification system
							Queue queue = QueueFactory.getDefaultQueue();
							queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+issueEnt.getParent().getName()));
							queue.add(TaskOptions.Builder.withUrl("/rest/notifications/followers/"+issueKeyStr).param("title", "CityAid")
																.param("icon", "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg")
																.param("body", "A ocorrência que estás a seguir foi resolvida")
																.param("url", "https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr)
																.param("emailTitle", "Ocorrência resolvida")
																.param("emailBody", "A ocorrência <a href=\"https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr+"\">\""+((String)issueEnt.getProperty("title"))+"\"</a> foi resolvida.")
																.param("type", NotificationType.ISSUE_SOLVED.toString()));
							
							return Response.status(Status.NO_CONTENT).build();
						} else {
							//User employer is not assigned to issue
							txn.rollback();
							
							LOG.warning("User attempted to perform operation without required access level: " + authUsername);
							return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
						}
					} else {
						//Wrong role
						txn.rollback();
						
						LOG.warning("User attempted to perform operation without required access level: " + authUsername);
						return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
					}
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					txn.rollback();
					
					LOG.warning("Issue does not exist: " + issueKeyStr);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/{issue}/log")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewIssueLog(@PathParam("issue") String issueKeyStr, @QueryParam("cursor") String cursor,
			@Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				//User has permission to perform operation
				txn.commit();
				
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				try {
					// Get issue
					datastore.get(issueKey);

					// Retrieve issue log entries
					Query query = new Query("IssueLog").setAncestor(issueKey).addSort("date", SortDirection.DESCENDING);
					
					FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
					
					if (cursor != null && !cursor.equals("null")) {
						fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
					}
					
					QueryResultList<Entity> results = datastore.prepare(query).asQueryResultList(fetchOptions);

					List<IssueLogEntry> entries = new ArrayList<IssueLogEntry>(results.size());
					for (Entity entry : results) {
						entries.add(new IssueLogEntry(KeyFactory.keyToString(entry.getKey()),
								(Date) entry.getProperty("date"), (String) entry.getProperty("type"),
								(String) entry.getProperty("description"), (String) entry.getProperty("user")));
					}
					
					// Cursor
					Cursor c = results.getCursor();
					if (c != null){
						cursor = c.toWebSafeString();
					} else {
						cursor = null;
					}
					
					IssueLogListing ret = new IssueLogListing(entries, cursor);
					return Response.status(Status.OK).entity(g.toJson(ret)).build();
				} catch (EntityNotFoundException e) {
					// Issue does not exist

					LOG.warning("Issue does not exist: " + issueKeyStr);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@POST
	@Path("/{issue}/log")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response submitLogComment(@PathParam("issue") String issueKeyStr, CommentData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				try {
					//Get issue
					Entity issueEnt = datastore.get(txn, issueKey);
					
					if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
						//User is backoffice; User has permission
						
						//Add issue log entry
						Entity issueLogEntry = new Entity("IssueLog", issueKey);
						issueLogEntry.setProperty("date", new Date());
						issueLogEntry.setUnindexedProperty("type", IssueLogType.LOG_COMMENT.toString());
						issueLogEntry.setUnindexedProperty("user", authUsername);
						issueLogEntry.setUnindexedProperty("description", data.comment);
						datastore.put(txn, issueLogEntry);
						
						txn.commit();
							
						return Response.status(Status.NO_CONTENT).build();
					} else if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.WORKER.name())){
						//User is worker
						
						@SuppressWarnings("unchecked")
						List<String> entities = (List<String>) issueEnt.getProperty("entities");
						if (entities == null){
							entities = new ArrayList<String>(1);
						}
						if (entities.contains((String)user.getProperty("employer"))) {
							//User employer is assigned to issue; User has permission
							
							//Add issue log entry
							Entity issueLogEntry = new Entity("IssueLog", issueKey);
							issueLogEntry.setProperty("date", new Date());
							issueLogEntry.setUnindexedProperty("type", IssueLogType.LOG_COMMENT.toString());
							issueLogEntry.setUnindexedProperty("user", authUsername);
							issueLogEntry.setUnindexedProperty("description", data.comment);
							datastore.put(txn, issueLogEntry);
							
							txn.commit();
							
							//Create user log entry
							Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
							userLogEntry.setProperty("date", new Date());
							userLogEntry.setUnindexedProperty("type", UserLogType.ISSUE_LOG_COMMENT.toString());
							userLogEntry.setUnindexedProperty("description", "Comunicou atualiza��es na ocorr�ncia \""+(String)issueEnt.getProperty("title")+"\"");
							userLogEntry.setUnindexedProperty("remote_addr", "Desconhecido");
							userLogEntry.setUnindexedProperty("remote_host", "Desconhecido");
							userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
							userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
							userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
							datastore.put(userLogEntry);
							
							//Notify followers
							Queue queue = QueueFactory.getDefaultQueue();
							queue.add(TaskOptions.Builder.withUrl("/rest/notifications/followers/"+issueKeyStr)
															.param("title", "CityAid")
															.param("icon", "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg")
															.param("body", authUsername+" submeteu uma atualiza��o sobre a ocorr�ncia que est�s a seguir.")
															.param("url", "https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr)
															.param("emailTitle", "Atualiza��o sobre ocorr�ncia")
															.param("emailBody", "O utilizador <a href=\"https://city-aid.appspot.com/frontoffice/viewUser.html?username="+authUsername+"\">"+(authUsername)+"</a> submeteu a seguinte atualiza��o sobre a ocorr�ncia <a href=\"https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr+"\">\""+((String)issueEnt.getProperty("title"))+"\"</a>:<br><br><div style=\"margin-left:5em; display:block;\">\""+data.comment+"\"</div>")
															.param("type", NotificationType.LOG_COMMENT.toString()));
							
							return Response.status(Status.NO_CONTENT).build();
						} else {
							//User employer is not assigned to issue
							txn.rollback();
							
							LOG.warning("User attempted to perform operation without required access level: " + authUsername);
							return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
						}
					} else {
						//Wrong role
						txn.rollback();
						
						LOG.warning("User attempted to perform operation without required access level: " + authUsername);
						return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
					}
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					txn.rollback();
					
					LOG.warning("Issue does not exist: " + issueKeyStr);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@PUT
	@Path("/{issue}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response editIssue(@PathParam("issue") String issueKeyStr, @Context HttpServletRequest request, @Context HttpHeaders headers, IssueEditData data) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				txn.commit();
				
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				if (issueKey.getParent().getName().equals(authUsername)){
					//User has permission to perform operation
					try {						
						txn = datastore.beginTransaction();
						//Get issue
						Entity issueEnt = datastore.get(txn, issueKey);
							
						//Update issue data
						issueEnt.setUnindexedProperty("title", data.title);
						issueEnt.setUnindexedProperty("description", data.description);
						issueEnt.setProperty("type", data.type);
						datastore.put(txn, issueEnt);
						txn.commit();
								
						//Add issue log entry
						Entity issueLogEntry = new Entity("IssueLog", issueKey);
						issueLogEntry.setProperty("date", new Date());
						issueLogEntry.setUnindexedProperty("type", IssueLogType.EDITED.toString());
						issueLogEntry.setUnindexedProperty("user", authUsername);
						issueLogEntry.setUnindexedProperty("description", "Ocorrência foi editada pelo autor.");
						datastore.put(issueLogEntry);
						
						//Create user log entry
						Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.EDITED.toString());
						userLogEntry.setUnindexedProperty("description", "Editou a ocorr�ncia \""+(String)issueEnt.getProperty("title")+"\"");
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);
						
						//Notify followers
						Queue queue = QueueFactory.getDefaultQueue();
						queue.add(TaskOptions.Builder.withUrl("/rest/notifications/followers/"+issueKeyStr)
										.param("title", "CityAid")
										.param("icon", "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg")
										.param("body", "Uma ocorr�ncia que est�s a seguir foi editada")
										.param("url", "https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr)
										.param("emailTitle", "Ocorr�ncia editada")
										.param("emailBody", "A ocorr�ncia <a href=\"https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr+"\">\""+((String)issueEnt.getProperty("title"))+"\"</a> foi editada.")
										.param("type", NotificationType.EDIT.toString())
										.param("except", authUsername));
								
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// Issue does not exist
						txn.rollback();
						
						LOG.warning("Issue does not exist: " + issueKeyStr);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//User is not the author
					LOG.warning("User is not the author "+authUsername+","+issueKeyStr);					
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@POST
	@Path("/{issue}/photos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addIssuePhotos(@PathParam("issue") String issueKeyStr, IssuePhotoData data, @Context HttpServletRequest request, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				txn.commit();
				
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				if (issueKey.getParent().getName().equals(authUsername)){
					//User has permission to perform operation
					try {
						txn = datastore.beginTransaction();
						//Get issue
						Entity issueEnt = datastore.get(txn, issueKey);
						
						@SuppressWarnings("unchecked")
						List<String> pictures = (List<String>)issueEnt.getProperty("picture_ids");
						@SuppressWarnings("unchecked")
						List<String> thumbnails = (List<String>)issueEnt.getProperty("thumbnail_ids");
						
						if (pictures.size() + data.pictures.size() > MAX_NUM_PHOTOS_UPLOAD){
							return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
						}
						
						//Save photos on Cloud Storage
						List<String> pictureIds = null;
						List<String> thumbnailIds = null;
						if (data.pictures != null) {
							try {
								pictureIds = new ArrayList<String>(data.pictures.size());
								thumbnailIds = new ArrayList<String>(data.pictures.size());
								for (PictureData picture : data.pictures){
									String photoId = uploadIssuePhoto(authUsername, picture.base64, picture.extension);
									pictureIds.add(photoId+"."+picture.extension);
									thumbnailIds.add(photoId+"_thumb."+picture.extension);
								}
							} catch (IOException e) {
								LOG.warning("Failed to upload issue photo. Exception: "+e.getMessage());
							} catch (Exception e) {
								LOG.warning("Something happenned. Exception: "+e.getMessage());
							}
						}
						
						//Update issue data
						pictures.addAll(pictureIds);
						issueEnt.setUnindexedProperty("picture_ids", pictures);
						thumbnails.addAll(thumbnailIds);
						issueEnt.setUnindexedProperty("thumbnail_ids", thumbnails);
						datastore.put(txn, issueEnt);
						txn.commit();
								
						//Add issue log entry
						Entity issueLogEntry = new Entity("IssueLog", issueKey);
						issueLogEntry.setProperty("date", new Date());
						issueLogEntry.setUnindexedProperty("type", IssueLogType.EDITED.toString());
						issueLogEntry.setUnindexedProperty("user", authUsername);
						issueLogEntry.setUnindexedProperty("description", "Fotografias adicionadas pelo autor.");
						datastore.put(issueLogEntry);
						
						//Create user log entry
						Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.EDITED.toString());
						userLogEntry.setUnindexedProperty("description", "Adicionadas fotos á ocorrência \""+(String)issueEnt.getProperty("title")+"\"");
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);
						
						//Notify followers
						Queue queue = QueueFactory.getDefaultQueue();
						queue.add(TaskOptions.Builder.withUrl("/rest/notifications/followers/"+issueKeyStr)
											.param("title", "CityAid")
											.param("icon", "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg")
											.param("body", "Foram adicionadas fotos a uma ocorrência que estás a seguir")
											.param("url", "https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr)
											.param("emailTitle", "Fotografias adicionadas a ocorrência")
											.param("emailBody", "Foram adicionadas fotografias à ocorrência <a href=\"https://city-aid.appspot.com/frontoffice/view.html?issue="+issueKeyStr+"\">\""+((String)issueEnt.getProperty("title"))+"\"</a>.")
											.param("type", NotificationType.NEW_PHOTOS.toString())
											.param("except", authUsername));
								
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// Issue does not exist
						txn.rollback();
						
						LOG.warning("Issue does not exist: " + issueKeyStr);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//User is not the author
					LOG.warning("User is not the author "+authUsername+","+issueKeyStr);					
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/{issue}/photos/{photo}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response removeIssuePhoto(@PathParam("issue") String issueKeyStr, @PathParam("photo") String photoId, @Context HttpServletRequest request, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				if (issueKey.getParent().getName().equals(authUsername)){
					//User has permission to perform operation
					Transaction txn = datastore.beginTransaction();
					try {						
						//Get issue
						Entity issueEnt = datastore.get(txn, issueKey);
						
						@SuppressWarnings("unchecked")
						List<String> pictures = (List<String>)issueEnt.getProperty("picture_ids");
						@SuppressWarnings("unchecked")
						List<String> thumbnails = (List<String>)issueEnt.getProperty("thumbnail_ids");
						
						//Remove photo
						boolean photoExisted = pictures.remove(photoId);
						if (!photoExisted){
							//Photo does not exist
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.PHOTO_NOT_EXISTS))).build();
						}
						int point = photoId.lastIndexOf('.');
						String thumbnailId = photoId.substring(0, point)+"_thumb"+photoId.substring(point);
						thumbnails.remove(thumbnailId);
						
						//Update issue data
						issueEnt.setUnindexedProperty("picture_ids", pictures);
						issueEnt.setUnindexedProperty("thumbnail_ids", thumbnails);
						datastore.put(txn, issueEnt);
						txn.commit();

						//Add issue log entry
						Entity issueLogEntry = new Entity("IssueLog", issueKey);
						issueLogEntry.setProperty("date", new Date());
						issueLogEntry.setUnindexedProperty("type", IssueLogType.EDITED.toString());
						issueLogEntry.setUnindexedProperty("user", authUsername);
						issueLogEntry.setUnindexedProperty("description", "Fotografia removida pelo autor.");
						datastore.put(issueLogEntry);
						
						//Create user log entry
						Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.EDITED.toString());
						userLogEntry.setUnindexedProperty("description", "Fotografia removida da ocorrência \""+(String)issueEnt.getProperty("title")+"\"");
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);
								
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// Issue does not exist
						txn.rollback();
						
						LOG.warning("Issue does not exist: " + issueKeyStr);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//User is not the author
					LOG.warning("User is not the author "+authUsername+","+issueKeyStr);					
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	/**
	 * Upload issue photo to Google Cloud Storage
	 * @param user			username of user creating the issue
	 * @param base64Photo	photo data enconded in base 64
	 * @param extension		photo file extension
	 * @return photoId		photo id in Cloud Storage
	 * @throws IOException 
	 */
	private String uploadIssuePhoto(String user, String base64Photo, String extension) throws IOException{
		//Generate photo name
		String photoId = user+"_"+System.currentTimeMillis();
		String photoName = photoId+"."+extension;
		
		//Decode base64 photo
		byte[] binPhoto = Base64.decodeBase64(base64Photo);
	    
		//Upload full photo to Cloud Storage
		GcsFileOptions instance = new GcsFileOptions.Builder().mimeType("image/"+extension).build();
	    GcsFilename fileName = new GcsFilename("city-aid.appspot.com", photoName);
	    GcsOutputChannel outputChannel = gcsService.createOrReplace(fileName, instance);
	    OutputStream output = Channels.newOutputStream(outputChannel);
		try {
			output.write(binPhoto, 0, binPhoto.length);
		} finally {
			output.close();
		}
		
		//Generate thumbnail
		//Get an instance of the imagesService we can use to transform images.
		ImagesService imagesService = ImagesServiceFactory.getImagesService();
		
		//Make an image directly from a byte array, and transform it.
		Image image = ImagesServiceFactory.makeImage(binPhoto);
		//int[] newSize = ImageUtil.getScaledDimension(image.getWidth(), image.getHeight(), 256, 256);
		Transform resize = ImagesServiceFactory.makeResize(256, 256, 0.5, 0.5);
		Image resizedImage = imagesService.applyTransform(resize, image);
		
		//Write the transformed image back to a Cloud Storage object.
		gcsService.createOrReplace(
				new GcsFilename("city-aid.appspot.com", photoId+"_thumb."+extension),
				new GcsFileOptions.Builder().mimeType("image/"+extension).build(),
				ByteBuffer.wrap(resizedImage.getImageData()));
		
		return photoId;
	}
}
