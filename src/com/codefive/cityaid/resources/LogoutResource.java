package com.codefive.cityaid.resources;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.gson.Gson;

@Path("/logout")
public class LogoutResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LogoutResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public LogoutResource() { } //Nothing to be done here...
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogout(@Context HttpServletRequest request, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				datastore.get(txn, session.getParent());
				//User has permission to perform operation (only the user himself can logout)
				datastore.delete(txn, sessionKey);
				
				txn.commit();
				
				LOG.info("Logged out: " + authUsername);
				return Response.status(Status.NO_CONTENT).build();
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
}
