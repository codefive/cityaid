package com.codefive.cityaid.resources;

import java.util.Date;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.IssueType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.Rating;
import com.codefive.cityaid.util.requestData.RatingData;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.Gson;

@Path("/issues/{issue}/ratings")
public class RatingResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RatingResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public RatingResource() { } //Nothing to be done here...
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response submitIssueRating(@PathParam("issue") String issueKeyStr, RatingData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){				
				//User has permission to perform operation
				txn.commit();
				
				TransactionOptions options = TransactionOptions.Builder.withXG(true);
				txn = datastore.beginTransaction(options);
				Key issueKey = KeyFactory.stringToKey(issueKeyStr);
				try {
					//Get issue
					Entity issueEnt = datastore.get(txn, issueKey);
					
					if ((boolean) issueEnt.getProperty("forceRating")){
						//Rating is being forced, can't update rating
						txn.rollback();
						
						return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.FORCED_RATING))).build();
					}
					
					try {
						//If the rating does not exist an Exception is thrown. Otherwise,
						Key ratingKey = KeyFactory.createKey(issueKey, "Rating", authUsername);
						datastore.get(txn, ratingKey);
						txn.rollback();
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.RATING_ALREADY_EXISTS))).build();
					} catch (EntityNotFoundException e){
						//Create issue rating
						Entity rating = new Entity("Rating", authUsername, issueKey);
						rating.setIndexedProperty("priority", data.priority);
						datastore.put(txn, rating);
						
						//Create rating flag, if it doesn't exist
						try {
							Key ratingFlagKey = KeyFactory.createKey("IssueRatingFlag", issueKeyStr);
							datastore.get(txn, ratingFlagKey);
						} catch (EntityNotFoundException e1){
							Entity ratingFlag = new Entity("IssueRatingFlag", issueKeyStr);
							datastore.put(txn, ratingFlag);
						}
						
						//Update points
						Key appPointsKey = KeyFactory.createKey("Points", "_APP_");
						Entity appPoints = datastore.get(appPointsKey);
						Key userGamificationStatsKey = KeyFactory.createKey("UserGamificationStats", authUsername);
						Entity userGamificationStats = datastore.get(txn, userGamificationStatsKey);
						long newPoints = ((long)userGamificationStats.getProperty("points"))+((long)appPoints.getProperty("rateIssue"));
						userGamificationStats.setUnindexedProperty("points", newPoints);
						datastore.put(txn, userGamificationStats);
						
						//Get user stats
						Key userRateStatsKey = KeyFactory.createKey("UserRateStats", authUsername);
						Entity userRateStatsEnt = datastore.get(txn, userRateStatsKey);
						
						//Update user stats
						userRateStatsEnt.setUnindexedProperty("issues_rated", ((long)userRateStatsEnt.getProperty("issues_rated"))+1);
						switch(IssueType.valueOf(((String)issueEnt.getProperty("type")).toUpperCase())){
							case ACCIDENT:
								userRateStatsEnt.setUnindexedProperty("issues_rated_type_accident", ((long)userRateStatsEnt.getProperty("issues_rated_type_accident"))+1);
								break;
							case FURNITURE_DAMAGE:
								userRateStatsEnt.setUnindexedProperty("issues_rated_type_furnituredmg", ((long)userRateStatsEnt.getProperty("issues_rated_type_furnituredmg"))+1);
								break;
							case GARBAGE:
								userRateStatsEnt.setUnindexedProperty("issues_rated_type_garbage", ((long)userRateStatsEnt.getProperty("issues_rated_type_garbage"))+1);
								break;
							case NATURE:
								userRateStatsEnt.setUnindexedProperty("issues_rated_type_nature", ((long)userRateStatsEnt.getProperty("issues_rated_type_nature"))+1);
								break;
							case OTHER:
								userRateStatsEnt.setUnindexedProperty("issues_rated_type_other", ((long)userRateStatsEnt.getProperty("issues_rated_type_other"))+1);
								break;
							case STREET_DAMAGE:
								userRateStatsEnt.setUnindexedProperty("issues_rated_type_streetdmg", ((long)userRateStatsEnt.getProperty("issues_rated_type_streetdmg"))+1);
								break;
							case VANDALISM:
								userRateStatsEnt.setUnindexedProperty("issues_rated_type_vandalism", ((long)userRateStatsEnt.getProperty("issues_rated_type_vandalism"))+1);
								break;
							default:
								break;					
						}
						datastore.put(txn, userRateStatsEnt);
						
						txn.commit();
						
						//Create user log entry
						Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.O_RATED.toString());
						userLogEntry.setUnindexedProperty("description", "Avaliou a prioridade da ocorrência \""+(String)issueEnt.getProperty("title")+"\" como "+data.priority);
						userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
						userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);
						
						//Update gamification system
						Queue queue = QueueFactory.getDefaultQueue();
						queue.add(TaskOptions.Builder.withUrl("/rest/gamification/users/"+authUsername));
						
						return Response.status(Status.NO_CONTENT).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} catch (EntityNotFoundException e) {
					// Issue does not exist
					txn.rollback();
					
					LOG.warning("Issue does not exist: " + issueKeyStr + "|" + authUsername);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@PUT
	@Path("/{username}")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateIssueRating(@PathParam("issue") String issueKeyStr, @PathParam("username") String username, RatingData data, @Context HttpServletRequest request, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName()) && authUsername.equals(username)){				
				//User has permission to perform operation
				Key ratingKey = KeyFactory.createKey(KeyFactory.stringToKey(issueKeyStr), "Rating", authUsername);
				try {
					//Get rating
					Entity ratingEnt = datastore.get(txn, ratingKey);
					
					Key issueKey = KeyFactory.stringToKey(issueKeyStr);
					Entity issueEnt = datastore.get(txn, issueKey);					
					if ((boolean) issueEnt.getProperty("forceRating")){
						//Rating is being forced, can't update rating
						txn.rollback();
						
						return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.FORCED_RATING))).build();
					}
					
					//Update rating
					long oldRating = (long)ratingEnt.getProperty("priority");
					ratingEnt.setProperty("priority", data.priority);
					datastore.put(txn, ratingEnt);
					
					//Create rating flag, if it doesn't exist
					try {
						Key ratingFlagKey = KeyFactory.createKey("IssueRatingFlag", issueKeyStr);
						datastore.get(txn, ratingFlagKey);
					} catch (EntityNotFoundException e1){
						Entity ratingFlag = new Entity("IssueRatingFlag", issueKeyStr);
						datastore.put(txn, ratingFlag);
					}
					
					//Create user log entry
					Entity userLogEntry = new Entity("UserLog", sessionKey.getParent());
					userLogEntry.setProperty("date", new Date());
					userLogEntry.setUnindexedProperty("type", UserLogType.O_RATED.toString());
					userLogEntry.setUnindexedProperty("description", "Alteração da prioridade da ocorrência \""+(String)issueEnt.getProperty("title")+"\" de "+oldRating+" para "+data.priority);
					userLogEntry.setUnindexedProperty("remote_addr", request.getRemoteAddr());
					userLogEntry.setUnindexedProperty("remote_host", request.getRemoteHost());
					userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
					userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
					userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
					datastore.put(txn, userLogEntry);
					
					txn.commit();
					
					return Response.status(Status.NO_CONTENT).build();
				} catch (EntityNotFoundException e) {
					// Rating does not exist
					txn.rollback();
					
					LOG.warning("Rating does not exist: " + issueKeyStr + "|" + authUsername);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.RATING_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@GET
	@Path("/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getIssueRating(@PathParam("issue") String issueKeyStr, @PathParam("username") String username, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName()) && authUsername.equals(username)){				
				//User has permission to perform operation
				txn.commit();
				
				txn = datastore.beginTransaction();
				Key ratingKey = KeyFactory.createKey(KeyFactory.stringToKey(issueKeyStr), "Rating", authUsername);
				try {
					//Get rating
					Entity ratingEnt = datastore.get(txn, ratingKey);
					
					txn.commit();

					//Create response
					Rating rating = new Rating(username, (long)ratingEnt.getProperty("priority"));
					
					return Response.status(Status.OK).entity(g.toJson(rating)).build();
				} catch (EntityNotFoundException e) {
					// Rating does not exist
					txn.rollback();
					
					LOG.warning("Rating does not exist: " + issueKeyStr + "|" + authUsername);
					return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.RATING_NOT_EXISTS))).build();
				} finally {
					if (txn.isActive()) {
						txn.rollback();
						return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
					}
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@PUT
	@Path("/force")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response forceIssueRating(@PathParam("issue") String issueKeyStr, RatingData data, @Context HttpServletRequest request, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
					//User has permission to perform operation
					Key issueKey = KeyFactory.stringToKey(issueKeyStr);
					try {
						//Update issue data
						Entity issueEnt = datastore.get(txn, issueKey);
						issueEnt.setProperty("rating", data.priority);
						issueEnt.setUnindexedProperty("forceRating", true);	
						datastore.put(txn, issueEnt);
						
						txn.commit();
						
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// Issue does not exist
						txn.rollback();
						
						LOG.warning("Issue does not exist: " + issueKeyStr);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.ISSUE_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required role: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
}
