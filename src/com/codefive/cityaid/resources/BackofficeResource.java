package com.codefive.cityaid.resources;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.codefive.cityaid.util.AvatarFactory;
import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.enums.UserLogType;
import com.codefive.cityaid.util.enums.UserRole;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.User;
import com.codefive.cityaid.util.replyData.UserListing;
import com.codefive.cityaid.util.requestData.BackofficeEmployeeData;
import com.codefive.cityaid.util.requestData.BackofficeEmployeeRegistrationData;
import com.codefive.cityaid.util.requestData.BackofficeManagerData;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.gson.Gson;

@Path("/backoffice")
public class BackofficeResource {
	
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(BackofficeResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public BackofficeResource() { } //Nothing to be done here...
	
	@POST
	@Path("/managers")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addBackofficeManager(BackofficeManagerData data, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) && ((boolean)user.getProperty("isManager"))){
					//User has permission to perform operation
					Key managerKey = KeyFactory.createKey("User", data.username);
					try {
						Entity manager = datastore.get(txn, managerKey);
						
						//Check if not a manager already
						if (((String)manager.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) && ((boolean)manager.getProperty("isManager"))){
							txn.rollback();
							LOG.warning("User already is a backoffice manager "+data.username);
							
							return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.USER_ALREADY_MANAGER))).build();
						}

						//Update user data
						manager.setProperty("role", UserRole.BACKOFFICE.toString());
						manager.setProperty("isManager", true);
						datastore.put(txn, manager);

						txn.commit();
						LOG.info("Added backoffice manager " +data.username);
						
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e){
						LOG.warning("Tried to add non-existing manager ("+data.username+") to backoffice");
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@DELETE
	@Path("/managers/{username}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response removeBackofficeManager(@PathParam("username") String username, @Context HttpHeaders headers){
		//Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()){
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(txn, session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) && ((boolean)user.getProperty("isManager"))){
					//User has permission to perform operation
					Key managerKey = KeyFactory.createKey("User", username);
					try {
						Entity manager = datastore.get(txn, managerKey);
						
						//Check if not a manager
						if (!((String)manager.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) || !((boolean)manager.getProperty("isManager"))){
							txn.rollback();
							LOG.warning("User isn't a backoffice manager "+username);
							
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_MANAGER))).build();
						}
						
						//Update user data
						manager.setProperty("isManager", false);
						datastore.put(txn, manager);
						
						txn.commit();
						LOG.info("Removed backoffice manager " +username);
						
						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e){
						LOG.warning("Tried to remove non-existing manager ("+username+") from backoffice");
						txn.rollback();
						
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					txn.rollback();

					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				txn.rollback();
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@Path("/employees")
	@POST
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addBackofficeEmployee(BackofficeEmployeeData data, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity u = datastore.get(txn, session.getParent());
				if (((String) u.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) && ((boolean) u.getProperty("isManager"))) {
					// User has permission to perform operation
					txn.commit();

					txn = datastore.beginTransaction();
					Key userKey = KeyFactory.createKey("User", data.username);
					try {
						// Get user
						Entity employee = datastore.get(txn, userKey);
						
						if (((String) employee.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
							txn.rollback();

							LOG.warning("User is already a backoffice employee!");
							return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.USER_ALREADY_BACKOFFICE))).build();
						}
						
						employee.setProperty("role", UserRole.BACKOFFICE.toString());
						datastore.put(txn, employee);

						txn.commit();

						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// User does not exist
						txn.rollback();

						LOG.warning("User does not exist: " + data.username);
						return Response.status(Status.NOT_FOUND)
								.entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR)
									.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					// User does not have permission
					txn.rollback();

					LOG.warning("User attempted to perform operation without permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED)))
							.build();
				} 
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@Path("/employees/create")
	@POST
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response createBackofficeEmployee(BackofficeEmployeeRegistrationData data, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		if (data == null || !data.valid()){
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			//Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())){
				Entity user = datastore.get(session.getParent());
				if (((String)user.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) && ((boolean)user.getProperty("isManager"))) {
					//User has permission to perform operation
					
					Transaction txn = datastore.beginTransaction();
					try {
						//If the user does not exist an Exception is thrown. Otherwise,
						Key userKey = KeyFactory.createKey("User", data.username);
						datastore.get(txn, userKey);
						txn.rollback();
						return Response.status(Status.CONFLICT).entity(g.toJson(new ErrorInfo(ErrorType.USER_ALREADY_EXISTS))).build();
					} catch (EntityNotFoundException e){
						String generatedPassword = UUID.randomUUID().toString();
						
						//Create user
						Entity backUser = new Entity("User", data.username);
						backUser.setUnindexedProperty("password", DigestUtils.sha512Hex(generatedPassword));
						backUser.setUnindexedProperty("email", data.email);
						backUser.setUnindexedProperty("name", data.name);
						backUser.setUnindexedProperty("phone", data.phone);
						backUser.setUnindexedProperty("gender", data.gender);
						backUser.setUnindexedProperty("birth_date", data.birthdate);
						backUser.setProperty("role", UserRole.BACKOFFICE.toString());
						backUser.setProperty("registered_since", new Date());
						backUser.setProperty("verified", true);
						backUser.setProperty("frozen", false);
						backUser.setProperty("isManager", false);
						backUser.setProperty("employer", null);
						backUser.setUnindexedProperty("job", null);
						datastore.put(txn, backUser);
						txn.commit();
						
						//Initialize options
						Entity userOptionsEnt = new Entity("UserOptions", data.username);
						userOptionsEnt.setUnindexedProperty("auto_follow", true);
						userOptionsEnt.setUnindexedProperty("push_medal", true);
						userOptionsEnt.setUnindexedProperty("push_level_up", true);
						userOptionsEnt.setUnindexedProperty("push_issue_wip", true);
						userOptionsEnt.setUnindexedProperty("push_issue_closed", true);
						userOptionsEnt.setUnindexedProperty("push_issue_solved", true);
						userOptionsEnt.setUnindexedProperty("push_log_comment", true);
						userOptionsEnt.setUnindexedProperty("push_edit", true);
						userOptionsEnt.setUnindexedProperty("push_new_photos", true);
						userOptionsEnt.setUnindexedProperty("push_comment", true);
						userOptionsEnt.setUnindexedProperty("email_medal", true);
						userOptionsEnt.setUnindexedProperty("email_level_up", true);
						userOptionsEnt.setUnindexedProperty("email_issue_wip", true);
						userOptionsEnt.setUnindexedProperty("email_issue_closed", true);
						userOptionsEnt.setUnindexedProperty("email_issue_solved", true);
						userOptionsEnt.setUnindexedProperty("email_log_comment", true);
						userOptionsEnt.setUnindexedProperty("email_edit", true);
						userOptionsEnt.setUnindexedProperty("email_new_photos", true);
						userOptionsEnt.setUnindexedProperty("email_comment", true);
						userOptionsEnt.setUnindexedProperty("mobile_cache", 5);
						datastore.put(userOptionsEnt);
						
						//Initialize stats
						Entity userGamificationStatsEnt = new Entity("UserGamificationStats", data.username);
						userGamificationStatsEnt.setUnindexedProperty("points", 0);
						userGamificationStatsEnt.setProperty("level", 1);
						userGamificationStatsEnt.setUnindexedProperty("num_awarded_medals", 0);
						datastore.put(userGamificationStatsEnt);
						Entity userIssueStatsEnt = new Entity("UserIssueStats", data.username);
						userIssueStatsEnt.setUnindexedProperty("issues_created", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_vandalism", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_garbage", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_accident", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_streetdmg", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_furnituredmg", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_nature", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_created_type_other", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_in_progress", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_solved", 0);
						userIssueStatsEnt.setUnindexedProperty("issues_closed", 0);
						datastore.put(userIssueStatsEnt);
						Entity userCommentStatsEnt = new Entity("UserCommentStats", data.username);
						userCommentStatsEnt.setUnindexedProperty("issues_commented", 0);
						userCommentStatsEnt.setUnindexedProperty("issues_commented_type_vandalism", 0);
						userCommentStatsEnt.setUnindexedProperty("issues_commented_type_garbage", 0);
						userCommentStatsEnt.setUnindexedProperty("issues_commented_type_accident", 0);
						userCommentStatsEnt.setUnindexedProperty("issues_commented_type_streetdmg", 0);
						userCommentStatsEnt.setUnindexedProperty("issues_commented_type_furnituredmg", 0);
						userCommentStatsEnt.setUnindexedProperty("issues_commented_type_nature", 0);
						userCommentStatsEnt.setUnindexedProperty("issues_commented_type_other", 0);
						datastore.put(userCommentStatsEnt);
						Entity userFollowStatsEnt = new Entity("UserFollowStats", data.username);
						userFollowStatsEnt.setUnindexedProperty("issues_followed", 0);
						userFollowStatsEnt.setUnindexedProperty("issues_followed_type_vandalism", 0);
						userFollowStatsEnt.setUnindexedProperty("issues_followed_type_garbage", 0);
						userFollowStatsEnt.setUnindexedProperty("issues_followed_type_accident", 0);
						userFollowStatsEnt.setUnindexedProperty("issues_followed_type_streetdmg", 0);
						userFollowStatsEnt.setUnindexedProperty("issues_followed_type_furnituredmg", 0);
						userFollowStatsEnt.setUnindexedProperty("issues_followed_type_nature", 0);
						userFollowStatsEnt.setUnindexedProperty("issues_followed_type_other", 0);
						datastore.put(userFollowStatsEnt);
						Entity userRateStatsEnt = new Entity("UserRateStats", data.username);
						userRateStatsEnt.setUnindexedProperty("issues_rated", 0);
						userRateStatsEnt.setUnindexedProperty("issues_rated_type_vandalism", 0);
						userRateStatsEnt.setUnindexedProperty("issues_rated_type_garbage", 0);
						userRateStatsEnt.setUnindexedProperty("issues_rated_type_accident", 0);
						userRateStatsEnt.setUnindexedProperty("issues_rated_type_streetdmg", 0);
						userRateStatsEnt.setUnindexedProperty("issues_rated_type_furnituredmg", 0);
						userRateStatsEnt.setUnindexedProperty("issues_rated_type_nature", 0);
						userRateStatsEnt.setUnindexedProperty("issues_rated_type_other", 0);
						datastore.put(userRateStatsEnt);
						
						//Create user log entry
						Entity userLogEntry = new Entity("UserLog", backUser.getKey());
						userLogEntry.setProperty("date", new Date());
						userLogEntry.setUnindexedProperty("type", UserLogType.REGISTERED.toString());
						userLogEntry.setUnindexedProperty("description", "Conta registada (Empregado de Backoffice)");
						userLogEntry.setUnindexedProperty("remote_addr", "");
						userLogEntry.setUnindexedProperty("remote_host", "");
						userLogEntry.setUnindexedProperty("X-AppEngine-CityLatLong", headers.getHeaderString("X-AppEngine-CityLatLong"));
						userLogEntry.setUnindexedProperty("X-AppEngine-City", headers.getHeaderString("X-AppEngine-City"));
						userLogEntry.setUnindexedProperty("X-AppEngine-Country", headers.getHeaderString("X-AppEngine-Country"));
						datastore.put(userLogEntry);
						
						LOG.info("User registered " + data.username);
						
						//Send verification email
						Properties props = new Properties();
						Session emailSession = Session.getDefaultInstance(props, null);

						try {
							Message msg = new MimeMessage(emailSession);
							msg.setFrom(new InternetAddress("noreply@city-aid.appspotmail.com", "CityAid"));
							msg.addRecipient(Message.RecipientType.TO, new InternetAddress(data.email, data.name));
							msg.setSubject(MimeUtility.encodeText("Credenciais da sua conta CityAid", "UTF-8", "Q"));

							String htmlBody = "Foi criada uma conta de gestor backoffice nova.<br><b>Nome de utilizador:</b> "+data.username+"<br><b>Password:</b> "+generatedPassword+"<br><br>Se n�o criou uma conta CityAid, ignore esta mensagem."+NotificationResource.getEmailSignature();
							Multipart mp = new MimeMultipart();

							MimeBodyPart htmlPart = new MimeBodyPart();
							htmlPart.setContent(htmlBody, "text/html");
							mp.addBodyPart(htmlPart);

							msg.setContent(mp);

							Transport.send(msg);
							
							LOG.fine("Account credentials email sent to "+data.email);
						} catch (AddressException e1) {
							LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
						} catch (MessagingException e1) {
							LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
						} catch (UnsupportedEncodingException e1) {
							LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
						} catch (Exception e1) {
							LOG.warning("Account credentials email not sent to "+data.email+"\nException: "+e1.getMessage());
						}
						
						return Response.status(Status.NO_CONTENT).build();
						
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					//Wrong user role
					
					LOG.warning("User does not have required permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				//Token does not belong to user
				
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();			
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
	
	@DELETE
	@Path("/employees/{employee}")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response removeBackofficeEmployee(@PathParam("employee") String username, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		if (username == null || username.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM)))
					.build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				Entity u = datastore.get(txn, session.getParent());
				if (((String) u.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name()) && ((boolean) u.getProperty("isManager"))) {
					// User has permission to perform operation
					txn.commit();

					txn = datastore.beginTransaction();
					Key userKey = KeyFactory.createKey("User", username);
					try {
						// Get user
						Entity employee = datastore.get(txn, userKey);
						
						if (!((String) employee.getProperty("role")).equalsIgnoreCase(UserRole.BACKOFFICE.name())){
							txn.rollback();

							LOG.warning("User isn't a backoffice employee!");
							return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_BACKOFFICE))).build();
						}
						
						//Update user data
						employee.setProperty("role", UserRole.FROZEN.toString());
						employee.setProperty("frozen", true);
						datastore.put(txn, employee);
						
						//Delete user auth tokens
						Query query = new Query("Session").setAncestor(KeyFactory.createKey("User", username));
						List<Entity> tokens = datastore.prepare(query.setKeysOnly()).asList(FetchOptions.Builder.withDefaults());
						List<Key> tokenKeys = new ArrayList<Key>(tokens.size());
						for (Entity token : tokens) {
							tokenKeys.add(token.getKey());
						}
						datastore.delete(tokenKeys);
						
						txn.commit();

						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// User does not exist
						txn.rollback();

						LOG.warning("User does not exist: " + username);
						return Response.status(Status.NOT_FOUND)
								.entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR)
									.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					// User does not have permission
					txn.rollback();

					LOG.warning("User attempted to perform operation without permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED)))
							.build();
				}
			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
	
	@Path("/employees")
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getBackofficeEmployees(@QueryParam("cursor") String cursor,
			@QueryParam("page_size") long page_size, @Context HttpHeaders headers) {
		
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");
		
		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(txn, sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				
				Query query = new Query("User").addSort(Entity.KEY_RESERVED_PROPERTY, SortDirection.ASCENDING);
				query.setFilter(new FilterPredicate("role", FilterOperator.EQUAL, UserRole.BACKOFFICE.toString()));
				
				FetchOptions fetchOptions = FetchOptions.Builder.withLimit((int) page_size);
				
				if (cursor != null && !cursor.equals("null")) {
					fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
				}
				
				QueryResultList<Entity> users = datastore.prepare(query).asQueryResultList(fetchOptions);
				
				// Create response
				List<User> res = new ArrayList<User>(users.size());
				
				txn.commit();
				for (Entity result : users) {
					User user = new User(result.getKey().getName(), (String) result.getProperty("role"),
						(String) result.getProperty("name"), (String) result.getProperty("email"),
						(String) result.getProperty("address"), (String) result.getProperty("phone"),
						(String) result.getProperty("gender"), (Date) result.getProperty("birth_date"),
						(Date) result.getProperty("registered_since"), (boolean) result.getProperty("isManager"),
						(String) result.getProperty("employer"),(String) result.getProperty("job"),
						AvatarFactory.getAvatarUrl((String) result.getProperty("email")),
						(boolean) result.getProperty("frozen"), 0, 1);
					
					res.add(user);
				}
				
				// Cursor
				Cursor c = users.getCursor();
				if (c != null){
					cursor = c.toWebSafeString();
				} else {
					cursor = null;
				}
				
				UserListing ret = new UserListing(res, cursor);
				return Response.status(Status.OK).entity(g.toJson(ret)).build();

			} else {
				// Token does not belong to user
				txn.rollback();

				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			txn.rollback();

			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
			}
		}
	}
}
