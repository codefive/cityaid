package com.codefive.cityaid.resources;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.codefive.cityaid.util.enums.ErrorType;
import com.codefive.cityaid.util.replyData.ErrorInfo;
import com.codefive.cityaid.util.replyData.UserOptions;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;

@Path("/users/{username}/options")
public class OptionsResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(OptionsResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public OptionsResource() { } //Nothing to be done here...
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response viewUserOptions(@PathParam("username") String username, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				if (authUsername.equals(username)){
					// User has permission to perform operation
					Key userOptionsKey = KeyFactory.createKey("UserOptions", username);
					try {
						// Get user options
						Entity userOptionsEnt = datastore.get(userOptionsKey);
						
						UserOptions options = new UserOptions(
								(boolean) userOptionsEnt.getProperty("auto_follow"),
								(boolean) userOptionsEnt.getProperty("push_medal"),
								(boolean) userOptionsEnt.getProperty("push_level_up"),
								(boolean) userOptionsEnt.getProperty("push_issue_wip"),
								(boolean) userOptionsEnt.getProperty("push_issue_closed"),
								(boolean) userOptionsEnt.getProperty("push_issue_solved"),
								(boolean) userOptionsEnt.getProperty("push_log_comment"),
								(boolean) userOptionsEnt.getProperty("push_edit"),
								(boolean) userOptionsEnt.getProperty("push_new_photos"),
								(boolean) userOptionsEnt.getProperty("push_comment"),
								(boolean) userOptionsEnt.getProperty("email_medal"),
								(boolean) userOptionsEnt.getProperty("email_level_up"),
								(boolean) userOptionsEnt.getProperty("email_issue_wip"),
								(boolean) userOptionsEnt.getProperty("email_issue_closed"),
								(boolean) userOptionsEnt.getProperty("email_issue_solved"),
								(boolean) userOptionsEnt.getProperty("email_log_comment"),
								(boolean) userOptionsEnt.getProperty("email_edit"),
								(boolean) userOptionsEnt.getProperty("email_new_photos"),
								(boolean) userOptionsEnt.getProperty("email_comment"),
								(long) userOptionsEnt.getProperty("mobile_cache")
							);

						return Response.status(Status.OK).entity(options).build();
					} catch (EntityNotFoundException e) {
						// User does not exist
						LOG.warning("User does not exist: " + username);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					}
				} else {
					// User does not have permission
					LOG.warning("User attempted to perform operation without permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				// Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateUserOptions(@PathParam("username") String username, UserOptions data, @Context HttpHeaders headers) {
		// Get token and user IDs from request header for authentication
		String authToken = headers.getHeaderString("Authorization");
		String authUsername = headers.getHeaderString("User");

		if (authToken == null || authUsername == null || authToken.isEmpty() || authUsername.isEmpty()) {
			return Response.status(Status.UNAUTHORIZED).entity(g.toJson(new ErrorInfo(ErrorType.UNAUTHORIZED))).build();
		}

		if (data == null) {
			return Response.status(Status.BAD_REQUEST).entity(g.toJson(new ErrorInfo(ErrorType.MISSING_WRONG_PARAM))).build();
		}

		Key sessionKey = new KeyFactory.Builder("User", authUsername).addChild("Session", authToken).getKey();
		try {
			// Get session
			Entity session = datastore.get(sessionKey);
			if (authUsername.equals(session.getParent().getName())) {
				if (authUsername.equals(username)){
					// User has permission to perform operation
					Key userOptionsKey = KeyFactory.createKey("UserOptions", username);
					Transaction txn = datastore.beginTransaction();
					try {
						// Get user options
						Entity userOptionsEnt = datastore.get(txn, userOptionsKey);

						// Update user options
						userOptionsEnt.setUnindexedProperty("auto_follow", data.auto_follow);
						userOptionsEnt.setUnindexedProperty("push_medal", data.push_medal);
						userOptionsEnt.setUnindexedProperty("push_level_up", data.push_level_up);
						userOptionsEnt.setUnindexedProperty("push_issue_wip", data.push_issue_wip);
						userOptionsEnt.setUnindexedProperty("push_issue_closed", data.push_issue_closed);
						userOptionsEnt.setUnindexedProperty("push_issue_solved", data.push_issue_solved);
						userOptionsEnt.setUnindexedProperty("push_log_comment", data.push_log_comment);
						userOptionsEnt.setUnindexedProperty("push_edit", data.push_edit);
						userOptionsEnt.setUnindexedProperty("push_new_photos", data.push_new_photos);
						userOptionsEnt.setUnindexedProperty("push_comment", data.push_comment);
						userOptionsEnt.setUnindexedProperty("email_medal", data.email_medal);
						userOptionsEnt.setUnindexedProperty("email_level_up", data.email_level_up);
						userOptionsEnt.setUnindexedProperty("email_issue_wip", data.email_issue_wip);
						userOptionsEnt.setUnindexedProperty("email_issue_closed", data.email_issue_closed);
						userOptionsEnt.setUnindexedProperty("email_issue_solved", data.email_issue_solved);
						userOptionsEnt.setUnindexedProperty("email_log_comment", data.email_log_comment);
						userOptionsEnt.setUnindexedProperty("email_edit", data.email_edit);
						userOptionsEnt.setUnindexedProperty("email_new_photos", data.email_new_photos);
						userOptionsEnt.setUnindexedProperty("email_comment", data.email_comment);
						userOptionsEnt.setUnindexedProperty("email_comment", data.email_comment);
						userOptionsEnt.setUnindexedProperty("mobile_cache", data.mobile_cache);
						datastore.put(txn, userOptionsEnt);
						txn.commit();

						return Response.status(Status.NO_CONTENT).build();
					} catch (EntityNotFoundException e) {
						// User does not exist
						txn.rollback();

						LOG.warning("User does not exist: " + username);
						return Response.status(Status.NOT_FOUND).entity(g.toJson(new ErrorInfo(ErrorType.USER_NOT_EXISTS))).build();
					} finally {
						if (txn.isActive()) {
							txn.rollback();
							return Response.status(Status.INTERNAL_SERVER_ERROR).entity(g.toJson(new ErrorInfo(ErrorType.INTERNAL_SERVER_ERROR))).build();
						}
					}
				} else {
					// User does not have permission
					LOG.warning("User attempted to perform operation without permission: " + authUsername);
					return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
				}
			} else {
				// Token does not belong to user
				LOG.warning("User attempted to perform operation with another users token: " + authUsername);
				return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN)))
						.build();
			}
		} catch (EntityNotFoundException e) {
			// Token does not exist
			LOG.warning("Invalid token: " + authUsername);
			return Response.status(Status.FORBIDDEN).entity(g.toJson(new ErrorInfo(ErrorType.INVALID_TOKEN))).build();
		}
	}
}
