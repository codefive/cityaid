var user;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading Data...");
	document.getElementById('logout').onclick = logout;
	getUser();
	var frms1 = $('form[name="createWorker"');
	var frms2 = $('form[name="addWorker"]');
	frms1[0].onsubmit = create;
	frms2[0].onsubmit = add;
}

function getUser() {
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	$.ajax({
		type : "GET",
		url : "../../rest/users/" + userID,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			user = response;
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token Inválido');
			} else if (response.status == 404) {
				showSnackbar('Utilizador inexistente');
			} else if (response.status == 500) {
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};

var create = function(event) {
	
	var data = $('form[name="createWorker"]').jsonify();
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "POST",
		url : "../../rest/entities/" + user.employer + "/workers/create",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		// dataType: "json",
		success : function(response) {
			showSnackbar("Trabalhador registado!");
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetro errado ou vazio');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 409) {
				showSnackbar('Utilizador já existe');
			} else if (response.status == 500) {
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});

	event.preventDefault();
};

var add = function(event) {
	
	var data = $('form[name="addWorker"]').jsonify();
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "POST",
		url : "../../rest/entities/" + user.employer + "/workers",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		// dataType: "json",
		success : function(response) {
			showSnackbar("Trabalhador adicionado!");
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetro errado ou vazio');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Utilizador inexistente');
			} else if (response.status == 409) {
				showSnackbar('Utilizador já é trabalhador');
			} else if (response.status == 500) {
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});

	event.preventDefault();
};