var cursors = ["null"];
var current = 0;
var hasNext = true;
var user;

window.onload = function() {
	checkLogin();
	document.getElementById('logout').onclick = logout;
	showSnackbar("Loading data...");
	getWorker();
	
	if(current == 0)
		document.getElementById('previous').disabled = true;

	//Go to previous page
	document.getElementById('previous').onclick = function(event) {
		document.getElementById('workers').innerHTML = "";
		if (current > 0) {
			//there is a previous page
			current--;
			if (current <= 0) {
				//previous page was the first page
				document.getElementById('previous').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('previous').disabled = false;
			}
			document.getElementById('next').disabled = false;
			getWorkers();
		}
	};
	
	//Go to next page
	document.getElementById('next').onclick = function(event) {
		document.getElementById('workers').innerHTML = "";
		if (hasNext) {
			//there might be a next page
			current++;
			document.getElementById('previous').disabled = false;
			getWorkers();
		}
	};
};

function showEntityWorkers(data) {

	var workers = document.getElementById("workers");
	if (data.length == 0) {
		if (cursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem nenhum utilizador!";
			workers.insertBefore(title, workers.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem mais trabalhadores";
			workers.insertBefore(title, workers.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ref = document.createElement ("a");
			ref.href = "./viewUser.html?username=" + data[i].username;
			var worker = document.createElement("div");
			worker.className = "container dados-block";
			var div1 = document.createElement("div");
			div1.className = "dados-header";
			div1.innerHTML = data[i].name;
			worker.appendChild(div1);
			
			var div2= document.createElement("div");
			div2.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			imag.src = "../../resource/Boneco.png";
			var titleJob = document.createElement("p");
			titleJob.innerHTML = data[i].job;
			titleJob.className = "typeOcorr";
			div2.appendChild (imag);
			div2.appendChild(titleJob);
			worker.appendChild(div2);
			ref.appendChild(worker);
			workers.insertBefore(ref, workers.children[0]);
		}
		document.getElementById('next').disabled = false;
	    hasNext = true;
	}
	document.getElementById('current').innerHTML = (current+1);
};

function getWorker() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../../rest/users/" + userID,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			user = response;
			getWorkers();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar("Missing or empty authorization headers");
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar("User does not exist");
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function getWorkers() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cursors[current];
	
	//Generate url
	var url;
	if(cursor=="null")
		url = "../../rest/entities/" + user.employer + "/workers?page_size=10";
	else
		url = "../../rest/entities/" + user.employer + "/workers?cursor=" + cursor + "&page_size=10";
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			showEntityWorkers(response.users);
			if ((current+1) >= cursors.length)
				cursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Entity does not exist');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
}