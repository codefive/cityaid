window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	getUser();
};

function getUser() {

	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];

	$.ajax({
		type : "GET",
		url : "../../rest/users/" + userID,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			getEntity(response);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('User does not exist');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function getEntity(user) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../../rest/entities/" + user.employer,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			var entity = response;
			document.getElementById("rankingEntity").innerHTML = entity.rating;
			getEntityStats(entity);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Entity does not exist');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function getEntityStats(entity) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../../rest/issues?state=solved&entity=" + entity.nif,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			document.getElementById("issuesResolved").innerHTML = response.issues.length;
		},
		error : function(response) {
			showSnackbar("Error: " + response.status);
		}
	});

	$.ajax({
		type : "GET",
		url : "../../rest/issues?state=work_in_progress&entity=" + entity.nif,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			document.getElementById("issuesUnresolved").innerHTML = response.issues.length;
		},
		error : function(response) {
			showSnackbar("Error: " + response.status);
		}
	});

	$.ajax({
		type : "GET",
		url : "../../rest/entities/" + entity.nif + "/workers/",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			console.log(response);
			document.getElementById("numWorkers").innerHTML = response.users.length;
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Entity does not exist');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};