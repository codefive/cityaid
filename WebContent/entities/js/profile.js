var cursors = ["null"];
var current = 0;
var hasNext = true;
var user;
var entity = "";

window.onload = function() {
	checkLogin();
	showSnackbar("A Carregar Informação do Utilizador...");
	document.getElementById('logout').onclick = logout;
	
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true") {
		toggleOn("statsManager");
		toggleOn("workersManager");
	}
	//...................................................
	
	if(current == 0)
		document.getElementById('previous').disabled = true;

	//Go to previous page
	document.getElementById('previous').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (current > 0) {
			//there is a previous page
			current--;
			if (current <= 0) {
				//previous page was the first page
				document.getElementById('previous').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('previous').disabled = false;
			}
			document.getElementById('next').disabled = false;
			getLogs();
		}
	};
	
	//Go to next page
	document.getElementById('next').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (hasNext) {
			//there might be a next page
			current++;
			document.getElementById('previous').disabled = false;
			getLogs();
		}
	};
	
	getUserData();
};

function getUserData() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/users/" + userID,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			user = response;
			updateInfo(response);
			getEntity(localStorage['entity']);
			editsButtons();
			getLogs();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Empty or missing authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('User does not exist');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function updateInfo(user) {
	
	if(user.avatar==null)
		document.getElementById("avatar").src = "../resource/Boneco.png";
	else
		document.getElementById("avatar").src = user.avatar;
	
	document.getElementById("username").innerHTML = user.username;
	document.getElementById("name").innerHTML = user.name;
	var add = user.address;
	if(add == null)
		document.getElementById("address").innerHTML = "Morada não definida";
	else
		document.getElementById("address").innerHTML = add;
	
	document.getElementById("email").innerHTML = user.email;
	var date = new Date(user.registered);
	var months = [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
			"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ]
	document.getElementById("month").innerHTML = months[date.getMonth()];
	document.getElementById("day").innerHTML = date.getDate();	
	document.getElementById("year").innerHTML = date.getFullYear();
	
	var dateB = new Date(user.birthdate);
	document.getElementById("monthBirth").innerHTML = months[dateB.getMonth()];
	document.getElementById("dayBirth").innerHTML = dateB.getDate();	
	document.getElementById("yearBirth").innerHTML = dateB.getFullYear();
	
	document.getElementById("gender").innerHTML = user.gender;
	document.getElementById("phone").innerHTML = user.phone;
	
};

function getLogs() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cursors[current];
	
	//Generate url
	var url;
	if(cursor=="null")
		url="../rest/users/" + userID + "/log";
	else
		url="../rest/users/" + userID + "/log?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateLogs(response.logs);
			if ((current+1) >= cursors.length)
				cursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Utilizador inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};

function getEntity(idEntity) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];

	$.ajax({
		type : "GET",
		url : "../rest/entities/"+idEntity ,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			document.getElementById("entity").innerHTML  = response.name;
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Utilizador inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};

function populateLogs(data) {
	
	var messages = document.getElementById("logs");
	if (data.length == 0) {
		if(cursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existe histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existe mais histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var commt = document.createElement("div");
			commt.className = "container";
			
			var userBox = document.createElement("div");
			userBox.className = "comentario-user";
			var title = document.createElement("span");
			title.className = "title";
			title.innerHTML = data[i].date;
			userBox.appendChild(title);
			commt.appendChild(userBox);
			
			var commentBox = document.createElement("div");
			commentBox.className = "conteudo";
			var commentText = document.createElement("span");
			commentText.className = "commentText";
			commentText.innerHTML = data[i].description;
			commentBox.appendChild(commentText);
			
			commt.appendChild(commentBox);
			
			messages.insertBefore(commt, messages.children[0]);
		}
		document.getElementById('next').disabled = false;
		hasNext = true;
	}
	document.getElementById('current').innerHTML = (current+1);
};

function editsButtons() {
	
		$("#editsNameUser").show();
		$("#editsAddressUser").show();
		$("#editsPhoneUser").show();
		$("#editsBirthUser").show();
		
		//esconder tudo de mudar nome excepto o botao de editar
		$("#nameChange").hide();
		$("#editName").show();
		$("#cancelName").hide();
		$("#saveName").hide();

		//esconder tudo de mudar a morada excepto o botao de editar
		$("#addressChange").hide();
		$("#editAddress").show();
		$("#cancelAddress").hide();
		$("#saveAddress").hide();

		//esconder tudo de mudar o numero de telemovel excepto o botao de editar
		$("#phoneChange").hide();
		$("#editPhone").show();
		$("#cancelPhone").hide();
		$("#savePhone").hide();
		
		//esconder tudo de mudar a data de nascimento excepto o botao de editar
		$("#birthChange").hide();
		$("#editBirth").show();
		$("#cancelBirth").hide();
		$("#saveBirth").hide();
		
		//fazer resto...
		
// ...............................Update Name User.............................
		document.getElementById("editName").addEventListener("click",function(e) {
					$("#name").hide();
					$("#nameChange").show();
					$("#editName").hide();
					$("#cancelName").show();
					$("#saveName").show();
					document.getElementById("newName").value = user.name;
		});

		document.getElementById("saveName").addEventListener("click",function(e) {
					var data = document.getElementById("newName").value;
					var oldDate = new Date(user.birthdate);
					var year = oldDate.getFullYear();
					var month = oldDate.getMonth() + 1;
					var day = oldDate.getDate();
					var birthdayNew = year + "-" + month + "-" + day;
					console.log(data);
					updateDTUser(data, user.email, user.address, user.phone, user.gender, birthdayNew);
							//	 name,      email,      address,      phone,      gender, 	 birthday
		});

		document.getElementById("cancelName").addEventListener("click",function(e) {
					var data = document.getElementById("newName").value;
					$("#name").show();
					$("#nameChange").hide();
					$("#editName").show();
					$("#cancelName").hide();
					$("#saveName").hide();
		});
		
// ...............................Update User Address..........................
		
		document.getElementById("editAddress").addEventListener("click", function(e) {
					$("#address").hide();
					$("#addressChange").show();
					$("#editAddress").hide();
					$("#cancelAddress").show();
					$("#saveAddress").show();
					if(user.address != null){
						document.getElementById("newAddress").value = user.address;
					}else{
						document.getElementById("newAddress").value = "";
					}
		});
	
		document.getElementById("saveAddress").addEventListener("click", function(e) {
					var data = document.getElementById("newAddress").value;
					var oldDate = new Date(user.birthdate);
					var year = oldDate.getFullYear();
					var month = oldDate.getMonth() + 1;
					var day = oldDate.getDate();
					var birthdayNew = year + "-" + month + "-" + day;
					console.log(data);
					updateDTUser(user.name, user.email, data, user.phone, user.gender, birthdayNew);
					// 			      name,      email,address,    phone,      gender,    birthday
		});
	
		document.getElementById("cancelAddress").addEventListener("click",
				function(e) {
					var data = document.getElementById("newAddress").value;
					$("#address").show();
					$("#addressChange").hide();
					$("#editAddress").show();
					$("#cancelAddress").hide();
					$("#saveAddress").hide();
		});
		
// ...............................Update User Phone............................

		
		document.getElementById("editPhone").addEventListener("click", function(e) {
					$("#phone").hide();
					$("#phoneChange").show();
					$("#editPhone").hide();
					$("#cancelPhone").show();
					$("#savePhone").show();
					document.getElementById("newPhone").value = user.phone;					
		});
	
		document.getElementById("savePhone").addEventListener("click", function(e) {
					var data = document.getElementById("newPhone").value;
					if(data.length == 9){
					var oldDate = new Date(user.birthdate);
					var year = oldDate.getFullYear();
					var month = oldDate.getMonth() + 1;
					var day = oldDate.getDate();
					var birthdayNew = year + "-" + month + "-" + day;
					console.log(data);
					updateDTUser(user.name, user.email, user.address, data, user.gender, birthdayNew);
					// 			      name,      email,      address,phone,      gender,    birthday
					}else{
						showSnackbar('Número Inválido!');
					}
		});
	
		document.getElementById("cancelPhone").addEventListener("click",
				function(e) {
					var data = document.getElementById("newPhone").value;
					$("#phone").show();
					$("#phoneChange").hide();
					$("#editPhone").show();
					$("#cancelPhone").hide();
					$("#savePhone").hide();
		});
		
// ...............................Update User Birthdate........................

		
		document.getElementById("editBirth").addEventListener("click", function(e) {
					$("#birthday").hide();
					$("#birthChange").show();
					$("#editBirth").hide();
					$("#cancelBirth").show();
					$("#saveBirth").show();
					
		});
	
		document.getElementById("saveBirth").addEventListener("click", function(e) {
		
					var data = new Date(document.getElementById("newBirthdate").value);
					var year = data.getFullYear();
					var month = data.getMonth() + 1;
					var day = data.getDate();
					var birthdayNew = year + "-" + month + "-" + day;
					console.log(data);
					updateDTUser(user.name, user.email, user.address, user.phone, user.gender, birthdayNew);
					// 			      name,      email,      address,      phone,      gender,    birthday
					
		});
	
		document.getElementById("cancelBirth").addEventListener("click",
				function(e) {
					var data = document.getElementById("newBirthdate").value;
					$("#birthday").show();
					$("#birthChange").hide();
					$("#editBirth").show();
					$("#cancelBirth").hide();
					$("#saveBirth").hide();
		});
};

function updateDTUser(name, email, address, phone, gender, birthday){
	
	var data = {};
	data.name = name; data.email=email; data.address=address; 
	data.phone=phone; data.gender=gender; data.birthdate=birthday;
	console.log(data);
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "PUT",
		url : "../rest/users/" + user.username,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			showSnackbar('Informação do Utilizador Actualizada!');						
			location.reload();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 400) {
				showSnackbar('Edição inválida!');
			} else if (response.status == 404) {
				showSnackbar('Utilizador Não Existe!');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
	
};
