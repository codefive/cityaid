//Credit: https://www.w3schools.com/howto/howto_js_snackbar.asp
function showSnackbar(htmlMessage) {
	// Get the snackbar DIV
	var x = document.getElementById("snackbar")

	// Add the "show" class to DIV
	x.className = "show";

	x.innerHTML = htmlMessage;

	// After 3 seconds, remove the show class from DIV
	setTimeout(function() {
		x.className = x.className.replace("show", "");
	}, 3000);
};

//Credit: https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript/901144#901144
function getQueryParameterByName(name, url) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex
			.exec(url);
	if (!results)
		return null;
	if (!results[2])
		return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
};

function checkLogin() {
	if (!localStorage['tokenID']) {
		// User is not logged in. Redirecting...
		window.location.replace("../../index.html");
	}
};

function logout() {
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	$.ajax({
		type : "DELETE",
		url : "../../rest/logout",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			localStorage.removeItem('tokenID');
			localStorage.removeItem('userID');
			localStorage.removeItem('role');
			localStorage.removeItem('isModerator');
			window.location.href = "../../index.html";
		},
		error : function(response) {
			alert("Error: " + response.status);
		},
	});
};

function toggleOn(name) {
	var form = document.getElementById(name);
	if ('hidden' in form) { // Google Chrome and Internet Explorer
		if (typeof (form.hidden) === "boolean") { // Google Chrome
			form.hidden = !form.hidden;
		} else { // Internet Explorer
			form.hidden = (form.hidden == "true") ? "false" : "true";
		}
	} else {
		var value = form.getAttribute("hidden");
		form.setAttribute("hidden", value == "true" ? "false" : "true");
	}
};

function toggleOff(name) {
	var form = document.getElementById(name);
	if ('hidden' in form) { // Google Chrome and Internet Explorer
		if (typeof (form.hidden) === "boolean") { // Google Chrome
			form.hidden = !form.hidden;
		} else { // Internet Explorer
			form.hidden = (form.hidden == "false") ? "true" : "false";
		}
	} else {
		var value = form.getAttribute("hidden");
		form.setAttribute("hidden", value == "false" ? "true" : "false");
	}
};

function wait(ms) {
	console.log("[function wait(ms) | city-aid.js] Shame there ain't no sleep in JS");
	return;	
	//Old code below
	var start = new Date().getTime();
	var end = start;
	while(end < start + ms)
		end = new Date().getTime();
};

function myFunction() {
	var x = document.getElementById("myTopnav");
		 if (x.className === "topnav") {
    	x.className += " responsive";
	} else {
   	 x.className = "topnav";
	}
};

function getType(type) {
	var result;
	switch (type) {
	case "Acidente":
		result = "accident";
		break;
	case "Danos de material público":
		result = "furniture_damage";
		break;
	case "Lixo":
		result = "garbage";
		break;
	case "Natureza":
		result = "nature";
		break;
	case "Outros":
		result = "other";
		break;
	case "Danos de vias públicas":
		result = "street_damage";
		break;
	case "Vandalismo":
		result = "vandalism";
		break;
	default:
		result = "unknownType";
		break;
	}
	return result;
}

document.addEventListener('DOMContentLoaded', function () {
	//Request Notification permission
	if (window.location.pathname !== "/landing.html" && window.location.pathname !== "/index.html" && window.location.pathname !== "/register.html"){
		if (!("Notification" in window)) {
			alert('This browser does not support desktop notification'); 
			return;
		}		
		if (Notification.permission !== "granted" && Notification.permission !== "denied"){
			Notification.requestPermission().then(function(){
				if (Notification.permission === "granted"){
					//Create notification
					var notification = new Notification("CityAid", {
						icon: "https://city-aid.appspot.com/resource/cityaid-logo-mini.jpg",
						body: "Ativaste as Notificações Web"
					});
					notification.onclick = function () {
						window.open(notificationData.url);      
					};
				}
			});
		}
		if (Notification.permission === "granted"){
			//Create Pusher client and subscribe to notification channel
			//Pusher.logToConsole = true; // Enable pusher logging - don't include this in production
			var pusher = new Pusher('bc5606e25bb8d1738c33', {cluster: 'eu', encrypted: true, authEndpoint: '/rest/notifications/auth?token='+localStorage["tokenID"]+'&user='+localStorage["userID"]});
			var channel = pusher.subscribe('private-'+localStorage["userID"]);
			channel.bind('notification', function(data) {
				console.log(data.message);
	
				//Grab data
				var notificationData = JSON.parse(data.message);
				
				//Create notification
				var notification = new Notification(notificationData.title, {
					icon: notificationData.icon,
					body: notificationData.body
				});
				notification.onclick = function () {
					window.open(notificationData.url);      
				};
			});
			pusher.connection.bind( 'error', function( err ) {
				if( err.error.data.code === 4004 ) {
					console.log("Pusher connection limit exceeded.");
				}
			});
		}
	}
});