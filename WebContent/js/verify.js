window.onload = function() {
    var user = getQueryParameterByName("user", window.location);
	var token = getQueryParameterByName("token", window.location);
	verifyAccount(user, token);
};

function verifyAccount(user, token) {
    $.ajax({
        type: "GET",
        url: "../rest/users/"+user+"/verify?token="+token,
		crossDomain: true,
        dataType: "json",
        success: function(response) {
			window.location.replace("index.html");
        },
        error: function(response) {
			if (response.status == 400){
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 404){
				showSnackbar('User does not exist');
			} else if (response.status == 500){
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: "+ response.status);
				console.log(response);
			}
        }
    });
}