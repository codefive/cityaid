captureData = function(event) {
	var data = $('form[name="register"]').jsonify();

	if (data.password != data.confirm_password) {
		alert("Password mismatch!");
		return false;
	}

	data.confirm_password = undefined;

	$.ajax({
		type : "POST",
		url : "../rest/register",
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		// dataType: "json",
		success : function(response) {
			showSnackbar("Email de confirmação enviado");
			window.location.href = "./index.html";
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetro errado ou vazio');
			} else if (response.status == 409) {
				showSnackbar('Utilizador já existe');
			} else if (response.status == 500) {
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});

	event.preventDefault();
};

window.onload = function() {
	var frms = $('form[name="register"]');
	frms[0].onsubmit = captureData;
}