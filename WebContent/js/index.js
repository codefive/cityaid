captureData = function(event) {
    showSnackbar("Logging in");
    var data = $('form[name="login"]').jsonify();
    data.device = "Web"
    $.ajax({
        type: "POST",
        url: "../rest/login",
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        dataType: "json",
        success: function(response) {
            localStorage.setItem('tokenExpiration', response.expiration);
            localStorage.setItem('tokenID', response.token);
            localStorage.setItem('userID', data.username);
            localStorage.setItem('role', response.role);
            localStorage.setItem('isModerator', response.isModerator);
            localStorage.setItem('isManager', response.isManager);
            localStorage.setItem('entity', response.employer);
            
            if(response.role=="worker") {
				 window.location.href="./entities/dashboard.html";
			}
			else if(response.role=="backoffice")
				window.location.href="./backoffice/dashboard.html";
			else window.location.href="./frontoffice/dashboard.html";
            
        },
        error: function(response) {
        	if (response.status == 400) {
        		//do nothing
        	} else if (response.status == 403) {
				showSnackbar(response.responseJSON.message);
			} else if (response.status == 500) {
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: "+ response.status);
			}
        },
        data: JSON.stringify(data)
    });
    event.preventDefault();
};


window.onload = function() {
    var frms = $('form[name="login"]');     //var frms = document.getElementsByName("login");
    frms[0].onsubmit = captureData;
    document.getElementById("forgotPassword").addEventListener("click", requestPasswordReset);
}

requestPasswordReset = function(event){
	var user = document.getElementById("username").value;
	if (user === undefined || user.length == 0){
		showSnackbar("Indique o nome de utilizador");
		return;
	}
	
	$.ajax({
        type: "GET",
        url: "../rest/users/"+user+"/password/reset",
		crossDomain: true,
        dataType: "json",
        success: function(response) {
			showSnackbar('Email de reset enviado');
        },
        error: function(response) {
			if (response.status == 404){
				showSnackbar('Utilizador não existe');
			} else if (response.status == 500){
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: "+ response.status);
			}
        }
    });
}