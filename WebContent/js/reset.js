window.onload = function() {
	var frms = $('form[name="reset"]');
    frms[0].onsubmit = resetPassword;
}

resetPassword = function(event) {
	event.preventDefault();
	
	var user = getQueryParameterByName("user", window.location);
	var token = getQueryParameterByName("token", window.location);
	
	var data = $('form[name="reset"]').jsonify();
	if (data.newPassword != data.newPasswordConfirmation){
		showSnackbar("As passwords não são iguais!");
		return;
	}
	data.newPasswordConfirmation = undefined;
	data.resetToken = token;
	
	console.log(data);
	
	$.ajax({
        type: "PUT",
        url: "../rest/users/"+user+"/password",
        contentType: "application/json; charset=utf-8",
		crossDomain: true,
        dataType: "json",
        success: function(response) {
			window.location.replace("./index.html");
        },
        error: function(response) {
			if (response.status == 400){
				showSnackbar('Parâmetro errado ou vazio');
			} else if (response.status == 403){
				showSnackbar('Reset Token inválido');
			} else if (response.status == 404){
				showSnackbar('Utilizador inexistente');
			} else if (response.status == 500){
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: "+ response.status);
				console.log(response);
			}
        },
        data: JSON.stringify(data)
    });
};