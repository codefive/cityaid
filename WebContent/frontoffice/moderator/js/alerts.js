var iCursors = ["null"];
var cCursors = ["null"];
var iCounter = 0;
var cCounter = 0;
var iHasNext = true;
var cHasNext = true;

var okey = true;
var notOkey = false;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	
	if(iCounter == 0)
		document.getElementById('iPrevious').disabled = true;
	if(cCounter == 0)
		document.getElementById('cPrevious').disabled = true;
	
	document.getElementById('iPrevious').onclick = function(event) {
		document.getElementById('rissues').innerHTML = "";
		if (iCounter > 0) {
			//there is a previous page
			iCounter--;
			if (iCounter <= 0) {
				//previous page was the first page
				document.getElementById('iPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('iPrevious').disabled = false;
			}
			document.getElementById('iNext').disabled = false;
			getIssuesReports();
		}
	};
	
	//Go to next page
	document.getElementById('iNext').onclick = function(event) {
		document.getElementById('rissues').innerHTML = "";
		if (iHasNext) {
			//there might be a next page
			iCounter++;
			document.getElementById('iPrevious').disabled = false;
			getIssuesReports();
		}
	};
	
	document.getElementById('cPrevious').onclick = function(event) {
		document.getElementById('rcomments').innerHTML = "";
		if (cCounter > 0) {
			//there is a previous page
			cCounter--;
			if (cCounter <= 0) {
				//previous page was the first page
				document.getElementById('cPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('cPrevious').disabled = false;
			}
			document.getElementById('cNext').disabled = false;
			getCommentsReports();
		}
	};
	
	//Go to next page
	document.getElementById('cNext').onclick = function(event) {
		document.getElementById('rcomments').innerHTML = "";
		if (cHasNext) {
			//there might be a next page
			cCounter++;
			document.getElementById('cPrevious').disabled = false;
			getCommentsReports();
		}
	};
	getIssuesReports();
	getCommentsReports();
};

function populateIssueReports(data) {
	
	var repIssues = document.getElementById("rissues");
	if (data.length == 0) {
		if(iCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem nenhuma ocorrência reportada por resolver.";
			repIssues.insertBefore(title, repIssues.children[0]);
			document.getElementById('iNext').disabled = true;
			iHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem mais nenhuma ocorrência reportada por resolver.";
			repIssues.insertBefore(title, repIssues.children[0]);
			document.getElementById('iNext').disabled = true;
			iHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ocorr = document.createElement("div");
			ocorr.className = "container";
			var div2 = document.createElement("div");
			var title = document.createElement("p");
			title.className = "reportText";
			title.innerHTML = "Título da Ocorrência: " + data[i].title;
			var issueRef = document.createElement("a");
			issueRef.href = "../view.html?issue=" + data[i].issue;
			issueRef.appendChild(title);
			div2.appendChild(issueRef);
			var reporter = document.createElement("p");
			reporter.className = "reportText";
			reporter.innerHTML = "Ocorrência reportada por: " + data[i].reporter;
			div2.appendChild(reporter);
			var submiter = document.createElement("p");
			submiter.className = "reportText";
			submiter.innerHTML = "Criador ocorrência: " + data[i].submiter;
			div2.appendChild(submiter);
			var comment = document.createElement("p");
			comment.className ="reportText";
			comment.innerHTML = "Detalhes da denúncia: <p>" + data[i].comment;
			div2.appendChild(comment);
			
			
			var divButtons = document.createElement("div");
			divButtons.className = "reportButtons";
			divButtons.id = "reportButtonsIssues";	
			var buttonOkey = document.createElement("button");
			buttonOkey.className = "check-icon";
			buttonOkey.id = "acceptIssue";
			buttonOkey.setAttribute("onclick", "readIssue(\""+data[i].id+"\","+okey+",\""+data[i].issue+"\");")
			var tooltip = document.createElement("span");
			tooltip.className = "tooltiptext";
			tooltip.innerHTML = "Marcar Ocorrência como Apropriada";
			var imagBttn = document.createElement("i");
			imagBttn.innerHTML = "  Ignorar";
			imagBttn.className = "fa fa-check-circle  ";
			buttonOkey.appendChild(tooltip);
			buttonOkey.appendChild(imagBttn);
			divButtons.appendChild(buttonOkey);
				
			var buttonNotOkey = document.createElement("button");
			buttonNotOkey.className = "check-icon";
			buttonNotOkey.id = "acceptIssueNot";
			buttonNotOkey.setAttribute("onclick", "readIssue(\""+data[i].id+"\","+notOkey+",\""+data[i].issue+"\");")
			console.log(notOkey);
			var tooltip = document.createElement("span");
			tooltip.className = "tooltiptext";
			tooltip.innerHTML = "Marcar Ocorrência como Imprópria";
			var imagBttn = document.createElement("i");
			imagBttn.innerHTML = " Eliminar";
			imagBttn.className = "fa fa-trash-o";
			buttonNotOkey.appendChild(tooltip);
			buttonNotOkey.appendChild(imagBttn);
			divButtons.appendChild(buttonNotOkey);	
			div2.appendChild(divButtons);
			ocorr.appendChild(div2);			
			repIssues.insertBefore(ocorr, repIssues.children[0]);
		}
		document.getElementById('iNext').disabled = false;
		iHasNext = true;
	}
	document.getElementById('iCurrent').innerHTML = (iCounter+1);
}

function getIssuesReports() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = iCursors[iCounter];

	var url;
	if(cursor=="null")
		url="../../rest/reports/issues";
	else
		url="../../rest/reports/issues?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateIssueReports(response.reports);
			if((iCounter+1) >= iCursors.length)
				iCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function populateCommentsReports(data) {
	
	var repComments = document.getElementById("rcomments");
	if (data.length == 0) {
		if(cCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem nenhum comentário reportado por resolver.";
			repComments.insertBefore(title, repComments.children[0]);
			document.getElementById('cNext').disabled = true;
			cHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem mais nenhum comentário reportado por resolver.";
			repComments.insertBefore(title, repComments.children[0]);
			document.getElementById('cNext').disabled = true;
			cHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {	
			var reprt = document.createElement("div");
			reprt.className = "container";
			var title = document.createElement("p");
			title.className = "reportText";
			title.innerHTML = "Título da ocorrência: " + data[i].title;
			reprt.appendChild(title);
			var reporter = document.createElement("p");
			reporter.className = "reportText";
			reporter.innerHTML = "Ocorrência reportada por : " + data[i].reporter;
			reprt.appendChild(reporter);
			var submiter = document.createElement("p");
			submiter.className = "reportText";
			submiter.innerHTML = "Criador da ocorrência: " + data[i].submiter;
			reprt.appendChild(submiter);
			var commentReported = document.createElement("p");
			commentReported.className = "reportText";
			commentReported.innerHTML = "Comentário em causa: " + data[i].text;
			reprt.appendChild(commentReported);
			var comment = document.createElement("p");
			comment.className = "reportText";
			comment.innerHTML = "Detalhes da denúncia: <p>" + data[i].comment;
			reprt.appendChild(comment);
			
			var divButtons = document.createElement("div");
			divButtons.className = "reportButtons";
			divButtons.id = "reportButtonsComments";
			var buttonOkey = document.createElement("button");
			buttonOkey.className = "check-icon";
			buttonOkey.id = "acceptComment";
			buttonOkey.setAttribute("onclick", "readComment(\""+data[i].id+"\","+okey+",\""+data[i].issue+"\",\""+data[i].idComment+"\");")
			var tooltip = document.createElement("span");
			tooltip.className = "tooltiptext";
			tooltip.innerHTML = "Marcar Comentário como Apropriada";
			var imagBttn = document.createElement("i");
			imagBttn.innerHTML = "  Ignorar"
			imagBttn.className = "fa fa-check-circle  ";
			buttonOkey.appendChild(tooltip);
			buttonOkey.appendChild(imagBttn);
			divButtons.appendChild(buttonOkey);
				
			var buttonNotOkey = document.createElement("button");
			buttonNotOkey.className = "check-icon";
			buttonNotOkey.id = "acceptCommentNot";
			console.log(data[i].id);
			buttonNotOkey.setAttribute("onclick", "readComment(\""+data[i].id+"\","+notOkey+",\""+data[i].issue+"\",\""+data[i].idComment+"\");")
			var tooltip = document.createElement("span");
			tooltip.className = "tooltiptext";
			tooltip.innerHTML = "Marcar Comentário como Impróprio";
			var imagBttn = document.createElement("i");
			imagBttn.innerHTML = " Eliminar"
			imagBttn.className = "fa fa-trash-o";
			buttonNotOkey.appendChild(tooltip);
			buttonNotOkey.appendChild(imagBttn);
			divButtons.appendChild(buttonNotOkey);		
			
			reprt.appendChild(divButtons);	
			
			repComments.insertBefore(reprt, repComments.children[0]);
		}
		document.getElementById('cNext').disabled = false;
		cHasNext = true;
	}
	document.getElementById('cCurrent').innerHTML = (cCounter+1);
};

function getCommentsReports() {

	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cCursors[cCounter];
	
	var url;
	if(cursor=="null")
		url="../../rest/reports/comments";
	else
		url="../../rest/reports/comments?cursor=" + cursor;

	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateCommentsReports(response.reports);
			if((cCounter+1) >= cCursors.length)
				cCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function readComment(idReport, okey, idIssue, idComment){
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "PUT",
		url : "../../rest/reports/comments/" + idReport,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			if(okey){
				showSnackbar('Obrigado pela sua Colaboração!');
				location.reload();
			}else{
				deleteComment(idComment, idIssue);				
			}
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 400) {
				showSnackbar('Edição inválida!');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
	});		
};

function deleteComment(idComment, idIssue){
	

	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "DELETE",
		url : "../../rest/issues/"+idIssue+"/comments/"+idComment,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			showSnackbar('Obrigado pela sua Colaboração!');
			location.reload();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 400) {
				showSnackbar('Edição inválida!');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
	});
};


function readIssue(idReport, okey, idIssue){
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "PUT",
		url : "../../rest/reports/issues/" + idReport,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			if(okey) {
				showSnackbar('Obrigado pela sua Colaboração!');
				location.reload();
			} else{
				deleteIssue(idIssue);				
			}
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 400) {
				showSnackbar('Edição inválida!');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
	});		
};

function deleteIssue(idIssue){	

	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "DELETE",
		url : "../../rest/issues/"+idIssue,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			showSnackbar('Obrigado pela sua Colaboração!');
			location.reload();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 400) {
				showSnackbar('Edição inválida!');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
	});
};