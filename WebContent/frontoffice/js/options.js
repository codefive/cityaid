var userOptions = {};

window.onload = function() {
	checkLogin();
	showSnackbar("A Carregar Informação...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Moderadores.......................
	var isMdtr = localStorage['isModerator'];
	if(isMdtr=="true"){		
		$( "moderatorAlerts" ).addClass( "hidden" );
	}
	//...................................................
	getUserStats();
}

function getUserStats() {
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];

	$.ajax({
		type : "GET",
		url : "../rest/users/"+ userID +"/options",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {		
			userOptions = response;
			buttonsOptions(response);			
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function buttonsOptions(response){
	
	//watchlist options
	if(response.auto_follow){
		document.getElementById("autoFollow").checked = true;
	}else{
		document.getElementById("autoFollow").checked = false;
	}
	
	//push notifications options
	if(response.push_medal){
		document.getElementById("pushMedals").checked = true;
	}else{
		document.getElementById("pushMedals").checked = false;
	}
	if(response.push_level_up){
		document.getElementById("pushLevelUp").checked = true;
	}else{
		document.getElementById("pushLevelUp").checked = false;
	}
	if(response.push_issue_wip){
		document.getElementById("pushIssueWIP").checked = true;
	}else{
		document.getElementById("pushIssueWIP").checked = false;
	}
	if(response.push_issue_closed){
		document.getElementById("pushIssueClosed").checked = true;
	}else{
		document.getElementById("pushIssueClosed").checked = false;
	}
	if(response.push_issue_solved){
		document.getElementById("pushIssueSolved").checked = true;
	}else{
	document.getElementById("pushIssueSolved").checked = false;
	}
	if(response.push_log_comment){
		document.getElementById("pushLogComment").checked = true;
	}else{
		document.getElementById("pushLogComment").checked = false;
	}
	if(response.push_edit){
		document.getElementById("pushEdit").checked = true;
	}else{
		document.getElementById("pushEdit").checked = false;
	}
	if(response.push_comment){
		document.getElementById("pushComment").checked = true;
	}else{
		document.getElementById("pushComment").checked = false;
	}
	
	//email options
	if(response.email_medal){
		document.getElementById("emailMedal").checked = true;
	}else{
		document.getElementById("emailMedal").checked = false;
	}
	if(response.email_level_up){
		document.getElementById("emailLevelUp").checked = true;
	}else{
		document.getElementById("emailLevelUp").checked = false;
	}
	if(response.email_issue_wip){
		document.getElementById("emailIssueWIP").checked = true;
	}else{
		document.getElementById("emailIssueWIP").checked = false;
	}
	if(response.email_issue_closed){
		document.getElementById("emailIssueClosed").checked = true;
	}else{
		document.getElementById("emailIssueClosed").checked = false;
	}
	if(response.email_issue_solved){
		document.getElementById("emailIssueSolved").checked = true;
	}else{
	document.getElementById("emailIssueSolved").checked = false;
	}
	if(response.email_log_comment){
		document.getElementById("emailLogComment").checked = true;
	}else{
		document.getElementById("emailLogComment").checked = false;
	}
	if(response.email_edit){
		document.getElementById("emailEdit").checked = true;
	}else{
		document.getElementById("emailEdit").checked = false;
	}
	if(response.email_comment){
		document.getElementById("emailComment").checked = true;
	}else{
		document.getElementById("emailComment").checked = false;
	}
	
	document.getElementById('saveOptions').onclick = function(event) {
		updateOptionsUser();		
	};		
};

function updateOptionsUser(){
	
	var data = {};
	
	data.auto_follow = document.getElementById("autoFollow").checked;
	
	data.push_medal = document.getElementById("pushMedals").checked; 
	data.push_level_up = document.getElementById("pushLevelUp").checked; 
	data.push_issue_wip = document.getElementById("pushIssueWIP").checked; 
	data.push_issue_closed = document.getElementById("pushIssueClosed").checked; 
	data.push_issue_solved = document.getElementById("pushIssueSolved").checked;
	data.push_log_comment = document.getElementById("pushLogComment").checked; 
	data.push_edit = document.getElementById("pushEdit").checked; 
	data.push_comment = document.getElementById("pushComment").checked; 
	
	data.email_medal = document.getElementById("emailMedal").checked; 
	data.email_level_up = document.getElementById("emailLevelUp").checked; 
	data.email_issue_wip = document.getElementById("emailIssueWIP").checked; 
	data.email_issue_closed = document.getElementById("emailIssueClosed").checked; 
	data.email_issue_solved = document.getElementById("emailIssueSolved").checked;
	data.email_log_comment = document.getElementById("emailLogComment").checked; 
	data.email_edit = document.getElementById("emailEdit").checked; 
	data.email_comment = document.getElementById("emailComment").checked; 
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "PUT",
		url : "../rest/users/"+userID+"/options",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {						
			location.reload();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 400) {
				showSnackbar('Edição inválida!');
			} else if (response.status == 404) {
				showSnackbar('Utilizador Não Existe!');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
	
};