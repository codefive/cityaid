var cursors = ["null"];
var current = 0;
var hasNext = true;
var user;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	
	//Editar Menu para Moderadores.............................................
	var isMdtr = localStorage['isModerator'];
	
	if(isMdtr=="true") {		
		$( "moderatorAlerts" ).addClass( "hidden" );
	}
	//.........................................................................
	
	user = getQueryParameterByName("username", window.location);
	
	if(current == 0)
		document.getElementById('previous').disabled = true;

	//Go to previous page
	document.getElementById('previous').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (current > 0) {
			//there is a previous page
			current--;
			if (current <= 0) {
				//previous page was the first page
				document.getElementById('previous').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('previous').disabled = false;
			}
			document.getElementById('next').disabled = false;
			getLogs();
		}
	};
	
	//Go to next page
	document.getElementById('next').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (hasNext) {
			//there might be a next page
			current++;
			document.getElementById('previous').disabled = false;
			getLogs();
		}
	};
	
	getUserData();
	
//.........................................REPORT ISSUE........................
	
	// Get the modal
	var modal = document.getElementById('myModal');

	// When the user clicks the , open the modal 
	document.getElementById("reportUser").addEventListener("click", function(e) {
		document.getElementById("reportText").value = '';
		$(document.getElementById('myModal')).show();
		$(document.getElementById('userModal')).show();
	});
	
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		$(document.getElementById('userModal')).hide();
		$(document.getElementById('myModal')).hide();
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	    	$(document.getElementById('userModal')).hide();
			$(document.getElementById('myModal')).hide();
	    }
	}
	
	document.getElementById("reportBttn").addEventListener("click", function(e) {
		if($('input:radio:checked').length > 0)
			reportUser();
		else
			showSnackbar('Selecione uma das opcões!');
	});
//.............................................................................
	
};

function getUserData() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/users/" + user,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			getInfo(response);
			getLogs();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Empty or missing authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Issue does not exist');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function getInfo(user) {
	
	if(user.avatar==null)
		document.getElementById("avatar").src = "../resource/Boneco.png";
	else
		document.getElementById("avatar").src = user.avatar;
	
	document.getElementById("username").innerHTML = user.username;
	document.getElementById("name").innerHTML = user.name;
	var add = user.address;
	if(add == null)
		document.getElementById("address").innerHTML = "Morada não definida";
	else
		document.getElementById("address").innerHTML = add;
	
	document.getElementById("email").innerHTML = user.email;
	var dateR = new Date(user.registered);
	var months = [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
			"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ]
	document.getElementById("month").innerHTML = months[dateR.getMonth()];
	document.getElementById("day").innerHTML = dateR.getDate();	
	document.getElementById("year").innerHTML = dateR.getFullYear();
	
	var date = new Date(user.birthdate);
	document.getElementById("monthBirth").innerHTML = months[date.getMonth()];
	document.getElementById("dayBirth").innerHTML = date.getDate();
	document.getElementById("yearBirth").innerHTML = date.getFullYear();
	document.getElementById("gender").innerHTML = user.gender;
	document.getElementById("phone").innerHTML = user.phone;
	
	var emp = user.employer;
	if(emp == null)
		document.getElementById("entity").innerHTML = "Não trabalha para nenhuma entidade";
	else
		document.getElementById("entity").innerHTML = emp;
};

function getLogs() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cursors[current];
	
	//Generate url
	var url;
	if(cursor=="null")
		url="../rest/users/" + user + "/log";
	else
		url="../rest/users/" + user + "/log?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateLogs(response.logs);
			if ((current+1) >= cursors.length)
				cursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};

function populateLogs(data) {
	
	var messages = document.getElementById("logs");
	if (data.length == 0) {
		if(cursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "O utilizador não tem histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "O utilizador não tem mais histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var commt = document.createElement("div");
			commt.className = "container";
			
			var userBox = document.createElement("div");
			userBox.className = "comentario-user";
			var title = document.createElement("span");
			title.className = "title";
			title.innerHTML = data[i].date;
			userBox.appendChild(title);
			commt.appendChild(userBox);
			
			var commentBox = document.createElement("div");
			commentBox.className = "conteudo";
			var commentText = document.createElement("span");
			commentText.className = "commentText";
			commentText.innerHTML = data[i].description;
			commentBox.appendChild(commentText);
			
			commt.appendChild(commentBox);
			
			messages.insertBefore(commt, messages.children[0]);
		}
		document.getElementById('next').disabled = false;
		hasNext = true;
	}
	document.getElementById('current').innerHTML = (current+1);
};

function reportUser() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.user = user;
	
	var type = "";
	var op = document.getElementsByName("answer");
	for (var i = 0; i < op.length; i++) {
		if (op[i].checked)
			type = op[i].value;
	}
	var commtext = document.getElementById("reportText").value;
	var text = "";
	if(commtext != "")
		text = " - " + commtext;
	data.comment = type + text;
	console.log(data.comment);
	
	$.ajax({
		type : "POST",
		url : "../rest/reports/users",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			$(document.getElementById('userModal')).hide();
			$(document.getElementById('myModal')).hide();
			showSnackbar('Utilizador Reportado!');
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos ou em falta');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 409) {
				$(document.getElementById('myModal')).hide();
				$(document.getElementById('userModal')).hide();
				showSnackbar('Já reportou este Utilizador!');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
};