

window.onload = function() {
	checkLogin();
	showSnackbar("A Carregar Informação...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Moderadores.......................
	var isMdtr = localStorage['isModerator'];
	if(isMdtr=="true"){		
		$( "moderatorAlerts" ).addClass( "hidden" );}
	//...................................................
		
	document.getElementById('createIssue').onclick = function(event) {
		$("#tcreateIssue").toggle();	
	};
	document.getElementById('changePhoto').onclick = function(event) {
		$("#tchangePhoto").toggle();	
	};	
	document.getElementById('saveWatchlist').onclick = function(event) {
		$("#tsaveWatchlist").toggle();	
	};	
	document.getElementById('winPoints').onclick = function(event) {
		$("#twinPoints").toggle();	
	};	
	document.getElementById('winMedals').onclick = function(event) {
		$("#twinMedals").toggle();	
	};	
	document.getElementById('levelUp').onclick = function(event) {
		$("#tlevelUp").toggle();	
	};	
	document.getElementById('reportIssue').onclick = function(event) {
		$("#treportIssue").toggle();	
	};	
	document.getElementById('reportUser').onclick = function(event) {
		$("#treportUser").toggle();	
	};	
};
