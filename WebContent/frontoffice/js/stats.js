window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Moderadores.......................
	var isMdtr = localStorage['isModerator'];
	if(isMdtr=="true"){		
		$( "moderatorAlerts" ).addClass( "hidden" );
	}
	//...................................................
	getUserStats();
}

function getUserStats() {
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];

	$.ajax({
		type : "GET",
		url : "../rest/stats/users/" + userID,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			document.getElementById("issuesCreated").innerHTML = response.issuesCreated;
			document.getElementById("issuesResolved").innerHTML = response.issuesSolved;
			document.getElementById("issuesUnresolved").innerHTML = response.issuesCreated - response.issuesProgress
			- response.issuesSolved;
			document.getElementById("issuesInProgress").innerHTML = response.issuesProgress;
			document.getElementById("issuesFollowed").innerHTML = response.issuesFollowed;
			document.getElementById("comments").innerHTML = response.issuesCommented;
			document.getElementById("ratings").innerHTML = response.issuesRated;
			
			document.getElementById("numberMedals").innerHTML = response.numAwardedMedals;
			document.getElementById("rank").innerHTML = response.level;
			document.getElementById("points").innerHTML = response.points;
			document.getElementById("pointsUntilNext").innerHTML = response.thresholdNextLevel - response.points;
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};