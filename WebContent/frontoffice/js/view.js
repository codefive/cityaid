var issue;
var fullIssue = {};
var map;
var following = false;
var commentToReport = {};
var slideIndex = 1;
var entities;

var uCursors = ["null"];
var wCursors = ["null"];
var uCounter = 0;
var wCounter = 0;
var uHasNext = true;
var wHasNext = true;

window.onload = function() {
	checkLogin();
	showSnackbar("A carregar a página...");
	document.getElementById('logout').onclick = logout;
	
	//Editar Menu para Moderadores.........................
	var isMdtr = localStorage['isModerator'];
	if(isMdtr=="true") {		
		$( "moderatorAlerts" ).addClass( "hidden" );
	}
	//.....................................................
	
	issue = getQueryParameterByName("issue", window.location);
	getIssueData();
	getRating();
	
	if(uCounter == 0)
		document.getElementById('uPrevious').disabled = true;
	if(wCounter == 0)
		document.getElementById('wPrevious').disabled = true;
	
	document.getElementById('uPrevious').onclick = function(event) {
		document.getElementById('comments').innerHTML = "";
		if (uCounter > 0) {
			//there is a previous page
			uCounter--;
			if (uCounter <= 0) {
				//previous page was the first page
				document.getElementById('uPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('uPrevious').disabled = false;
			}
			document.getElementById('uNext').disabled = false;
			getComments();
		}
	};
	
	//Go to next page
	document.getElementById('uNext').onclick = function(event) {
		document.getElementById('comments').innerHTML = "";
		if (uHasNext) {
			//there might be a next page
			uCounter++;
			document.getElementById('uPrevious').disabled = false;
			getComments();
		}
	};
	
	document.getElementById('wPrevious').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (wCounter > 0) {
			//there is a previous page
			wCounter--;
			if (wCounter <= 0) {
				//previous page was the first page
				document.getElementById('wPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('wPrevious').disabled = false;
			}
			document.getElementById('wNext').disabled = false;
			getLogs();
		}
	};
	
	//Go to next page
	document.getElementById('wNext').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (wHasNext) {
			//there might be a next page
			wCounter++;
			document.getElementById('wPrevious').disabled = false;
			getLogs();
		}
	};
	
	getComments();
	getLogs();

	document.getElementById("follow").addEventListener("click", function(e) {
		followIssue();
	});

	document.getElementById("saveComment").addEventListener("click", function(e) {
		var data = document.getElementById("comment").value;
		saveComment(data);
	});
	
	document.getElementById("priority-low").addEventListener("click", function(e) {
		rate(1);
	});
	document.getElementById("priority-medium").addEventListener("click", function(e) {
		rate(2);
	});
	document.getElementById("priority-high").addEventListener("click", function(e) {
		rate(3);
	});
	
	document.getElementById("guardarEntityRating").addEventListener("click", function(e) {
		var entityRating = document.getElementById("entityRatingValue").value;
		rateEntity(Number.parseInt(entityRating));
	});
	
	//.........................................REPORT ISSUE..............................
	
	// Get the modal
	var modal = document.getElementById('myModal');

	// When the user clicks the , open the modal 
	document.getElementById("reportIssue").addEventListener("click", function(e) {
		document.getElementById("reportText").value = '';
		$(document.getElementById('myModal')).show();
		$(document.getElementById('issueModal')).show();
		$(document.getElementById('commentModal')).hide();
		$(document.getElementById('modalImages')).hide();
		$(document.getElementById('modalImagesContent')).hide();
	});
	
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		$(document.getElementById('issueModal')).hide();
		$(document.getElementById('commentModal')).hide();
		$(document.getElementById('modalImages')).hide();
		$(document.getElementById('myModal')).hide();
		$(document.getElementById('modalImagesContent')).hide();
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	    	$(document.getElementById('issueModal')).hide();
			$(document.getElementById('commentModal')).hide();
			$(document.getElementById('modalImages')).hide();
			$(document.getElementById('myModal')).hide();
			$(document.getElementById('modalImagesContent')).hide();
	    }
	}
	
	document.getElementById("reportBttn").addEventListener("click", function(e) {
		if($('input:radio:checked').length > 0)
			reportIssue();
		else
			showSnackbar('Selecione uma das opcões!');
	});
};

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center : {
			lat : 0,
			lng : 0
		},
		zoom : 16
	});
};

function getIssueData() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/issues/" + issue,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			fullIssue = response;
			editsButtons(fullIssue.submitter);
			following = response.following;
			if(following) {
				$("#follow").toggleClass("activated");
				$("#follow").prop('value', 'Adicionado');
			}
			solved = (response.state === "solved");
			if (solved){
				$("#seguir").hide();
				$("#prior").hide();
				if (following){
					$("#ratingEntity").show();
					entities = response.entities;
				} else {
					$("#ratingEntity").hide();
				}
				console.log("is solved");
			} else {
				$("#seguir").show();
				$("#prior").show();
				$("#ratingEntity").hide();
				console.log("is not");
			}
			placeMarker(response);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};

function followIssue() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.issue = issue;
		
	if (!following) {
		$.ajax({
			type : "POST",
			url : "../rest/users/" + userID + "/watchlist",
			headers : {
				'Authorization' : tokenID,
				'User' : userID
			},
			contentType : "application/json; charset=utf-8",
			crossDomain : true,
			dataType : "json",

			success : function(response) {
				showSnackbar("Seguiu a ocorrência");
				$("#follow").toggleClass("activated");
				following = true;
				$("#follow").prop('value', 'Adicionado');
			},
			error : function(response) {
				if (response.status == 400) {
					showSnackbar('Parâmetros inválidos');
				} else if (response.status == 401) {
					showSnackbar('Autorização inválida');
				} else if (response.status == 403) {
					showSnackbar('Token inválido');
				} else if (response.status == 404) {
					showSnackbar('Ocorrência inexistente');
				} else if (response.status == 409) {
					showSnackbar('Utilizador já segue a ocorrência');
				} else if (response.status == 500) {
					showSnackbar('Erro de servidor');
				} else {
					showSnackbar("Erro: " + response.status);
				}
			},
			data : JSON.stringify(data)
		});
	} else {
		$.ajax({
			type : "DELETE",
			url : "../rest/users/" + userID + "/watchlist/" + issue,
			headers : {
				'Authorization' : tokenID,
				'User' : userID
			},
			contentType : "application/json; charset=utf-8",
			crossDomain : true,
			dataType : "json",
			success : function(response) {
				showSnackbar("Deixou de seguir a ocorrência");
				$("#follow").toggleClass("activated");
				following = false;
				$("#follow").prop('value', 'Adicionar');
			},
			error : function(response) {
				if (response.status == 400) {
					showSnackbar('Parâmetros inválidos');
				} else if (response.status == 401) {
					showSnackbar('Autorização inválida');
				} else if (response.status == 403) {
					showSnackbar('Token inválido');
				} else if (response.status == 404) {
					showSnackbar('Utilizador não está a seguir a ocorrência');
				} else if (response.status == 500) {
					showSnackbar('Erro de servidor');
				} else {
					showSnackbar("Erro: " + response.status);
				}
			},
		});
	}
};

function placeMarker(issueData) {
	
	map = new google.maps.Map(document.getElementById('map'), {
		center : {
			lat : issueData.latitude,
			lng : issueData.longitude
		},
		zoom : 16
	});

	var coords = new google.maps.LatLng(issueData.latitude, issueData.longitude);

	var marker = new google.maps.Marker({
		position : coords,
		map : map,
		title : "Ocorrência"
	});

	var add;

	var geocoder = new google.maps.Geocoder;

	geocoder.geocode({'location' : marker.position}, function(results, status) {
		if (status === 'OK') {
			if (results[1]) {
				add = results[1].formatted_address;
				document.getElementById("location").innerHTML = add;
			} else {
				document.getElementById("location").innerHTML = "Morada não encontrada";
			}
		} else {
			document.getElementById("location").innerHTML = "Morada não encontrada";
		}
	});
	
	var thumbs = document.getElementById("thumbnails");
	var putModel = document.getElementById("modalImages-content");
	for (var i = 1; i < issueData.pictures.length + 1; i++) {
		var pics = document.createElement("div");
		pics.className = "picContainer";
		var imagem = document.createElement("img");
		imagem.className = "imagem";
		imagem.src = "../gcs/city-aid.appspot.com/" + issueData.pictures[i - 1];
		imagem.className = "houver-shadow";
		var attribute =i;
		imagem.setAttribute("onclick", "openModal();currentSlide("+attribute+");closeModalGray()");
		pics.appendChild(imagem);
		thumbs.appendChild(pics);
	}
	
	var putModel = document.getElementById("modalImagesContent");
	for (var j = 1; j < issueData.pictures.length + 1; j++) {
		var pics = document.createElement("div");
		pics.className = "mySlides";
		var text = document.createElement("div");
		text.className = "numbertext";
		text.innerHTML = j + " / " + issueData.pictures.length;
		pics.appendChild(text);
		var imagem = document.createElement("img");
		imagem.src = "../gcs/city-aid.appspot.com/" + issueData.pictures[j-1];
		imagem.className = "houver-shadow modalImage";
		pics.appendChild(imagem);
		putModel.appendChild(pics);
	}
	
	document.getElementById("title").innerHTML = issueData.title;
	document.getElementById("description").innerHTML = issueData.description;
	document.getElementById("type").innerHTML = issueData.type;
	document.getElementById("submitter").innerHTML = issueData.submitter;
	document.getElementById("submitter").href = "./viewUser.html?username=" + issueData.submitter;
	
	var date = new Date(issueData.created);
	var months = [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
		"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ]
	document.getElementById("month").innerHTML = months[date.getMonth()];
	document.getElementById("year").innerHTML = date.getFullYear();
	document.getElementById("day").innerHTML = date.getDate();
	
	var ret = "";
	
	switch(issueData.priority) {
		case 1:
			ret = "Baixa";
			break;
		case 2:
			ret = "Média";
			break;
		case 3:
			ret = "Alta";
			break;
	}
	
	switch(issueData.state) {
	case 'created':
		$("#progress-bar-created").addClass('created');
		$("#created-label").addClass('active');
		break;
	case 'work_in_progress':
		$("#progress-bar-created").addClass('wip');
		$("#progress-bar-line-to-wip").addClass('wip');
		$("#progress-bar-wip").addClass('wip');
		$("#wip-label").addClass('active');
		break;
	case 'solved':
		$("#progress-bar-created").addClass('solved');
		$("#progress-bar-line-to-wip").addClass('solved');
		$("#progress-bar-wip").addClass('solved');
		$("#progress-bar-line-to-solved").addClass('solved');
		$("#progress-bar-solved").addClass('solved');
		$("#solved-label").addClass('active');
		break;
	case 'closed':
		break;
	}
};

function rate(priority) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.priority = priority;
	
	$.ajax({
		type : "POST",
		url : "../rest/issues/" + issue + "/ratings",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Rating inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else if (response.status == 409) {
				updateRating(priority);
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
};

function updateRating(priority) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.priority = priority;
	
	$.ajax({
		type : "PUT",
		url : "../rest/issues/" + issue + "/ratings/" + userID,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
};

function getRating() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/issues/" + issue + "/ratings/" + userID,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			switch(response.priority) {
			case 1:
				$("#priority-low").toggleClass("activated");
				break;
			case 2:
				$("#priority-medium").toggleClass("activated");
				break;
			case 3:
				$("#priority-high").toggleClass("activated");
				break;
			}
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				console.log('Utilizador ainda não avaliou a ocorrência');
			} else if (response.status == 404) {
				console.log('Utilizador ainda não avaliou a ocorrência');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
	});
	
};

function saveComment(commt) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.comment = commt;
	
	$.ajax({
		type : "POST",
		url : "../rest/issues/" + issue + "/comments",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});

	event.preventDefault();
};

function populateComments(data) {
	
	var messages = document.getElementById("comments");
	if (data.length == 0) {
		if(uCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "A ocorrência não tem comentários!";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "A ocorrência não tem mais comentários.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var commt = document.createElement("div");
			commt.className = "container";
			
			var userBox = document.createElement("div");
			userBox.className = "comentario-user";
			var title = document.createElement("p");
			title.className = "title";
			title.innerHTML = data[i].author;
			
			var bttnReport = document.createElement("button");
			bttnReport.className = "report-icon";
			bttnReport.id = "reportComment";
			bttnReport.setAttribute("onclick", "takeCareReportComment(\""+data[i].id+"\");")
			var tooltip = document.createElement("span");
			tooltip.className = "tooltiptext";
			tooltip.innerHTML = "Reportar Comentário";
			var imagBttn = document.createElement("i");
			imagBttn.className = "fa fa-exclamation-triangle";
			bttnReport.appendChild(tooltip);
			bttnReport.appendChild(imagBttn);
			title.appendChild(bttnReport);
			userBox.appendChild(title);
			commt.appendChild(userBox);
			
			var commentBox = document.createElement("div");
			commentBox.className = "conteudo";
			var commentText = document.createElement("span");
			commentText.className = "commentText";
			commentText.innerHTML = data[i].comment;
			commentBox.appendChild(commentText);
			
			var postedTime = document.createElement("span");
			postedTime.className = "postedTime";
			postedTime.innerHTML = "<br>" + data[i].submitted;	
			commentBox.appendChild(postedTime);
			commt.appendChild (commentBox);
			
			messages.insertBefore(commt, messages.children[data.length]);
		}
		document.getElementById('uNext').disabled = false;
		uHasNext = true;
	}
	document.getElementById('uCurrent').innerHTML = (uCounter+1);
};


function getComments() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = uCursors[uCounter];
	
	//Generate url
	var url;
	if(cursor=="null")
		url="../rest/issues/" + issue + "/comments";
	else
		url="../rest/issues/" + issue + "/comments?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateComments(response.comments);
			if((uCounter+1) >= uCursors.length)
				uCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};

function reportIssue() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.issue = issue;
	var type = "";
	var op = document.getElementsByName("answer");
	for (var i = 0; i < op.length; i++) {
		if (op[i].checked) {
			type = op[i].value;
		}
	}
	var commtext = document.getElementById("reportText").value;
	var text = "";
	console.log(commtext);
	if(commtext != ""){
		text = " - " + commtext;
	}
	data.comment = type + text;
	
	$.ajax({
		type : "POST",
		url : "../rest/reports/issues",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			$(document.getElementById('issueModal')).hide();
			$(document.getElementById('myModal')).hide();
			showSnackbar('Ocorrência Reportada!');
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos ou em falta');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 409) {
				$(document.getElementById('commentModal')).hide();
				$(document.getElementById('myModal')).hide();
				$(document.getElementById('issueModal')).hide();
				$(document.getElementById('modalImages')).hide();
				$(document.getElementById('modalImagesContent')).hide();
				showSnackbar('Já reportou esta ocorrência!');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
	
};

function reportComment(id) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.report = id;
	var type = "";
	var op = document.getElementsByName("answerCommt");
	for (var i = 0; i < op.length; i++) {
		if (op[i].checked) {
			type = op[i].value;
		}
	}
	var commtext = document.getElementById("reportTextCommt").value
	var text = "";
	if(commtext != null){
		text = " - " + commtext;
	}
	data.comment = type + text;
	
	$.ajax({
		type : "POST",
		url : "../rest/reports/comments/",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			$(document.getElementById('commentModal')).hide();
			$(document.getElementById('myModal')).hide();
			showSnackbar('Comentário Reportado!');
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else if (response.status == 409) {
				$(document.getElementById('commentModal')).hide();
				$(document.getElementById('issueModal')).hide();
				$(document.getElementById('modalImages')).hide();
				$(document.getElementById('modalImagesContent')).hide();
				$(document.getElementById('myModal')).hide();
				showSnackbar('Já reportou esta ocorrência!');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
	
};

function takeCareReportComment(data) {
	
	document.getElementById("reportTextCommt").value = '';
	// Get the modal
	var modal = document.getElementById('myModal');
	
	$(document.getElementById('modalImagesContent')).hide();
	$(document.getElementById('issueModal')).hide();
	$(document.getElementById('modalImages')).hide();
	$(document.getElementById('myModal')).show();
	$(document.getElementById('commentModal')).show();
	

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[1];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		$(document.getElementById('commentModal')).hide();
		$(document.getElementById('myModal')).hide();
		$(document.getElementById('issueModal')).hide();		
		$(document.getElementById('modalImagesContent')).hide();
		$(document.getElementById('modalImages')).hide();
		
	}
	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
		if (event.target == modal) {
			$(document.getElementById('commentModal')).hide();
			$(document.getElementById('myModal')).hide();
			$(document.getElementById('issueModal')).hide();		
			$(document.getElementById('modalImagesContent')).hide();
			$(document.getElementById('modalImages')).hide();
		}
	}
	document.getElementById("reportCommtBttn").addEventListener("click", function(e) {
		if ($('input:radio:checked').length > 0)
			reportComment(data);
		else
			showSnackbar('Selecione uma das opcões!');
	});
};

//.....................Codigo display das fotos da ocorrencia..................
function openModal() {
	$(document.getElementById('issueModal')).hide();
	$(document.getElementById('commentModal')).hide();
	$(document.getElementById('myModal')).hide();
	$(document.getElementById('modalImages')).show();
	$(document.getElementById('modalImagesContent')).show();

};

function closeModal() {
	$(document.getElementById('modalImagesContent')).hide();
	$(document.getElementById('modalImages')).hide();
	$(document.getElementById('issueModal')).hide();
	$(document.getElementById('commentModal')).hide();
	$(document.getElementById('myModal')).hide();

};

function closeModalGray() {
	window.onclick = function(event) {
		var modal = document.getElementById('modalImages');
		if (event.target == modal) {
			$(document.getElementById('modalImagesContent')).hide();
			$(document.getElementById('modalImages')).hide();
			$(document.getElementById('issueModal')).hide();
			$(document.getElementById('commentModal')).hide();
			$(document.getElementById('myModal')).hide();
		}
	}
};

function plusSlides(n) {
	showSlides(slideIndex += n);
}

function currentSlide(n) {
	 showSlides(slideIndex = n);
}

function showSlides(n) {
	 var i;
	 var slides = document.getElementsByClassName("mySlides");
	 var captionText = document.getElementById("caption");
	 if (n > slides.length) {slideIndex = 1}
	 if (n < 1) {slideIndex = slides.length}
	 for (i = 0; i < slides.length; i++) {
	     slides[i].style.display = "none";
	 }
	 slides[slideIndex-1].style.display = "block";
}
//.............................................................................

function getLogs() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = wCursors[wCounter];
	
	var url;
	if(cursor=="null")
		url="../rest/issues/" + issue + "/log";
	else
		url="../rest/issues/" + issue + "/log?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateLogs(response.logs);
			if((wCounter+1) >= wCursors.length)
				wCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};


function populateLogs(data) {
	
	var messages = document.getElementById("logs");
	if (data.length == 0) {
		if(wCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "A ocorrência não tem histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "A ocorrência não tem mais histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var commt = document.createElement("div");
			commt.className = "container";
			
			var userBox = document.createElement("div");
			userBox.className = "comentario-user";
			var title = document.createElement("p");
			title.className = "title";
			title.innerHTML = data[i].user;
			userBox.appendChild(title);
			commt.appendChild(userBox);
			
			var commentBox = document.createElement("div");
			commentBox.className = "conteudo";
			var commentText = document.createElement("span");
			commentText.className = "commentText";
			commentText.innerHTML = data[i].description;
			commentBox.appendChild(commentText);
			
			var postedTime = document.createElement("span");
			postedTime.className = "postedTime";
			postedTime.innerHTML = "<br>" + data[i].date;	
			commentBox.appendChild(postedTime);
			commt.appendChild (commentBox);
			
			messages.insertBefore(commt, messages.children[data.length]);
		}
		document.getElementById('wNext').disabled = false;
		wHasNext = true;
	}
	document.getElementById('wCurrent').innerHTML = (wCounter+1);
};

function updateDTIssue(descrip, title, type){
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];

	var data = {};
	data.title = title;
	data.description = descrip;
	data.type = getType(type);
	console.log(data);
	
	$.ajax({
		type : "PUT",
		url : "../rest/issues/" + issue,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			showSnackbar('Descrição da Ocorrência Actualizada!');						
			location.reload();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 400) {
				showSnackbar('Edição inválida!');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
}

function editsButtons(submitter) {
	
	console.log("Imprimir qual é o submitter: "+submitter);
	if (submitter == localStorage['userID']) {
		
		$("#editsTitleUser").show();
		$("#editsDescriptionUser").show();
		$("#editsTypeUser").show();
		
		$("#descriptionChange").hide();
		$("#saveDescription").hide();
		$("#cancelDescription").hide();

		$("#titleChange").hide();
		$("#saveTitle").hide();
		$("#cancelTitle").hide();

		$("#typeChange").hide();
		$("#saveType").hide();
		$("#cancelType").hide();
		
// ...............................Update Description...........................
		
		document.getElementById("editDescription").addEventListener("click",function(e) {
			$("#description").hide();
			$("#descriptionChange").show();
			$("#editDescription").hide();
			$("#saveDescription").show();
			$("#cancelDescription").show();
			document.getElementById("newDescription").value = fullIssue.description;
		});

		document.getElementById("saveDescription").addEventListener("click",function(e) {
			var data = document.getElementById("newDescription").value;
			updateDTIssue(data, fullIssue.title, fullIssue.type);
		});

		document.getElementById("cancelDescription").addEventListener("click",function(e) {
			var data = document.getElementById("newDescription").value;
			$("#description").show();
			$("#descriptionChange").hide();
			$("#editDescription").show();
			$("#saveDescription").hide();
			$("#cancelDescription").hide();
		});
		
// ...............................Update Title.................................
		
		document.getElementById("editTitle").addEventListener("click",function(e) {
			$("#title").hide();
			$("#titleChange").show();
			$("#editTitle").hide();
			$("#saveTitle").show();
			$("#cancelTitle").show();
			document.getElementById("newTitle").value = fullIssue.title;
		});

		document.getElementById("saveTitle").addEventListener("click",function(e) {
			var data = document.getElementById("newTitle").value;
			updateDTIssue(fullIssue.description, data, fullIssue.type);
		});

		document.getElementById("cancelTitle").addEventListener("click",function(e) {
			document.getElementById("newTitle").value = "";
			$("#title").show();
			$("#titleChange").hide();
			$("#editTitle").show();
			$("#saveTitle").hide();
			$("#cancelTitle").hide();
		});
		
// ...............................Update Type..................................
		
		document.getElementById("editType").addEventListener("click",function(e) {
			$("#type").hide();
			$("#typeChange").show();
			$("#editType").hide();
			$("#saveType").show();
			$("#cancelType").show();
		});

		document.getElementById("saveType").addEventListener("click",function(e) {
			var data = document.getElementById("newType").value;
			updateDTIssue(fullIssue.description, fullIssue.title, data);
		});

		document.getElementById("cancelType").addEventListener("click",function(e) {
			document.getElementById("newType").value = "";
			$("#type").show();
			$("#typeChange").hide();
			$("#editType").show();
			$("#saveType").hide();
			$("#cancelType").hide();
		});
	}
};

function rateEntity(entityRating){
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.rating = entityRating;
	
	for (i = 0; i<entities.length; i++){
		entity = entities[i].nif;
		
		$.ajax({
			type : "POST",
			url : "../rest/issues/" + issue + "/entities/" + entity + "/ratings",
			headers : {
				'Authorization' : tokenID,
				'User' : userID
			},
			contentType : "application/json; charset=utf-8",
			crossDomain : true,
			dataType : "json",
			success : function(response) {
				location.reload();
			},
			error : function(response) {
				if (response.status == 400) {
					showSnackbar('Parâmetros inválidos');
				} else if (response.status == 401) {
					showSnackbar('Autorização inválida');
				} else if (response.status == 403) {
					showSnackbar('Token inválido');
				} else if (response.status == 404) {
					showSnackbar('Rating inexistente');
				} else if (response.status == 409) {
					showSnackbar('Já avaliou o desempenho da empresa');
				} else if (response.status == 500) {
					showSnackbar('Erro de servidor');
				} else if (response.status == 409) {
					updateRating(priority);
				} else {
					showSnackbar("Erro: " + response.status);
				}
			},
			data : JSON.stringify(data)
		});
	}
}


