var lat;
var lng;

window.onload = function() {
	checkLogin()
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	document.getElementById('submit').onclick = captureData;
	//Editar Menu para Moderadores.......................
	var isMdtr = localStorage['isModerator'];
	if(isMdtr == "true"){
		$( "moderatorAlerts" ).addClass( "hidden" );}
	//...................................................
	
	document.getElementById("priority-low").addEventListener("click", function(e) {
		$('#priority-low').addClass("activated");
		$('#priority-medium').removeClass("activated");
		$('#priority-high').removeClass("activated");
		$('#priority')[0].value = 1;
	});
	document.getElementById("priority-medium").addEventListener("click", function(e) {
		$('#priority-low').removeClass("activated");
		$('#priority-medium').addClass("activated");
		$('#priority-high').removeClass("activated");
		$('#priority')[0].value = 2;
	});
	document.getElementById("priority-high").addEventListener("click", function(e) {
		$('#priority-low').removeClass("activated");
		$('#priority-medium').removeClass("activated");
		$('#priority-high').addClass("activated");
		$('#priority')[0].value = 3;
	});
}

function initMap() {

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom : 16,
		center : {
			lat : 0,
			lng : 0
		}
	});

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			center = new google.maps.LatLng(position.coords.latitude,
					position.coords.longitude);
			map.setCenter(center);
		});
	}
	
	map.setOptions({
		maxZoom : 19,
		minZoom : 12
	})
	
	map.data.loadGeoJson('../resource/map.json');
	
	var input = document.getElementById('pac-input');
	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.bindTo('bounds', map);
	
	map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);

	autocomplete.addListener('place_changed', function() {
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			// User entered the name of a Place that was not suggested and
			// pressed the Enter key, or the Place Details request failed.
			window.alert("No details available for input: '" + place.name + "'");
			return;
		}
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(16);
		}
	});
	
	map.data.setStyle({
		fillColor: 'orange',
		strokeWeight: 2,
		fillOpacity: 0.1,
		clickable: true,
	});
	
	var marker;
	
	map.data.addListener('click', function(event) {
		if (marker == null)
			marker = new google.maps.Marker({
				position : event.latLng,
				map : map
			});
		else {
			marker.setMap(null);
			marker = new google.maps.Marker({
				position : event.latLng,
				map : map
			});
		}
		lat = marker.position.lat();
		lng = marker.position.lng();
	});
	
	map.addListener('click', function(event) {
		showSnackbar('Posição inválida');
	});
};

function captureData(event) {
	
	event.preventDefault();
	var data = $('form[name="create"]').jsonify();
	if(lat==null) {
		showSnackbar('Defina uma posição no mapa!');
		return;
	}
	data.longitude = lng;
	data.latitude = lat;
	
	data.pictures = [];
	var numLoadedPhotos = 0;
	var numPhotos;
	
	//Extract the data we want from loaded file
	var addImageData = function() {
		var extension = this.result.match(/png|jpeg|jpg|gif/);
		
		if (extension != null){
			//Supported filetype
			extension = extension[0];
			var base64 = this.result.replace(/^data:image\/(png|jpeg|jpg|gif);base64,/, "");
			data.pictures.push({extension, base64});
			
			console.log("Loaded a "+extension);
			numLoadedPhotos++;
			if (numLoadedPhotos == numPhotos) {
				sendIssueData(data);
			}
		} else {
			//Not supported filetype
			numPhotos--;
			if (numLoadedPhotos == numPhotos) {
				sendIssueData(data);
			}
		}
	};
	
	//Load all selected files
	var files = document.getElementById("photoIn").files;
	numPhotos = files.length;
	if (numLoadedPhotos == numPhotos){
		sendIssueData(data);
	}
	for (var i = 0; i<files.length; i++){
		var reader = new FileReader();
		reader.addEventListener('load', addImageData);
		reader.readAsDataURL(document.getElementById("photoIn").files[i]);
	}
};

function sendIssueData(data) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	console.log(data);
	
	$.ajax({
		type : "POST",
		url : "../rest/issues",
		headers : {
			'Authorization' : tokenID,
			'User' : userID,
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		// dataType: "json",
		success : function(response) {
			document.getElementById('submit').disabled = true;
			showSnackbar("Ocorrência criada com sucesso!");
			wait(5000);
			window.location.href = "./dashboard.html";
		},
		error : function(response) {
			if (response.status == 400){
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 401){
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403){
				showSnackbar('Invalid token');
			} else if (response.status == 500){
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
}