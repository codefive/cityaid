var iCursors = ["null"];
var cCursors = ["null"];
var uCursors = ["null"];
var iCounter = 0;
var cCounter = 0;
var uCounter = 0;
var iHasNext = true;
var cHasNext = true;
var uHasNext = true;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true"){
		toggleOn("usersListManager");
		toggleOn("gamificationManager");}
	else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	
	if(iCounter == 0)
		document.getElementById('iPrevious').disabled = true;
	if(cCounter == 0)
		document.getElementById('cPrevious').disabled = true;
	if(uCounter == 0)
		document.getElementById('uPrevious').disabled = true;
	
	document.getElementById('iPrevious').onclick = function(event) {
		document.getElementById('rIssues').innerHTML = "";
		if (iCounter > 0) {
			//there is a previous page
			iCounter--;
			if (iCounter <= 0) {
				//previous page was the first page
				document.getElementById('iPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('iPrevious').disabled = false;
			}
			document.getElementById('iNext').disabled = false;
			getIssuesReports();
		}
	};
	
	//Go to next page
	document.getElementById('iNext').onclick = function(event) {
		document.getElementById('rIssues').innerHTML = "";
		if (iHasNext) {
			//there might be a next page
			iCounter++;
			document.getElementById('iPrevious').disabled = false;
			getIssuesReports();
		}
	};
	
	document.getElementById('cPrevious').onclick = function(event) {
		document.getElementById('rComments').innerHTML = "";
		if (cCounter > 0) {
			//there is a previous page
			cCounter--;
			if (cCounter <= 0) {
				//previous page was the first page
				document.getElementById('cPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('cPrevious').disabled = false;
			}
			document.getElementById('cNext').disabled = false;
			getCommentsReports();
		}
	};
	
	//Go to next page
	document.getElementById('cNext').onclick = function(event) {
		document.getElementById('rComments').innerHTML = "";
		if (cHasNext) {
			//there might be a next page
			cCounter++;
			document.getElementById('cPrevious').disabled = false;
			getCommentsReports();
		}
	};
	
	document.getElementById('uPrevious').onclick = function(event) {
		document.getElementById('rUsers').innerHTML = "";
		if (uCounter > 0) {
			//there is a previous page
			uCounter--;
			if (uCounter <= 0) {
				//previous page was the first page
				document.getElementById('uPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('uPrevious').disabled = false;
			}
			document.getElementById('uNext').disabled = false;
			getUserReports();
		}
	};
	
	//Go to next page
	document.getElementById('uNext').onclick = function(event) {
		document.getElementById('rUsers').innerHTML = "";
		if (uHasNext) {
			//there might be a next page
			uCounter++;
			document.getElementById('uPrevious').disabled = false;
			getUserReports();
		}
	};
	
	getIssuesReports();
	getCommentsReports();
	getUserReports();
};

function populateIssueReports(data) {
	
	var repIssues = document.getElementById("rIssues");
	if (data.length == 0) {
		if(iCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem nenhuma ocorrência reportada por resolver.";
			repIssues.insertBefore(title, repIssues.children[0]);
			document.getElementById('iNext').disabled = true;
			iHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem mais nenhuma ocorrência reportada por resolver.";
			repIssues.insertBefore(title, repIssues.children[0]);
			document.getElementById('iNext').disabled = true;
			iHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ocorr = document.createElement("div");
			ocorr.className = "container";
			var div2 = document.createElement("div");
			var title = document.createElement("p");
			title.className = "reportText";
			title.innerHTML = "Título da Ocorrência: " + data[i].title;
			var issueRef = document.createElement("a");
			issueRef.href = "./view.html?issue=" + data[i].issue;
			issueRef.appendChild(title);
			div2.appendChild(issueRef);
			var reporter = document.createElement("p");
			reporter.className = "reportText";
			reporter.innerHTML = "Quem reportou ocorrência: " + data[i].reporter;
			div2.appendChild(reporter);
			var submiter = document.createElement("p");
			submiter.className = "repportText";
			submiter.innerHTML = "Criador da ocorrência: " + data[i].submiter;
			div2.appendChild(submiter);
			var comment = document.createElement("p");
			comment.className = "reportName";
			comment.innerHTML = "Detalhes da denúncia: " + data[i].comment;
			div2.appendChild(comment);
			ocorr.appendChild(div2);
			repIssues.insertBefore(ocorr, repIssues.children[0]);
		}
		document.getElementById('iNext').disabled = false;
		iHasNext = true;
	}
	document.getElementById('iCurrent').innerHTML = (iCounter+1);
}

function getIssuesReports() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = iCursors[iCounter];

	var url;
	if(cursor=="null")
		url="../rest/reports/issues";
	else
		url="../rest/reports/issues?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateIssueReports(response.reports);
			if((iCounter+1) >= iCursors.length)
				iCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function populateCommentsReports(data) {
	
	var repComments = document.getElementById("rComments");
	if (data.length == 0) {
		if(cCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem nenhum comentário reportado por resolver.";
			repComments.insertBefore(title, repComments.children[0]);
			document.getElementById('cNext').disabled = true;
			cHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem mais nenhum comentário reportado por resolver.";
			repComments.insertBefore(title, repComments.children[0]);
			document.getElementById('cNext').disabled = true;
			cHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {	
			var reprt = document.createElement("div");
			reprt.className = "container";
			var div2 = document.createElement("div");
			var title = document.createElement("p");
			title.className = "reportText";
			title.innerHTML = "Título da ocorrência: " + data[i].title;
			div2.appendChild(title);
			var reporter = document.createElement("p");
			reporter.innerHTML = "Quem reportou ocorrência: " + data[i].reporter;
			reprt.appendChild(reporter);
			var submiter = document.createElement("p");
			submiter.innerHTML = "Quem criou ocorrência: <p>" + data[i].submiter;
			reprt.appendChild(submiter);
			var commentReported = document.createElement("p");
			commentReported.innerHTML = "Comentário em causa: <p>" + data[i].text;
			reprt.appendChild(commentReported);
			var comment = document.createElement("p");
			comment.innerHTML = "Detalhes da denúncia: <p>" + data[i].comment;
			reprt.appendChild(comment);
			repComments.insertBefore(reprt, repComments.children[0]);
		}
		document.getElementById('cNext').disabled = false;
		cHasNext = true;
	}
	document.getElementById('cCurrent').innerHTML = (cCounter+1);
};

function getCommentsReports() {

	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cCursors[cCounter];
	
	var url;
	if(cursor=="null")
		url="../rest/reports/comments";
	else
		url="../rest/reports/comments?cursor=" + cursor;

	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateCommentsReports(response.reports);
			if((cCounter+1) >= cCursors.length)
				cCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function populateUserReports(data) {
	
	var repUsers = document.getElementById("rUsers");
	if (data.length == 0) {
		if(uCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem nenhum utilizador reportado.";
			repUsers.insertBefore(title, repUsers.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem mais nenhum utilizador reportado.";
			repUsers.insertBefore(title, repUsers.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {	
			var reprt = document.createElement("div");
			var title = document.createElement("p");
			title.innerHTML = "Título da ocorrência: <p>" + data[i].title;
			reprt.appendChild(title);
			var reporter = document.createElement("p");
			reporter.innerHTML = "Quem reportou ocorrência: <p>" + data[i].reporter;
			reprt.appendChild(reporter);
			var submiter = document.createElement("p");
			submiter.innerHTML = "Quem criou ocorrência: <p>" + data[i].submiter;
			reprt.appendChild(submiter);
			var commentReported = document.createElement("p");
			commentReported.innerHTML = "Comentário em causa: <p>" + data[i].text;
			reprt.appendChild(commentReported);
			var comment = document.createElement("p");
			comment.innerHTML = "Detalhes da denúncia: <p>" + data[i].comment;
			reprt.appendChild(comment);
			repUsers.insertBefore(reprt, repUsers.children[0]);
		}
		document.getElementById('uNext').disabled = false;
		uHasNext = true;
	}
	document.getElementById('uCurrent').innerHTML = (uCounter+1);
};

function getUserReports() {

	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = uCursors[uCounter];
	
	var url;
	if(cursor=="null")
		url="../rest/reports/users";
	else
		url="../rest/reports/users?cursor=" + cursor;

	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateUserReports(response.reports);
			if((uCounter+1) >= uCursors.length)
				uCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};