var cursors = ["null"];
var current = 0;
var hasNext = true;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true") {
		toggleOn("usersListManager");
		toggleOn("gamificationManager");
	}else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	
	if(current == 0)
		document.getElementById('previous').disabled = true;

	//Go to previous page
	document.getElementById('previous').onclick = function(event) {
		document.getElementById('ocorrencias').innerHTML = "";
		if (current > 0) {
			//there is a previous page
			current--;
			if (current <= 0) {
				//previous page was the first page
				document.getElementById('previous').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('previous').disabled = false;
			}
			document.getElementById('next').disabled = false;
			getResolved();
		}
	};
	
	//Go to next page
	document.getElementById('next').onclick = function(event) {
		document.getElementById('ocorrencias').innerHTML = "";
		if (hasNext) {
			//there might be a next page
			current++;
			document.getElementById('previous').disabled = false;
			getResolved();
		}
	};

	//Get first page
	getResolved();
};

function populate(data) {
	
	var resolved = document.getElementById("ocorrencias");
	if (data.length == 0) {
		if(cursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem ocorrências resolvidas.";
			resolved.insertBefore(title, resolved.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem mais ocorrências resolvidas.";
			resolved.insertBefore(title, resolved.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		}
	} else {
		for(var i = 0; i < data.length; i++) {
			var ref = document.createElement("a");
			ref.href = "./view.html?issue=" + data[i].issue;
			var ocorr = document.createElement("div");
			ocorr.className = "container dados-block";
			var divti = document.createElement("div");
			divti.className = "dados-header";
			divti.innerHTML = data[i].title;
			ocorr.appendChild(divti);
			var div1 = document.createElement("div");
			div1.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			if(data[i].pictures[0] === undefined)
				 imag.src = "../gcs/city-aid.appspot.com/default.jpg";
			else 
				imag.src = "../gcs/city-aid.appspot.com/"+data[i].pictures[0];
			var titleJob = document.createElement("p");
			titleJob.innerHTML = data[i].type;
			titleJob.className = "typeOcorr";
			div1.appendChild(imag);
			div1.appendChild(titleJob);
			ocorr.appendChild(div1);
			ref.appendChild(ocorr);
			resolved.insertBefore(ref, resolved.children[0]);
		}
		document.getElementById('next').disabled = false;
	    hasNext = true;
	}
	document.getElementById('current').innerHTML = (current+1);
};

function getResolved() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cursors[current];
	
	//Generate url
	var url;
	if(cursor=="null")
		url = "../rest/issues?state=solved&page_size=10";
	else
		url = "../rest/issues?state=solved&cursor=" + cursor + "&page_size=10";
	
    $.ajax({
        type: "GET",
        url: url,
        headers : {
			'Authorization' : tokenID,
			'User' : userID,
		},
        crossDomain: true,
        dataType: "json",
        success: function(response) {
        	populate(response.issues);
			if ((current+1) >= cursors.length)
				cursors.push(response.cursor);
        },
        error: function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
        }
    });
};