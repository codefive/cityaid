window.onload = function() {
	checkLogin();
	showSnackbar("Loading Data...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true"){
		toggleOn("usersListManager");
		toggleOn("gamificationManager");}
	else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................

	document.getElementById('addEntityandManager').onclick = captureEntityAndManagerData;
}

function captureEntityAndManagerData(){
	var data = {};
	var dataManager = {};

	//Get entity data
	var name = document.getElementById("nome").value;
	var nif = "" + document.getElementById("nif").value;
	var type = document.getElementById("type").value;
	var tele = "" + document.getElementById("phone").value;

	//Get manager data
	var usernameManager = document.getElementById("username").value;
	var nomeManager = document.getElementById("nomeManager").value;
	var email = document.getElementById("email").value;
	var teleManager = document.getElementById("phoneManager").value;
	var gender = document.getElementById("gender").value;
	var birthdayManager = document.getElementById("birthdate").value;
	var job = document.getElementById("job").value;

	//Set entity data
	data.name = name;
	data.nif = nif;
	data.type = type;
	data.phone = tele;
	
	//Set manager data
	var newDate = new Date(document.getElementById("birthdate").value);
	dataManager.username = usernameManager;
	dataManager.name = nomeManager;
	dataManager.email = email;
	dataManager.phone = teleManager;
	dataManager.gender = gender;
	var year = newDate.getFullYear();
	var month = newDate.getMonth() + 1;
	var day = newDate.getDate();
	dataManager.birthdate = year + "-" + month + "-" + day;
	dataManager.job = job;
	
	//Extract the data we want from loaded file
	var addImageData = function() {
		var extension = this.result.match(/png|jpeg|jpg|gif/);
		
		if (extension != null){
			//Supported filetype
			extension = extension[0];
			var base64 = this.result.replace(/^data:image\/(png|jpeg|jpg|gif);base64,/, "");
			data.logo = {};
			data.logo.extension = extension;
			data.logo.base64 = base64;
			
			console.log("Loaded a "+extension);
			console.log("All photos loaded");
			newEntity(data, dataManager);
		} else {
			//Not supported filetype
			console.log("All photos loaded");
			newEntity(data, dataManager);
		}
	};
	
	//Load single file
	var files = document.getElementById("photoIn").files;
	if (files.length == 0){
		console.log("All photos loaded");
		newEntity(data, dataManager);
	}
	var reader = new FileReader();
	reader.addEventListener('load', addImageData);
	reader.readAsDataURL(document.getElementById("photoIn").files[0]);
}

function newEntity(data, dataManager) {
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	$.ajax({
		type : "POST",
		url : "../rest/entities",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		//dataType: "json",
		success : function(response) {
			showSnackbar('Entidade' + data.name + 'registada com sucesso!');
			createManager(data.nif, dataManager);
		},
		error : function(response) {
			if (response.status == 409) {
				showSnackbar('Entidade já existente');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else
				alert("Erro: " + response.status);
		},	
		data : JSON.stringify(data)
	});
}

function createManager(nif, dataManager) {
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	$.ajax({
		type : "POST",
		url : "../rest/entities/" + nif + "/managers/create",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		// dataType: "json",
		success : function(response) {
			showSnackbar('Gestor da entidade registado com sucesso!');
			location.reload();
		},
		error : function(response) {	
			if (response.status == 409) {
				showSnackbar('Utilizador já existente');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else
				alert("Erro: " + response.status);
		},
		data : JSON.stringify(dataManager)
	});
}
