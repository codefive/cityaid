var issue;
var map;
var slideIndex = 1;

var uCursors = ["null"];
var wCursors = ["null"];
var uCounter = 0;
var wCounter = 0;
var uHasNext = true;
var wHasNext = true;

window.onload = function() {
	checkLogin();
	showSnackbar("A carregar a página...");
	document.getElementById('logout').onclick = logout;
	
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true") {
		toggleOn("usersListManager");
		toggleOn("gamificationManager");
	}else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	
	issue = getQueryParameterByName("issue", window.location);
	getIssueData();
	getEntities();
	
	if(uCounter == 0)
		document.getElementById('uPrevious').disabled = true;
	if(wCounter == 0)
		document.getElementById('wPrevious').disabled = true;
	
	document.getElementById('uPrevious').onclick = function(event) {
		document.getElementById('comments').innerHTML = "";
		if (uCounter > 0) {
			//there is a previous page
			uCounter--;
			if (uCounter <= 0) {
				//previous page was the first page
				document.getElementById('uPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('uPrevious').disabled = false;
			}
			document.getElementById('uNext').disabled = false;
			getComments();
		}
	};
	
	//Go to next page
	document.getElementById('uNext').onclick = function(event) {
		document.getElementById('comments').innerHTML = "";
		if (uHasNext) {
			//there might be a next page
			uCounter++;
			document.getElementById('uPrevious').disabled = false;
			getComments();
		}
	};
	
	document.getElementById('wPrevious').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (wCounter > 0) {
			//there is a previous page
			wCounter--;
			if (wCounter <= 0) {
				//previous page was the first page
				document.getElementById('wPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('wPrevious').disabled = false;
			}
			document.getElementById('wNext').disabled = false;
			getLogs();
		}
	};
	
	//Go to next page
	document.getElementById('wNext').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (wHasNext) {
			//there might be a next page
			wCounter++;
			document.getElementById('wPrevious').disabled = false;
			getLogs();
		}
	};
	
	getComments();
	getLogs();
	
	document.getElementById("saveComment").addEventListener("click", function(e) {
		var data = document.getElementById("makeComment").value;
		saveComment(data);
	});

	document.getElementById("guardarEntity").addEventListener("click", function(e) {
		var entity = document.getElementById("entity").value;
		associateIssue(entity);
	});
	
	document.getElementById("saveComment").addEventListener("click", function(e) {
		var data = document.getElementById("comment").value;
		saveComment(data);
	});
	
	var modal = document.getElementById('myModal');

	// When the user clicks the , open the modal 
	document.getElementById("revolve").addEventListener("click", function(e) {
		document.getElementById("reportText").value = '';
		$(document.getElementById('myModal')).show();
		$(document.getElementById('issueModal')).show();
	});
	
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
		$(document.getElementById('issueModal')).hide();
		$(document.getElementById('myModal')).hide();
	};

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	    	$(document.getElementById('issueModal')).hide();
			$(document.getElementById('myModal')).hide();
	    }
	};
	
	document.getElementById("reportBttn").addEventListener("click", function(e) {
		if(document.getElementById("reportText").value != "")
			solveIssue();
		else
			showSnackbar('Descreva o que foi feito para a resolução do problema!');
	});
};

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		center : {
			lat : 0,
			lng : 0
		},
		zoom : 16
	});
};

function getIssueData() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/issues/" + issue,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			placeMarker(response);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};


function placeMarker(issueData) {
	
	map = new google.maps.Map(document.getElementById('map'), {
		center : {
			lat : issueData.latitude,
			lng : issueData.longitude
		},
		zoom : 16
	});

	var coords = new google.maps.LatLng(issueData.latitude, issueData.longitude);

	var marker = new google.maps.Marker({
		position : coords,
		map : map,
		title : "Ocorrência"
	});

	var add;

	var geocoder = new google.maps.Geocoder;

	geocoder.geocode({'location' : marker.position}, function(results, status) {
		if (status === 'OK') {
			if (results[1]) {
				add = results[1].formatted_address;
				document.getElementById("location").innerHTML = add;
			} else {
				document.getElementById("location").innerHTML = "Morada não encontrada";
			}
		} else {
			document.getElementById("location").innerHTML = "Morada não encontrada";
		}
	});
	
	var thumbs = document.getElementById("thumbnails");
	var putModel = document.getElementById("modalImages-content");
	for (var i = 1; i < issueData.pictures.length + 1; i++) {
		var pics = document.createElement("div");
		pics.className = "picContainer";
		var imagem = document.createElement("img");
		imagem.className = "imagem";
		imagem.src = "../gcs/city-aid.appspot.com/" + issueData.pictures[i - 1];
		imagem.className = "houver-shadow";
		var attribute =1;
		imagem.setAttribute("onclick", "openModal();currentSlide("+attribute+");closeModalGray()");
		pics.appendChild(imagem);
		thumbs.appendChild(pics);
	}
	
	var putModel = document.getElementById("modalImagesContent");
	for (var j = 1; j < issueData.pictures.length + 1; j++) {
		var pics = document.createElement("div");
		pics.className = "mySlides";
		var text = document.createElement("div");
		text.className = "numbertext";
		text.innerHTML = j + " / " + issueData.pictures.length;
		pics.appendChild(text);
		var imagem = document.createElement("img");
		imagem.src = "../gcs/city-aid.appspot.com/" + issueData.pictures[j-1];
		imagem.className = "houver-shadow modalImage";
		pics.appendChild(imagem);
		putModel.appendChild(pics);
	}
	
	document.getElementById("title").innerHTML = issueData.title;
	document.getElementById("description").innerHTML = issueData.description;
	document.getElementById("type").innerHTML = issueData.type;
	document.getElementById("submitter").innerHTML = issueData.submitter;
	document.getElementById("submitter").href = "./viewUser.html?username=" + issueData.submitter;
	
	var date = new Date(issueData.created);
	var months = [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
		"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ]
	document.getElementById("month").innerHTML = months[date.getMonth()];
	document.getElementById("year").innerHTML = date.getFullYear();
	document.getElementById("day").innerHTML = date.getDate();
	
	var ret = "";
	
	switch(issueData.priority) {
		case 1:
			ret = "Baixa";
			break;
		case 2:
			ret = "Média";
			break;
		case 3:
			ret = "Alta";
			break;
	}
	
	document.getElementById("priority").innerHTML = ret;
	
	switch(issueData.state) {
	case 'created':
		$("#progress-bar-created").addClass('created');
		$("#created-label").addClass('active');
		break;
	case 'work_in_progress':
		$("#progress-bar-created").addClass('wip');
		$("#progress-bar-line-to-wip").addClass('wip');
		$("#progress-bar-wip").addClass('wip');
		$("#wip-label").addClass('active');
		break;
	case 'solved':
		$("#progress-bar-created").addClass('solved');
		$("#progress-bar-line-to-wip").addClass('solved');
		$("#progress-bar-wip").addClass('solved');
		$("#progress-bar-line-to-solved").addClass('solved');
		$("#progress-bar-solved").addClass('solved');
		$("#solved-label").addClass('active');
		break;
	case 'closed':
		break;
	}
};

function saveLog(commt) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.comment = commt;
	
	$.ajax({
		type : "POST",
		url : "../rest/issues/" + issue + "/log",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});

	event.preventDefault();
};

function saveComment(commt) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.comment = commt;
	
	$.ajax({
		type : "POST",
		url : "../rest/issues/" + issue + "/comments",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});

	event.preventDefault();
};

function populateComments(data) {
	
	var messages = document.getElementById("comments");
	if (data.length == 0) {
		if(uCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "A ocorrência não tem comentários!";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "A ocorrência não tem mais comentários.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var commt = document.createElement("div");
			commt.className = "container";
			var title = document.createElement("p");
			title.className = "title";
			title.innerHTML = data[i].author;
			commt.appendChild(title);
			
			var commentText = document.createElement("span");
			commentText.className = "commentText";
			commentText.innerHTML = data[i].comment;
			commt.appendChild(commentText);
			
			var postedTime = document.createElement("span");
			postedTime.className = "postedTime";
			postedTime.innerHTML = "<br>" + data[i].submitted;	
			commt.appendChild(postedTime);
		
			messages.insertBefore(commt, messages.children[0]);
		}
		document.getElementById('uNext').disabled = false;
		uHasNext = true;
	}
	document.getElementById('uCurrent').innerHTML = (uCounter+1);
};


function getComments() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = uCursors[uCounter];
	
	//Generate url
	var url;
	if(cursor=="null")
		url="../rest/issues/" + issue + "/comments";
	else
		url="../rest/issues/" + issue + "/comments?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateComments(response.comments);
			if((uCounter+1) >= uCursors.length)
				uCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};


//.....................Codigo display das fotos da ocorrencia..................
function openModal() {
	$(document.getElementById('modalImages')).show();
	$(document.getElementById('modalImagesContent')).show();
}

function closeModal() {
	$(document.getElementById('modalImagesContent')).hide();
	$(document.getElementById('modalImages')).hide();
}

function closeModalGray() {
	window.onclick = function(event) {
		var modal = document.getElementById('modalImages');
		if (event.target == modal) {
			$(document.getElementById('modalImagesContent')).hide();
			$(document.getElementById('modalImages')).hide();
		}
	}
}

function plusSlides(n) {
	showSlides(slideIndex += n);
}

function currentSlide(n) {
	 showSlides(slideIndex = n);
}

function showSlides(n) {
	 var i;
	 var slides = document.getElementsByClassName("mySlides");
	 var captionText = document.getElementById("caption");
	 if (n > slides.length) {slideIndex = 1}
	 if (n < 1) {slideIndex = slides.length}
	 for (i = 0; i < slides.length; i++) {
	     slides[i].style.display = "none";
	 }
	 slides[slideIndex-1].style.display = "block";
}
//.............................................................................

function getLogs() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = wCursors[wCounter];
	
	var url;
	if(cursor=="null")
		url="../rest/issues/" + issue + "/log";
	else
		url="../rest/issues/" + issue + "/log?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateLogs(response.logs);
			if((wCounter+1) >= wCursors.length)
				wCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};


function populateLogs(data) {
	
	var messages = document.getElementById("logs");
	if (data.length == 0) {
		if(wCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "A ocorrência não tem histórico!";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "A ocorrência não tem mais histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var commt = document.createElement("div");
			commt.className = "container";
			
			var title = document.createElement("p");
			title.className = "title";
			title.innerHTML = data[i].user;
			commt.appendChild(title);
			
			var commentText = document.createElement("span");
			commentText.className = "commentText";
			commentText.innerHTML = data[i].description;
			commt.appendChild(commentText);
			
			var postedTime = document.createElement("span");
			postedTime.className = "postedTime";
			postedTime.innerHTML = "<br>" + data[i].date;	
			commt.appendChild(postedTime);
			
			messages.insertBefore(commt, messages.children[0]);
		}
		document.getElementById('wNext').disabled = false;
		wHasNext = true;
	}
};

function solveIssue() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = "";
	//ainda por fazer
	data = document.getElementById("reportText").value;
	
	$.ajax({
		type : "PUT",
		url : "../rest/issues/" + issue + "/status",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros inválidos');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
};

//....................Associar entidade a ocorrencia...........................

function associateIssue(entity) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.entity = entity;
	
	$.ajax({
		type : "POST",
		url : "../rest/issues/" + issue + "/entities",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		// dataType: "json",
		success : function(response) {
			location.reload();
		},
		error : function(response) {	
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter!');
			} else if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 404) {
				showSnackbar('Issue does not exist; Entity does not exist!');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			}else if (response.status == 409) {
				showSnackbar('Entity is already assigned 🙂');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error');
			} else
				alert("Error: " + response.status);
		},
		data : JSON.stringify(data)
	});
}

function getEntities() {
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	$.ajax({
		type : "GET",
		url : "../rest/entities/",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
				entities = response;
				putEntities(response);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function putEntities(entitiesData) {
	var entities = document.getElementById("entity");
	for (var i = 0; i < entitiesData.length; i++) {
		var one = document.createElement("option");
		one.value = entitiesData[i].nif;
		var name = document.createElement("span");
		name.className = "name";
		name.innerHTML = entitiesData[i].name;
		one.appendChild(name);
		entities.insertBefore(one, entities.children[0]);
	}
}