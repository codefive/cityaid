var uCursors = ["null"];
var wCursors = ["null"];
var uCounter = 0;
var wCounter = 0;
var uHasNext = true;
var wHasNext = true;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true"){
		toggleOn("usersListManager");
		toggleOn("gamificationManager");}
	else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	
	if(uCounter == 0)
		document.getElementById('uPrevious').disabled = true;
	if(wCounter == 0)
		document.getElementById('wPrevious').disabled = true;
	
	document.getElementById('uPrevious').onclick = function(event) {
		document.getElementById('unresolved').innerHTML = "";
		if (uCounter > 0) {
			//there is a previous page
			uCounter--;
			if (uCounter <= 0) {
				//previous page was the first page
				document.getElementById('uPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('uPrevious').disabled = false;
			}
			document.getElementById('uNext').disabled = false;
			getUnresolved();
		}
	};
	
	//Go to next page
	document.getElementById('uNext').onclick = function(event) {
		document.getElementById('unresolved').innerHTML = "";
		if (uHasNext) {
			//there might be a next page
			uCounter++;
			document.getElementById('uPrevious').disabled = false;
			getUnresolved();
		}
	};
	
	document.getElementById('wPrevious').onclick = function(event) {
		document.getElementById('work_in_progress').innerHTML = "";
		if (wCounter > 0) {
			//there is a previous page
			wCounter--;
			if (wCounter <= 0) {
				//previous page was the first page
				document.getElementById('wPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('wPrevious').disabled = false;
			}
			document.getElementById('wNext').disabled = false;
			getWorkInProgress();
		}
	};
	
	//Go to next page
	document.getElementById('wNext').onclick = function(event) {
		document.getElementById('work_in_progress').innerHTML = "";
		if (wHasNext) {
			//there might be a next page
			wCounter++;
			document.getElementById('wPrevious').disabled = false;
			getWorkInProgress();
		}
	};
	getUnresolved();
	getWorkInProgress();
};

function populateUnresolved(data) {
	
	var unresolved = document.getElementById("unresolved");
	if (data.length == 0) {
		if(uCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem ocorrências por resolver.";
			unresolved.insertBefore(title, unresolved.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem mais ocorrências por resolver.";
			unresolved.insertBefore(title, unresolved.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ref = document.createElement("a");
			ref.href = "./view.html?issue=" + data[i].issue;
			var ocorr = document.createElement("div");
			ocorr.className = "container dados-block";
			var divti = document.createElement("div");
			divti.className = "dados-header";
			divti.innerHTML = data[i].title;
			ocorr.appendChild(divti);
			var div1 = document.createElement("div");
			div1.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			if (data[i].pictures[0] === undefined)
				imag.src = "../gcs/city-aid.appspot.com/default.jpg";
			else
				imag.src = "../gcs/city-aid.appspot.com/" + data[i].pictures[0];
			var titleJob = document.createElement("p");
			titleJob.innerHTML = data[i].type;
			titleJob.className = "typeOcorr";
			div1.appendChild(imag);
			div1.appendChild(titleJob);
			ocorr.appendChild(div1);
			ref.appendChild(ocorr);
			unresolved.insertBefore(ref, unresolved.children[0]);
		}
		document.getElementById('uNext').disabled = false;
		uHasNext = true;
	}
	document.getElementById('uCurrent').innerHTML = (uCounter+1);
}

function getUnresolved() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = uCursors[uCounter];

	var url;
	if(cursor=="null")
		url="../rest/issues?state=created&page_size=5";
	else
		url="../rest/issues?state=created&page_size=5" + "&cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateUnresolved(response.issues);
			if((uCounter+1) >= uCursors.length)
				uCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function populateWorkInProgress(data) {
	
	var work_in_progress = document.getElementById("work_in_progress");
	if (data.length == 0) {
		if(wCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem ocorrências em progresso.";
			work_in_progress.insertBefore(title, work_in_progress.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem mais ocorrências em progresso.";
			work_in_progress.insertBefore(title, work_in_progress.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ocorr = document.createElement("div");
			ocorr.className = "container";
			var div1 = document.createElement("div");
			div1.className = "div1";
			var imagRef = document.createElement("a");
			imagRef.href = "./view.html?issue=" + data[i].issue;
			var imag = document.createElement("img");
			imag.className = "image";
			if (data[i].pictures[0] === undefined) {
				imag.src = "../gcs/city-aid.appspot.com/default.jpg";
			} else
				imag.src = "../gcs/city-aid.appspot.com/" + data[i].pictures[0];
			imagRef.appendChild(imag);
			ocorr.appendChild(div1);
			div1.appendChild(imagRef);
			var title = document.createElement("div");
			title.className = "title";
			var titleLink = document.createElement("a");
			titleLink.href = "./view.html?issue=" + data[i].issue;
			titleLink.innerHTML = data[i].title;
			title.appendChild(titleLink);
			var titleJob = document.createElement("p");
			titleJob.innerHTML = data[i].type;
			title.appendChild(titleJob);
			ocorr.appendChild(title);
			work_in_progress.insertBefore(ocorr, work_in_progress.children[0]);
		}
		document.getElementById('wNext').disabled = false;
		wHasNext = true;
	}
	document.getElementById('wCurrent').innerHTML = (wCounter+1);
}

function getWorkInProgress() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = wCursors[wCounter];
	
	var url;
	if(cursor=="null")
		url="../rest/issues?state=work_in_progress&page_size=5";
	else
		url="../rest/issues?state=work_in_progress&page_size=5" + "&cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateWorkInProgress(response.issues);
			if((wCounter+1) >= wCursors.length)
				wCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};