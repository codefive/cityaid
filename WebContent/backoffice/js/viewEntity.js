var entityId;
var entity;

var wCursors = ["null"];
var wCounter = 0;
var wHasNext = true;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	
	//Editar Menu para Managers..........................
	
	var isManager = localStorage['isManager'];
	
	if(isManager == "true"){
		toggleOn("usersListManager");
		toggleOn("gamificationManager");
	}else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	
	entityId = getQueryParameterByName("entity", window.location);
	getEntityManagers();
	getWorkers();
	
	if(wCounter == 0)
		document.getElementById('wPrevious').disabled = true;
	
	document.getElementById('wPrevious').onclick = function(event) {
		document.getElementById('work_in_progress').innerHTML = "";
		if (wCounter > 0) {
			//there is a previous page
			wCounter--;
			if (wCounter <= 0) {
				//previous page was the first page
				document.getElementById('wPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('wPrevious').disabled = false;
			}
			document.getElementById('wNext').disabled = false;
			getWorkInProgress();
		}
	};
	
	//Go to next page
	document.getElementById('wNext').onclick = function(event) {
		document.getElementById('work_in_progress').innerHTML = "";
		if (wHasNext) {
			//there might be a next page
			wCounter++;
			document.getElementById('wPrevious').disabled = false;
			getWorkInProgress();
		}
	};
	
};

function showEntityManagers(data) {
	
	var managers = document.getElementById("managers");
	for (var i = 0; i < data.length; i++) {
		var manager = document.createElement("a");
		manager.href = "./viewUser.html?username=" + data[i].username;
		manager.innerHTML = data[i].name;

		var space = document.createElement("span");
		if (i != data.length - 1)
			space.innerHTML = ", ";
		managers.appendChild(manager);
		managers.appendChild(space);
		managers.insertBefore(manager, managers.children[0]);
	}
};

function getEntityManagers() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/entities/" + entityId,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			entity = response;
			entityID = response.nif;
			showEntityManagers(entity.managers);
			getEntityInfo(entity);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Entity does not exist');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function getWorkers() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/entities/" + entityId + "/workers",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			console.log(response);
			document.getElementById("nWorkers").innerHTML = response.users.length;
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Entity does not exist');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function getEntityInfo(entity) {
	
	if(entity.logo == null)
		document.getElementById("entLogo").src = "../gcs/city-aid.appspot.com/default.jpg";
	else
		document.getElementById("entLogo").src = "../gcs/city-aid.appspot.com/"+ entity.logo;
	document.getElementById("nifEntity").innerHTML = entity.nif;
	document.getElementById("entityTitle").innerHTML = entity.name;
	document.getElementById("typeCompany").innerHTML = entity.type;
	document.getElementById("phoneNumber").innerHTML = entity.phone;
	document.getElementById("ratingEntity").innerHTML = entity.rating;
	
	getWorkers(); 
	getNumberIssuesResolved();
	getNumberIssuesUnresolved();
	getWorkInProgress();
};

function getNumberIssuesResolved() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/issues?state=solved&entity=" + entityId,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			document.getElementById("issuesResolved").innerHTML = response.issues.length;
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Entity does not exist');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function getNumberIssuesUnresolved() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/issues?state=work_in_progress&entity=" + entityId,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			document.getElementById("issuesUnresolved").innerHTML = response.issues.length;
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Entity does not exist');
			} else if (response.status == 500) {
				showSnackbar('Internal Server Error!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};


function populateWorkInProgress(data) {
	
	var work_in_progress = document.getElementById("work_in_progress");
	if (data.length == 0) {
		if(wCursors.length==1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem nenhuma ocorrência em progresso!";
			work_in_progress.insertBefore(title, work_in_progress.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não tem mais ocorrências em progresso";
			work_in_progress.insertBefore(title, work_in_progress.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ref = document.createElement("a");
			ref.href = "./view.html?issue=" + data[i].issue;
			var ocorr = document.createElement("div");
			ocorr.className = "container dados-block";
			var divti = document.createElement("div");
			divti.className = "dados-header";
			divti.innerHTML = data[i].title;
			ocorr.appendChild(divti);
			var div1 = document.createElement("div");
			div1.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			if (data[i].pictures[0] === undefined)
				imag.src = "../gcs/city-aid.appspot.com/default.jpg";
			else
				imag.src = "../gcs/city-aid.appspot.com/" + data[i].pictures[0];
			var titleJob = document.createElement("p");
			titleJob.innerHTML = data[i].type;
			titleJob.className = "typeOcorr1";
			div1.appendChild(imag);
			div1.appendChild(titleJob);
			ocorr.appendChild(div1);
			ref.appendChild(ocorr);
			work_in_progress.insertBefore(ref, work_in_progress.children[0]);
		}
		document.getElementById('wNext').disabled = false;
		wHasNext = true;
	}
	document.getElementById('wCurrent').innerHTML = (wCounter+1);
}

function getWorkInProgress() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = wCursors[wCounter];
	
	var url;
	if(cursor=="null")
		url="../rest/issues?state=work_in_progress&entity=" + entityID + "&page_size=5";
	else
		url="../rest/issues?state=work_in_progress&entity=" + entityID + "&page_size=5" + "&cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateWorkInProgress(response.issues);
			if((wCounter+1) >= wCursors.length)
				wCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};