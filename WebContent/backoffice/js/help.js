
window.onload = function() {
	checkLogin();
	showSnackbar("A Carregar Informação...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Moderadores.......................
	var isMngr = localStorage['isManager'];
	if(isMngr=="true"){		
		toggleOn("usersListManager");
		toggleOn("gamificationManager");
		toggleOn("updateGamificationDiv");
		toggleOn("addMedalDiv");
		}else{
			toggleOn("allIssuesBO");
			toggleOn("alertasBO");
		}
	//...................................................
		
	document.getElementById('addEntity').onclick = function(event) {
		$("#taddEntity").toggle();	
	};
	document.getElementById('banUser').onclick = function(event) {
		$("#tbanUser").toggle();	
	};	
	document.getElementById('updateGamification').onclick = function(event) {
		$("#tupdateGamification").toggle();	
	};	
	document.getElementById('addMedal').onclick = function(event) {
		$("#taddMedal").toggle();	
	};	
	
};
