var rCursors = ["null"];
var uCursors = ["null"];
var wCursors = ["null"];
var rCounter = 0;
var uCounter = 0;
var wCounter = 0;
var rHasNext = true;
var uHasNext = true;
var wHasNext = true;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true") {
		toggleOn("usersListManager");
		toggleOn("gamificationManager");
	}else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	
	if(rCounter == 0)
		document.getElementById('rPrevious').disabled = true;
	if(uCounter == 0)
		document.getElementById('uPrevious').disabled = true;
	if(wCounter == 0)
		document.getElementById('wPrevious').disabled = true;
	
	document.getElementById('rPrevious').onclick = function(event) {
		document.getElementById('reporters').innerHTML = "";
		if (rCounter > 0) {
			//there is a previous page
			rCounter--;
			if (rCounter <= 0) {
				//previous page was the first page
				document.getElementById('rPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('rPrevious').disabled = false;
			}
			document.getElementById('rNext').disabled = false;
			getReporters();
		}
	};
	
	//Go to next page
	document.getElementById('rNext').onclick = function(event) {
		document.getElementById('reporters').innerHTML = "";
		if (rHasNext) {
			//there might be a next page
			rCounter++;
			document.getElementById('rPrevious').disabled = false;
			getReporters();
		}
	};
	
	document.getElementById('uPrevious').onclick = function(event) {
		document.getElementById('backoffice').innerHTML = "";
		if (uCounter > 0) {
			//there is a previous page
			uCounter--;
			if (uCounter <= 0) {
				//previous page was the first page
				document.getElementById('uPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('uPrevious').disabled = false;
			}
			document.getElementById('uNext').disabled = false;
			getBackoffice();
		}
	};
	
	//Go to next page
	document.getElementById('uNext').onclick = function(event) {
		document.getElementById('backoffice').innerHTML = "";
		if (uHasNext) {
			//there might be a next page
			uCounter++;
			document.getElementById('uPrevious').disabled = false;
			getBackoffice();
		}
	};
	
	document.getElementById('wPrevious').onclick = function(event) {
		document.getElementById('workers').innerHTML = "";
		if (wCounter > 0) {
			//there is a previous page
			wCounter--;
			if (wCounter <= 0) {
				//previous page was the first page
				document.getElementById('wPrevious').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('wPrevious').disabled = false;
			}
			document.getElementById('wNext').disabled = false;
			getWorkers();
		}
	};
	
	//Go to next page
	document.getElementById('wNext').onclick = function(event) {
		document.getElementById('workers').innerHTML = "";
		if (wHasNext) {
			//there might be a next page
			wCounter++;
			document.getElementById('wPrevious').disabled = false;
			getWorkers();
		}
	};
	
	getReporters();
	getBackoffice();
	getWorkers();
};

function populateReporters(data) {
	
	var reporters = document.getElementById("reporters");
	if (data.length == 0) {
		if(rCursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem utilizadores normais.";
			reporters.insertBefore(title, reporters.children[0]);
			document.getElementById('rNext').disabled = true;
			rHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem mais utilizadores normais.";
			reporters.insertBefore(title, reporters.children[0]);
			document.getElementById('rNext').disabled = true;
			rHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ref = document.createElement ("a");
			ref.href = "./viewUser.html?username=" + data[i].username;
			var user = document.createElement("div");
			user.className = "container dados-block";
			var div1 = document.createElement("div");
			div1.className = "dados-header";
			div1.innerHTML = data[i].username;
			user.appendChild(div1);
			
			var div2 = document.createElement("div");
			div2.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			if(data[i].avatar === undefined)
				imag.src = "../resource/Boneco.png";
			else
				imag.src = data[i].avatar;
			div2.appendChild(imag);
			user.appendChild(div2);
			ref.appendChild(user);
			reporters.insertBefore(ref, reporters.children[0]);
		}
		document.getElementById('rNext').disabled = false;
		rHasNext = true;
	}
	document.getElementById('rCurrent').innerHTML = (rCounter+1);
};

function getReporters() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = rCursors[rCounter];
	
	var url;
	if(cursor=="null")
		url="../rest/users/reporters?page_size=5";
	else
		url="../rest/users/reporters?page_size=5&cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateReporters(response.users);
			if((rCounter+1) >= rCursors.length)
				rCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function populateBackoffice(data) {
	
	var backoffice = document.getElementById("backoffice");
	if (data.length == 0) {
		if(uCursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem utilizadores backoffice.";
			backoffice.insertBefore(title, backoffice.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem mais utilizadores backoffice.";
			backoffice.insertBefore(title, backoffice.children[0]);
			document.getElementById('uNext').disabled = true;
			uHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ref = document.createElement ("a");
			ref.href = "./viewUser.html?username=" + data[i].username;
			var user = document.createElement("div");
			user.className = "container dados-block";
			var div1 = document.createElement("div");
			div1.className = "dados-header";
			div1.innerHTML = data[i].username;
			user.appendChild(div1);
			
			var div2 = document.createElement("div");
			div2.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			if(data[i].avatar === undefined)
				imag.src = "../resource/Boneco.png";
			else
				imag.src = data[i].avatar;
			div2.appendChild (imag);
			user.appendChild(div2);
			ref.appendChild(user)
			backoffice.insertBefore(ref, backoffice.children[0]);
		}
		document.getElementById('uNext').disabled = false;
		uHasNext = true;
	}
	document.getElementById('uCurrent').innerHTML = (uCounter+1);
};

function getBackoffice() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = uCursors[uCounter];

	var url;
	if(cursor=="null")
		url="../rest/backoffice/employees?page_size=5";
	else
		url="../rest/backoffice/employees?page_size=5&cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateBackoffice(response.users);
			if((uCounter+1) >= uCursors.length)
				uCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function populateWorkers(data) {
	
	var workers = document.getElementById("workers");
	if (data.length == 0) {
		if(wCursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem trabalhadores.";
			workers.insertBefore(title, workers.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem mais trabalhadores.";
			workers.insertBefore(title, workers.children[0]);
			document.getElementById('wNext').disabled = true;
			wHasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ref = document.createElement ("a");
			ref.href = "./viewUser.html?username=" + data[i].username;
			var user = document.createElement("div");
			user.className = "container dados-block";
			var div1 = document.createElement("div");
			div1.className = "dados-header";
			div1.innerHTML = data[i].username;
			user.appendChild(div1);
			
			var div2 = document.createElement("div");
			div2.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			if(data[i].avatar === undefined)
				imag.src = "../resource/Boneco.png";
			else
				imag.src = data[i].avatar;
			div2.appendChild (imag);
			user.appendChild(div2);
			ref.appendChild(user);
			workers.insertBefore(ref, workers.children[0]);
		}
		document.getElementById('wNext').disabled = false;
		wHasNext = true;
	}
	document.getElementById('wCurrent').innerHTML = (wCounter+1);
};

function getWorkers() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = wCursors[wCounter];
	
	var url;
	if(cursor=="null")
		url="../rest/users/workers?page_size=5";
	else
		url="../rest/users/workers?page_size=5&cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateWorkers(response.users);
			if((wCounter+1) >= wCursors.length)
				wCursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
}