var map;
var markers = new Array();
var infowindow = new Array();

var status = "";
var type = "";

function initMap() {

	map = new google.maps.Map(document.getElementById('map'), {
		zoom : 16,
		center : {
			lat : 0,
			lng : 0
		}
	});

	navigator.geolocation.getCurrentPosition(function(position) {
		center = new google.maps.LatLng(position.coords.latitude,
				position.coords.longitude);
		map.setCenter(center);
	});
	
	map.setOptions({
		maxZoom : 19,
		minZoom : 12
	})
	
	map.data.loadGeoJson('../resource/map.json');
	
	var input = document.getElementById('pac-input');
	var autocomplete = new google.maps.places.Autocomplete(input);
	autocomplete.bindTo('bounds', map);
	
	map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);

	autocomplete.addListener('place_changed', function() {
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			// User entered the name of a Place that was not suggested and
			// pressed the Enter key, or the Place Details request failed.
			window.alert("No details available for input: '" + place.name + "'");
			return;
		}
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(16);
		}
	});
	
	map.data.setStyle({
		  fillColor: 'orange',
		  strokeWeight: 2,
		  fillOpacity: 0.1,
		  clickable: false,
	});
	
//	google.maps.event.addListener(map, 'bounds_change', function() {
//		northeast = map.getBounds().getNorthEast();
//		southwest = map.getBounds().getSouthWest();
//	});
};

function clearMarkers() {
	for (var i = 0; i < markers.length; i++ )
		markers[i].setMap(null);
	markers.length = 0;
};

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true"){
		toggleOn("usersListManager");
		toggleOn("gamificationManager");
	}else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	
	if(navigator.geolocation) {
		fillMap(null, null, map.getBounds().getNorthEast(), map.getBounds().getSouthWest());
	}
	
	google.maps.event.addListener(map, 'mouseup', function() {
		fillMap(type, status, map.getBounds().getNorthEast(), map.getBounds().getSouthWest());
	});
	
	document.getElementById("vandalism").onchange = function() {
		if(document.getElementById("vandalism").checked) {
			document.getElementById("garbage").checked = false;
			document.getElementById("accident").checked = false;
			document.getElementById("street_damage").checked = false;
			document.getElementById("furniture_damage").checked = false;
			document.getElementById("nature").checked = false;
			document.getElementById("other").checked = false;
			type = "vandalism";
		} else {
			type = "";
		}
	}
	
	document.getElementById("garbage").onchange = function() {
		if(document.getElementById("garbage").checked) {
			document.getElementById("vandalism").checked = false;
			document.getElementById("accident").checked = false;
			document.getElementById("street_damage").checked = false;
			document.getElementById("furniture_damage").checked = false;
			document.getElementById("nature").checked = false;
			document.getElementById("other").checked = false;
			type = "garbage";
		} else {
			type = "";
		}
	}
	
	document.getElementById("accident").onchange = function() {
		if(document.getElementById("accident").checked) {
			document.getElementById("vandalism").checked = false;
			document.getElementById("garbage").checked = false;
			document.getElementById("street_damage").checked = false;
			document.getElementById("furniture_damage").checked = false;
			document.getElementById("nature").checked = false;
			document.getElementById("other").checked = false;
			type = "accident";
		} else {
			type = "";
		}
	}
	
	document.getElementById("street_damage").onchange = function() {
		if(document.getElementById("street_damage").checked) {
			document.getElementById("vandalism").checked = false;
			document.getElementById("garbage").checked = false;
			document.getElementById("accident").checked = false;
			document.getElementById("furniture_damage").checked = false;
			document.getElementById("nature").checked = false;
			document.getElementById("other").checked = false;
			type = "street_damage";
		} else {
			type = "";
		}
	}
	
	document.getElementById("furniture_damage").onchange = function() {
		if(document.getElementById("furniture_damage").checked) {
			document.getElementById("vandalism").checked = false;
			document.getElementById("garbage").checked = false;
			document.getElementById("accident").checked = false;
			document.getElementById("street_damage").checked = false;
			document.getElementById("nature").checked = false;
			document.getElementById("other").checked = false;
			type = "furniture_damage";
		} else {
			type = "";
		}
	}
	
	document.getElementById("nature").onchange = function() {
		if(document.getElementById("nature").checked) {
			document.getElementById("vandalism").checked = false;
			document.getElementById("garbage").checked = false;
			document.getElementById("accident").checked = false;
			document.getElementById("street_damage").checked = false;
			document.getElementById("furniture_damage").checked = false;
			document.getElementById("other").checked = false;
			type = "nature";
		} else {
			type = "";
		}
	}
	
	document.getElementById("other").onchange = function() {
		if(document.getElementById("other").checked) {
			document.getElementById("vandalism").checked = false;
			document.getElementById("garbage").checked = false;
			document.getElementById("accident").checked = false;
			document.getElementById("street_damage").checked = false;
			document.getElementById("furniture_damage").checked = false;
			document.getElementById("nature").checked = false;
			type = "other";
		} else {
			type = "";
		}
	}
	
	document.getElementById("created").onchange = function() {
		if(document.getElementById("created").checked) {
			document.getElementById("work_in_progress").checked = false;
			document.getElementById("solved").checked = false;
			status = "created";
		} else {
			status = "";
		}
	}
	
	document.getElementById("work_in_progress").onchange = function() {
		if(document.getElementById("work_in_progress").checked) {
			document.getElementById("created").checked = false;
			document.getElementById("solved").checked = false;
			status = "work_in_progress";
		} else {
			status = "";
		}
	}
	
	document.getElementById("solved").onchange = function() {
		if(document.getElementById("solved").checked) {
			document.getElementById("created").checked = false;
			document.getElementById("work_in_progress").checked = false;
			status = "solved";
		} else {
			status = "";
		}
	}
	
	/* When the user clicks on the button, 
	toggle between hiding and showing the dropdown content */
	$("#type").click(function() {
		$("#dropStatus").css("display","none");
		$("#dropType").css("display","block");
	});
	$("#status").click(function() {
		$("#dropType").css("display","none");
		$("#dropStatus").css("display","block");
	});
	$("#dropType").mouseleave(checkTypes);
	$("#dropStatus").mouseleave(checkStatus);
};

function checkTypes() {
	$("#dropType").css("display","none")
	
	fillMap(type, status, map.getBounds().getNorthEast(), map.getBounds().getSouthWest());
};

function checkStatus() {
	$("#dropStatus").css("display","none")
	
	fillMap(type, status, map.getBounds().getNorthEast(), map.getBounds().getSouthWest());
};

function fillMap(type,status, ne, sw) {
	
	clearMarkers();
	
	var url;
	if(type !== "undefined" && type != null && type != "" && status !== "undefined" && status != null && status != "")
		url = "../rest/issues?type=" + type + "&state=" + status + "&northeastlat=" + ne.lat()
		+ "&northeastlng=" + ne.lng() + "&southwestlat=" + sw.lat() + "&southwestlng=" + sw.lng();
	else if(type !== "undefined" && type != null && type != "" && (status === "undefined" || status == "" || status == null))
		url = "../rest/issues?type=" + type + "&northeastlat=" + ne.lat()
		+ "&northeastlng=" + ne.lng() + "&southwestlat=" + sw.lat() + "&southwestlng=" + sw.lng();
	else if((type === "undefined" || type == "" || type == null) && status !== "undefined" && status != "" && status != null)
		url = "../rest/issues?state=" + status + "&northeastlat=" + ne.lat()
		+ "&northeastlng=" + ne.lng() + "&southwestlat=" + sw.lat() + "&southwestlng=" + sw.lng();
	else if((type === "undefined" || type == "" || type == null) && (status === "undefined" || status == "" || status == null))
		url = "../rest/issues?northeastlat=" + ne.lat() + "&northeastlng=" + ne.lng()
		+ "&southwestlat=" + sw.lat() + "&southwestlng=" + sw.lng();
	else
		url = "../rest/issues?northeastlat=" + ne.lat() + "&northeastlng=" + ne.lng()
		+ "&southwestlat=" + sw.lat() + "&southwestlng=" + sw.lng();

	console.log(url);
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			var issues = response.issues;
			for (i = 0; i < issues.length; i++) {
				var lat = issues[i].latitude;
				var lng = issues[i].longitude;
				var pri = issues[i].priority;
				
				switch(pri) {
				case 1:
					pri = "Baixa";
					break;
				case 2:
					pri = "Média";
					break;
				case 3:
					pri = "Alta"
					break;
				}
				var contentString = '<div id="content">'
					+ '<h1>'
					+ issues[i].title
					+ '</h1><br><br>'
					+ issues[i].description
					+ '<br><br>'
					+ issues[i].type
					+ '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
					+ 'Prioridade:    ' + pri
					+ '<br><br><a href="./view.html?issue='
					+ issues[i].issue
					+ '">Ver detalhes</a></div>';
				
				infowindow[i] = new google.maps.InfoWindow({
					content : contentString,
				});
				
				if (issues[i].state == "solved") {
					var image = '../resource/marker_green.png';
					markers[i] = new google.maps.Marker({
						position : new google.maps.LatLng(lat, lng),
						map : map,
						infowindow : infowindow[i],
						icon : image
					});
				} else if (issues[i].state == "closed") {
					var image = '../resource/marker_red.png';
					markers[i] = new google.maps.Marker({
						position : new google.maps.LatLng(lat, lng),
						map : map,
						infowindow : infowindow[i],
						icon : image
					});
				} else if (issues[i].state == "work_in_progress") {
					var image = '../resource/marker_orange.png';
					markers[i] = new google.maps.Marker({
						position : new google.maps.LatLng(lat, lng),
						map : map,
						infowindow : infowindow[i],
						icon : image
					});
				} else {
					var image = '../resource/marker_blue.png';
					markers[i] = new google.maps.Marker({
						position : new google.maps.LatLng(lat, lng),
						map : map,
						infowindow : infowindow[i],
						icon : image
					});
				}
				
				google.maps.event.addListener(markers[i], 'click',
					function() {
						markers.forEach(function(item) {
						item.infowindow.close();
					});
					this.infowindow.open(map, this);
				});
			}
		}, error : function(response) {
			if (response.status == 401) {
				showSnackbar('Empty or missing authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Issue does not exist');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		},
	});
};