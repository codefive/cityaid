window.onload = function() {
	checkLogin();
	showSnackbar("Loading Data...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true"){
		toggleOn("usersListManager");
		toggleOn("gamificationManager");}
	else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	getEntities();
}

function populate(data) {
	
	var entities = document.getElementById("entities");
	if (data.length == 0) {
		var title = document.createElement("p");
		title.className = "empty";
		title.innerHTML = "Não existem entidades!";
		entities.insertBefore(title, entities.children[0]);
	}
	for (var i = 0; i < data.length; i++) {
		var ref = document.createElement("a");
		ref.href = "./viewEntity.html?entity=" + data[i].nif;
		var box = document.createElement("div");
		box.className = "container dados-block";
		var div1 = document.createElement("div");
		div1.className = "dados-header";
		div1.innerHTML = data[i].name;
		box.appendChild(div1);
		var div2 = document.createElement("div");
		div2.className = "div1 dados-content";
		var logo = document.createElement("img");
		logo.className = "image";
		if (data[i].logo === undefined)
			logo.src = "../gcs/city-aid.appspot.com/default.jpg";
		else
			logo.src = "../gcs/city-aid.appspot.com/" + data[i].logo;
		div2.appendChild(logo);
		box.appendChild(div2);
		ref.appendChild(box);
		entities.appendChild(ref);
	}
}

function getEntities() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "GET",
		url : "../rest/entities/",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populate(response);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};