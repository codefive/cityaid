window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true"){
		toggleOn("usersListManager");
		toggleOn("gamificationManager");}
	else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
	getStats();
};

function getStats() {
	$.ajax({
		type : "GET",
		url : "../rest/stats",
		crossDomain : true,
		success : function(response) {
			document.getElementById("nUsers").innerHTML = response.users;
			document.getElementById("nReporters").innerHTML = response.reporters;
			document.getElementById("nBackoffice").innerHTML = response.backoffice;
			document.getElementById("nWorkers").innerHTML = response.workers;
			document.getElementById("nActiveUsers").innerHTML = response.activeUsers;
			document.getElementById("nEntities").innerHTML = response.entities;
			document.getElementById("nModerators").innerHTML = response.moderators;
			document.getElementById("nTotalIssues").innerHTML = response.issues;
			document.getElementById("nIssuesUnresolved").innerHTML = response.issuesCreated
			+ response.issuesProgress;
			document.getElementById("nIssuesFakeOrRepeat").innerHTML = response.issuesClosed
			+ response.issuesSolved;
			document.getElementById("nIssuesBeingResolved").innerHTML = response.issuesProgress;
			document.getElementById("nIssuesResolved").innerHTML = response.issuesSolved;
			document.getElementById("averageIssuesUsers").innerHTML = (response.issues / response.reporters);
		},
		error : function(response) {
			showSnackbar("Error: " + response.status);
		}
	});
};