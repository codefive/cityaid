var cursors = ["null"];
var current = 0;
var hasNext = true;
var user;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	
	user = getQueryParameterByName("username", window.location);
	
	if(current == 0)
		document.getElementById('previous').disabled = true;

	//Go to previous page
	document.getElementById('previous').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (current > 0) {
			//there is a previous page
			current--;
			if (current <= 0) {
				//previous page was the first page
				document.getElementById('previous').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('previous').disabled = false;
			}
			document.getElementById('next').disabled = false;
			getLogs();
		}
	};
	
	//Go to next page
	document.getElementById('next').onclick = function(event) {
		document.getElementById('logs').innerHTML = "";
		if (hasNext) {
			//there might be a next page
			current++;
			document.getElementById('previous').disabled = false;
			getLogs();
		}
	};
	
	getUserData();
};

function getUserData() {
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	$.ajax({
		type : "GET",
		url : "../../rest/users/" + user,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			getInfo(response);
			user=response;
			unlock();
			getLogs();
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Empty or missing authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 404) {
				showSnackbar('Issue does not exist');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
};

function unlock() {
	//Editar Menu para Managers..........................
	var isManager = localStorage['isManager'];
	
	if(isManager == "true"){
		toggleOn("usersListManager");
		toggleOn("gamificationManager");
		console.log(localStorage['userID']);
		console.log(user.username);
		console.log(user.role);
		if(localStorage['userID'] != user.username  && user.role != "worker" )
		toggleOn("fireWorker");}
	else{
		toggleOn("allIssuesBO");
		toggleOn("alertasBO");
	}
	//...................................................
}

function getInfo(user) {
	
	if(user.avatar==null)
		document.getElementById("avatar").src = "../resource/Boneco.png";
	else
		document.getElementById("avatar").src = user.avatar;
	
	document.getElementById("username").innerHTML = user.username;
	document.getElementById("name").innerHTML = user.name;
	var add = user.address;
	if(add == null)
		document.getElementById("address").innerHTML = "Morada não definida";
	else
		document.getElementById("address").innerHTML = add;
	
	document.getElementById("email").innerHTML = user.email;
	var dateR = new Date(user.registered);
	var months = [ "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
			"Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" ]
	document.getElementById("month").innerHTML = months[dateR.getMonth()];
	document.getElementById("day").innerHTML = dateR.getDate();	
	document.getElementById("year").innerHTML = dateR.getFullYear();
	
	var date = new Date(user.birthdate);
	document.getElementById("monthBirth").innerHTML = months[date.getMonth()];
	document.getElementById("dayBirth").innerHTML = date.getDate();
	document.getElementById("yearBirth").innerHTML = date.getFullYear();
	document.getElementById("gender").innerHTML = user.gender;
	document.getElementById("phone").innerHTML = user.phone;
	
	var emp = user.employer;
	if(emp == null)
		document.getElementById("entity").innerHTML = "Não trabalha para nenhuma entidade";
	else
		getEntity(user.employer);
	
	document.getElementById("fireWorker").onclick = function(event) {
	    deleteWorker(user.username, user.employer);
	};
};

function getLogs() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cursors[current];
	
	//Generate url
	var url;
	if(cursor=="null")
		url="../../rest/users/" + userID + "/log";
	else
		url="../../rest/users/" + userID + "/log?cursor=" + cursor;
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populateLogs(response.logs);
			if ((current+1) >= cursors.length)
				cursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Ocorrência inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};

function populateLogs(data) {
	
	var messages = document.getElementById("logs");
	if (data.length == 0) {
		if(cursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "O utilizador não tem histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "O utilizador não tem mais histórico.";
			messages.insertBefore(title, messages.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var commt = document.createElement("div");
			commt.className = "container";
			
			var userBox = document.createElement("div");
			userBox.className = "comentario-user";
			var title = document.createElement("span");
			title.className = "title";
			title.innerHTML = data[i].date;
			userBox.appendChild(title);
			commt.appendChild(userBox);
			
			var commentBox = document.createElement("div");
			commentBox.className = "conteudo";
			var commentText = document.createElement("span");
			commentText.className = "commentText";
			commentText.innerHTML = data[i].description;
			commentBox.appendChild(commentText);
			
			commt.appendChild(commentBox);
			
			messages.insertBefore(commt, messages.children[0]);
		}
		document.getElementById('next').disabled = false;
		hasNext = true;
	}
	document.getElementById('current').innerHTML = (current+1);
};


function deleteWorker(idUser, idEntity){	

	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var urlF = "";
	if(user.role === "backoffice"){
		urlF = "../../rest/backoffice/employees/"+idUser;
	}
	if(user.role === "reporter"){
		urlF = "../../rest/users/"+idUser;
	}
	$.ajax({
		type : "DELETE",
		url : urlF,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			window.location = "/backoffice/dashboard.html";
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 400) {
				showSnackbar('Parametros Errados');
			} else if (response.status == 404) {
				showSnackbar('Algo não existe!');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
	});
};


function getEntity(idEntity) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];

	$.ajax({
		type : "GET",
		url : "../../rest/entities/"+idEntity ,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			document.getElementById("entity").innerHTML  = response.name;
		},
		error : function(response) {
			if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Utilizador inexistente');
			} else if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		}
	});
};