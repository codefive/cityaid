var medal = {};

window.onload = function() {
	checkLogin()
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	showDefaults();
	document.getElementById('select').onchange = function(event) {
		switch(document.getElementById('select').value) {
			case "default":
				document.getElementById('preconds').style.display = "none";
				showDefaults();
				break;
			case "create":
				document.getElementById('defaultPreconds').style.display = "none";
				showTypes("Created");
				break;
			case "comment":
				document.getElementById('defaultPreconds').style.display = "none";
				showTypes("Commented");
				break;
			case "follow":
				document.getElementById('defaultPreconds').style.display = "none";
				showTypes("Followed");
				break;
			case "rate":
				document.getElementById('defaultPreconds').style.display = "none";
				showTypes("Rated");
				break;
			default:
				break;
		}
	};
	document.getElementById('submit').onclick = captureData;
};

function captureData(event) {
	
	event.preventDefault();
	var data = $('form[name="create"]').jsonify();
	
	medal.title = data.title;
	medal.description = data.description;
	medal.points = Number.parseInt(data.points);
	
	//Extract the data we want from loaded file
	var addImageData = function() {
		var extension = this.result.match(/png|jpeg|jpg|gif/);
		
		if (extension != null){
			//Supported filetype
			extension = extension[0];
			var base64 = this.result.replace(/^data:image\/(png|jpeg|jpg|gif);base64,/, "");
			medal.picture = {};
			medal.picture.extension = extension;
			medal.picture.base64 = base64;
			
			console.log("Loaded a "+extension);
			console.log("All photos loaded");
			sendMedalData();
		} else {
			//Not supported filetype
			console.log("All photos loaded");
			sendMedalData();
		}
	};
	
	//Load single file
	var files = document.getElementById("photoIn").files;
	if (files.length == 0) {
		console.log("All photos loaded");
		sendMedalData();
	}
	var reader = new FileReader();
	reader.addEventListener('load', addImageData);
	reader.readAsDataURL(document.getElementById("photoIn").files[0]);
};

function showDefaults() {
	
	var defaults = document.getElementById("defaultPreconds");
	
	if(document.getElementById("points")==null) {
		var p1 = document.createElement("p");
		p1.innerHTML = "Pontos: ";
		var points = document.createElement("input");
		points.setAttribute("type", "number");
		points.setAttribute("value", 0);
		points.id = "points";
		
		var p2 = document.createElement("p");
		p2.innerHTML = "Nível: ";
		var level = document.createElement("input");
		level.setAttribute("type", "number");
		level.setAttribute("value", 0);
		level.id = "level";
		
		var p3 = document.createElement("p");
		p3.innerHTML = "Medalhas: ";
		var medals = document.createElement("input");
		medals.setAttribute("type", "number");
		medals.setAttribute("value", 0);
		medals.id = "medals";
		
		var p4 = document.createElement("p");
		p4.innerHTML = "Ocorrências criadas: ";
		var issuesCreated = document.createElement("input");
		issuesCreated.setAttribute("type", "number");
		issuesCreated.setAttribute("value", 0);
		issuesCreated.id = "issuesCreated";
		
		var p5 = document.createElement("p");
		p5.innerHTML = "Ocorrências em progresso: ";
		var issuesProgress = document.createElement("input");
		issuesProgress.setAttribute("type", "number");
		issuesProgress.setAttribute("value", 0);
		issuesProgress.id = "issuesProgress";
		
		var p6 = document.createElement("p");
		p6.innerHTML = "Ocorrências resolvidas: ";
		var issuesSolved = document.createElement("input");
		issuesSolved.setAttribute("type", "number");
		issuesSolved.setAttribute("value", 0);
		issuesSolved.id = "issuesSolved";
		
		var p7 = document.createElement("p");
		p7.innerHTML = "Ocorrências fechadas: ";
		var issuesClosed = document.createElement("input");
		issuesClosed.setAttribute("type", "number");
		issuesClosed.setAttribute("value", 0);
		issuesClosed.id = "issuesClosed";
		
		var p8 = document.createElement("p");
		p8.innerHTML = "Ocorrências comentadas: ";
		var issuesCommented = document.createElement("input");
		issuesCommented.setAttribute("type", "number");
		issuesCommented.setAttribute("value", 0);
		issuesCommented.id = "issuesCommented";
		
		var p9 = document.createElement("p");
		p9.innerHTML = "Ocorrências seguidas: ";
		var issuesFollowed = document.createElement("input");
		issuesFollowed.setAttribute("type", "number");
		issuesFollowed.setAttribute("value", 0);
		issuesFollowed.id = "issuesFollowed";
		
		var p10 = document.createElement("p");
		p10.innerHTML = "Ocorrências avaliadas: ";
		var issuesRated = document.createElement("input");
		issuesRated.setAttribute("type", "number");
		issuesRated.setAttribute("value", 0);
		issuesRated.id = "issuesRated";
		
		var submit = document.createElement("input");
		submit.setAttribute("type", "button");
		submit.setAttribute("value", "Guardar precondições");
		submit.id = "submitPreconds";
		
		defaults.appendChild(p1);
		defaults.appendChild(points);
		defaults.appendChild(p2);
		defaults.appendChild(level);
		defaults.appendChild(p3);
		defaults.appendChild(medals);
		defaults.appendChild(p4);
		defaults.appendChild(issuesCreated);
		defaults.appendChild(p5);
		defaults.appendChild(issuesProgress);
		defaults.appendChild(p6);
		defaults.appendChild(issuesSolved);
		defaults.appendChild(p7);
		defaults.appendChild(issuesClosed);
		defaults.appendChild(p8);
		defaults.appendChild(issuesCommented);
		defaults.appendChild(p9);
		defaults.appendChild(issuesFollowed);
		defaults.appendChild(p10);
		defaults.appendChild(issuesRated);
		defaults.appendChild(submit);
	} else {
		defaults.style.display = "block";
	}
	
	document.getElementById("submitPreconds").onclick = function(event) {
		medal.preconditions = {};
		medal.preconditions.points = Number.parseInt(document.getElementById("points").value);
		medal.preconditions.level = Number.parseInt(document.getElementById("level").value);
		medal.preconditions.numAwardedMedals = Number.parseInt(document.getElementById("medals").value);
		medal.preconditions.issuesCreated = Number.parseInt(document.getElementById("issuesCreated").value);
		medal.preconditions.issuesProgress = Number.parseInt(document.getElementById("issuesProgress").value);
		medal.preconditions.issuesSolved = Number.parseInt(document.getElementById("issuesSolved").value);
		medal.preconditions.issuesClosed = Number.parseInt(document.getElementById("issuesClosed").value);
		medal.preconditions.issuesCommented = Number.parseInt(document.getElementById("issuesCommented").value);
		medal.preconditions.issuesFollowed = Number.parseInt(document.getElementById("issuesFollowed").value);
		medal.preconditions.issuesRated = Number.parseInt(document.getElementById("issuesRated").value);
		defaults.style.display = "none";
	}
};

function showTypes(type) {
	
	var defaults = document.getElementById("preconds");

	if(document.getElementById("vandalism")==null) {
		var p1 = document.createElement("p");
		p1.innerHTML = "Tipo vandalismo: ";
		var vandalism = document.createElement("input");
		vandalism.setAttribute("type", "number");
		vandalism.setAttribute("value", 0);
		vandalism.id = "vandalism";
		
		var p2 = document.createElement("p");
		p2.innerHTML = "Tipo lixo: ";
		var garbage = document.createElement("input");
		garbage.setAttribute("type", "number");
		garbage.setAttribute("value", 0);
		garbage.id = "garbage";
		
		var p3 = document.createElement("p");
		p3.innerHTML = "Tipo acidente: ";
		var accident = document.createElement("input");
		accident.setAttribute("type", "number");
		accident.setAttribute("value", 0);
		accident.id = "accident";
		
		var p4 = document.createElement("p");
		p4.innerHTML = "Danos de vias públicas: ";
		var streetDamage = document.createElement("input");
		streetDamage.setAttribute("type", "number");
		streetDamage.setAttribute("value", 0);
		streetDamage.id = "streetDamage";
		
		var p5 = document.createElement("p");
		p5.innerHTML = "Danos de material público: ";
		var furnitureDamage = document.createElement("input");
		furnitureDamage.setAttribute("type", "number");
		furnitureDamage.setAttribute("value", 0);
		furnitureDamage.id = "furnitureDamage";
		
		var p6 = document.createElement("p");
		p6.innerHTML = "Natureza: ";
		var nature = document.createElement("input");
		nature.setAttribute("type", "number");
		nature.setAttribute("value", 0);
		nature.id = "nature";
		
		var p7 = document.createElement("p");
		p7.innerHTML = "Outros: ";
		var other = document.createElement("input");
		other.setAttribute("type", "number");
		other.setAttribute("value", 0);
		other.id = "other";
		
		var submit = document.createElement("input");
		submit.setAttribute("type", "button");
		submit.setAttribute("value", "Guardar precondições");
		submit.id = "submitPreconditions";
		
		defaults.appendChild(p1);
		defaults.appendChild(vandalism);
		defaults.appendChild(p2);
		defaults.appendChild(garbage);
		defaults.appendChild(p3);
		defaults.appendChild(accident);
		defaults.appendChild(p4);
		defaults.appendChild(streetDamage);
		defaults.appendChild(p5);
		defaults.appendChild(furnitureDamage);
		defaults.appendChild(p6);
		defaults.appendChild(nature);
		defaults.appendChild(p7);
		defaults.appendChild(other);
		defaults.appendChild(submit);
	} else {
		defaults.style.display = "block";
	}
	
	document.getElementById("submitPreconditions").onclick = function(event) {
		medal.preconditions = {};
		switch(type) {
			case "Created":
				medal.preconditions.issuesCreatedTypeVandalism = Number.parseInt(document.getElementById("vandalism").value);
				medal.preconditions.issuesCreatedTypeGarbage = Number.parseInt(document.getElementById("garbage").value);
				medal.preconditions.issuesCreatedTypeAccident = Number.parseInt(document.getElementById("accident").value);
				medal.preconditions.issuesCreatedTypeStreetDamage = Number.parseInt(document.getElementById("streetDamage").value);
				medal.preconditions.issuesCreatedTypeFurnitureDamage = Number.parseInt(document.getElementById("furnitureDamage").value);
				medal.preconditions.issuesCreatedTypeNature = Number.parseInt(document.getElementById("nature").value);
				medal.preconditions.issuesCreatedTypeOther = Number.parseInt(document.getElementById("other").value);
				break;
			case "Commented":
				medal.preconditions.issuesCommentedTypeVandalism = Number.parseInt(document.getElementById("vandalism").value);
				medal.preconditions.issuesCommentedTypeGarbage = Number.parseInt(document.getElementById("garbage").value);
				medal.preconditions.issuesCommentedTypeAccident = Number.parseInt(document.getElementById("accident").value);
				medal.preconditions.issuesCommentedTypeStreetDamage = Number.parseInt(document.getElementById("streetDamage").value);
				medal.preconditions.issuesCommentedTypeFurnitureDamage = Number.parseInt(document.getElementById("furnitureDamage").value);
				medal.preconditions.issuesCommentedTypeNature = Number.parseInt(document.getElementById("nature").value);
				medal.preconditions.issuesCommentedTypeOther = Number.parseInt(document.getElementById("other").value);
				break;
			case "Followed":
				medal.preconditions.issuesFollowedTypeVandalism = Number.parseInt(document.getElementById("vandalism").value);
				medal.preconditions.issuesFollowedTypeGarbage = Number.parseInt(document.getElementById("garbage").value);
				medal.preconditions.issuesFollowedTypeAccident = Number.parseInt(document.getElementById("accident").value);
				medal.preconditions.issuesFollowedTypeStreetDamage = Number.parseInt(document.getElementById("streetDamage").value);
				medal.preconditions.issuesFollowedTypeFurnitureDamage = Number.parseInt(document.getElementById("furnitureDamage").value);
				medal.preconditions.issuesFollowedTypeNature = Number.parseInt(document.getElementById("nature").value);
				medal.preconditions.issuesFollowedTypeOther = Number.parseInt(document.getElementById("other").value);
				break;
			case "Rated":
				medal.preconditions.issuesRatedTypeVandalism = Number.parseInt(document.getElementById("vandalism").value);
				medal.preconditions.issuesRatedTypeGarbage = Number.parseInt(document.getElementById("garbage").value);
				medal.preconditions.issuesRatedTypeAccident = Number.parseInt(document.getElementById("accident").value);
				medal.preconditions.issuesRatedTypeStreetDamage = Number.parseInt(document.getElementById("streetDamage").value);
				medal.preconditions.issuesRatedTypeFurnitureDamage = Number.parseInt(document.getElementById("furnitureDamage").value);
				medal.preconditions.issuesRatedTypeNature = Number.parseInt(document.getElementById("nature").value);
				medal.preconditions.issuesRatedTypeOther = Number.parseInt(document.getElementById("other").value);
				break;
			default:
				break;
		}
		defaults.style.display = "none";
	}
};

function sendMedalData() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.title = medal.title;
	data.description = medal.description;
	data.points = medal.points;
	data.picture = medal.picture;
	data.preconditions = medal.preconditions;
	
	console.log(medal);
	console.log(data);
	
	$.ajax({
		type : "POST",
		url : "../../rest/gamification/medals",
		headers : {
			'Authorization' : tokenID,
			'User' : userID,
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType: "json",
		success : function(response) {
			document.getElementById('submit').disabled = true;
			showSnackbar("Medalha criada com sucesso!");
			window.location.href = "../dashboard.html";
		},
		error : function(response) {
			if (response.status == 400){
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 401){
				showSnackbar('Missing or empty authorization headers');
			} else if (response.status == 403){
				showSnackbar('Invalid token');
			} else if (response.status == 500){
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
}