window.onload = function() {
	checkLogin();
	showSnackbar("Loading Data...");
	document.getElementById('logout').onclick = logout;
	var frms = $('form[name="createUser"');
	document.getElementById('promote').onclick = add;
	frms[0].onsubmit = create;
}

var create = function(event) {
	
	var data = $('form[name="createUser"]').jsonify();
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "POST",
		url : "../../rest/backoffice/employees/create",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		// dataType: "json",
		success : function(response) {
			showSnackbar("Utilizador registado!");
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetro errado ou vazio');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 409) {
				showSnackbar('Utilizador já existente');
			} else if (response.status == 500) {
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});

	event.preventDefault();
};

var add = function(event) {
	
	var data = {};
	data.username = document.getElementById('usern').value;
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "POST",
		url : "../../rest/backoffice/employees",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		// dataType: "json",
		success : function(response) {
			showSnackbar("Utilizador adicionado!");
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetro errado ou vazio');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else if (response.status == 404) {
				showSnackbar('Utilizador inexistente');
			} else if (response.status == 409) {
				showSnackbar('Utilizador já é backoffice');
			} else if (response.status == 500) {
				showSnackbar('Ocorreu um erro!');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});

	event.preventDefault();
};