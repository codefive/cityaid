var cursors = ["null"];
var current = 0;
var hasNext = true;

window.onload = function() {
	checkLogin();
	showSnackbar("Loading messages...");
	document.getElementById('logout').onclick = logout;
	
	if(current == 0)
		document.getElementById('previous').disabled = true;

	//Go to previous page
	document.getElementById('previous').onclick = function(event) {
		document.getElementById('backofficeEmpl').innerHTML = "";
		if (current > 0) {
			//there is a previous page
			current--;
			if (current <= 0) {
				//previous page was the first page
				document.getElementById('previous').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('previous').disabled = false;
			}
			document.getElementById('next').disabled = false;
			getUsers();
		}
	};
	
	//Go to next page
	document.getElementById('next').onclick = function(event) {
		document.getElementById('backofficeEmpl').innerHTML = "";
		if (hasNext) {
			//there might be a next page
			current++;
			document.getElementById('previous').disabled = false;
			getUsers();
		}
	};

	//Get first page
	getUsers();
};

function populate(data) {
	
	var emplBack = document.getElementById("backofficeEmpl");
	if (data.length == 0) {
		if(cursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem utilizadores backoffice.";
			emplBack.insertBefore(title, emplBack.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem mais utilizadores backoffice.";
			emplBack.insertBefore(title, emplBack.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		}
	} else {
		for (var i = 0; i < data.length; i++) {
			var ref = document.createElement("a");
			ref.href = "../viewUser.html?username=" + data[i].username;
			var user = document.createElement("div");
			user.className = "container dados-block";
			var div1 = document.createElement("div");
			div1.className = "dados-header";
			div1.innerHTML = data[i].name;
			user.appendChild(div1);
			
			var div2 = document.createElement("div");
			div2.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			if (data[i].avatar === undefined)
				imag.src = "../../resource/Boneco.png";
			else
				imag.src = data[i].avatar;
			div2.appendChild(imag);
			user.appendChild(div2);
			ref.appendChild(user);
			emplBack.insertBefore(ref, emplBack.children[0]);
		}
		document.getElementById('next').disabled = false;
	    hasNext = true;
	}
	document.getElementById('current').innerHTML = (current+1);
};

function getUsers() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cursors[current];
	
	//Generate url
	var url;
	if(cursor=="null")
		url = "../../rest/backoffice/employees?page_size=10";
	else
		url = "../../rest/backoffice/employees?cursor=" + cursor + "&page_size=10";
	
	$.ajax({
		type : "GET",
		url : url,
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			populate(response.users);
			if ((current+1) >= cursors.length)
				cursors.push(response.cursor);
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
		}
	});
}