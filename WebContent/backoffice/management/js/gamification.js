var cursors = ["null"];
var current = 0;
var hasNext = true;

var pontos = {};

window.onload = function() {
	checkLogin();
	showSnackbar("Loading data...");
	document.getElementById('logout').onclick = logout;
	
	viewPoints();
	
	if(current == 0)
		document.getElementById('previous').disabled = true;

	//Go to previous page
	document.getElementById('previous').onclick = function(event) {
		document.getElementById('medals').innerHTML = "";
		if (current > 0) {
			//there is a previous page
			current--;
			if (current <= 0) {
				//previous page was the first page
				document.getElementById('previous').disabled = true;
			} else {
				//previous page was not the first page
				document.getElementById('previous').disabled = false;
			}
			document.getElementById('next').disabled = false;
			getMedals();
		}
	};
	
	//Go to next page
	document.getElementById('next').onclick = function(event) {
		document.getElementById('medals').innerHTML = "";
		if (hasNext) {
			//there might be a next page
			current++;
			document.getElementById('previous').disabled = false;
			getMedals();
		}
	};

	//Get first page
	getMedals();
};

function populate(data) {
	
	var medals = document.getElementById("medals");
	if (data.length == 0) {
		if(cursors.length == 1) {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem medalhas na aplicação.";
			medals.insertBefore(title, medals.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		} else {
			var title = document.createElement("p");
			title.className = "empty";
			title.innerHTML = "Não existem mais medalhas na aplicação.";
			medals.insertBefore(title, medals.children[0]);
			document.getElementById('next').disabled = true;
			hasNext = false;
		}
	} else {
		for(var i = 0; i < data.length; i++) {
			var ocorr = document.createElement("div");
			ocorr.className = "container dados-block";
			var divti = document.createElement("div");
			divti.className = "dados-header";
			divti.innerHTML = data[i].title;
			ocorr.appendChild(divti);
			var div1 = document.createElement("div");
			div1.className = "div1 dados-content";
			var imag = document.createElement("img");
			imag.className = "image";
			if(data[i].picture === undefined)
				 imag.src = "../../gcs/city-aid.appspot.com/default.jpg";
			else 
				imag.src = "../../gcs/city-aid.appspot.com/"+data[i].picture;
			var active = document.createElement("input");
			active.setAttribute("type", "checkbox");
			active.id = "check" + i;
			if(data[i].active)
				active.checked = true;
			else
				active.checked = false;
			var titleJob = document.createElement("p");
			titleJob.innerHTML = data[i].description;
			titleJob.className = "typeOcorr";
			div1.appendChild(active);
			div1.appendChild(imag);
			div1.appendChild(titleJob);
			ocorr.appendChild(div1);
			medals.insertBefore(ocorr, medals.children[0]);
			
			$('#check'+i).click(function() {
				alterState(data[i]);
			});
		}
		document.getElementById('next').disabled = false;
	    hasNext = true;
	}
	document.getElementById('current').innerHTML = (current+1);
};

function getMedals() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	//Get current cursor
	var cursor = cursors[current];
	
	//Generate url
	var url;
	if(cursor=="null")
		url = "../../rest/gamification/medals";
	else
		url = "../../rest/gamification/medals?cursor=" + cursor;
	
    $.ajax({
        type: "GET",
        url: url,
        headers : {
			'Authorization' : tokenID,
			'User' : userID,
		},
        crossDomain: true,
        dataType: "json",
        success: function(response) {
        	populate(response.medals);
			if ((current+1) >= cursors.length)
				cursors.push(response.cursor);
        },
        error: function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
        }
    });
};

function viewPoints() {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
    $.ajax({
        type: "GET",
        url: "../../rest/gamification/points",
        headers : {
			'Authorization' : tokenID,
			'User' : userID,
		},
        crossDomain: true,
        dataType: "json",
        success: function(response) {
        	pontos = response;
        	getPoints(response);
        	editsButtons();
        },
        error: function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 401) {
				showSnackbar('	Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
        }
    });
};

function alterState(medal) {
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	var data = {};
	data.medal = medal.id;
	
    $.ajax({
        type: "PUT",
        url: "../../rest/gamification",
        headers : {
			'Authorization' : tokenID,
			'User' : userID,
		},
        crossDomain: true,
        dataType: "json",
        success: function(response) {
        	if(data.active)
        		showSnackbar("Medalha desativada");
        	else	
            	showSnackbar("Medalha ativada");
        },
        error: function(response) {
			if (response.status == 400) {
				showSnackbar('Missing or wrong parameter');
			} else if (response.status == 401) {
				showSnackbar('	Missing or empty authorization headers');
			} else if (response.status == 403) {
				showSnackbar('Invalid token');
			} else if (response.status == 500) {
				showSnackbar('An error has occurred!');
			} else {
				showSnackbar("Error: " + response.status);
			}
        }
    });
};

function getPoints(data) {
	
	document.getElementById("pointsMod").innerHTML = data.modLevel;
	document.getElementById("pointsCreate").innerHTML = data.createIssue;
	document.getElementById("pointsComment").innerHTML = data.comment;	
	document.getElementById("pointsRateI").innerHTML = data.rateIssue;
	document.getElementById("pointsRateE").innerHTML = data.rateEntity;
	document.getElementById("pointsDeleteI").innerHTML = data.deleteIssue;	
	document.getElementById("pointsDeleteC").innerHTML = data.deleteComment;	
	document.getElementById("pointsInProg").innerHTML = data.issueWIP;	
	document.getElementById("pointsDone").innerHTML = data.issueSolved;	
	document.getElementById("pointsClose").innerHTML = data.issueClosed;
};


function editsButtons() {
	
	$("#editsMod").show();
	$("#editsCreate").show();
	$("#editsComment").show();
	$("#editsRateI").show();
	$("#editsRateE").show();
	$("#editsDeleteI").show();
	$("#editsDeleteC").show();
	$("#editsInProg").show();
	$("#editsDone").show();
	$("#editsClose").show();

	// esconder tudo de mudar pontos para se tornar moderador excepto o botao de editar
	$("#modChange").hide();
	$("#editMod").show();
	$("#cancelMod").hide();
	$("#saveMod").hide();
	// esconder tudo de mudar pontos ao criar ocorrencia excepto o botao de editar
	$("#createChange").hide();
	$("#editCreate").show();
	$("#cancelCreate").hide();
	$("#saveCreate").hide();
	// esconder tudo de mudar pontos ao comentar ocorrencia excepto o botao de editar
	$("#commentChange").hide();
	$("#editComment").show();
	$("#cancelComment").hide();
	$("#saveComment").hide();
	// esconder tudo de mudar pontos ao avaliar ocorrencia excepto o botao de editar
	$("#rateIChange").hide();
	$("#editRateI").show();
	$("#cancelRateI").hide();
	$("#saveRateI").hide();
	// esconder tudo de mudar pontos ao avaliar entidade excepto o botao de editar
	$("#rateEChange").hide();
	$("#editRateE").show();
	$("#cancelRateE").hide();
	$("#saveRateE").hide();
	// esconder tudo de mudar pontos ao apagar uma ocorrencia excepto o botao de editar
	$("#deleteIChange").hide();
	$("#editDeleteI").show();
	$("#cancelDeleteI").hide();
	$("#saveDeleteI").hide();
	// esconder tudo de mudar pontos ao apagar um comentário excepto o botao de editar
	$("#deleteCChange").hide();
	$("#editDeleteC").show();
	$("#cancelDeleteC").hide();
	$("#saveDeleteC").hide();
	// esconder tudo de mudar pontos quando o estado de uma issue passa a wip excepto o botao de editar
	$("#inProgChange").hide();
	$("#editInProg").show();
	$("#cancelInProg").hide();
	$("#saveInProg").hide();
	// esconder tudo de mudar pontos quando uma issue é resolvida excepto o botao de editar
	$("#doneChange").hide();
	$("#editDone").show();
	$("#cancelDone").hide();
	$("#saveDone").hide();
	// esconder tudo de mudar pontos quando uma issue é fechada excepto o botao de editar
	$("#closeChange").hide();
	$("#editClose").show();
	$("#cancelClose").hide();
	$("#saveClose").hide();

//...............................Update toModerator Points.....................
	document.getElementById("editMod").addEventListener("click", function(e) {
		$("#pointsMod").hide();
		$("#modChange").show();
		$("#editMod").hide();
		$("#cancelMod").show();
		$("#saveMod").show();
		document.getElementById("newPointsMod").value = pontos.modLevel;
	});

	document.getElementById("saveMod").addEventListener("click",function(e) {
				var data = document.getElementById("newPointsMod").value;
				configurePoints(data, pontos.createIssue, pontos.comment, pontos.rateIssue, pontos.rateEntity, 
						pontos.deleteIssue, pontos.deleteComment, pontos.issueWIP, pontos.issueClosed, pontos.issueSolved);
			});

	document.getElementById("cancelMod").addEventListener("click",function(e) {
				var data = document.getElementById("newPointsMod").value;
				$("#pointsMod").show();
				$("#modChange").hide();
				$("#editMod").show();
				$("#cancelMod").hide();
				$("#saveMod").hide();
	});
//...............................Update CreateIssue Points.....................
	document.getElementById("editCreate").addEventListener("click", function(e) {
		$("#pointsCreate").hide();
		$("#createChange").show();
		$("#editCreate").hide();
		$("#cancelCreate").show();
		$("#saveCreate").show();
		document.getElementById("newPointsCreate").value = pontos.createIssue;
	});

	document.getElementById("saveCreate").addEventListener("click",function(e) {
				var data = document.getElementById("newPointsCreate").value;
				configurePoints(pontos.modLevel, data, pontos.comment, pontos.rateIssue, pontos.rateEntity, 
						pontos.deleteIssue, pontos.deleteComment, pontos.issueWIP, pontos.issueClosed, pontos.issueSolved);
			});

	document.getElementById("cancelCreate").addEventListener("click",function(e) {
				var data = document.getElementById("newPointsCreate").value;
				$("#pointsCreate").show();
				$("#createChange").hide();
				$("#editCreate").show();
				$("#cancelCreate").hide();
				$("#saveCreate").hide();
	});
//...............................Update CommentIssue Points....................
	document.getElementById("editComment").addEventListener("click", function(e) {
		$("#pointsComment").hide();
		$("#commentChange").show();
		$("#editComment").hide();
		$("#cancelComment").show();
		$("#saveComment").show();
		document.getElementById("newPointsComment").value = pontos.comment;
	});

	document.getElementById("saveComment").addEventListener("click",function(e) {
				var data = document.getElementById("newPointsComment").value;
				configurePoints(pontos.modLevel, pontos.createIssue, data, pontos.rateIssue, pontos.rateEntity, 
						pontos.deleteIssue, pontos.deleteComment, pontos.issueWIP, pontos.issueClosed, pontos.issueSolved);
			});

	document.getElementById("cancelComment").addEventListener("click",function(e) {
				var data = document.getElementById("newPointsComment").value;
				$("#pointsComment").show();
				$("#commentChange").hide();
				$("#editComment").show();
				$("#cancelComment").hide();
				$("#saveComment").hide();
	});
//...............................Update RateIssue Points.......................
	document.getElementById("editRateI").addEventListener("click", function(e) {
		$("#pointsRateI").hide();
		$("#rateIChange").show();
		$("#editRateI").hide();
		$("#cancelRateI").show();
		$("#saveRateI").show();
		document.getElementById("newPointsRateI").value = pontos.rateIssue;
	});

	document.getElementById("saveRateI").addEventListener("click",function(e) {
				var data = document.getElementById("newPointsRateI").value;
				configurePoints(pontos.modLevel, pontos.createIssue, pontos.comment, data, pontos.rateEntity, 
						pontos.deleteIssue, pontos.deleteComment, pontos.issueWIP, pontos.issueClosed, pontos.issueSolved);
			});

	document.getElementById("cancelRateI").addEventListener("click",function(e) {
				var data = document.getElementById("newPointsRateI").value;
				$("#pointsRateI").show();
				$("#rateIChange").hide();
				$("#editRateI").show();
				$("#cancelRateI").hide();
				$("#saveRateI").hide();
	});
//...............................Update RateEntity Points......................
		document.getElementById("editRateE").addEventListener("click", function(e) {
			$("#pointsRateE").hide();
			$("#rateEChange").show();
			$("#editRateE").hide();
			$("#cancelRateE").show();
			$("#saveRateE").show();
			document.getElementById("newPointsRateE").value = pontos.rateEntity;
		});

		document.getElementById("saveRateE").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsRateE").value;
					configurePoints(pontos.modLevel, pontos.createIssue, pontos.comment, pontos.rateIssue, data, 
							pontos.deleteIssue, pontos.deleteComment, pontos.issueWIP, pontos.issueClosed, pontos.issueSolved);
				});

		document.getElementById("cancelRateE").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsRateE").value;
					$("#pointsRateE").show();
					$("#rateEChange").hide();
					$("#editRateE").show();
					$("#cancelRateE").hide();
					$("#saveRateE").hide();
		});
//...............................Update DeleteIssue Points.....................
		document.getElementById("editDeleteI").addEventListener("click", function(e) {
			$("#pointsDeleteI").hide();
			$("#deleteIChange").show();
			$("#editDeleteI").hide();
			$("#cancelDeleteI").show();
			$("#saveDeleteI").show();
			document.getElementById("newPointsDeleteI").value = pontos.deleteIssue;
		});

		document.getElementById("saveDeleteI").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsDeleteI").value;
					configurePoints(pontos.modLevel, pontos.createIssue, pontos.comment, pontos.rateIssue, pontos.rateEntity, 
							data, pontos.deleteComment, pontos.issueWIP, pontos.issueClosed, pontos.issueSolved);
				});

		document.getElementById("cancelDeleteI").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsDeleteI").value;
					$("#pointsDeleteI").show();
					$("#deleteIChange").hide();
					$("#editDeleteI").show();
					$("#cancelDeleteI").hide();
					$("#saveDeleteI").hide();
		});
//...............................Update DeleteComment Points...................
		document.getElementById("editDeleteC").addEventListener("click", function(e) {
			$("#pointsDeleteC").hide();
			$("#deleteCChange").show();
			$("#editDeleteC").hide();
			$("#cancelDeleteC").show();
			$("#saveDeleteC").show();
			document.getElementById("newPointsDeleteC").value = pontos.deleteComment;
		});

		document.getElementById("saveDeleteC").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsDeleteC").value;
					configurePoints(pontos.modLevel, pontos.createIssue, pontos.comment, pontos.rateIssue, pontos.rateEntity, 
							pontos.deleteIssue, data, pontos.issueWIP, pontos.issueClosed, pontos.issueSolved);
				});

		document.getElementById("cancelDeleteC").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsDeleteC").value;
					$("#pointsDeleteC").show();
					$("#deleteCChange").hide();
					$("#editDeleteC").show();
					$("#cancelDeleteC").hide();
					$("#saveDeleteC").hide();
		});
//...............................Update inProgressIssue Points.................
		document.getElementById("editInProg").addEventListener("click", function(e) {
			$("#pointsInProg").hide();
			$("#inProgChange").show();
			$("#editInProg").hide();
			$("#cancelInProg").show();
			$("#saveInProg").show();
			document.getElementById("newPointsInProg").value = pontos.issueWIP;
		});

		document.getElementById("saveInProg").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsInProg").value;
					configurePoints(pontos.modLevel, pontos.createIssue, pontos.comment, pontos.rateIssue, pontos.rateEntity, 
							pontos.deleteIssue, pontos.deleteComment, data, pontos.issueClosed, pontos.issueSolved);
				});

		document.getElementById("cancelInProg").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsInProg").value;
					$("#pointsInProg").show();
					$("#inProgChange").hide();
					$("#editInProg").show();
					$("#cancelInProg").hide();
					$("#saveInProg").hide();
		});
//...............................Update CloseIssue Points...................
		document.getElementById("editClose").addEventListener("click", function(e) {
			$("#pointsClose").hide();
			$("#closeChange").show();
			$("#editClose").hide();
			$("#cancelClose").show();
			$("#saveClose").show();
			document.getElementById("newPointsClose").value = pontos.issueClosed;
		});

		document.getElementById("saveClose").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsClose").value;
					configurePoints(pontos.modLevel, pontos.createIssue, pontos.comment, pontos.rateIssue, pontos.rateEntity, 
							pontos.deleteIssue, pontos.deleteComment,  pontos.issueWIP,data, pontos.issueSolved);
				});

		document.getElementById("cancelClose").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsClose").value;
					$("#pointsClose").show();
					$("#closeChange").hide();
					$("#editClose").show();
					$("#cancelClose").hide();
					$("#saveClose").hide();
		});
//...............................Update ResolvedIssue Points...................
		document.getElementById("editDone").addEventListener("click", function(e) {
			$("#pointsDone").hide();
			$("#doneChange").show();
			$("#editDone").hide();
			$("#cancelDone").show();
			$("#saveDone").show();
			document.getElementById("newPointsDone").value = pontos.issueSolved;
		});

		document.getElementById("saveDone").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsDone").value;
					configurePoints(pontos.modLevel, pontos.createIssue, pontos.comment, pontos.rateIssue, pontos.rateEntity, 
							pontos.deleteIssue, pontos.deleteComment, pontos.issueWIP, pontos.issueClosed, data);
				});

		document.getElementById("cancelDone").addEventListener("click",function(e) {
					var data = document.getElementById("newPointsDone").value;
					$("#pointsDone").show();
					$("#doneChange").hide();
					$("#editDone").show();
					$("#cancelDone").hide();
					$("#saveDone").hide();
		});		
};


function configurePoints(modLevel, createIssue, comment, rateIssue, rateEntity, deleteIssue, 
		deleteComment, issueWIP, issueClosed, issueSolved) {
	
	var data = {};
	data.modLevel = modLevel; data.createIssue = createIssue; data.comment = comment; 
	data.rateIssue = rateIssue; data.rateEntity = rateEntity; data.deleteIssue = deleteIssue;
	data.deleteComment = deleteComment; data.issueWIP = issueWIP; data.issueClosed = issueClosed;
	data.issueSolved = issueSolved;
	
	console.log(data);
	
	var tokenID = localStorage['tokenID'];
	var userID = localStorage['userID'];
	
	$.ajax({
		type : "PUT",
		url : "../../rest/gamification/points",
		headers : {
			'Authorization' : tokenID,
			'User' : userID
		},
		contentType : "application/json; charset=utf-8",
		crossDomain : true,
		dataType : "json",
		success : function(response) {
			showSnackbar('Informação Actualizada!');
			location.reload();
		},
		error : function(response) {
			if (response.status == 400) {
				showSnackbar('Parâmetros Inválidos!');
			} else if (response.status == 401) {
				showSnackbar('Autorização inválida');
			} else if (response.status == 403) {
				showSnackbar('Token inválido');
			} else  if (response.status == 500) {
				showSnackbar('Erro de servidor');
			} else {
				showSnackbar("Erro: " + response.status);
			}
		},
		data : JSON.stringify(data)
	});
	
};